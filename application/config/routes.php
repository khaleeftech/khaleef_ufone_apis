<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "fantasy/index";
// $route['featured_series'] = "featured_series/index";

// $route["news/(:any)"] = "news/index/$1";
// $route["more_news/(:any)"] = "My_Controller/more_news/$1";
// $route["more_news/(:any)/(:any)"] = "My_Controller/more_news/$1/$2";
// $route["news-update/(:any)/(:any)"] = "news/news_detail/$1/$2";

// $route["cricket-articles"] = "articles/index";
// $route["articles/(:any)"] = "articles/index/$1";
// $route["more_articles/(:any)"] = "articles/more_articles/$1";
// $route["article/(:any)/(:any)"] = "articles/article_detail/$1/$2";

// $route["more_series/(:any)"] = "video/more_series/$1";



// $route["videos"] = "video/index";
// $route["jazz-rising-star"] = "video/index/1/1";
// $route["video/more_videos/(:any)"] = "video/more_videos/$1";
// $route["video/more_timeline_related_videos/(:any)/(:any)"] = "video/more_timeline_related_videos/$1/$2";
// //$route["video/(:any)/(:any)"] = "video/play_video/$1/$2";

// // $route["highlights/(:any)/(:any)"] = "video/timeline/$1/$2";
// $route["highlights/(:any)/(:any)/(:any)"] = "video/timeline2/$1/$2/$3";




// $route["join-quiz"] = "home/join_quiz";
// $route["logout"] = "login/logout";
// $route["ball2ball/(:any)"] = "home/ball2ball/$1";
// $route["scorecard/(:any)"] = "home/scorecard/$1";
// $route["forget-password"] = "login/forget_password";
// $route["register-1"] = "login/register_1";
// $route["register-2"] = "login/register_2";
// $route["sub"] = "home/sub";
// $route["lowbalance"] = "home/lowbalance";
// $route["embed/video/(:any)"] = "embed/video/$1";
// $route["embed/serve/(:any)"] = "embed/serve/$1";
// $route["embed/tweet/(:any)"] = "embed/tweet/$1";
// $route["embed/(:any)"] = "embed/index/$1";
// $route["squad/(:any)/(:any)"] = "squad/index/$1/$2";
// $route["livematch/(:any)/(:any)"] = "timeline/livematch_r/$1/$2";
// $route["postmatch/(:any)/(:any)"] = "timeline/postmatch_r/$1/$2";


// $route["livestream"] = "timeline/livestream/$1/$2";
// $route["livestream/(:any)"] = "timeline/livestream/$1/$2";
// $route["livestream/(:any)/(:any)"] = "timeline/livestream/$1/$2";


// $route["match/(:any)/(:any)"] = "timeline/postmatch/$1/$2";
// $route["live/(:any)/(:any)"] = "timeline/livematch/$1/$2";
// $route["post/(:any)/(:any)"] = "blog/post/$1/$2";
// $route["blog/"] = "blog/index";
// $route["appstart"] = "My_Controller/appstart";
// $route["userapi/get_header_phone"] = "test/get_header_phone";

// // $route["promo/(:any)"] = "home/promo/$1";
// $route["continue_subscription"] = "login/continue_subscription";
// $route["continue_subscription_phone"] = "login/continue_subscription_phone";

// $route['rankings'] = "rankings/index";
// $route['rankings/(:any)'] = "rankings/index/$1";


// $route['get_home_videos'] = "home/get_home_videos";
// $route['get_home_news'] = "home/get_home_news";
// $route['purchase_stream_pakvswi'] = "login/purchase_stream_pakvswi";

// $route['get_featured_series'] = "featured_series/get_featured_series";
// $route['get_featured_videos'] = "featured_series/get_featured_videos";
// $route['get_home_twitter'] = "featured_series/get_home_twitter";

// $route['get_home_articles'] = "featured_series/get_home_articles";


// $route['PAK-vs-NZ-2018'] = "psl/index";



$route['404_override'] = '';

$route['send_pin'] = 'api/send_pin';
$route['confirm_pin'] = 'api/confirm_pin';

$route["jazz/confirm_pin"] = "fantasy/confirm_pin_jazz_he/cricwick";
$route["api/subscription_click"] = "fantasy/subscription_click/cricwick";
$route['api/send_pin'] = 'fantasy/send_pin/cricwick';
$route['api/confirm_pin'] = 'fantasy/confirm_pin/cricwick';

$route["paidpk/confirm_pin_jazz_he"] = "fantasy/confirm_pin_jazz_he/paidpk";
$route["paidpk/subscription_click"] = "fantasy/subscription_click/paidpk";
$route['paidpk/send_pin'] = 'fantasy/send_pin/paidpk';
$route['paidpk/confirm_pin'] = 'fantasy/confirm_pin/paidpk';

$route['pk_telco/send_pin'] = 'fantasy/send_pin/psl';
$route['pk_telco/confirm_pin'] = 'fantasy/confirm_pin/psl';

$route['cricwick/send_pin'] = 'fantasy/send_pin/mbl';
$route['cricwick/confirm_pin'] = 'fantasy/confirm_pin/mbl';

$route['pk_inapp/send_pin'] = 'fantasy/send_pin/psl_inapp';
$route['pk_inapp/confirm_pin'] = 'fantasy/confirm_pin/psl_inapp';
$route['pk_inapp/lookup'] = 'fantasy/lookup/pk_inapp';

$route['gago/send_pin'] = 'fantasy/send_pin/gago';
$route['gago/confirm_pin'] = 'fantasy/confirm_pin/gago';
$route['gago/lookup'] = 'fantasy/lookup/gago';

$route['testgago/send_pin'] = 'fantasy/send_pin/testgago';
$route['testgago/confirm_pin'] = 'fantasy/confirm_pin/testgago';

$route['kht/send_pin'] = 'fantasy/send_pin/kht';
$route['kht/confirm_pin'] = 'fantasy/confirm_pin/kht';
$route["kht/subscription_click"] = "fantasy/subscription_click/kht";
$route['kht/lookup'] = 'fantasy/lookup/kht';
$route["kht/confirm_pin_jazz_he"] = "fantasy/confirm_pin_jazz_he/kht";
/* End of file routes.php */
/* Location: ./application/config/routes.php */
