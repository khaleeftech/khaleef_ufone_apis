<?php

$lang["phone_password_requried"] = "Phone Number/Pincode Required";
$lang["phone_required"] = "Phone Number Required!";
$lang["invalid_phone_format"] = "Please enter a valid Ufone number in this format \n 03xxxxxxxxx";
$lang["phone_not_registered"] = "Phone Number is not registered, please subscribe now to get full access.";
$lang["phone_already_registered"] = "Phone Number already registered, please login to continue!";
$lang["password_invalid"] = "Invalid Pincode!";
$lang["low_balance"] = "Sorry! You don't have enough balance to subscribe for the service.";
$lang["general_error"] = "Something went wrong, please try again later!";
$lang["login_wd_sms_pin"] = "We have sent PIN Code to your mobile number through SMS!";

$lang["register_or_login"] = "Register | Login Now!";
$lang["label_login"] = "Login!";
$lang["label_register"] = "Register!";
$lang["enter_phone_number"] = "Please enter your mobile number*:";
$lang["enter_pincode"] = "Please enter pincode:";
$lang["btn_continue"] = "Continue";
$lang["label_please_wait"] = "Please Wait...";
$lang["zain_number_only"] = "Sorry! Only Zain Users are allowed to access this service.";
$lang["forget_pin_anchor"] = "Forgot PIN Code? Click Here!";
$lang["label_subscribe"] = "Subscribe";
?>