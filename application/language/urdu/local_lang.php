<?php

$lang["phone_password_requried"] = "Phone Number/Pincode Required";
$lang["phone_required"] = "Phone Number Required!";
$lang["invalid_phone_format"] = "موبائل نمبر غلط ہے";
$lang["phone_not_registered"] = "Phone Number is not registered, please subscribe now to get full access.";
$lang["phone_already_registered"] = "Phone Number already registered, please login to continue!";
$lang["password_invalid"] = "پن کوڈ درست نہیں ہے";
$lang["low_balance"] = "Sorry! You don't have enough balance to subscribe for the service.";
$lang["general_error"] = "Something went wrong, please try again later!";
$lang["login_wd_sms_pin"] = "یس ایم ایس کے ذریعہ آپکے موبائل نمبر پر پن کوڈ بھیجا گیا ہے.";

$lang["register_or_login"] = "رجسٹر | لاگ ان کریں!";
$lang["label_login"] = "لاگ ان کریں!";
$lang["label_register"] = "رجسٹر!";
$lang["enter_phone_number"] = "اپنا موبائل نمبر* درج کریں:";
$lang["enter_pincode"] = "پن کوڈ درج کریں:";
$lang["btn_continue"] = "جاری رکھیں";
$lang["label_please_wait"] = "انتظار کریں";
$lang["zain_number_only"] = "معاف کیجئے گا! صرف زین صارفین اس سروس تک رسائی حاصل کرنے کی اجازت ہے.";
$lang["forget_pin_anchor"] = "پن کوڈ بھول گیا ہے؟ یہاں کلک کریں!";
$lang["label_subscribe"] = "سبسکرائب";



?>