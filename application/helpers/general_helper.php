<?php

function is_valid_mobilink_number($phone_number){
	$pattern = '/^03\d{9}$/';
	$result = preg_match($pattern,$phone_number);
	if ($result){
		return true;
	} else {
		return false;
	}
}

function normalize_pk_to_local($phone_number){
	$pattern = '/^923\d{9}$/';
	$result = preg_match($pattern,$phone_number);
	if ($result){
		return "0".substr($phone_number, 2); 
	} else {
		return $phone_number;
	}
}

function assets_url(){
	return base_url()."assets/april19/";
}

function assets_url1(){
	return base_url()."assets/jan18/";
}

function if_file_exists($url){
	if(strpos($url, 'missing') === false && !empty($url)){
		//good to go
		return true;
	}else{
		return false;
	}
	return false;
}

function trimer($string, $length, $strip_tags = true){
	if ($strip_tags){
		$string = strip_tags($string);
	}
	if (strlen($string)>$length){
		$string = substr($string,0, $length);
		$string = trim($string);
		$string .= "...";
	}
	return $string;
}

function seo_url($string){
	$string = preg_replace('/[^a-zA-Z0-9_]/', '-', $string);
	$string =  preg_replace('/-+/', '-', $string);
	$string = preg_replace('/^\W+|\W+$/', '', $string);
	$string = strtolower($string);
	return $string;
}

function in_pakistan(){
	//return 1;

	$remote_ip = array($_SERVER["REMOTE_ADDR"]);
	//$_SERVER["HTTP_X_FORWARDED_FOR"] = "66.249.93.228";
	//$_SERVER["HTTP_X_FORWARDED_FOR"] = "122.129.79.150, 66.249.93.228";
	//$_SERVER["HTTP_X_FORWARDED_FOR"] = "31.31.31.199, 10.11.3.31, 111.68.102.25";
	if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$remote_ip = explode(",", $_SERVER["HTTP_X_FORWARDED_FOR"]);
	}
	if ($_SERVER["REMOTE_ADDR"]=='127.0.0.1'){
		return 1;
	}
	foreach($remote_ip as $ip){
		$ip = trim($ip);
		//$url = "http://ip-api.com/json/$ip";
		$url = "https://ipfind.co/?ip=$ip&auth=4029d1e5-8c9f-482d-9dce-d1cd064fd737";
		$response = curl_call($url);
		$response = json_decode($response);
		if (!empty($response->country)){
			if ($response->country=='Pakistan'){
				return 1;
			}
		}
	}
	return 0;

}

function is_android_user(){
	$CI = &get_instance();
	$headers = $CI->input->request_headers();

	$useragent = @$headers["User-agent"];
	if (empty($useragent)){
		$useragent = @$headers["User-Agent"];
	}
	$pattern = '/Android/';
	$result = preg_match($pattern,$useragent);
	if ($result){
		return true;
	} else {
		return false;
	}
}
function curl_call($url){
	$curl = curl_init($url);
	curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl,CURLOPT_HEADER, 0 );
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
	// curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 40); 
	curl_setopt($curl, CURLOPT_TIMEOUT, 60); //timeout in seconds
	//curl_setopt($curl, CURLOPT_TIMEOUT_MS, 40); //timeout in seconds
	$response = curl_exec($curl);
	curl_close ( $curl );
	return $response;
}

function international_number($number){
	$pattern = '/^03\d{9}$|966\d{9}$|05\d{8}$/';

	$pattern = '/^966\d{9}$/';
	$result = preg_match($pattern,$number);
	if ($result==1){
		return $number;
	}

	$pattern = '/^05\d{8}$/';
	$result = preg_match($pattern,$number);
	if ($result==1){
		$number = ltrim($number, '0');
		return "966".$number;
	}

	$pattern = '/^03\d{9}$/';
	$result = preg_match($pattern,$number);
	if ($result==1){
		$number = ltrim($number, '0');
		return "92".$number;
	}
}

function format_date($date, $time){
	date_default_timezone_set("Asia/Karachi");
	$dateTime = $date." ".$time;
	$strtotime = strtotime($dateTime.' GMT');
	$local["date"] =  date("d",$strtotime);
	$local["month"] =  date("M",$strtotime);
	$local["year"] =  date("Y",$strtotime);
	$local["time"] =  date("h:i A",$strtotime);
	return $local;
}

function format_date_newserver($dateTime){
	date_default_timezone_set("Asia/Karachi");
	//$dateTime = $date." ".$time;
	$strtotime = strtotime($dateTime.' GMT');
	$local["date"] =  date("d",$strtotime);
	$local["month"] =  date("M",$strtotime);
	$local["year"] =  date("Y",$strtotime);
	$local["time"] =  date("h:i A",$strtotime);
	return $local;
}
function format_date_newserver2($dateTime){
	date_default_timezone_set("Asia/Karachi");
	//$dateTime = $date." ".$time;
	$strtotime = strtotime($dateTime.' GMT');
	$local["date"] =  date("d",$strtotime);
	$local["month"] =  date("F",$strtotime);
	$local["year"] =  date("Y",$strtotime);
	$local["time"] =  date("g:i A",$strtotime);
	return $local;
}

function format_date_homebanner($date, $time){
	date_default_timezone_set("Asia/Karachi");
	$dateTime = $date." ".$time;
	$strtotime = strtotime($dateTime.' GMT');
	$local["day"] =  date("l",$strtotime);
	$local["date"] =  date("d",$strtotime);
	$local["month"] =  date("F",$strtotime);
	$local["year"] =  date("Y",$strtotime);
	$local["hours"] =  date("g",$strtotime);
	$local["minutes"] =  date("i",$strtotime);
	$local["meridiem"] =  date("A",$strtotime);
	return $local;
}

function format_date_homebanner_newserver($dateTime){
	date_default_timezone_set("Asia/Karachi");
	//$dateTime = $date." ".$time;
	$strtotime = strtotime($dateTime.' GMT');
	$local["day"] =  date("l",$strtotime);
	$local["date"] =  date("d",$strtotime);
	$local["month"] =  date("F",$strtotime);
	$local["year"] =  date("Y",$strtotime);
	$local["hours"] =  date("g",$strtotime);
	$local["minutes"] =  date("i",$strtotime);
	$local["meridiem"] =  date("A",$strtotime);
	return $local;
}

function get_ball_score_status_timeline_scorecard($ball){
	$string = "";

	$ball->no_ball = empty($ball->no_ball)?0:$ball->no_ball;
	$ball->wide_ball = empty($ball->wide_ball)?0:$ball->wide_ball;
	$ball->extra_leg_bye = empty($ball->extra_leg_bye)?0:$ball->extra_leg_bye;
	$ball->extra_bye = empty($ball->extra_bye)?0:$ball->extra_bye;

	if ($ball->no_ball){
		$string .= "nb";
	} else if ($ball->wide_ball) {
		$string .= "wd";
	} else if ($ball->extra_leg_bye!=0){
		$string .= "lb";
	} else if ($ball->extra_bye!=0){
		$string .= "b";
	}

	$total_score = $ball->runs_scored + $ball->extra_leg_bye + $ball->extra_bye + $ball->extra_penalty_runs;

	if ($ball->boundary_6){
		$string = "6".$string;
	} else if ($ball->boundary_4){
		$string = "4".$string;
	} else if ($ball->wicket){
		$string = "W";
	} else {
		if ($total_score>0 || $ball->no_ball || $ball->wide_ball || $ball->extra_leg_bye>0 || $ball->extra_bye>0){
			$string = $total_score.$string;
		} else {
			$string = "0";
		}

	}
	if ($ball->boundary_6){
		$string = '<div class="b-blue ball-circle"><p>'.$string.'</p></div> ';
	} else if ($ball->boundary_4){
		$string = '<div class="b-green ball-circle"><p>'.$string.'</p></div> ';
	} else if ($ball->wicket){
		$string = '<div class="b-red ball-circle"><p>'.$string.'</p></div> ';
	} else {
		if ($total_score>0 || $ball->no_ball || $ball->wide_ball || $ball->extra_leg_bye>0 || $ball->extra_bye>0){
			$string = '<div class="b-grey ball-circle"><p>'.$string.'</p></div> ';
		} else {
			$string = '<div class="b-grey ball-circle"><p>0</p></div> ';
		}

	}

	return $string;
}

function get_ball_score_status_timeline($ball){
	$string = "";
	/*
	if ($ball->no_ball){
		$string .= "nb";
	} else if ($ball->wide_ball) {
		$string .= "wd";
	} else if ($ball->extra_leg_bye!=0){
		$string .= "lb";
	} else if ($ball->extra_bye!=0){
		$string .= "b";
	}
	*/
	$total_score = $ball->runs_scored + $ball->extra_leg_bye + $ball->extra_bye + $ball->extra_penalty_runs;

	if ($ball->boundary_6){
		$string = '<div class="b-blue ball-circle"><p> 6 </p></div>'.$string;
	} else if ($ball->boundary_4){
		$string = '<div class="b-green ball-circle"><p> 4 </p></div>'.$string;
	} else if ($ball->wicket){
		$string = '<div class="b-red ball-circle"><p> W </p></div>'.$string;
	} else {
		if ($total_score>0 || $ball->no_ball || $ball->wide_ball || $ball->extra_leg_bye>0 || $ball->extra_bye>0){
			//$string = $total_score.$string;
			$string = '<div class="b-grey ball-circle"><p> '.$total_score.' </p></div>'.$string;
		} else {
			$string = '<div class="b-grey ball-circle"><p> 0 </p></div>'.$string;
		}

	}

	return $string;
}

function get_ball_score_status_small($ball){
	$string = "";

	if ($ball->no_ball){
		//$ball->runs_scored = $ball->runs_scored + 1;
		$string .= "nb";
	} else if ($ball->wide_ball) {
		//$ball->runs_scored = $ball->runs_scored + 1;
		$string .= "wd";
	} else if ($ball->extra_leg_bye!=0){
		//$ball->runs_scored = $ball->runs_scored + $ball->extra_leg_bye;
		$string .= "lb";
	} else if ($ball->extra_bye!=0){
		//$ball->runs_scored = $ball->runs_scored + $ball->extra_bye;
		$string .= "b";
	}

	$total_score = $ball->runs_scored + $ball->extra_leg_bye + $ball->extra_bye + $ball->extra_penalty_runs;

	if ($ball->boundary_6){
		$string = "6".$string;
	} else if ($ball->boundary_4){
		$string = "4".$string;
	} else if ($ball->wicket){
		$string = "W";
	} else {
		if ($total_score>0 || $ball->no_ball || $ball->wide_ball || $ball->extra_leg_bye>0 || $ball->extra_bye>0){
			$string = $total_score.$string;
		} else {
			$string = "•";
		}

	}

	return $string;
}

function get_ball_score_status_full($ball){
	$string = "";

	if ($ball->no_ball){
		//$ball->runs_scored = $ball->runs_scored + 1;
		$string .= ", No Ball";
	} else if ($ball->wide_ball) {
		//$ball->runs_scored = $ball->runs_scored + 1;
		$string .= ", Wide Ball";
	} else if ($ball->extra_leg_bye!=0){
		//$ball->runs_scored = $ball->runs_scored + $ball->extra_leg_bye;
		$string .= ", Leg Bye";
	} else if ($ball->extra_bye!=0){
		//$ball->runs_scored = $ball->runs_scored + $ball->extra_bye;
		$string .= ", Bye";
	}

	$total_score = $ball->runs_scored + $ball->extra_leg_bye + $ball->extra_bye + $ball->extra_penalty_runs;

	if ($ball->boundary_6){
		$string .= ", Six";
	} else if ($ball->boundary_4){
		$string .= ", Four";
	} else if ($ball->wicket){
		$string .= ", OUT";
		if ($total_score==1){
			$string .= ", 1 Run";
		} else if ($total_score>1){
			$string .= ", $total_score Runs";
		}
	} else {
		if ($total_score!=0){
			if ($ball->extra_penalty_runs>0){
				$string .= "Penalty";
			}
			if ($total_score>1){
				$string .= ", ".$total_score." Runs";
			} else {
				$string .= ", Single";
			}
		} else {
			if (!$ball->no_ball && !$ball->wide_ball){
				$string .= ", No Run";
			}

		}

	}
	return $string;

}

function get_ball_score_status_timeline_tag($ball){
	$string = "";

	if ($ball->no_ball){
		//$ball->runs_scored = $ball->runs_scored + 1;
		$string .= "No Ball, ";
	} else if ($ball->wide_ball) {
		//$ball->runs_scored = $ball->runs_scored + 1;
		$string .= "Wide, ";
	} else if ($ball->extra_leg_bye!=0){
		//$ball->runs_scored = $ball->runs_scored + $ball->extra_leg_bye;
		$string .= "Legbye, ";
	} else if ($ball->extra_bye!=0){
		//$ball->runs_scored = $ball->runs_scored + $ball->extra_bye;
		$string .= "Bye, ";
	}

	$total_score = $ball->runs_scored + $ball->extra_leg_bye + $ball->extra_bye + $ball->extra_penalty_runs;

	if ($ball->boundary_6){
		$string .= "Six";
	} else if ($ball->boundary_4){
		$string .= "Four";
	} else if ($ball->wicket){
		$string .= "OUT";
		if ($total_score==1){
			$string .= ", 1 Run";
		} else if ($total_score>1){
			$string .= ", $total_score Runs";
		}
	} else {
		if ($total_score!=0){
			if ($ball->extra_penalty_runs>0){
				$string .= "Penalty";
			}
			if ($total_score>1){
				$string .= $total_score." Runs";
			} else {
				$string .= "1 Run";
			}
		} else {
			if (!$ball->no_ball && !$ball->wide_ball){
				$string .= "Dot Ball";
			}
		}
	}
	return rtrim(trim($string), ",");

}

 function post_curl($url, $params){
    $curl = curl_init($url);
    //curl_setopt($curl,CURLOPT_HEADER, 0 );
    curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
    $response = curl_exec($curl);
    curl_close ( $curl );
    return $response;
  }

function demo(){
    $response['pp_mobily'] = "You will subscribe to CricWick App for 0.66 SAR daily. If you are already subscribed then it will be just a phone verification.";
    $response['pp_zain_kw'] = "You will subscribe to CricWick App for 0.10 KD daily. New Users will be given a 1 day free trial, then will be charged the daily rate hereafter. To unsubscribe send UNSUB 5 to 94010 or use the unsubscribe button in the menu within app. Any enquiries, please contact: info@cricwick.net";

    $response['pp_zain'] = "You will subscribe to CricWick App for 5 SAR weekly. If you are already subscribed then it will be just a phone verification.";

    $response['subscribed_streams'] = [];

    $response['status_code'] = 1;

    $response['pp_ooredoo'] = "";

    $response['purchased_matches'] = [];

    $response['pp_zain_bh'] = "You will subscribe to CricWick App for 0.10 BHD daily. New Users will be given a 1 day free trial, then will be charged the daily rate hereafter. To unsubscribe send UNSUB 1 to 94005 or use the unsubscribe button in the menu within app. Any enquiries, please contact: info@cricwick.net";

    $response['pp_etisalat_uae'] = "You will subscribe to CricWick App for 1 AED daily. If you are already subscribed then it will be just a phone verification.";

    $response['details'] = "";

    $response['live_stream_price'] = 3;

    $response['pp_3_bh'] = "You will subscribe to CricWick App for 0.10 BHD daily. New Users will be given a 1 day free trial, then will be charged the daily rate hereafter. To unsubscribe send UNSUB 1 to 94005 or use the unsubscribe button in the menu within app. Any enquiries, please contact: info@cricwick.net";

    $response['pp_stc'] = "You will subscribe to CricWick App for 0.66 SAR daily. If you are already subscribed then it will be just a phone verification.";

    $response['status_message'] = "OK";

    $response['pp_1_kw'] = "You will subscribe to CricWick App for 0.10 KD daily. New Users will be given a 1 day free trial, then will be charged the daily rate hereafter. To unsubscribe send UNSUB 5 to 94010 or use the unsubscribe button in the menu within app. Any enquiries, please contact: info@cricwick.net";

    $response['user']['id'] = 1;

    $response['user']['phone'] = 03001111111;

    $response['user']['pin'] = "1234";

    $response['user']['total_amount'] = 1.99;

    $response['user']['total_days'] = 1;

    $response['user']['status'] = 1;

    $response['user']['total_installments'] = 1;

    $response['user']['current_tick'] = 0;

    $response['user']['unsub_reason'] = null;

    $response['user']['created_at'] = "September 05, 2018 15:39";

    $response['user']['updated_at'] = "September 05, 2018 15:39";

    $response['user']['udid'] = "e13d8ebca564cf32_353151070054691_000_000_000_000_000_000";

    $response['user']['next_billing'] = "September 05, 2018 15:39";

    $response['user']['last_billed'] = "September 05, 2018 15:39";

    $response['user']['renewal_date'] = "September 05, 2018 15:39";

    $response['user']['reattempt_billing'] = null;

    $response['user']['subscription_type'] = 1;

    $response['user']['trial_started_at'] = null;

    $response['user']['trial_ended_at'] = null;

    $response['user']['gcm_token'] = "eWnYhmV7bWM:APA91bFsS5IIep1h92Wm6vubvponiIf3d8FxAWpPPXykh3xa1osNaXCMdGcCYeqrcyB46FE2uAiydqHTgz_ftEiMii8qBWDpRffMS5VI73vhwBifOPmkXpnQxOvLkAMqSrKzBQLnXLJ3";

	$response['user']['is_postpaid'] = false;
	
	$response['user']['telco_id'] = 5;


	$response['user']['last_accessed_at'] = "2018-09-28T09:35:08.906Z";
	
	$response['user']['free_trial'] = 0;

	$response['user']['last_billed_ts'] = "1538089238";

	$response['user']['next_billing_ts'] = "1538089238";

	$response['user']['renewal_date_ts'] = "1538089238";

	$response['user']['remain_days'] = 0;

	$response['user']['free_trial_over_msg'] = "";

	$response['user']['welcome_free_trial'] = "";

	

    $response = json_decode(json_encode($response), false);
    return $response;


}
function generate_wowza_hash($secret, $end_time){
	$key = "live/ipl_240p?$secret&wowzatokenendtime=".$end_time;
	//echo $key."<BR>";
	$hash = hash("sha256",$key, true);
	//echo $hash."<BR>";
	$hash = str_replace(array("+","/"), array("-","_"), base64_encode($hash));
	//echo $hash."<BR>";
	return $hash;
}


function display($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

function display_json($json){
	header('Content-Type:application/json');
	echo $json;
}
?>
