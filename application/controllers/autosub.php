<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Autosub extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	 
	public function index(){
		
		//$_SERVER["HTTP_MSISDN"] = "966111111114";
		$phone = $_SERVER["HTTP_MSISDN"];
		
		
		if ($this->input->is_ajax_request()){
			
			$params = array();
			$params["udid"] = $phone;
			$params["phone"] = $phone;
			$params["service_id"] = 36;
			$params["web_user"] = 1;
			
			$url = API_PATH."appstart?";
			$url = $url.http_build_query($params);
			$response = curl_call($url);
			$response = json_decode($response);
			
			if ($response->user->subscribe_status!=1){
				$url = API_PATH."api_hit_subscribe?";
				$url = $url.http_build_query($params);
				sleep(10);
			}
			exit;
			
		} else {
			$data["playstore_url"] = "https://play.google.com/store/apps/details?id=com.Khaleef.cricwick&hl=en";
			$this->load->view("appdirect", $data);
		}
		
	}
	
	


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */