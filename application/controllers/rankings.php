<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Rankings extends My_Controller {

	function __construct(){
		parent::__construct();
	}

	function index($type='test'){
		
		if($type != 'test' && $type != 'odi' && $type != 't20'){
			$type= 'test';
		}
		// print_r($type);
		// exit;

		$url = BACKEND."ranking?type=test&telco=".CONTENT_TELCO;
		$test = json_decode(curl_call($url));

		$url = BACKEND."ranking?type=odi&telco=".CONTENT_TELCO;
		$odi = json_decode(curl_call($url));

		$url = BACKEND."ranking?type=t20&telco=".CONTENT_TELCO;
		$t20 = json_decode(curl_call($url));

		$this->data["ranks"] = [];
		$this->data["ranks"]["test"] = $test;
		$this->data["ranks"]["odi"] = $odi;
		$this->data["ranks"]["t20"] = $t20;


		$this->data["rankings_for"] = $type;	

		$this->data['timeline_page'] = 1;
		$this->data["page"] = $this->load->view('april19/rankings', $this->data, true);
		$this->data["remove_responsive"] = true;
		$this->load->view('april19/template', $this->data);
	}
}