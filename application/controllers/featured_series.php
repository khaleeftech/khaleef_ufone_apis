<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Featured_series extends My_Controller {

	public $data;
	function __construct(){
		parent::__construct();
		$summary_url = BACKEND."series_summary/81/summary";
		$summary_response = json_decode(curl_call($summary_url));
		$this->data["summary"] = $summary_response;
		// $this->data["series_title"] = $summary_response->recent_matches[0]->series->title;
		$this->data["series_title"] = "Indian Premier League, 2018";
	}

    public function get_featured_series() {
        if($this->input->is_ajax_request()){


                $response = [];
                $response = $this->memcached_library->get('cricwick_ufone_home_videos');
                if(empty($response)){
                    $path = API_PATH."home?&web_user=1";
                    $response = curl_call($path);
                    $this->memcached_library->set('cricwick_ufone_home_videos', $response, 600);
                }
                $response = mb_convert_encoding($response, "UTF-8");
                $response = json_decode($response);

                $data = [];
                $data["timeline_videos"] = $response->timeline_videos;
                // print_r($response->timeline_videos);exit;
                echo $this->load->view('april19/featured_series_home', $data, true);

        }
    }

    public function get_featured_videos() {
        $this->load->view('april19/featured_videos');
    }

    public function get_home_articles() {
        
		if($this->input->is_ajax_request()){


                $response = [];
                $response = $this->memcached_library->get('get_home_articles');
                if(empty($response)){
                    $url = BACKEND."articles?page=1&page_size=3&telco=".CONTENT_TELCO;
                    $response = curl_call($url);
                    $this->memcached_library->set('get_home_articles', $response, 600);
                }
                $response = mb_convert_encoding($response, "UTF-8");
                $response = json_decode($response);
                // print_r($response);exit;
                $data = [];
                $data["articles"] = $response;
                echo $this->load->view('april19/home_articles', $data, true);

        }
        
    }

    public function get_home_twitter() {
        $url = "http://back.cricwick.net/api/news?page=1&page_size=3";
        // $url = API_PATH."get_news?page_number=$page_no&page_size=$page_size";
        $response = curl_call($url);
        // $response = mb_convert_encoding($response, "UTF-8");
        $response = json_decode($response);
        // print_r($response);
        $data = [];
        $data['twitter'] = $response;
        
        $this->load->view('april19/home_twitter' , $data);
    }

	

}
