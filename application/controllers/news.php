<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class News extends My_Controller {

	function __construct(){
		parent::__construct();
	}


	function index($page_no=1){

		//$this->check_session();
		//$this->check_subscription();

		$this->data["news"] = json_decode($this->more_news($page_no));
		
		
		$this->data["page_heading"] = "News";
		$this->data["inner_page"] = $this->load->view('april19/news/news', $this->data, true);

		$this->data["page"] = $this->load->view("april19/template_wd_left_col", $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$this->load->view('april19/template', $this->data);

	}

	// more_news() FUNCTION IS MOVED TO MY CONTROLLER

	// function more_news($page_no=1, $page_size=8){

	// 	$url = "http://52.37.162.49:8080/api/news?page=$page_no&per_page=$page_size";
	// 	$url = BACKEND."news?page=$page_no&per_page=$page_size";

	// 	// $url = API_PATH."get_news?page_number=$page_no&page_size=$page_size";
	// 	$response = curl_call($url);
	// 	$response = json_decode($response);
	// 	foreach($response->data as $key=>$item){
	// 		// $item->details = trimer($item->details, 400);

	// 		// $item->created_at = date('F j, Y', $item->created_at/1000);
	// 		$item->created_at = date('F j, Y', strtotime($item->created_at));
	// 		// $d = explode(' ', $item->created_at)[0];
	// 		// $item->created_at = $d;
	// 		// $item->created_at = date('F j, Y', $d);



	// 		$item->seo_url = base_url()."news-update/".$item->id."/".seo_url($item->title)."/";
	// 		$response->news[$key] = $item;
	// 	}
	// 	$response = json_encode($response);
	// 	if ($this->input->is_ajax_request()){
	// 		echo $response;
	// 		return;
	// 	} else {
	// 		return $response;
	// 	}
	// }

	function news_detail($news_id, $seokey=false){

		$this->check_session();
		$this->check_subscription();

		$url = BACKEND."news/$news_id&telco=".CONTENT_TELCO;
		$curl = curl_init($url);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl,CURLOPT_HEADER, 0 );
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($curl);
		curl_close ( $curl );
		$response = json_decode($response);


		// $this->data["current_news"] = $response->news;
		$this->data["current_article"] = $response;

		$this->data["current_article"]->seo_url = base_url()."news-update/".$response->id."/".seo_url($response->title)."/";

		$this->data["more_articles"] = json_decode($this->more_news(1, 8))->data;

		// $this->data["inner_page"] = $this->load->view('news/news_detail', $this->data, true);
		// $this->data["inner_page"] = $this->load->view('news/news_detail', $this->data, true);

		$this->data['article_type'] = 'news';
		$this->data["inner_page"] = $this->load->view('april19/articles/article_detail', $this->data, true);


		// $this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		$this->data["page"] = $this->load->view("april19/template_wd_left_col", $this->data, true);

		$this->data["add_video_id_to_body"] = true;
		$default_social = new stdClass();
		$default_social->link = current_url()."/";
		$default_social->title =  $response->title." - CricWick";
		$default_social->descr = trimer(strip_tags($response->body),200);
		$default_social->image = $response->image;

		$this->data["og_tags"] = $default_social;
		$this->load->view('april19/template', $this->data);

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
