<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require 'application/controllers/My_Controller.php';

class Dev extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	function reset_mem(){
		$key = $this->input->get("key");
		if ($key){
			//$this->load->library('memcached_library');
			$result = $this->memcached_library->get($key);
			if ($result){
				$this->memcached_library->delete($key);
				echo "Key Deleted";
			} else {
				echo "key Not Found";
			}
		} else {
			echo "Missing Key";
		}
		
	}
	
	function video(){
		
		$this->load->view("videos/test_video");
		
	}
	
	function test(){
		echo assets_url();
	}
	
}
