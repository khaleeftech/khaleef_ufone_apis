<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Test extends My_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	function index(){
		
		$this->load->view("test/twitter");
	}
	
	function header(){
		$headers = $this->input->request_headers();
		echo "<pre>";
		print_r($headers);
		echo "</pre>";
	}
	
	function get_header_phone(){
		$headers = $_SERVER;//$this->input->request_headers();
		
		$response = new stdClass();
		$response->status = 1;
		if (!empty($headers["HTTP_MSISDN"])){
			$response->phone = $headers["HTTP_MSISDN"];
		}
		echo json_encode($response);
	}

	
}
