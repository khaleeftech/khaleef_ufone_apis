<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Controller extends CI_Controller {

	public $data = array();
	function __construct(){
		parent::__construct();

	}

	function isBlocking($partner_id, $operator, $campaign, $in_pinflow=1, $sub_type){
	    //$this->output->enable_profiler(TRUE);

	    $internal_operator = $operator;
	    if (in_array($internal_operator, array("mobilink","warid"))){
	    	$internal_operator = "jazz";
	    }

	    $key = $partner_id."_".$internal_operator."_".$campaign;

	    $sp_name = $in_pinflow?"getServicePartnerCampaignPinFlowStatsForTheDay":"getServicePartnerCampaignClickFlowStatsForTheDay";

	    $file_name = "blocking_data.json";
	    $myfile = fopen($file_name, "r") or die("Unable to open file!");
	    $data = fread($myfile,filesize($file_name));
	    fclose($myfile);
	    $data = json_decode($data);
	    
	    if (!empty($data->$key)) {
	      $data = $data->$key;
	      $today = date('Y-m-d', time()+(5*3600)); //Add 5 hours to get PK Time
	      //$this->load->database();
		    $product = $operator."_".$sub_type;
	      $sql = "CALL $sp_name($partner_id, '$product', '$campaign', '$today', '+05:00')";
	      $dbresult = $this->db->query($sql);
		    $result = $dbresult->row();
	      $data->total_subs = $result->total;
	      $data->paid = $result->total - $result->blocked;

	      //echo json_encode($data);
	      $to_be_blocked = $this->decideBlocking($data);
	      $response = true;
	      if (!$to_be_blocked){
	        $response = false;
	      }

		    //$dbresult->close();
		    //$this->db->next_result();
        $dbresult->next_result(); 
        $dbresult->free_result(); 

	      return $response;

	    } else {
	      return false;
	    }
	    //echo json_encode($data->$key);
  	}

	function decideBlocking($data){
		//$data = json_decode($data);
		if ( $data->total_subs==0 ){
		  return false;
		}

		$allowed_percent = 100 - $data->block;

		$paid_percent = (@$data->paid / @$data->total_subs) * 100;

		if ($paid_percent <= $allowed_percent){
		  return false;
		} else {
		  return true;
		}
		//echo $data->timezone; 
	}

	function  getBlockingErrorMessage(){
		return "Already Subscribed";
		// $random = mt_rand();
		// $resp_msg = ($random%2 == 0)?"Already Subscribed":"Invalid Pincode";
		// return $resp_msg;
	}

	function check_if_internal_blocked ($msisdn, $in_pinflow=1) {

		$table_name = $in_pinflow?"acquisitions_new":"acquisitions_he_new";

		$sql = "select count(id) as total from $table_name where phone='$msisdn' and is_blocked=1";
		$result = $this->cric_db->query($sql)->row();
		return $result->total;
	}



}
