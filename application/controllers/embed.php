<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Embed extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	function index($video_id){
		
		$url = API_PATH."get_single_video?id=$video_id";
		$response = curl_call($url);
		$response = json_decode($response);
		$this->data["current_video"] = $response->video;
		$url = API_PATH."get_videos?page_number=1&page_size=12";
		$more_response = curl_call($url);
		$more_response = json_decode($more_response);
		$this->data["more_videos"] = $more_response->videos;
		$this->data["inner_page"] = $this->load->view('videos/play_video', $this->data, true);
		$this->data["page_heading"] = "Video";
		$this->data["banner_heading"] = "CricBoom";
		$this->data["banner_tagline"] = "videos";
		$this->data["banner_img_class"] = "back-img-video";
		$this->data["video_page"] = TRUE;
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		$this->load->view('template', $this->data);
	}
	
	function video($video_id){
		$url = API_PATH."get_single_video?id=$video_id";
		$response = curl_call($url);
		$response = json_decode($response);
		$data["video"] = $response->video;
		$data["ad_file"] = base_url()."assets/images/ads/pepsi_ad.mp4";
		$data["video_file"] = $response->video->video_file; 
		$this->load->view("embed/video",$data);
	}
	
	function serve($video_id){
		$url = API_PATH."get_single_video?id=$video_id";
		$response = curl_call($url);
		$response = json_decode($response);
		header("Content-type: video/mp4");
		$file = base_url()."assets/images/ads/pepsi_ad.mp4";
		readfile($file);
		$file = $response->video->video_file;
		readfile($file);
	}
	
	function tweet($tweet_id){
			
		$data["tweet_id"] = $tweet_id;
		$this->load->view("embed/tweet", $data);
		
	}

}