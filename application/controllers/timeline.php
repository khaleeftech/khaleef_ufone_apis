<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Timeline extends My_Controller {

	function __construct(){
		parent::__construct();

	}

	public function index(){
		$this->check_session();
		$this->check_subscription();

		$this->data["video_allowed"] = in_pakistan();

		$path = API_PATH."home?telco=ufone";
		$response = curl_call($path);
		$response = json_decode($response);

		$this->data["live_matches"] = $response->live_matches;

		$players = array();
		$teams = array();
		foreach($response->live_matches as $LiveMatch){
			foreach($LiveMatch->fixture->Playing_11->{"Team A"} as $player){
			$players[$player->id] = $player;
			}
			foreach($LiveMatch->fixture->Playing_11->{"Team B"} as $player){
				$players[$player->id] = $player;
			}
			$teams[$LiveMatch->fixture->teamA->id] = $LiveMatch->fixture->teamA;
			$teams[$LiveMatch->fixture->teamB->id] = $LiveMatch->fixture->teamB;
		}
		$this->data["players"] = $players;
		$this->memcached_library->set("liveMatchPlayers", $players, 21600); //Six Hours
		$this->memcached_library->set("liveMatchTeams", $teams, 21600); //Six Hours

		$this->data["page"] = $this->load->view('timeline/main', $this->data, true);
		$this->data["home_css_load"] = true;
		$this->load->view('template', $this->data);
	}

	function livematch($id, $seokey=false){

		$this->check_session();
		$this->check_subscription();
		$this->data["video_allowed"] = in_pakistan();

		$path = BACKEND."live_matches?telco=ufone";
		$response = curl_call($path);

		if(!empty(json_decode($response))){
			$this->memcached_library->set('cricwick_ufone_home_live_matches',$response,86400);
		}else{
			$response = $this->memcached_library->get('cricwick_ufone_home_live_matches');
		}


		$response = json_decode($response);

		$this->data["live"] = "";

		foreach($response->live_matches as $live){
			if ($live->id==$id){
				$this->data["live"] = $live;
			}
		}

		if ($this->data["live"]==""){
			$url = base_url()."match/$id/$seokey/";
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: $url");
			exit;
		}



		$teams = array();

		foreach($response->live_matches as $LiveMatch){
			if (!empty($LiveMatch->innings)){
				$inning = $LiveMatch->innings[0];
				if ($inning->batting_team_id == $LiveMatch->team_1_id){
					$teams[$inning->batting_team_id] = $LiveMatch->team_1->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $LiveMatch->team_2->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_1->team;
				}
				$teams[$inning->batting_team_id]->flag_url =  $teams[$inning->batting_team_id]->full_flag_url;
				$teams[$inning->fielding_team_id]->flag_url =  $teams[$inning->fielding_team_id]->full_flag_url;
			}
		}

		$this->data["teams"] = $teams;
		$this->memcached_library->set("liveMatchTeams", $teams, 21600); //Six Hours

		$this->data["match_id"] = $this->data["live"]->id;

		/*
		$url = API_PATH."get_match_highlight_tags?match_id=".$this->data["live"]->id;
		$response = curl_call($url);
		$tags_stats = json_decode($response);
		*/
		$this->data["tags_stats"] = array();

		$this->data["tags_stats"]["t_six"] = 0;
		$this->data["tags_stats"]["t_four"] = 0;
		$this->data["tags_stats"]["t_out"] = 0;
		$this->data["tags_stats"]["t_all"] = 0;
		$this->data["tags_stats"]["t_boom"] = 0;

		$this->data["tags_stats"]["t_total"] = $this->data["tags_stats"]["t_six"] + $this->data["tags_stats"]["t_four"] + $this->data["tags_stats"]["t_out"];
		$this->data["tags_stats"]["t_total"] += $this->data["tags_stats"]["t_all"] + $this->data["tags_stats"]["t_boom"];

		$this->data["is_live"] = true;

		$this->data["live_stream"] = "";


		if (!empty($this->data["live"]->live_stream_url)){
			//$url = $this->data["live"]->live_stream_url;

			$this->data["live_sources"] = array();
			// $access_token = $this->get_stream_token();

			$this->data["live_stream"] = $this->data["live"]->live_stream_url;
			// $this->data["live_stream"] = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream";
			$useragent = $_SERVER["HTTP_USER_AGENT"];
			$pattern = '/Android/';
			$result = preg_match($pattern,$useragent);
			if (!$result){
				$sourceObject = new stdClass();
				//$sourceObject->file = "rtmp://679868516.r.cdnsun.net/679868516/_definst_/stream_480p_khaleef?token=".$access_token;
				$sourceObject->file = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef?token=".$access_token;
				$sourceObject->label = "480P";
				$sourceObject->type = "rtmp";
				$this->data["live_sources"][] = $sourceObject;

				$sourceObject = new stdClass();
				//$sourceObject->file = "http://679868516.r.cdnsun.net/679868516/_definst_/stream_480p_khaleef/playlist.m3u8?token=".$access_token;
				$sourceObject->file = "http://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef/playlist.m3u8?token=".$access_token;
				$sourceObject->label = "480P";
				$sourceObject->type = "hls";
				$this->data["live_sources"][] = $sourceObject;
			} else {
				$sourceObject = new stdClass();
				//$sourceObject->file = "http://58445210.r.cdnsun.net/58445210/_definst_/stream_480p_khaleef/playlist.m3u8";
				$sourceObject->file = "http://924938945.r.cdnsun.net/924938945/_definst_/stream_480p_khaleef/playlist.m3u8";
				$sourceObject->label = "480P";
				$sourceObject->type = "hls";
				$this->data["live_sources"][] = $sourceObject;
			}

			$this->data["live_sources"] = json_encode($this->data["live_sources"]);
		}

		// echo "<pre>";
		// print_r($this->data["sub_streams"] );
		// echo "</pre>";
		// exit;

		// $this->data["match_stream_id"] = $this->data["live"]->id;
		// if (in_array($this->data["match_stream_id"], $this->data["sub_streams"])){
			$this->data["stream_allowed"] = 1;
		// } else {
			// $this->data["stream_allowed"] = 0;
		// }

		$this->data["post_match_url"] = base_url()."match/".$id."/".$seokey."/";


		// $this->data["page"] = $this->load->view('april19/timeline/main', $this->data, true);
		// $this->data["remove_responsive"] = true;
		// $this->load->view('april19/template', $this->data);
		// $this->data["live_stream"] = "";
		$this->data["timeline_page"] = 1;
		$this->data["page"] = $this->load->view('april19/timeline/main', $this->data, true);
		$this->load->view('april19/template', $this->data);
	}

	function postmatch_r($id, $seokey=false){
		$url = base_url()."match/$id/".$seokey."/";
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $url");
    	exit;
	}
	function livematch_r($id, $seokey=false){
		$url = base_url()."live/$id/".$seokey."/";
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $url");
    	exit;
	}

	function postmatch($id, $seokey=false){

		$this->check_session();
		$this->check_subscription();

		$this->data["video_allowed"] = in_pakistan();

		$path = BACKEND."$id";
		$response = curl_call($path);
		$response = json_decode($response);

		if (empty($response)){
			redirect(base_url());
			return;
		} else if ($response->match_state=='Live'){
			$url = base_url()."live/".$response->id."/".seo_url($response->team_1->team->name." vs ".$response->team_2->team->name)."/";
			redirect($url);
			return;
		}

		$teams = array();
		$LiveMatch = $response;
		if (!empty($LiveMatch->innings)){
			$inning = $LiveMatch->innings[0];
			if ($inning->batting_team_id == $LiveMatch->team_1_id){
				$teams[$inning->batting_team_id] = $LiveMatch->team_1->team;
				$teams[$inning->fielding_team_id] = $LiveMatch->team_2->team;
			} else {
				$teams[$inning->batting_team_id] = $LiveMatch->team_2->team;
				$teams[$inning->fielding_team_id] = $LiveMatch->team_1->team;
			}
			$teams[$inning->batting_team_id]->flag_url = $teams[$inning->batting_team_id]->full_flag_url;
			$teams[$inning->fielding_team_id]->flag_url = $teams[$inning->fielding_team_id]->full_flag_url;
		}

		$this->data["teams"] = $teams;


		$this->data["match_id"] = $response->id;
		$this->data["live"] = $response;
		/*
		$url = API_PATH."get_match_highlight_tags?match_id=".$response->match->id;
		$response = curl_call($url);
		$tags_stats = json_decode($response);
		*/
		$this->data["tags_stats"] = array();

		$this->data["tags_stats"]["t_six"] = 0;
		$this->data["tags_stats"]["t_four"] = 0;
		$this->data["tags_stats"]["t_out"] = 0;
		$this->data["tags_stats"]["t_all"] = 0;
		$this->data["tags_stats"]["t_boom"] = 0;
		/*
		if (isset($tags_stats->{1})){
			$this->data["tags_stats"]["t_six"] = $tags_stats->{1};
		}
		if (isset($tags_stats->{2})){
			$this->data["tags_stats"]["t_four"] = $tags_stats->{2};
		}
		if (isset($tags_stats->{3})){
			$this->data["tags_stats"]["t_out"] = $tags_stats->{3};
		}
		if (isset($tags_stats->{23})){
			$this->data["tags_stats"]["t_boom"] = $tags_stats->{23};
		}
		foreach($tags_stats as $key=>$val){
			if ($key!=1 && $key!=2 && $key!=3 && $key!=23){
				$this->data["tags_stats"]["t_all"] += $val;
			}
		}
		*/
		$this->data["tags_stats"]["t_total"] = $this->data["tags_stats"]["t_six"] + $this->data["tags_stats"]["t_four"] + $this->data["tags_stats"]["t_out"];
		$this->data["tags_stats"]["t_total"] += $this->data["tags_stats"]["t_all"] + $this->data["tags_stats"]["t_boom"];

		$this->data["is_live"] = false;
		$this->data["live_stream"] = "";//"rtmp://dev.wowza.longtailvideo.com/vod/_definst_/sintel/640.mp4";
		//$this->data["stream_url"] = "rtmp://dev.wowza.longtailvideo.com/vod/_definst_/sintel/640.mp4";
		/*
		$this->data["live_stream"] = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef";
		$useragent = $_SERVER["HTTP_USER_AGENT"];
		$pattern = '/Android|iPhone|iPad|iPod/';
		$result = preg_match($pattern,$useragent);
		if ($result){
			//$this->data["live_stream"] = "http://506206037.r.cdnsun.net/506206037/_definst_/stream_360p/playlist.m3u8";
			$this->data["live_stream"] = "http://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef/playlist.m3u8";
		}

		if (in_array($id, $this->data["sub_streams"])){
			$this->data["stream_allowed"] = 1;
		} else {
			$this->data["stream_allowed"] = 1;
		}
		*/
		/*
		$this->data["live_sources"] = array();
		$access_token = $this->get_stream_token();

		$this->data["live_stream"] = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream";
		$useragent = $_SERVER["HTTP_USER_AGENT"];
		$pattern = '/Android/';
		$result = preg_match($pattern,$useragent);
		if (!$result){
			$sourceObject = new stdClass();
			//$sourceObject->file = "rtmp://679868516.r.cdnsun.net/679868516/_definst_/stream_480p_khaleef?token=".$access_token;
			$sourceObject->file = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef?token=".$access_token;
			$sourceObject->label = "480P";
			$sourceObject->type = "rtmp";
			$this->data["live_sources"][] = $sourceObject;

			$sourceObject = new stdClass();
			//$sourceObject->file = "http://679868516.r.cdnsun.net/679868516/_definst_/stream_480p_khaleef/playlist.m3u8?token=".$access_token;
			$sourceObject->file = "http://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef/playlist.m3u8?token=".$access_token;
			$sourceObject->label = "480P";
			$sourceObject->type = "hls";
			$this->data["live_sources"][] = $sourceObject;
		} else {
			$sourceObject = new stdClass();
			//$sourceObject->file = "http://58445210.r.cdnsun.net/58445210/_definst_/stream_480p_khaleef/playlist.m3u8";
			$sourceObject->file = "http://924938945.r.cdnsun.net/924938945/_definst_/stream_480p_khaleef/playlist.m3u8";
			$sourceObject->label = "480P";
			$sourceObject->type = "hls";
			$this->data["live_sources"][] = $sourceObject;
		}

		$this->data["live_sources"] = json_encode($this->data["live_sources"]);
		*/


		$this->data["match_stream_id"] = $id;
		if (in_array($id, $this->data["sub_streams"])){
			$this->data["stream_allowed"] = 1;
		} else {
			$this->data["stream_allowed"] = 0;
		}
		$this->data["post_match_url"] = base_url()."match/".$id."/".$seokey."/";
		$this->data["remove_responsive"] = true;

		if ($this->input->get('old')) {
			# code...
			$this->data["page"] = $this->load->view('timeline/main', $this->data, true);
			$this->load->view('template', $this->data);
		}else{
			$this->data["timeline_page"] = 1;
			$this->data["page"] = $this->load->view('april19/timeline/main', $this->data, true);
			$this->load->view('april19/template', $this->data);
		}


	}

	function get_stream_token(){
		return '';
		$token = new TokenAuthGenerator();
		$token->setKey(CDN_SECRET_TOKEN);
		//$token->setKey("67bCE2bFigLOTWWf");
		$allowed_time = (5*60*60);
		$token->setExpire(time()+$allowed_time);

		//$allowed_domains = array('cric.sa.zain.com');
		//$allowed_domains[] = "*.jwplayer.com";
		//$token->setRefDeny(array('MISSING'));
		//$token->setRefAllow($allowed_domains);
		//echo $token->toString();
		return $token->toString();

	}

	function purchase_live_stream($match_id){

		$params = array();
		$params["msisdn"] = $this->data["phone_no"];
		$params["match_id"] = $match_id;
		$url = SUB_PATH."zain_reserve_amount?";
		$url = $url.http_build_query($params);
		$response = curl_call($url);
		$response = json_decode($response);

		if (!empty($response->status_code) && $response->status_code==1){
			$params = array();
			$params["msisdn"] = $this->data["phone_no"];
			$params["match_id"] = $match_id;
			$params["identifier"] = $response->identifier;
			$url = SUB_PATH."zain_charge_amount?";
			$url = $url.http_build_query($params);
			$response2 = curl_call($url);
			$response2 = json_decode($response2);
			if (!empty($response2->status_code) && $response2->status_code==1){
				echo json_encode(array("status"=>1,"message"=>"OK"));
				exit;
			} else {
				echo json_encode(array("status"=>0,"message"=>"Your Request Could not be processed right now, please try again later."));
				exit;
			}
		} else {
			echo json_encode(array("status"=>0,"message"=>"Please re-charge your account and try again later."));
			exit;
		}
	}

	function livestream($id=0, $detail=false){

		$this->check_session();
		//$this->update_purchased_streams();
		$this->check_subscription();

		$path = BACKEND."live_matches?telco=ufone";
		//$path = "http://staging.cric-live.net:8080/api/live_matches_temp";
		$response = curl_call($path);
		$response = json_decode($response);

		$this->data["stream_status"] = 0;
		$this->data["live"] = "";
		$teams = array();


		foreach($response->live_matches as $live){
			if ($live->id == $id && !empty($live->live_stream_url)){
				$live->live_stream_url = str_replace("https", "http", $live->live_stream_url);
			//if ($live->id==503){
				$this->data["live"] = $live;
				$this->data["stream_status"] = 1;
				$inning = $live->innings[0];
				if ($inning->batting_team_id == $live->team_1_id){
					$teams[$inning->batting_team_id] = $live->team_1->team;
					$teams[$inning->fielding_team_id] = $live->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $live->team_2->team;
					$teams[$inning->fielding_team_id] = $live->team_1->team;
				}
				$teams[$inning->batting_team_id]->flag_url =  $teams[$inning->batting_team_id]->full_flag_url;
				$teams[$inning->fielding_team_id]->flag_url = $teams[$inning->fielding_team_id]->full_flag_url;
				break;
			}else if($id == 0 && !empty($live->live_stream_url)){
				$live->live_stream_url = str_replace("https", "http", $live->live_stream_url);
			//if ($live->id==503){
				$this->data["live"] = $live;
				$this->data["stream_status"] = 1;
				$inning = $live->innings[0];
				if ($inning->batting_team_id == $live->team_1_id){
					$teams[$inning->batting_team_id] = $live->team_1->team;
					$teams[$inning->fielding_team_id] = $live->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $live->team_2->team;
					$teams[$inning->fielding_team_id] = $live->team_1->team;
				}
				$teams[$inning->batting_team_id]->flag_url =  $teams[$inning->batting_team_id]->full_flag_url;
				$teams[$inning->fielding_team_id]->flag_url = $teams[$inning->fielding_team_id]->full_flag_url;
				break;
			}
		}
		$this->data["teams"] = $teams;

		// $this->data["live"]->id = "9999";
		// $this->data["live"]->live_stream_url = "http://content.cricwick.net/inmatchclips/04f6739b6b35db9e0b5c051b321b5d50_1517295351.mp4";

		//$this->data["live"] = $response->recent_matches[0];
		if (empty($this->data["live"])){
			redirect(base_url());
			exit;
		}

		$this->data["match_stream_id"] = $this->data["live"]->id;
		$this->data["match_id"] = $this->data["live"]->id;
		/*
		if (in_array($this->data["match_stream_id"], $this->data["sub_streams"])){
			$this->data["stream_allowed"] = 1;
		} else {
			$this->data["stream_allowed"] = 0;
		}
		*/
		$this->data["stream_allowed"] = 1;

		$this->data["live_sources"] = array();
		$access_token = "";
		// $access_token = $this->get_stream_token();

		//$this->data["live_stream"] = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream";


		$this->data["live_stream"] = $this->data["live"]->live_stream_url;
		$useragent = $_SERVER["HTTP_USER_AGENT"];
		$pattern = '/Android/';
		$result = preg_match($pattern,$useragent);
		if (!$result){
			/*
			$sourceObject = new stdClass();
			//$sourceObject->file = "rtmp://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef?token=".$access_token;
			$sourceObject->file = $this->data["live"]->live_stream_url."?token=".$access_token;
			$sourceObject->label = "480P";
			$sourceObject->type = "rtmp";
			$this->data["live_sources"][] = $sourceObject;
			*/

			$sourceObject = new stdClass();
			//$sourceObject->file = "http://13335821.r.cdnsun.net/13335821/_definst_/stream_480p_khaleef/playlist.m3u8?token=".$access_token;
			$sourceObject->file = $this->data["live"]->live_stream_url;
			//$sourceObject->label = "480P";
			$sourceObject->type = "hls";
			$this->data["live_sources"][] = $sourceObject;
		} else {
			$sourceObject = new stdClass();
			//$sourceObject->file = "http://924938945.r.cdnsun.net/924938945/_definst_/stream_480p_khaleef/playlist.m3u8";
			$sourceObject->file = $this->data["live"]->live_stream_url;//."?token=".$access_token;
			//$sourceObject->label = "480P";
			//$sourceObject->type = "hls";
			$this->data["live_sources"][] = $sourceObject;
		}

		$this->data["live_sources"] = $this->data["live_sources"][0];
		$this->data["live_sources"] = json_encode($this->data["live_sources"]);
		/*
		echo "<pre>";
				print_r($this->data["live_sources"]);
				exit;*/


		$this->data["page"] = $this->load->view('april19/timeline/livestream', $this->data, true);
		$this->data["remove_responsive"] = true;
		$this->load->view('april19/template', $this->data);
	}

}
