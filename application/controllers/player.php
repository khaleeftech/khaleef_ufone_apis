<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Player extends My_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	function profile($id){
		
		$this->check_session();
		//$this->check_subscription();
		
		$url = API_PATH."get_player_profile?player_id=$id";
		$response = curl_call($url);
		$response = json_decode($response);
		
		$this->data["player"] = $response->profile;
		$server_constants = $this->memcached_library->get('cricwick_server_constants');
		$constants = array();
		foreach($server_constants->player_role_choices as $constant){
			$constants[$constant->key] = $constant->value;
		}
		foreach($server_constants->player_hand_choices as $constant){
			$constants[$constant->key] = $constant->value;
		}
		$this->data["constants"] = $constants;
		$this->data["inner_page"] = $this->load->view('player/profile', $this->data, true);
		$this->data["page_heading"] = "Profile";
		$this->data["banner_heading"] = "CricBoom";
		$this->data["banner_tagline"] = '&nbsp;';
		$this->data["banner_img_class"] = "back-img-t20";
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		
		$this->load->view('template', $this->data);
		
	}

	
}
