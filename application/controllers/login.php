<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public $data = array();
	function __construct(){
		parent::__construct();

		$this->load->library('session');

		$default_social = new stdClass();

		$default_social->title = "Ufone Cricket - Enjoy live cricket and watch key cricket moments";
		$default_social->descr = "Tune in to Ufon Cricket and never miss a cricket action again. Enjoy live and exclusive coverage of all cricket events. ";
		$default_social->image = assets_url()."assets/images/default_banner_icc.jpg";
		$this->data["og_tags"] = $default_social;

		//$this->lang->load("local","urdu");

		$set_lang = $this->input->get("lang");
		if ($set_lang && in_array($set_lang, array("english","urdu"))){
			$this->input->set_cookie("cric_lang",$set_lang,"604800");
			$this->lang->load('local', $set_lang);
			$this->session->set_userdata("language",$set_lang);
		} else {
			$language = $this->input->cookie('cric_lang');
			if ($language){
				$this->input->set_cookie("cric_lang",$language,"604800");
				$this->lang->load('local', $language);
				$this->session->set_userdata("language",$language);
			} else {
				$this->lang->load('local', 'english');
				$this->input->set_cookie("cric_lang","english","604800");
				$this->session->set_userdata("language","english");
			}
		}
		$this->data["response_status"] = 0;
		$this->data["response_message"] = lang('general_error');
		$this->data["response_default"] = json_encode(array("status"=> $this->data['response_status'], "message" => $this->data['response_message']));
	}


	function index(){

		/*
		$servers_constants = $this->memcached_library->get('cricwick_server_constants');
		if (!$servers_constants){
			$path = API_PATH."server_constants";
			$response = curl_call($path);
			$response = json_decode($response);
			$this->memcached_library->set('cricwick_server_constants',$response,604800);
		}
		*/

		$server = $_SERVER;
		if (!empty($server["HTTP_MSISDN"])){
			$header_number = $server["HTTP_MSISDN"];
			$header_number = "0".substr($header_number, 2);
			$url = SUB_PATH."find_sub_by_phone?web_user=1&telco=ufone&phone=".$header_number;
			$response = curl_call($url);
			$response = json_decode($response);
			if (!empty($response->user) && (in_array($response->user->status, array(1,2,3)) || $response->user->free_trial>0)){
				$this->session->set_userdata("user", $response->user);
				$this->data["phone_no"] = $response->user->phone;
				$this->data["udid"] = $response->user->udid;
				redirect(base_url());
				return;
			} else {
				$this->session->set_userdata("session_phone",$phone);
				redirect("login/step2");
				return;
			}
		}

		$this->data["pin_error"] = $this->session->userdata('pin_error');
		$this->session->set_userdata('pin_error', null);
		$this->data["login_page"] = 1;
		$this->data["phone_number"] = $this->session->userdata("login_phone");

		$this->data["page"] = $this->load->view('april19/login/enter_phone', $this->data, true);

		// $this->data["page"] = $this->load->view('login/new_screens/enter_phone', $this->data, true);
		$this->data["page_heading"] = "Login";
		$this->load->view('april19/template', $this->data);
		// $this->load->view('template', $this->data);

	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	function step2(){

		$phone = $this->session->userdata("session_phone");
		$subType = $this->session->userdata("session_subType");
		// echo $subType;exit;

		if (!empty($phone)){
			//$phone = international_number($phone);
			$url = SUB_PATH."find_sub_by_phone?web_user=1&telco=ufone&phone=".$phone;

			if(ENABLE_LOW_LOAD == 1){
				$response = curl_call($url);
				$response = json_decode($response);
				if(empty($response)){
					$demo = demo();
					$response = $demo;
					$response->user->phone = $phone;
					$response->user->udid = $phone;
				}

			}else{
				$response = curl_call($url);
				$response = json_decode($response);
				
			}


			if (!empty($response->user) && ($response->user->free_trial==1 || $response->user->status==1)){
				 // Comment this code and uncomment the last
				 // two lines of this IF in case you want to the user
				 // to submit the pin if he is already subscribed
				$this->session->set_userdata("user", $response->user);

				$ret_ur = $this->session->userdata("ret_url");
				if (empty($ret_ur)){
					$ret_ur = base_url();
				}
				$this->session->userdata("ret_url",NULL);
				$this->session->userdata("session_phone",NULL);
				redirect($ret_ur);
				return;

				//$this->data["page"] = $this->load->view('login/new_screens/enter_pin_login', $this->data, true);
				//$this->load->view('template', $this->data);
			} else {
				$params = array();
				$params["phone"] = $phone;
				$params["web_user"] = 1;
				$params["udid"] = $phone;
				$params["sub_type"] = 1;
				$params["telco"] = "ufone";
				$url = SUB_PATH."send_pin?".http_build_query($params);
				$response = json_decode(curl_call($url));
				if (empty($response->status) || $response->status != 1) {
					$this->session->set_userdata('pin_error', 'Please enter a valid Ufone number.');
					redirect(base_url()."login");
					exit;
				}
				$this->data["pin_phone"] = $phone;



				// $this->data["page"] = $this->load->view('login/new_screens/enter_pin_register', $this->data, true);
				// $this->load->view('template', $this->data);

				$this->data["login_page"] = 1;
				$this->data["page"] = $this->load->view('april19/login/enter_pin_register', $this->data, true);
				$this->load->view('april19/template', $this->data);
			}
		} else {
			redirect(base_url()."login");
			return;
		}
	}

	function validate_login_pin(){
		$phone = $this->session->userdata("session_phone");
		$array = array();
		$form = $this->input->post(NULL, true);
		$url = SUB_PATH."get_user_info?web_user=1&telco=ufone&phone=".$phone;
		$response = curl_call($url);
		$response = json_decode($response);
		if (!empty($response->user)){
			if ($response->user->pin==$form["pincode"]){
				$this->session->set_userdata("user", $response->user);
				$array["status"] = 1;
				$array['message'] = "OK";
				$ret_ur = $this->session->userdata("ret_url");
				if (!empty($ret_ur)){
					$array['url'] = $ret_ur;
				} else {
					$array['url'] = base_url();
				}
				$this->session->userdata("ret_url",NULL);
				$this->session->userdata("session_phone",NULL);
			} else {
				$array["status"] = 0;
				$array["message"] = lang("password_invalid");
			}
		} else {
			$array["status"] = 0;
			$array["message"] = lang("password_invalid");
		}
		echo json_encode($array);
	}
	//$url = "http://leafback.funworldpk.com/home/find_telco?phone=+".$phone;
	//$response = curl_call($url);
	function validate_phone_number(){
		$array = array();
		$phone = $this->input->post("phone");
		$subType = $this->input->post("radioB");
		// echo $subType;exit;
		if (is_valid_mobilink_number($phone)){
			$array["status"] = 1;
			$array["message"]  = "OK";
			$this->session->set_userdata("session_phone",$phone);
			$this->session->set_userdata("session_subType" ,$subType);
		} else {
			$array["status"] = 0;
			$array["message"]  = lang("invalid_phone_format");
		}
		echo json_encode($array);
	}


	function check_login(){
		$responseArray = array("status"=>0, "message"=>"", "clear_phone"=>false);
		$form = $this->input->post(NULL, true);
		if (empty($form["phone"]) || empty($form["password"])){
			$responseArray["message"] = lang("phone_password_requried");
		} else {
			if (is_valid_ksa_number($form["phone"])){

				$form["phone"] = international_number($form["phone"]);
				$url = SUB_PATH."get_user_info?web_user=1&telco=ufone&phone=".$form["phone"];

				$response = curl_call($url);
				$response = json_decode($response);
				if (!empty($response->user)){
					if ($response->user->pin==$form["password"]){
						$this->session->set_userdata("user", $response->user);
						$responseArray["status"] = 1;
						$responseArray['message'] = "OK";
						$ret_ur = $this->session->userdata("ret_url");
						if (!empty($ret_ur)){
							$responseArray['url'] = $ret_ur;
						} else {
							$responseArray['url'] = base_url();
						}
						$this->session->userdata("ret_url",NULL);
					} else {
						$responseArray["message"] = lang("password_invalid");
					}
				} else {
					$responseArray["message"] = lang("phone_not_registered");
					$responseArray["clear_phone"] = 1;
				}
			} else {
				$responseArray["message"] = lang("invalid_phone_format");
				$responseArray["clear_phone"] = 1;
			}
		}
		echo json_encode($responseArray);
	}

	function register_mobile(){
		$responseArray = array("status"=>0, "message"=>"");
		$form = $this->input->post(NULL, true);
		if (empty($form["register_phone"])){
			$responseArray["message"] = lang("phone_required");
		} else {
			if (is_valid_ksa_number($form["register_phone"])){

				$form["register_phone"] = international_number($form["register_phone"]);
				$url = SUB_PATH."get_user_info?web_user=1&telco=ufone&phone=".$form["register_phone"];
				$response = curl_call($url);
				$response = json_decode($response);
				if (empty($response->user)){
					$params = array();
					$params["phone"] = $form["register_phone"];
					//$params["udid"] = $form["register_phone"];
					$params["web_user"] = 1;
					$params["service_id"] = 65;
					$params["telco"] = "ufone";
					$url = SUB_PATH."send_pin_sms?".http_build_query($params);
					curl_call($url);
					$responseArray["message"] = lang("login_wd_sms_pin");
					$responseArray["status"] = 1;
					$this->session->set_userdata("login_phone", $form["register_phone"]);
				} else {
					$responseArray["message"] = lang("phone_already_registered");
				}
			} else {
				$responseArray["message"] = lang("invalid_phone_format");
			}
		}
		echo json_encode($responseArray);
	}

	public function reset_pin(){

		if ($this->input->is_ajax_request()){
			$responseArray = array("status"=>0, "message"=>"");
			$phone = $this->session->userdata("session_phone");

			$url = SUB_PATH."get_user_info?web_user=1&telco=ufone&phone=".$phone;
			$response = curl_call($url);
			$response = json_decode($response);
			if (!empty($response->user)){
				$params = array();
				$params["phone"] = $phone;
				$params["web_user"] = 1;
				$params["service_id"] = 65;
				$params["telco"] = "ufone";
				$url = SUB_PATH."forgot_pin?".http_build_query($params);
				curl_call($url);
				$responseArray["message"] = lang("login_wd_sms_pin");
				$responseArray["status"] = 1;
				$this->session->set_userdata("login_phone", $form["register_phone"]);
			} else {
				$responseArray["message"] = lang("phone_not_registered");
			}

			echo json_encode($responseArray);
			exit;
		}
	}

	public function forget_password(){

		if ($this->input->is_ajax_request()){
			$responseArray = array("status"=>0, "message"=>"");
			$form = $this->input->post(NULL, true);
			if (empty($form["register_phone"])){
				$responseArray["message"] = lang("phone_required");
			} else {
				if (is_valid_ksa_number($form["register_phone"])){

					$form["register_phone"] = international_number($form["register_phone"]);
					$url = SUB_PATH."get_user_info?web_user=1$telco=ufone&phone=".$form["register_phone"];
					$response = curl_call($url);
					$response = json_decode($response);
					if (!empty($response->user)){
						$params = array();
						$params["phone"] = $form["register_phone"];
						$params["web_user"] = 1;
						$params["service_id"] = 65;
						$params["telco"] = "ufone";
						$url = SUB_PATH."forgot_pin?".http_build_query($params);
						curl_call($url);
						$responseArray["message"] = lang("login_wd_sms_pin");
						$responseArray["status"] = 1;
						$this->session->set_userdata("login_phone", $form["register_phone"]);
					} else {
						$responseArray["message"] = lang("phone_not_registered");
					}
				} else {
					$responseArray["message"] = lang("invalid_phone_format");
				}
			}
			echo json_encode($responseArray);
			exit;
		}
		$this->data["page"] = $this->load->view('login/forget_password', $this->data, true);
		$this->data["page_heading"] = "Retrieve Password";
		$this->load->view('template', $this->data);
	}

	public function register_1(){
		$this->data["message"] = "Enter your mobile number we will send you a pin on sms";
		if ($this->input->post()){

			$form = $this->input->post(NULL, true);
			if (empty($form["register_phone"])){
				$this->data["message"] = lang("phone_required");
			} else {
				if (is_valid_ksa_number($form["register_phone"])){
					$form["register_phone"] = international_number($form["register_phone"]);
					$url = SUB_PATH."get_user_info?web_user=1&telco=ufone&phone=".$form["register_phone"];
					$response = curl_call($url);
					$response = json_decode($response);
					if (empty($response->user)){
						$params = array();
						$params["phone"] = $form["register_phone"];
						$params["web_user"] = 1;
						$params["service_id"] = 65;
						$params["telco"] = "ufone";
						$url = SUB_PATH."send_pin_sms?".http_build_query($params);
						curl_call($url);
						//$responseArray["message"] = lang("login_wd_sms_pin");
						//$responseArray["status"] = 1;
						$this->session->set_userdata("register_phone", $form["register_phone"]);
						redirect(base_url()."register-2");
					} else {
						$this->data["message"] = lang("phone_already_registered");
					}
				} else {
					$this->data["message"] = lang("invalid_phone_format");
				}
			}
		}

		$this->data["page"] = $this->load->view('login/regsiter_enter_phone', $this->data, true);
		$this->data["page_heading"] = "New User - Cricwick";
		$this->load->view('template', $this->data);
	}


	public function register_2(){
		$phone = $this->session->userdata("register_phone");
		if (!empty($phone)){
			$this->data["message"] = "Please confirm the pincode sent to you via sms.";
			$this->data["page"] = $this->load->view('login/register_enter_pincode', $this->data, true);
			$this->data["page_heading"] = "New User - Cricwick";
			$this->load->view('template', $this->data);
		} else {
			redirect(base_url()."register-1");
		}
	}

	function confirm_pin_register(){
		if ($this->input->is_ajax_request()){
			$phone = $this->session->userdata("session_phone");
			$subType = $this->session->userdata("session_subType");


			$responseArray = array("status"=>0, "message"=>"");
			$form = $this->input->post(NULL, true);
			if (empty($form["pincode"])){
				$responseArray["message"] = "Please enter the pincode!";
			} else {
				$params = array();
				$params["phone"] = $phone;
				$params["udid"] = $phone;
				$params["sub_type"] = 1;
				$params["web_user"] = 1;
				$params["telco"] = "ufone";
				$params["type"] = $subType;
				$params["pin"] = $form["pincode"];

				$url = SUB_PATH."confirm_pin_n_sub?".http_build_query($params);
				$response = curl_call($url);
				$response = json_decode($response);
				if (!empty($response->status)){
					sleep(5);

					$url = SUB_PATH."find_sub_by_phone?web_user=1&telco=ufone&phone=".$phone;
					$response = curl_call($url);
					$response = json_decode($response);
					$this->session->set_userdata("user", $response->user);
					$ret_ur = $this->session->userdata("ret_url");
					if (empty($ret_ur)){
						$ret_ur = base_url();
					}
					$this->session->userdata("ret_url",NULL);
					$this->session->userdata("session_phone",NULL);
					$responseArray = array("status"=>1, "url"=>$ret_ur);



				} else {
					if($response->msg){
						$responseArray["message"] = $response->msg;
					}else{
						$responseArray["message"] = lang("password_invalid");
					}
					$responseArray["rr"] = json_encode($response);
				}
			}
			echo json_encode($responseArray);
			exit;
		}
	}

	// $phone_number = str_replace('92', '0', $server["HTTP_MSISDN"]);
	// $phone_number = international_number($phone_number);
	function continue_subscription(){

		//if($this->input->is_ajax_request()){
			$visitor_id = $this->input->post('visitor_id');
			$agency = $this->input->post('agency');

			$server = $_SERVER;

			// $server["HTTP_MSISDN"] = "923314068410";
			// $server["HTTP_MSISDN"] = "923134970007";
			// $server["HTTP_MSISDN"] = "923134970006";
			//$server["HTTP_MSISDN"] = "923047292831";

			if (!empty($server["HTTP_MSISDN"]) && ( strlen($server["HTTP_MSISDN"]) == 12 ) ){
				$phone_number = "0".substr($server["HTTP_MSISDN"], 2); //str_replace('92', '0', $server["HTTP_MSISDN"]);
				$phone_number_0 = $phone_number;
				$params = array();
				//$params["udid"] = $this->data["phone_no"];
				$params["phone"] = $phone_number;
				$params["web_user"] = 1;
				$params["telco"] = "ufone";
				$url = SUB_PATH."find_sub_by_phone?";
				$url = $url.http_build_query($params);
				$response = curl_call($url);
				$response = json_decode($response);

				if ( isset($response->user) && !in_array($response->user->status, array('1','2','3') )  ) {
					//not sub
					$phone_number = "+".international_number($phone_number);

					$url = "http://mobisub.cricwick.mobi/incoming/sms?text=sub&telco=ufone&from=".$phone_number."&to=9876";
					$response = curl_call($url);

					if($response){
						$params["phone"] = $phone_number_0;
						$params["web_user"] = 1;
						$params["telco"] = "ufone";
						$url = SUB_PATH."find_sub_by_phone?";
						$url = $url.http_build_query($params);
						$response = curl_call($url);
						$response = json_decode($response);
						//
						if (isset($response->user) && ($response->user->status != '0') ){
						//
							if($visitor_id && $agency='billymob'){
								//only when user is new
								// $this->billymobi_postback($visitor_id);
							}

							$this->session->set_userdata("user", $response->user);
							echo json_encode(array("status"=>'2', "message"=>"ns"));
							return;
						}else{
							echo $this->data['response_default'];
							return;
						}
					}
				}elseif (!isset($response->user)) {
					# code...
					$phone_number = "+".international_number($phone_number);
					$url = "http://mobisub.cricwick.mobi/incoming/sms?text=sub&telco=ufone&from=".$phone_number."&to=9876";
					$response = curl_call($url);

					if($response){
						$params["phone"] = $phone_number_0;
						$params["web_user"] = 1;
						$params["telco"] = "ufone";
						$url = SUB_PATH."find_sub_by_phone?";
						$url = $url.http_build_query($params);
						$response = curl_call($url);
						$response = json_decode($response);
						//
						if (isset($response->user) && ($response->user->status != '0') ){
						//
							if($visitor_id && $agency='billymob'){
								$this->billymobi_postback($visitor_id);
							}

							$this->session->set_userdata("user", $response->user);
							echo json_encode(array("status"=>'2', "message"=>"ns"));
							return;
						}else{
							echo $this->data['response_default'];
							return;
						}
					}
				}else{
					//as
					$this->session->set_userdata("user", $response->user);
					echo json_encode(array("status"=>'1', "message"=>"as"));
					return;
				}

			}

			//}
			//echo $this->data['response_default'];

	}

	function continue_subscription_phone(){
		if($this->input->is_ajax_request()){
			$phone_number = $this->input->post('phone');

			if($phone_number){
				$phone_number = str_replace("+92", "0", $phone_number);
				// $phone_number = str_replace("92", "0", $phone_number);
				$phone_number_0 = $phone_number;


				if(is_valid_mobilink_number($phone_number)){
					//check telco
					$phone_number_92 = international_number($phone_number);
					$add = "http://vbox.pk:3456/?n=$phone_number_92";
					$add_r = json_decode(curl_call($add));

					if(!isset($add_r->telco) || ($add_r->telco->name != 'ufone')  ){
						echo json_encode(array("status"=>'0', "message"=>"Sorry! Only Ufone Users are allowed to access this service."));
						return;
					}



					$params["phone"] = $phone_number;
					$params["web_user"] = 1;
					$params["telco"] = "ufone";
					$url = SUB_PATH."find_sub_by_phone?";
					$url = $url.http_build_query($params);
					$response = curl_call($url);
					$response = json_decode($response);

					if ( isset($response->user) && !in_array($response->user->status, array('1','2','3') )  ) {
						$phone_number = "+".international_number($phone_number);
						$url = "http://mobisub.cricwick.mobi/incoming/sms?text=sub&telco=ufone&from=".$phone_number."&to=9876";
						$response = curl_call($url);

						if($response){
							$params["phone"] = $phone_number_0;
							$params["web_user"] = 1;
							$params["telco"] = "ufone";
							$url = SUB_PATH."find_sub_by_phone?";
							$url = $url.http_build_query($params);
							$response = curl_call($url);
							$response = json_decode($response);

							if($response){
								if (isset($response->user) && ($response->user->status != '0') ){
									$this->session->set_userdata("user", $response->user);
									echo json_encode(array("status"=>'1', "message"=>"ns"));
									return;
								}else{
									echo $this->data['response_default'];
									return;
								}
							}
						}
					}elseif (!isset($response->user)) {
						$phone_number = "+".international_number($phone_number);
						$url = "http://mobisub.cricwick.mobi/incoming/sms?text=sub&telco=ufone&from=".$phone_number."&to=9876";
						$response = curl_call($url);

						if($response){
							$params["phone"] = $phone_number_0;
							$params["web_user"] = 1;
							$params["telco"] = "ufone";
							$url = SUB_PATH."find_sub_by_phone?";
							$url = $url.http_build_query($params);
							$response = curl_call($url);
							$response = json_decode($response);

							if($response){
								if (isset($response->user) && ($response->user->status != '0') ){
									$this->session->set_userdata("user", $response->user);
									echo json_encode(array("status"=>'1', "message"=>"ns"));
									return;
								}else{
									echo $this->data['response_default'];
									return;
								}
							}
						}
					}else{
						$this->session->set_userdata("user", $response->user);
						echo json_encode(array("status"=>'1', "message"=>"as"));
						return;
					}





				}else{
					echo json_encode(array("status"=>'0', "message"=>lang('invalid_phone_format')));
					return;
				}
			}
			echo $this->data['response_default'];
		}
	}

	function billymobi_postback($visitor_id){
		if($visitor_id){
			$url = "http://trk.billysrv.com/conversion.php?amount=0.05&ccy=USD&visitor_id=".$visitor_id;
			$response = curl_call($url);
			//echo  json_encode(array("response"=>$response));
		}
	}

// PROMOTION FUNCTION PAK VS WI
function billymobi_postback2($visitor_id){
	if($visitor_id){
		$url = "http://trk.billysrv.com/conversion.php?amount=0.03&ccy=USD&visitor_id=".$visitor_id;
		$response = curl_call($url);
		//echo  json_encode(array("response"=>$response));
	}
}

function purchase_stream_pakvswi(){
	$match_id = $this->session->userdata('session_ls_match_id');
	$phone_number = $this->session->userdata('session_ls_phone');


	$params = array();
	//$params["udid"] = $this->data["phone_no"];
	$params["phone"] = $phone_number;
	$params["web_user"] = 1;
	$params["telco"] = "ufone";
	$url = SUB_PATH."find_sub_by_phone?";
	$url = $url.http_build_query($params);
	$response = curl_call($url);
	$response = json_decode($response);



	$purchase_stream = $this->purchase_stream($response, $match_id);
	$json["stream_purchased_status"] = $purchase_stream["status"];
	$json["stream_purchased_message"] = $purchase_stream["message"];


	if($json["stream_purchased_status"] == 1){
		redirect(base_url().'livestream');
	}else{
		redirect(base_url());
	}






}

function continue_subscription2(){

		//if($this->input->is_ajax_request()){
			$visitor_id = $this->input->post('visitor_id');
			$agency = $this->input->post('agency');
			$is_onsite_user = $this->session->userdata("session_onsite");

			$server = $_SERVER;

			// $server["HTTP_MSISDN"] = "923314068410";
			// $server["HTTP_MSISDN"] = "923134970007";
			// $server["HTTP_MSISDN"] = "923134970006";
			//$server["HTTP_MSISDN"] = "923047292831";
			// $server["HTTP_MSISDN"] = "923001111111";
			if (!empty($server["HTTP_MSISDN"]) && ( strlen($server["HTTP_MSISDN"]) == 12 ) ){
				$phone_number = "0".substr($server["HTTP_MSISDN"], 2); //str_replace('92', '0', $server["HTTP_MSISDN"]);
				$phone_number_0 = $phone_number;
				$params = array();
				//$params["udid"] = $this->data["phone_no"];
				$params["phone"] = $phone_number;
				$params["web_user"] = 1;
				$params["telco"] = "ufone";
				$url = SUB_PATH."find_sub_by_phone?";
				$url = $url.http_build_query($params);
				$response = curl_call($url);
				$response = json_decode($response);

				if ( isset($response->user) && !in_array($response->user->status, array('1','2','3') )  ) {
					//not sub
					$phone_number = "+".international_number($phone_number);

					$url = "http://mobisub.cricwick.mobi/incoming/sms?text=sub&telco=ufone&from=".$phone_number."&to=9876";
					$response = curl_call($url);

					if($response){
						$params["phone"] = $phone_number_0;
						$params["web_user"] = 1;
						$params["telco"] = "ufone";
						$url = SUB_PATH."find_sub_by_phone?";
						$url = $url.http_build_query($params);
						$response = curl_call($url);
						$response = json_decode($response);
						//
						if (isset($response->user) && ($response->user->status != '0') ){
						//
							// if($visitor_id && $agency='billymob'){
							// 	//only when user is new
							// 	// $this->billymobi_postback($visitor_id);
							// }


							$json = [];
							$json["status"] = 2;
							$json["message"] = "ns";
							$json["stream_purchased_status"] = 0;
							$json["stream_purchased_message"] = "";
							$json["postback_sent"] = 0;

							$purchase_stream = $this->purchase_stream($response);
							$json["stream_purchased_status"] = $purchase_stream["status"];
							$json["stream_purchased_message"] = $purchase_stream["message"];


							if($json["stream_purchased_status"] == 1){
								if($visitor_id && $agency='billymob'){
									$this->billymobi_postback2($visitor_id);
									$json["postback_sent"] = 1;
								}
							}


							$this->session->set_userdata("user", $response->user);
							echo json_encode(array("status"=>'2', "message"=>"ns"));
							return;
						}else{
							echo $this->data['response_default'];
							return;
						}
					}
				}elseif (!isset($response->user)) {
					# code...
					$phone_number = "+".international_number($phone_number);
					$url = "http://mobisub.cricwick.mobi/incoming/sms?text=sub&telco=ufone&from=".$phone_number."&to=9876";
					$response = curl_call($url);

					if($response){
						$params["phone"] = $phone_number_0;
						$params["web_user"] = 1;
						$params["telco"] = "ufone";
						$url = SUB_PATH."find_sub_by_phone?";
						$url = $url.http_build_query($params);
						$response = curl_call($url);
						$response = json_decode($response);
						//
						if (isset($response->user) && ($response->user->status != '0') ){
						//
							// if($visitor_id && $agency='billymob'){
							// 	$this->billymobi_postback($visitor_id);
							// }
							$json = [];
							$json["status"] = 2;
							$json["message"] = "ns";
							$json["stream_purchased_status"] = 0;
							$json["stream_purchased_message"] = "";
							$json["postback_sent"] = 0;

							$purchase_stream = $this->purchase_stream($response);
							$json["stream_purchased_status"] = $purchase_stream["status"];
							$json["stream_purchased_message"] = $purchase_stream["message"];


							if($json["stream_purchased_status"] == 1){
								if($visitor_id && $agency='billymob'){
									$this->billymobi_postback2($visitor_id);
									$json["postback_sent"] = 1;
								}
							}

							$this->session->set_userdata("user", $response->user);
							echo json_encode($json);
							return;
						}else{
							echo $this->data['response_default'];
							return;
						}
					}
				}else{
					//as
					$json = [];
					$json["status"] = 1;
					$json["message"] = "as";
					$json["stream_purchased_status"] = 0;
					$json["stream_purchased_message"] = "";
					$json["postback_sent"] = 0;

					$purchase_stream = $this->purchase_stream($response);
					$json["stream_purchased_status"] = $purchase_stream["status"];
					$json["stream_purchased_message"] = $purchase_stream["message"];


					if($json["stream_purchased_status"] == 1){
						if($visitor_id && $agency='billymob'){
							$this->billymobi_postback2($visitor_id);
							$json["postback_sent"] = 1;
						}
					}
					$this->session->set_userdata("user", $response->user);
					echo json_encode($json);
					return;
				}

			}

			//}
			//echo $this->data['response_default'];

	}//continue_subscription2


	function continue_subscription_phone2a(){
		if($this->input->is_ajax_request()){
			$phone_number = $this->input->post('phone');
			$agency = $this->session->userdata("session_agency");
			$visitor_id = $this->session->userdata("session_visitor_id");

			if($phone_number){
				$phone_number = str_replace("+92", "0", $phone_number);
				// $phone_number = str_replace("92", "0", $phone_number);
				$phone_number_0 = $phone_number;


				if(is_valid_mobilink_number($phone_number)){
					//check telco
					$phone_number_92 = international_number($phone_number);
					$add = "http://vbox.pk:3456/?n=$phone_number_92";
					$add_r = json_decode(curl_call($add));

					if(!isset($add_r->telco) || ($add_r->telco->name != 'Ufone')  ){
						echo json_encode(array("status"=>'0', "message"=>"Sorry! Only Ufone Users are allowed to access this service."));
						return;
					}



					$params["phone"] = $phone_number;
					$params["web_user"] = 1;
					$params["telco"] = "ufone";
					$url = SUB_PATH."find_sub_by_phone?";
					$url = $url.http_build_query($params);
					$response = curl_call($url);
					$response = json_decode($response);

					if (  !isset($response->user)  || ( isset($response->user) && !in_array($response->user->status, array('1','3')) ) ) {
						//send pin to existing unsubed user Or New user
						$params = array();
						$params["phone"] = $phone_number;
						$params["web_user"] = 1;
						$params["telco"] = "ufone";
						$params["udid"] = $phone_number;
						$url = SUB_PATH."send_pin?".http_build_query($params);
						$response = curl_call($url);
						$json = [];
						$json["status"] = 1;
						$json["message"] = "ps";
						$this->session->set_userdata("session_phone", $phone_number);
						echo json_encode($json);
						return;

					}else{
						//user already exist, is subscribed, purchase match
						$json = [];
						$json["status"] = 2;
						$json["message"] = "as";
						$json["stream_purchased_status"] = 0;
						$json["stream_purchased_message"] = "";
						$json["postback_sent"] = 0;

						$purchase_stream = $this->purchase_stream($response);
						$json["stream_purchased_status"] = $purchase_stream["status"];
						$json["stream_purchased_message"] = $purchase_stream["message"];


						if($json["stream_purchased_status"] == 1){
							if($visitor_id && $agency='billymob'){
								$this->billymobi_postback2($visitor_id);
								$json["postback_sent"] = 1;
							}
						}
						$this->session->set_userdata("user", $response->user);
						echo json_encode($json);
						return;
					}





				}else{
					echo json_encode(array("status"=>'0', "message"=>lang('invalid_phone_format')));
					return;
				}
			}
			echo $this->data['response_default'];
		}
	}


	function continue_subscription_phone2b(){
			if($this->input->is_ajax_request()){
				$pin = $this->input->post('pin');
				$agency = $this->session->userdata("session_agency");
				$visitor_id = $this->session->userdata("session_visitor_id");
				$phone = $this->session->userdata("session_phone");

				if (empty($pin)){
					echo json_encode(array( "status"=>'0', "message"=> "Please enter the pincode!" ));
					return;
				} else {
					$params = array();
					$params["phone"] = $phone;
					$params["udid"] = $phone;
					$params["telco"] = "ufone";
					$params["web_user"] = 1;
					$params["pin"] = $pin;

					$url = SUB_PATH."confirm_pin_n_sub?".http_build_query($params);
					$response = curl_call($url);
					$response = json_decode($response);
					if (!empty($response->status)){
						sleep(5);

						$url = SUB_PATH."find_sub_by_phone?web_user=1&telco=ufone&phone=".$phone;
						$response = curl_call($url);
						$response = json_decode($response);

						if(isset($response->user) && in_array($response->user->status, array(1,3))){
							$json = [];
							$json["status"] = 1;
							$json["message"] = "ns";
							$json["stream_purchased_status"] = 0;
							$json["stream_purchased_message"] = "";
							$json["postback_sent"] = 0;

							$purchase_stream = $this->purchase_stream($response);
							$json["stream_purchased_status"] = $purchase_stream["status"];
							$json["stream_purchased_message"] = $purchase_stream["message"];


							if($json["stream_purchased_status"] == 1){
								if($visitor_id && $agency='billymob'){
									$this->billymobi_postback2($visitor_id);
									$json["postback_sent"] = 1;
								}
							}

							$this->session->set_userdata("user", $response->user);
							echo json_encode($json);
							return;
						}else{
							//something went wrong may be a postpaid number

						}



					} else {
						//something went wrong
					}
				}




				echo $this->data['response_default'];
			}//is_ajax
		}

		function purchase_stream($find_sub_by_phone, $match_id='1447'){
			$response = [];
			$response["status"] = 0;
			$response["message"] = "stream already purchased";
			if(!in_array($match_id, $find_sub_by_phone->purchased_matches)){
				$phone = $find_sub_by_phone->user->phone;
				$url = "http://mobisub.cricwick.mobi/main/purchase_match_live_stream?match_id=$match_id&telco=ufone&phone=$phone";
				$resp = json_decode(curl_call($url));
				if($resp->status == '1'){
					$response["status"] = 1;
					$response["message"] = "stream purchased";
				}else{
					//tried to purchase but something went wrong
					$response["message"] = "Something went wrong";
				}
			}else{
				//already purchased
			}
			return $response;

		}


}
