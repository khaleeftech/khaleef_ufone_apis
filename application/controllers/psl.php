<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Psl extends My_Controller {

	public $data;
	function __construct(){
		parent::__construct();
		$summary_url = BACKEND."series_summary/233/summary";
		$summary_response = json_decode(curl_call($summary_url));
		$this->data["summary"] = $summary_response;
		// $this->data["series_title"] = $summary_response->recent_matches[0]->series->title;
		$this->data["series_title"] = "PAK-vs-NZ-2018";
	}



	function index($page_no=1){

		// $summary_url = BACKEND."series_summary/233/summary";
		// $summary_response = json_decode(curl_call($summary_url));
		$summary_response = $this->data['summary'];

		$this->data["recent_matches"] = $summary_response->recent_matches;
		$this->data["upcoming_matches"] = $summary_response->upcoming_matches;
		foreach ($summary_response->news as $keyN => $value) {
			if($keyN > 3){
				break;
			}
			$this->data["news"][] = $value;
		}

		// $this->data["recent_matches"][] =$this->data["upcoming_matches"][0];

		foreach($this->data["recent_matches"] as $key=>$result){

			$local = format_date_newserver2($result->match_start);
			//$local_time = $local['date']." ".$local["time"];
			$this->data["recent_matches"][$key]->local_time = $local;

			$seokey = $result->team_1->team->name." vs ".$result->team_2->team->name;
			$this->data["recent_matches"][$key]->seo_url = base_url()."match/$result->id/".seo_url($seokey)."/";

		}




		foreach($this->data["news"]  as $key=>$item){
			// $item->details = trimer($item->details, 400);

			// $item->created_at = date('F j, Y', $item->created_at/1000);
			$item->created_at = date('F j, Y', strtotime($item->created_at));
			// $d = explode(' ', $item->created_at)[0];
			// $item->created_at = $d;
			// $item->created_at = date('F j, Y', $d);



			$item->seo_url = base_url()."news-update/".$item->id."/".seo_url($item->title)."/";
			$response->news[$key] = $item;

			$item->stripped_body = substr(strip_tags($item->body),0, 180).'... ';
			$item->stripped_body_XS = substr(strip_tags($item->body),0, 233).'... ';

		}




		$this->data["points_table"] = $this->get_points_table();


		$this->data["page_heading"] = "PAK-vs-NZ-2018";


		$this->data["psl"] = true;
		$this->data["page"] = $this->load->view('april19/psl/psl_index', $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$this->data["new_video_page"] = true;
		$this->load->view('april19/template', $this->data);
	}

	function schedules($page_to_load = '1'){
		$result = "<h1>Schedules</h1>";

		$url = BACKEND."series_schedule/233/schedule";
		$response = json_decode(curl_call($url));


		$this->data["response"] = $response;

		$this->data["recent_matches"] = $response->recent_matches;
		$this->data["upcoming_matches"] = $response->upcoming_matches;
		$this->data["live_matches"] = $response->live_matches;


		// $this->data["recent_matches"] = $response->upcoming_matches;
		// $this->data["live_matches"] = $response->upcoming_matches;


		foreach($this->data["recent_matches"] as $key=>$result){

			$local = format_date_newserver2($result->match_start);
			//$local_time = $local['date']." ".$local["time"];
			$this->data["recent_matches"][$key]->local_time = $local;

			$seokey = $result->team_1->team->name." vs ".$result->team_2->team->name;
			$this->data["recent_matches"][$key]->seo_url = base_url()."match/$result->id/".seo_url($seokey)."/";

		}


		// if($this->input->is_ajax_request()){
			$result = $this->load->view('april19/psl/psl_schedules', $this->data, true);
			echo $result;
		// }else{
			// echo $result;
		// }
	}


	function pointstable($page_to_load = '1'){
		$response = $this->get_points_table();
		
		$this->data["points_table"] = $response;

		$result = $this->load->view('april19/psl/psl_points', $this->data, true);
		// if($this->input->is_ajax_request()){
			echo $result;
		// }else{
			// echo $result;
		// }
	}

	function get_points_table(){
		$url = BACKEND."series_points_table/233";
		$response = json_decode(curl_call($url));
		$response = $response->points_table;
		return $response;
	}

	function more_upcoming_matches($page_to_load = '1'){
		$url = BACKEND."series_scheduled_matches/233/scheduled_matches?page=$page_to_load&per_page=3";
		$response = json_decode(curl_call($url));

		$html = $this->load->view('april19/psl/psl_upcoming_matches', $response, true);
		echo $html;
	}
	function more_recent_matches($page_to_load = '1'){
		$url = BACKEND."series_recent_matches/233/recent_matches?page=$page_to_load&per_page=3";
		$response = json_decode(curl_call($url));

		$html = $this->load->view('april19/psl/psl_recent_matches', $response, true);
		echo $html;
	}

}
