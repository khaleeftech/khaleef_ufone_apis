<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Fantasy extends My_Controller {

	public $cricket_db = "";
    public $service = "";
    public $data = [];

	function __construct(){
		parent::__construct();

		$this->cricket_db = array("1","2");
		//1 Jazz
		//2 Ufone

		$this->service = array();
        // $this->service["65"]  = "Khaleef007";

        if(!in_array($this->input->get("partner_id"), array('1','14','8','12','13','2', '16', '15','17','18','10','11','19','22','24'))){
            $r = array("status"=>0, "message"=>"Something went wrong please try again later.");
            echo json_encode($r);
            exit;
        }
        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');
        $this->data['date'] = $date;
        $this->whitelistedMSISDNs = array("03314724218","03368444744", "03481154523","03494308420","03214807679","03178460142","03244079149","03003753669","03324744803","03004810052","03008481689", "03047292831");

        $this->paidCampaigns = array("paidpk");
        
    }

    function subscription_click($campaign='fantasy'){
        header("Access-Control-Allow-Origin: *");
        $params = $this->input->get();
        if (!in_array( $params["partner_id"], array(1,2,14,8,19, 10,11 ))) {
    
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }
        $token = $this->input->get("token");
        $msisdn = $this->input->get("msisdn");
        $partner_id = $this->input->get("partner_id");
        if($token){
            $param = array();
            $param["token"] = $token;
            $param["msisdn"] = $msisdn;
            $param["source"] = $partner_id;
            $param["sub_source"] = $params['utm_medium'];
            $param["sub_type"] = $this->input->get("sub_type")? $this->input->get("sub_type"):1;
            //$url = "http://3.126.71.59:9000/main/direct_sub_user_tp";
            $url = "http://mobisub.cricwick.mobi/main/direct_sub_user_tp";
            $response_header = post_curl($url, $param);

            // echo $response_header;
            // exit;
            $response_header = json_decode($response_header);
            if($response_header->status){
                $this->log_enriched_conversions($msisdn, $url, $response_header, $campaign);
                if (!in_array($campaign, $this->paidCampaigns) || (in_array($campaign, $this->paidCampaigns) && $response_header->is_charged)){
                    $resp["status"] = 1;
                    $resp["msisdn"] = @$response_header->msisdn;
                    $resp["msg"] = "User subscribed";
                } else {
                    $resp["status"] = 0;
                    $resp["msisdn"] = @$response_header->msisdn;
                    $resp["msg"] = "Insufficient Balance!";
                }
                
            }else{
                $resp["status"] = 0;
                $resp["msisdn"] = @$response_header->msisdn;
                $resp["msg"] = @$response_header->status_message;
            }
            echo json_encode($resp);
        }else{
            $resp["status"] = 0;
            $resp["msisdn"] = @$msisdn;
            $resp["msg"] = "Token not Found";
            echo json_encode($resp);
        }
        
    }

	function confirm_pin($campaign='fantasy'){
        header("Access-Control-Allow-Origin: *");
        $params = $this->input->get();
        if((empty(@$params["utm_source"])) && !in_array($params["partner_id"], array(1,8,2,17,22,24, 10,11))){
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }

        $msisdn = $params["msisdn"];
        $msisdn = normalize_pk_to_local($msisdn);

        $this->confirm_pin_mobisub($msisdn , $params["pincode"], $params["service_id"], $campaign);
    
	}

    function confirm_pin_jazz_he($campaign='fantasy'){
        header("Access-Control-Allow-Origin: *");
        $params = $this->input->get();
        if((empty(@$params["utm_source"])) && !in_array( $params["partner_id"], array(1,2))){
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }

        if (!in_array( $params["partner_id"], array(1,2,14,8,19,10,11))) {
    
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }

            
        if ($params["service_id"]=='1'){
            $this->confirm_jazz_he($params["msisdn"], $params["pincode"],$campaign);			
        }

	}

    function send_pin($campaign='fantasy'){
        header("Access-Control-Allow-Origin: *");
        $params = $this->input->get();

        $msisdn = $params["msisdn"];
        $msisdn = normalize_pk_to_local($msisdn);

        if((empty(@$params["utm_source"])) && !in_array($params["partner_id"], array(1,8,2,17,22,24)) ){
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }
		// $params = $this->input->get();
		if (in_array($params["service_id"], $this->cricket_db)){
    
            $already_sub = $this->check_user_status_cricket($msisdn , $params["service_id"]);
   
			if (!in_array($already_sub, array('0','2'))){
                $telco = $this->getTelcoFromUser($already_sub);
				$r = array("status"=>0, "message"=>"Already Sub", "telco" => $telco);
				echo json_encode($r);
				exit;
			// } else if($already_sub == 2) {
            //     $telco = $this->getTelcoFromUser($already_sub);
			// 	$r = array("status"=>0, "message"=>"User blacklisted", "telco" => $telco);
			// 	echo json_encode($r);
			// 	exit;
			} else {
                if (empty($params["telco"])){
                    $msisdn_international = international_number($msisdn);
                    $params["telco"] = $this->get_telco($msisdn_international);
                }
                if($params['telco'] != "telenor" && $campaign=='mbl'){  // Only Telenor is allowed for MBL Campaign
                    $r = array("status"=>0, "message"=>"Operator Not Allowed", "telco" => $params['telco']);
                    echo json_encode($r);
                    exit;
                }
                if( in_array($campaign,array("gago","psl_inapp")) && !in_array($params['telco'], array("mobilink","warid")) ){  // Only Jazz is allowed for Gago
                    $r = array("status"=>0, "message"=>"Operator Not Allowed", "telco" => $params['telco']);
                    echo json_encode($r);
                    exit;
                }
				$this->send_pin_mobisub($msisdn, $params["telco"], $params["service_id"],$campaign);

			}

		}
	}

    function check_user_status_cricket($msisdn, $service_id){
        $params = [];
        $params["phone"] = $msisdn;
        $params["web_user"] = 1;
        if($service_id == 2){
            $params['telco'] = 'ufone';
        }
        $url = "http://3.126.71.59:9000/main/find_sub_by_phone?";
        $url = $url.http_build_query($params);
        $response = curl_call($url);
        $response = json_decode($response);
        if( isset($response->user->status) && (in_array($response->user->status, array('1','2')))){
            return $response->user;
        }else if( isset($response->user->status) && $response->user->status == 0 && 
        (date_create($response->user->updated_at)->diff(date_create())->days >= 45  && !in_array($msisdn, $this->whitelistedMSISDNs)  && $response->user->unsub_reason != null) ){
            return 2;
        }else{
            return 0;
        }
    
    
    }


    function check_user_status_cricket_he($msisdn, $service_id){
        $params = [];
            $params["phone"] = $msisdn;
            $params["web_user"] = 1;
            if($service_id == 2){
                $params['telco'] = 'ufone';
            }
            $url = "http://3.126.71.59:9000/main/find_sub_by_phone?";
            $url = $url.http_build_query($params);
            $response = curl_call($url);
            $response = json_decode($response);
            if( isset($response->user->status) && (in_array($response->user->status, array('1','2')))){
                return $response->user;
            }else if( isset($response->user->status) && $response->user->status == 0 && 
            (date_create($response->user->updated_at)->diff(date_create())->days >= 45  && !in_array($msisdn, $this->whitelistedMSISDNs)  && $response->user->unsub_reason != null) ){
                return $response->user;
            }else{
                return $response->user;
            }
    }
    
    function get_telco($msisdn){
        //$url = "http://185.152.65.139:3456/find-telco?n=".$msisdn;
        $url = "http://knect.khaleef.com:9001/api/subscriptions/checkPkOperator?msisdn=".$msisdn;
        $response = curl_call($url);
        $response = json_decode($response);
        if($response->status){
            return $response->telco->name;
        }
	}


	function confirm_jazz_he($msisdn, $pincode, $campaign){
        $user = $this->check_user_status_cricket_he($msisdn, $this->input->get("service_id"));
        $pincode = $user->pin;
        $telco = $this->getTelcoFromUser($user);
        $params = array();
        $params["phone"] = $msisdn;
        $params["udid"] = $msisdn;
        $params["web_user"] = 1;
        $params["pin"] = $pincode;
        $params["sub_source"] = $this->input->get("utm_medium");
        $params["source"] = $this->input->get("partner_id");
        $params["sub_type"] = $this->input->get("sub_type")? $this->input->get("sub_type"):1;

        $agency = $this->input->get("partner_id");
        $partner_id = $this->input->get("partner_id");
     

        $url = "http://3.126.71.59:9000/main/confirm_pin_n_sub?".http_build_query($params);
        $response = curl_call($url);
        $response = json_decode($response);
        if (!empty($response->status) ){
          
            $this->load->database();

            $responseToLog = $response;
            unset($responseToLog->subscription_phone);

            $dataToInsert = array();    
            $dataToInsert["product"] = $telco."_".$params["sub_type"];
            $dataToInsert["product_id"] = 1;
            $dataToInsert["created_at"] = $this->data['date'];
            $dataToInsert["agency"] = $agency;
            $dataToInsert["phone"] = $msisdn;
            $dataToInsert["request"] = $url;
            $dataToInsert["response"] = json_encode($responseToLog);
            $dataToInsert["trans_type"] = 2;
            $dataToInsert["partner_id"] = $partner_id;
            $dataToInsert["is_charged"] = ($response->is_charged)?1:0;
            $dataToInsert["is_subscribed"] = 1;
            $dataToInsert["campaign"] = $campaign;

            if($telco!="zong"){
                $this->db->insert("acquisitions_he_new",$dataToInsert);
            }
            if (!in_array($campaign, $this->paidCampaigns) || (in_array($campaign, $this->paidCampaigns) && $response->is_charged)){
                $r = array("status"=>1, "message"=>"OK", "telco" => $telco);
                echo json_encode($r);
            } else {
                $r = array("status"=>0, "message"=>"Insufficient Balance!", "telco" => $telco);
                echo json_encode($r);
            }
			exit;
        }else{
            $r = array("status"=>0, "message"=>$response->msg);
			echo json_encode($r);
			exit;
        }

	}

    function send_pin_mobisub($msisdn, $telco, $service_id, $campaign){
        $params = array();
        $params["phone"] = $msisdn;
        $params["web_user"] = 1;
        $params["udid"] = $msisdn;
        $params["cpa_telco"] = $telco;
        $params["sub_source"] = $this->input->get("utm_medium");
        $params["source"] = $this->input->get("partner_id");
        $params["sub_type"] = $this->input->get("sub_type")? $this->input->get("sub_type"):1;
        $dataToInsert = array();    
        $dataToInsert["product_id"] = 1;

        if ($service_id==2){
            $params["sub_type"] = "daily";
            $params["telco"] = "ufone";
            $dataToInsert["product_id"] = 2;
        }

 
        $agency = $this->input->get("partner_id");
        $partner_id = $this->input->get("partner_id");

        $url = "http://3.126.71.59:9000/main/send_pin?";
        $url = $url.http_build_query($params);
        $response = json_decode(curl_call($url));
        $this->load->database();

       
        $dataToInsert["product"] = $telco."_".$params["sub_type"] ;
        $dataToInsert["created_at"] = $this->data['date'];
        $dataToInsert["agency"] = $agency;
        $dataToInsert["phone"] = $msisdn;
        $dataToInsert["request"] = $url;
        $dataToInsert["response"] = json_encode($response);
        $dataToInsert["trans_type"] = 1;
        $dataToInsert["partner_id"] = $partner_id;
        $dataToInsert["is_charged"] = 0;
        $dataToInsert["is_subscribed"] = 0;
        $dataToInsert["campaign"] = $campaign;

        $this->db->insert("acquisitions_new",$dataToInsert);

        $r = array("status"=>1, "message"=>"OK");
        echo json_encode($r);
        exit;

    }

	function confirm_pin_mobisub($msisdn, $pincode, $service_id, $campaign){

        $params = array();
        $params["phone"] = $msisdn;
        $params["udid"] = $msisdn;
        $params["web_user"] = 1;
        $params["pin"] = $pincode;
        $params["sub_source"] = $this->input->get("utm_medium");
        $params["source"] = $this->input->get("partner_id");
        $params["sub_type"] = $this->input->get("sub_type")? $this->input->get("sub_type"):1;
        $agency = $this->input->get("partner_id");
        $partner_id = $this->input->get("partner_id");

        $dataToInsert = array();    
        $dataToInsert["product_id"] = 1;

        if ($service_id==2){
            $params["sub_type"] = "daily";
            $params["telco"] = "ufone";
            $dataToInsert["product_id"] = 2;
        }

        $url = "http://3.126.71.59:9000/main/confirm_pin_n_sub?".http_build_query($params);
        $response = curl_call($url);
        $response = json_decode($response);
        // $response = new stdClass();
        // $url = "";
        // echo json_encode($response);
        // exit;
        if (!empty($response->status) ){
            $telco = $this->getTelcoFromUser($response->user);
            // $telco = "warid";
            $this->load->database();

            $responseToLog = $response;
            unset($responseToLog->subscription_phone);

            $dataToInsert["product"] = $telco."_".$params["sub_type"];
            $dataToInsert["created_at"] = $this->data['date'];
            $dataToInsert["agency"] = $agency;
            $dataToInsert["phone"] = $msisdn;
            $dataToInsert["request"] = $url;
            $dataToInsert["response"] = json_encode($responseToLog);
            $dataToInsert["trans_type"] = 2;
            $dataToInsert["partner_id"] = $partner_id;
            $dataToInsert["is_charged"] = ($response->is_charged)?1:0;
            $dataToInsert["is_subscribed"] = 1;
            $dataToInsert["campaign"] = $campaign;

            $is_blocking = false;

            if ($campaign == 'testgago' || 1){
                $is_blocking = $this->isBlocking($partner_id, $telco, $campaign, 1, $params["sub_type"]); // 1 for pin flow 
            }

            $dataToInsert["is_blocked"] = $is_blocking?1:0;
            // print_r($dataToInsert);
            // exit;
            if($telco!="zong"){
                //$this->db->query($sql, array($msisdn, $url, json_encode($response)));
                $this->db->insert("acquisitions_new",$dataToInsert);
            }

            if (!in_array($campaign, $this->paidCampaigns) || (in_array($campaign, $this->paidCampaigns) && $response->is_charged)){

                $r = array("status"=>1, "message"=>"OK", "telco" => $telco);
                if ($is_blocking ){
                    $err_msg = $this->getBlockingErrorMessage();
                    $r = array("status"=>0, "message"=> $err_msg , "telco" => $telco);
                }   
    			echo json_encode($r);

            } else {

                $r = array("status"=>0, "message"=>"Insufficient Balance!", "telco" => $telco);
                echo json_encode($r);

            }
			exit;

        } else {
            $resp_msg = isset($response->msg)? @$response->msg: @$response->status_message;

            $r = array("status"=>0, "message"=>$resp_msg );
			echo json_encode($r);
			exit;
        }

	}


	function post_curl($url, $params){
		$curl = curl_init($url);
		//curl_setopt($curl,CURLOPT_HEADER, 0 );
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
		$response = curl_exec($curl);
		curl_close ( $curl );
		return $response;
    }

    function getTelcoFromUser($user){
        $telco_id = $user->telco_id;
        switch ($telco_id) {
            case '8':
                return 'ufone';
                break;

            case '7':
                return 'telenor';
                break;

            case '6':
                return 'warid';
                break;

            case '5':
                return 'mobilink';
                break;

            case '4':
                return 'zong';
                break;
            
            default:
                return '';
                break;
        }
    }

    function log_enriched_conversions($msisdn, $url, $response_header, $campaign){
        $this->db = $this->load->database("default", true);
        $date = date("Y-m-d H:i:s");

        $agency = $this->input->get("partner_id");
        $partner_id = $this->input->get("partner_id");
        $sub_type = $this->input->get("sub_type")? $this->input->get("sub_type"):1;

        $responseToLog = $response_header;
        unset($responseToLog->subscription_phone);

        $dataToInsert = array();    
        $dataToInsert["product"] = "telenor"."_".$sub_type;
        $dataToInsert["product_id"] = 1;
        $dataToInsert["created_at"] = $this->data['date'];
        $dataToInsert["agency"] = $agency;
        $dataToInsert["phone"] = $msisdn;
        $dataToInsert["request"] = $url;
        $dataToInsert["response"] = json_encode($responseToLog);
        $dataToInsert["trans_type"] = 2;
        $dataToInsert["partner_id"] = $partner_id;
        $dataToInsert["is_charged"] = ($response_header->is_charged)?1:0;
        $dataToInsert["is_subscribed"] = 1;
        $dataToInsert["campaign"] = $campaign;

        $this->db->insert("acquisitions_he_new",$dataToInsert);
    }


    function lookup($campaign='fantasy'){
        //header("Access-Control-Allow-Origin: *");
        $params = $this->input->get();
        // if((empty(@$params["utm_source"])) && !in_array($params["partner_id"], array(1,8,2)) ){
        //     $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
        //     echo json_encode($r);
        //     exit;
        // }
		// $params = $this->input->get();
		//if (in_array($params["service_id"], $this->cricket_db)){
    
        $already_sub = $this->check_user_status_cricket($params["msisdn"], $params["service_id"]);
        $msisdn = $params['msisdn'];
        $msisdn = international_number($msisdn);
        //}
        if (!in_array($already_sub, array('0','2'))){
            echo "ACTIVE";
            exit;
        // } else if($already_sub == 2) {
        //     echo "BLACKLISTED";
        //     exit;
        } else {
            echo "INACTIVE";
            exit;
        }
    }

}

