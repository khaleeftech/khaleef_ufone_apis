<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require 'application/controllers/My_Controller.php';

class Cricwick extends CI_Controller {

	public $cricket_db = "";
    public $service = "";
    public $data = [];

	function __construct(){
		parent::__construct();

		$this->cricket_db = array("1","2");
		//1 Jazz
		//2 Ufone

		$this->service = array();
        // $this->service["65"]  = "Khaleef007";

        if(!in_array($this->input->get("partner_id"), array('1','14', '11'))){
            $r = array("status"=>0, "message"=>"Something went wrong please try again later.");
            echo json_encode($r);
            exit;
        }
        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');
        $this->data['date'] = $date;
        $this->whitelistedMSISDNs = array("03368444744", "03481154523");
        
    }

	function send_pin_bk(){

        $params = $this->input->get();
        if((empty(@$params["utm_source"])) && $params["partner_id"] != 1){
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }
		if (in_array($params["service_id"], $this->cricket_db)){
    
            $already_sub = $this->check_user_status_cricket($params["msisdn"], $params["service_id"]);
            $msisdn = $params['msisdn'];
            $msisdn = international_number($msisdn);
            $params['telco'] = $this->get_telco($msisdn);
            if($params['telco'] != "telenor"){
                $r = array("status"=>0, "message"=>"Invalid telco", "telco" => $params['telco']);
				echo json_encode($r);
				exit;
            }
			if (!in_array($already_sub, array('0','2'))){
                $telco = $this->getTelcoFromUser($already_sub);
				$r = array("status"=>0, "message"=>"Already Sub", "telco" => $telco);
				echo json_encode($r);
				exit;
			} else if($already_sub == 2) {
                $telco = $this->getTelcoFromUser($already_sub);
				$r = array("status"=>0, "message"=>"User blacklisted", "telco" => $telco);
				echo json_encode($r);
				exit;
			} else {

				if ($params["service_id"] =='1'){
					$this->send_pin_jazz($params["msisdn"], $params["telco"]);
				} else if ($params["service_id"] == '2'){
					$this->send_pin_ufone($params["msisdn"], $params["telco"]);
				}

			}

		}
	}

	function confirm_pin_bk(){

        $params = $this->input->get();
        if((empty(@$params["utm_source"])) && $params["partner_id"] != 1){
            $r = array("status"=>0, "message"=>"Request not from verified resource", "telco" => $params['telco']);
            echo json_encode($r);
            exit;
        }
        // $already_sub = $this->check_user_status_cricket($params["msisdn"], $params["service_id"]);

        // if($already_sub){
        //     $telco = $this->getTelcoFromUser($already_sub);
        //     $r = array("status"=>0, "message"=>"Already Sub", "telco" => $telco);
		// 	echo json_encode($r);
        // }else{

            
            if ($params["service_id"]=='1'){
                $this->confirm_jazz($params["msisdn"], $params["pincode"]);			
            } elseif ($params["service_id"] =='2'){
                $this->confirm_ufone($params["msisdn"], $params["pincode"]);			
            }

        // }
	}

	// function check_user_status_cricket($msisdn, $service_id){
 //        $this->current = $this->load->database('jazz/ufone', TRUE);
 //          if($msisdn){
 //              $sql = "select * from subscriptions where phone = $msisdn";
 //              $query = $this->current->query($sql);
 //              $response = $query->result_array();
 //              if($response){
 //                $response = $response[0];
 //              }
 //              if(!empty($response) && ($response["status"] != "0")){
 //                return $response;
 //              }else{
 //                return false;
 //              }
 //            }else{
 //              return 0;
 //            } 

 //    }

 function check_user_status_cricket($msisdn, $service_id){
        $params = [];
        $params["phone"] = $msisdn;
        $params["web_user"] = 1;
        if($service_id == 2){
            $params['telco'] = 'ufone';
        }
        $url = "http://3.126.71.59:9000/main/find_sub_by_phone?";
        $url = $url.http_build_query($params);
        $response = curl_call($url);
        $response = json_decode($response);
        if( isset($response->user->status) && (in_array($response->user->status, array('1','2')))){
            return $response->user;
        }else if( isset($response->user->status) && $response->user->status == 0 && 
        (date_create($response->user->updated_at)->diff(date_create())->days >= 45  && !in_array($msisdn, $this->whitelistedMSISDNs)  && $response->user->unsub_reason != null) ){
            return 2;
        }else{
            return 0;
        }
    

        // $url = "http://3.126.71.59:9000/main/find_sub_by_phone?web_user=1&telco=ufone&phone=".$msisdn;
        // $response = curl_call($url);
        // $response = json_decode($response);
        // if (!empty($response->user) && ($response->user->free_trial==1 || $response->user->status==1)){
        //     return $response->user;
        // }else{
        //     return 0;
        // }

}
    
    function get_telco($msisdn){
            $url = "http://vbox.pk:3456/find-telco?n=".$msisdn;
            $response = curl_call($url);
            $response = json_decode($response);
            if($response->status){
                return $response->telco->name;
            }
	}


	function send_pin_jazz($msisdn, $telco){
        $params = array();
        $params["phone"] = $msisdn;
        $params["web_user"] = 1;
        $params["udid"] = $msisdn;
        $params["cpa_telco"] = $telco;
        $params["sub_source"] = $this->input->get("utm_medium");
        $params["source"] = $this->input->get("partner_id");
        $agency = "";
        if($this->input->get("partner_id") == 1){
            $agency = "mbl_thomas";
        }else if($this->input->get("partner_id") == 8){
            $agency = "mbl_GLOBOCOM";
        }else if($this->input->get("partner_id") == 9){
            $agency = "mbl_GULFTECH";
        }else if($this->input->get("partner_id") == 10){
            $agency = "mbl_Tracking Company";
        }else if($this->input->get("partner_id") == 11){
            $agency = "mbl_MOBIPIUM";
        }else if($this->input->get("partner_id") == 12){
            $agency = "mbl_Witskies";
        }else if($this->input->get("partner_id") == 13){
            $agency = "mbl_MOBIKOK";
        }else if($this->input->get("partner_id") == 14){
            $agency = "mbl_SMADEX";
        }else if($this->input->get("partner_id") == 15){
            $agency = "mbl_DKDIGITAL";
        }

        $url = "http://3.126.71.59:9000/main/send_pin?";
        $url = $url.http_build_query($params);
        $response = json_decode(curl_call($url));
        $this->load->database();
        $sql = "insert into acquisitions (product, product_id, created_at, agency, phone, request, response, trans_type) values ('telenor', '1', '{$this->data['date']}', '{$agency}', ?, ?, ?, 1)";
        $this->db->query($sql, array($msisdn, $url, json_encode($response)));

        $r = array("status"=>1, "message"=>"OK");
        echo json_encode($r);
        exit;

    }

    function send_pin_ufone($msisdn, $telco){
        $params = array();
        $params["phone"] = $msisdn;
        $params["web_user"] = 1;
        $params["udid"] = $msisdn;
        $params["sub_type"] = "daily";
        $params["telco"] = "ufone";
        $params["cpa_telco"] = $telco;
        
        $url = "http://3.126.71.59:9000/main/send_pin?".http_build_query($params);
        $response = json_decode(curl_call($url));
        if (empty($response->status) || $response->status != 1) {
            $r = array("status"=>0, "message"=>$response->message);

            $this->load->database();
            $sql = "insert into acquisitions (product, product_id, created_at, agency, phone, request, response, trans_type) values ('ufone', '2', '{$this->data['date']}', 'thomas', ?, ?, ?, 1)";
            $this->db->query($sql, array($msisdn, $url, json_encode($response)));

			echo json_encode($r);
			exit;
        }else{
            $r = array("status"=>1, "message"=>"OK");
            echo json_encode($r);
            exit;
        }

    }

	function confirm_jazz($msisdn, $pincode){

        $params = array();
        $params["phone"] = $msisdn;
        $params["udid"] = $msisdn;
        $params["web_user"] = 1;
        $params["pin"] = $pincode;
        $params["sub_source"] = $this->input->get("utm_medium");
        $params["source"] = $this->input->get("partner_id");
        $agency = "mbl_thomas";
        if($this->input->get("partner_id") == 1){
            $agency = "mbl_thomas";
        }else if($this->input->get("partner_id") == 8){
            $agency = "mbl_GLOBOCOM";
        }else if($this->input->get("partner_id") == 9){
            $agency = "mbl_GULFTECH";
        }else if($this->input->get("partner_id") == 10){
            $agency = "mbl_Tracking Company";
        }else if($this->input->get("partner_id") == 11){
            $agency = "mbl_MOBIPIUM";
        }else if($this->input->get("partner_id") == 12){
            $agency = "mbl_Witskies";
        }else if($this->input->get("partner_id") == 13){
            $agency = "mbl_MOBIKOK";
        }else if($this->input->get("partner_id") == 14){
            $agency = "mbl_SMADEX";
        }else if($this->input->get("partner_id") == 15){
            $agency = "mbl_DKDIGITAL";
        }
        $url = "http://3.126.71.59:9000/main/confirm_pin_n_sub?".http_build_query($params);
        $response = curl_call($url);
        $response = json_decode($response);
        if (!empty($response->status) ){
            //log acquistion from thomas to db 
            $this->load->database();
            $sql = "insert into acquisitions (product, product_id, created_at, agency, phone, request, response, trans_type) values ('telenor', '1', '{$this->data['date']}', '{$agency}', ?, ?, ?, 2)";

            $telco = $this->getTelcoFromUser($response->user);
            if($telco!="zong"){
                $this->db->query($sql, array($msisdn, $url, json_encode($response)));
            }

            $r = array("status"=>1, "message"=>"OK", "telco" => $telco);
			echo json_encode($r);
			exit;
        }else{
            $r = array("status"=>0, "message"=>$response->msg);
			echo json_encode($r);
			exit;
        }

	}

	function confirm_ufone($msisdn, $pincode){

        $params = array();
        $params["phone"] = $msisdn;
        $params["udid"] = $msisdn;
        $params["sub_type"] = "daily";
        $params["web_user"] = 1;
        $params["telco"] = "ufone";
        $params["type"] = $subType;
        $params["pin"] = $pincode;

        $url = "http://3.126.71.59:9000/main/confirm_pin_n_sub?".http_build_query($params);
        $response = curl_call($url);
        $response = json_decode($response);
        if ( !empty($response->status) ){
            //log acquisition to db 
            $this->load->database();
            $sql = "insert into acquisitions (product, product_id, created_at, agency, phone, request, response, trans_type) values ('ufone', '2', '{$this->data['date']}', 'thomas', ?, ?, ?, 2)";
            $this->db->query($sql, array($msisdn, $url, json_encode($response)));
            

            $telco = $this->getTelcoFromUser($response->user);
            $r = array("status"=>1, "message"=>"OK", "telco" => $telco);
			echo json_encode($r);
			exit;
        }else{
            $r = array("status"=>0, "message"=>$response->msg);
			echo json_encode($r);
			exit;
        }

	}

	function post_curl($url, $params){
		$curl = curl_init($url);
		//curl_setopt($curl,CURLOPT_HEADER, 0 );
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
		$response = curl_exec($curl);
		curl_close ( $curl );
		return $response;
    }

    function getTelcoFromUser($user){
        $telco_id = $user->telco_id;
        switch ($telco_id) {
            case '8':
                return 'ufone';
                break;

            case '7':
                return 'telenor';
                break;

            case '6':
                return 'warid';
                break;

            case '5':
                return 'mobilink';
                break;

            case '4':
                return 'zong';
                break;
            
            default:
                return '';
                break;
        }
    }
    

}

