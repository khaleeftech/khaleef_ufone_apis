<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require 'application/controllers/My_Controller.php';

class Video extends My_Controller {

	function __construct(){
		parent::__construct();
	}

	function index_old($page_no=1, $rising_star=0){

		$this->check_session();
		$this->check_subscription();

		$special_videos = $this->more_filter_videos("special_videos");
		$featured = $this->more_filter_videos("featured_videos");
		$most_viewed = $this->more_filter_videos("most_viewed_videos");
		$latest = $this->more_filter_videos("latest_videos");

		$dataArray = array();
		$filterArray = new stdClass();
		$filterArray->label = "Featured";
		$filterArray->filter = "featured_videos";
		$filterArray->container_id = "featuredVideosContainer";
		$filterArray->items = $featured->timeline_videos;
		$filterArray->current_page = $featured->current_page;
		$filterArray->total_pages = $featured->total_pages;
		$dataArray[] = $filterArray;

		$filterArray = new stdClass();
		$filterArray->label = "Latest";
		$filterArray->filter = "latest_videos";
		$filterArray->container_id = "latestVideosContainer";
		$filterArray->items = $latest->timeline_videos;
		$filterArray->current_page = $latest->current_page;
		$filterArray->total_pages = $latest->total_pages;
		$dataArray[] = $filterArray;

		$filterArray = new stdClass();
		$filterArray->label = "Most Viewed";
		$filterArray->filter = "most_viewed_videos";
		$filterArray->container_id = "mostViewedVideosContainer";
		$filterArray->items = $most_viewed->timeline_videos;
		$filterArray->current_page = $most_viewed->current_page;
		$filterArray->total_pages = $most_viewed->total_pages;
		$dataArray[] = $filterArray;

		$this->data["sections"] = $dataArray;
		$this->data["specials"] = $special_videos->timeline_videos;

		// echo "<pre>";
		// print_r($this->data["specials"]);
		// echo "</pre>";
		// echo "lo";
		// print_r($this->data["specials"][0]->id);
		// return;


		if($rising_star == 1){
			$this->data['news'] = $this->memcached_library->get('cricwick_session_news');
			if(empty($this->data['news'])){
				$path = API_PATH."home?telco=ufone";
				$response = curl_call($path);
				$response = json_decode($response);
				$this->memcached_library->set('cricwick_session_news',$response->news,172800); // 2days
				$this->data["news"] = $response->news;
			}

			$this->data['rising_star'] = 1;
		}



		$this->data["video_allowed"] = in_pakistan();
		$this->data["confirm_view"] = base_url()."ajax/confirm_timeline_view/".$this->data["specials"][0]->id;

		$this->data["page"] = $this->load->view('april19/videos/list', $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$this->load->view('april19/template', $this->data);

	}

	function index($page_no=1, $rising_star=0){

		// $this->check_session();
		// $this->check_subscription();

		$response = $this->more_series(1);
		$this->data["series"] = $response->series;
		$this->data["main_series"] = $this->data["series"][0];

		// display_json(json_encode($this->data["main_series"]));
		// exit;

		$this->data["current_page"] = $response->current_page;
		$this->data["total_pages"] = $response->total_pages;

		$this->data["page"] = $this->load->view('april19/videos/newlist', $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$this->data["new_video_page"] = true;
		$this->load->view('april19/template', $this->data);

	}

	function more_filter_videos($filter, $page_no=1, $page_size=12){

		$url = API_PATH."$filter?page=$page_no&per_page=$page_size&telco=ufone";
		$response = curl_call($url);
		$response = json_decode($response);

		foreach($response->timeline_videos as $key=>$item){
			$item->seo_url = base_url()."highlights/".$item->id."/".seo_url($item->title)."/";
			$response->timeline_videos[$key] = $item;
		}
		if ($this->input->is_ajax_request()){
			$response = json_encode($response);
			echo $response;
		} else {
			return $response;
		}

	}

	function more_videos($page_no=1, $page_size=12){

		$url = API_PATH."get_timeline_videos?page=$page_no&per_page=$page_size&telco=ufone";
		$response = curl_call($url);
		$response = json_decode($response);

		foreach($response->timeline_videos as $key=>$item){
			$item->seo_url = base_url()."highlights/".$item->id."/".seo_url($item->title)."/";
			$response->timeline_videos[$key] = $item;
		}
		if ($this->input->is_ajax_request()){
			$response = json_encode($response);
			echo $response;
		} else {
			return $response;
		}

	}

	function play_video($video_id, $seokey=false){

		$this->check_session();
		$this->check_subscription();

		$this->data["video_allowed"] = in_pakistan();

		$url = API_PATH."get_single_video?id=$video_id";

		$response = curl_call($url);
		$response = json_decode($response);
		$this->data["current_video"] = $response->video;

		$playlist = array();
		if (!empty($response->video->pre_ad)){
			$object = new stdClass();
			$object->file = $response->video->pre_ad->video_file;
			$object->image = $response->video->thumb_file;
			$playlist[] = $object;
		}

		$qualities = array();
		foreach($response->video->qualities as $q){
			$object = new stdClass();
			$object->file = $q->video_file;
			$object->label = $q->height."P";
			if ($q->height==240){
				$object->default = true;
			}
			$qualities[] = $object;
		}

		if (!empty($response->video->video_file)){
			$object = new stdClass();
			$object->file = $response->video->video_file;
			$object->label = "720P";
			$qualities[] = $object;
		}

		$object = new stdClass();
		$object->sources = $qualities;
		$object->image = $response->video->thumb_file;
		$playlist[] = $object;

		if (!empty($response->video->post_ad)){
			$object = new stdClass();
			$object->file = $response->video->post_ad->video_file;
			$object->image = $response->video->thumb_file;
			$playlist[] = $object;
		}

		$this->data["playlist"] = json_encode($playlist);

		$this->data["more_videos"] = json_decode($this->more_videos(1, 12))->videos;
		$this->data["inner_page"] = $this->load->view('videos/play_video', $this->data, true);
		$this->data["page_heading"] = "Video";
		$this->data["banner_heading"] = "CricBoom";
		$this->data["banner_tagline"] = "videos";
		$this->data["banner_img_class"] = "back-img-video";
		$this->data["video_page"] = TRUE;

		$social = new stdClass();
		$social->link = base_url()."video/".$response->video->id."/".seo_url($response->video->title)."/";
		$social->title = $response->video->title;

		$social->description = $response->video->description;
		if (empty($social->description)){
			$social->description = $response->video->title;
		}
		$social->thumb_file = $response->video->thumb_file;

		$this->data["current_video"]->social = $social;
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		$this->load->view('template', $this->data);
	}

	function timline_old($video_id, $seokey=false){

		$this->check_session();
		$this->check_subscription();

		$this->data["video_allowed"] = in_pakistan();

		$url = API_PATH."get/video/timeline/related/$video_id?per_page=12";
		$response = curl_call($url);
		$response = json_decode($response);

		$this->data["timeline_url"] = "";
		$this->data["timeline_btn"] = false;
		if(isset($response->vf->match_obj)){
			if ($response->vf->match_obj->playing_state=='E'){
				$this->data["timeline_url"] = base_url()."match/".$response->vf->match_obj->id."/".seo_url($response->vf->match_obj->title)."/";
			} else {
				$this->data["timeline_url"] = base_url()."live/".$response->vf->match_obj->id."/".seo_url($response->vf->match_obj->title)."/";
			}
			$this->data["timeline_btn"] = true;
		}


		//Generate Play list Here
		$playlist = $this->generate_play_list($response->vf->video);

		$this->data["playlist"] = json_encode($playlist);

		$title = @$response->vf->title;
		if (empty($title)){
			$title = $response->vf->video->title;
		}
		$this->data["current_video"] = $response->vf;
		$this->data["more_videos"] = $response->related_videos;

		/*
		$social = new stdClass();
		$social->link = base_url()."highlights/".$response->vf->video->id."/".seo_url($title)."/";
		$social->title = $title;
		if (!empty($response->vf->ball->commentary)){
			$social->description = $response->vf->ball->commentary;
		} else {
			$social->description = $response->vf->desc;
		}
		*/
		$social = new stdClass();
		$social->link = base_url()."highlights/".$response->vf->video->id."/".seo_url($response->vf->video->title)."/";
		$social->title = $response->vf->video->title;
		if (!empty($response->vf->ball->commentary)){
			$social->description = $response->vf->ball->commentary;
		} else {
			if (!empty($response->vf->desc)){
				$social->description = $response->vf->desc;
			} else {
				$social->description = $title;
			}

		}
		$social->thumb_file = $response->vf->video->large_image;
		$this->data["social"] = $social;
		$this->data["confirm_view"] = base_url()."ajax/confirm_timeline_view/".$video_id;
		$this->data["inner_page"] = $this->load->view('april19/videos/timeline_video', $this->data, true);

		$this->data["video_page"] = TRUE;


		$this->data["current_video"]->social = $social;
		$this->data["page"] = $this->load->view("april19/template_wd_left_col", $this->data, true);
		$this->load->view('april19/template', $this->data);
	}

	function timeline($video_id, $seokey=false){

		$this->check_session();
		$this->check_subscription();

		$this->data["video_allowed"] = in_pakistan();

		$url = API_PATH."get/video/timeline/related/$video_id?per_page=12";
		$response = curl_call($url);
		$response = json_decode($response);

		$this->data["timeline_url"] = "";
		$this->data["timeline_btn"] = false;
		if(isset($response->vf->match_obj)){
			if ($response->vf->match_obj->playing_state=='E'){
				$this->data["timeline_url"] = base_url()."match/".$response->vf->match_obj->id."/".seo_url($response->vf->match_obj->title)."/";
			} else {
				$this->data["timeline_url"] = base_url()."live/".$response->vf->match_obj->id."/".seo_url($response->vf->match_obj->title)."/";
			}
			$this->data["timeline_btn"] = true;
		}


		//Generate Play list Here
		$playlist = $this->generate_play_list($response->vf->video);

		$this->data["playlist"] = json_encode($playlist);

		$title = @$response->vf->title;
		if (empty($title)){
			$title = $response->vf->video->title;
		}
		$this->data["current_video"] = $response->vf;
		$this->data["more_videos"] = $response->related_videos;

		/*
		$social = new stdClass();
		$social->link = base_url()."highlights/".$response->vf->video->id."/".seo_url($title)."/";
		$social->title = $title;
		if (!empty($response->vf->ball->commentary)){
			$social->description = $response->vf->ball->commentary;
		} else {
			$social->description = $response->vf->desc;
		}
		*/
		$social = new stdClass();
		$social->link = base_url()."highlights/".$response->vf->video->id."/".seo_url($response->vf->video->title)."/";
		$social->title = $response->vf->video->title;
		if (!empty($response->vf->ball->commentary)){
			$social->description = $response->vf->ball->commentary;
		} else {
			if (!empty($response->vf->desc)){
				$social->description = $response->vf->desc;
			} else {
				$social->description = $title;
			}

		}
		$social->thumb_file = $response->vf->video->large_image;
		$this->data["social"] = $social;
		$this->data["confirm_view"] = base_url()."ajax/confirm_timeline_view/".$video_id;
		$this->data["inner_page"] = $this->load->view('april19/videos/timeline_video', $this->data, true);

		$this->data["video_page"] = TRUE;



						$special_videos = $this->more_filter_videos("special_videos");
						$featured = $this->more_filter_videos("featured_videos");
						$most_viewed = $this->more_filter_videos("most_viewed_videos");
						$latest = $this->more_filter_videos("latest_videos");

						$dataArray = array();
						$filterArray = new stdClass();
						$filterArray->label = "Featured";
						$filterArray->filter = "featured_videos";
						$filterArray->container_id = "featuredVideosContainer";
						$filterArray->items = $featured->timeline_videos;
						$filterArray->current_page = $featured->current_page;
						$filterArray->total_pages = $featured->total_pages;
						$dataArray[] = $filterArray;

						$filterArray = new stdClass();
						$filterArray->label = "Latest";
						$filterArray->filter = "latest_videos";
						$filterArray->container_id = "latestVideosContainer";
						$filterArray->items = $latest->timeline_videos;
						$filterArray->current_page = $latest->current_page;
						$filterArray->total_pages = $latest->total_pages;
						$dataArray[] = $filterArray;

						$filterArray = new stdClass();
						$filterArray->label = "Most Viewed";
						$filterArray->filter = "most_viewed_videos";
						$filterArray->container_id = "mostViewedVideosContainer";
						$filterArray->items = $most_viewed->timeline_videos;
						$filterArray->current_page = $most_viewed->current_page;
						$filterArray->total_pages = $most_viewed->total_pages;
						$dataArray[] = $filterArray;

						$this->data["sections"] = $dataArray;



		$this->data["current_video"]->social = $social;
		$this->data["page"] = $this->load->view('april19/videos/list2', $this->data, true);
		$this->data["add_video_id_to_body"] = true;

		$this->load->view('april19/template', $this->data);
	}

	function more_timeline_related_videos($video_id, $page_no=1, $page_size=12){

		$url = API_PATH."get/video/timeline/related/$video_id?per_page=$page_size&page=$page_no";
		$response = curl_call($url);
		$response = json_decode($response);

		foreach($response->related_videos as $key=>$item){
			$item->seo_url = base_url()."highlights/".$item->id."/".seo_url($item->title)."/";
			$response->timeline_videos[$key] = $item;
		}
		if ($this->input->is_ajax_request()){
			$response = json_encode($response);
			echo $response;
		} else {
			return $response;
		}

	}

	function generate_play_list($video, $pre = true, $post = true){
		$playlist = array();
		if (!empty($video->pre_ad) && $pre){
			$object = new stdClass();
			$object->file = $video->pre_ad->video_file;
			$object->image = $video->med_image;
			$object->mediaid = "AD-".$video->pre_ad->id;
			//$object->mediaid = $video->id;
			$playlist[] = $object;
		}

		$qualities = array();
		foreach($video->qualities as $q){
			$object = new stdClass();
			$object->file = $q->video_file;
			$object->label = $q->height."P";
			if ($q->height==240){
				$object->default = true;
			}

			$qualities[] = $object;
		}

		if (!empty($video->video_file)){
			$object = new stdClass();
			$object->file = $video->video_file;
			$object->label = "720P";
			$qualities[] = $object;
		}

		$object = new stdClass();
		$object->sources = $qualities;
		$object->image = $video->med_image;
		$object->mediaid = $video->id;
		$playlist[] = $object;

		if (!empty($video->post_ad) && $post){
			$object = new stdClass();
			$object->file = $video->post_ad->video_file;
			$object->image = $video->med_image;
			$object->mediaid = "AD-".$video->post_ad->id;
			//$object->mediaid = $video->id;
			$playlist[] = $object;
		}

		return $playlist;
	}

	function more_related_videos($video_id, $series_id, $page_no='1', $per_page='10'){

		$url = API_PATH."get_videos_by_series?series_id=$series_id&video_id=$video_id&page=$page_no&per_page=$per_page";
		$response = curl_call($url);
		$response = json_decode($response);

		if(null != $response->videos){
			foreach($response->videos as $key=>$item){
				$item->seo_url = base_url()."highlights/".$item->video->id."/".$series_id."/".seo_url($item->video->title)."/";
				$response->timeline_videos[$key] = $item;
			}
		}
		if ($this->input->is_ajax_request()){
			$response = json_encode($response);
			echo $response;
		} else {
			return $response;
		}

	}

	function more_series($page_no='1', $per_page='4'){

		$url = API_PATH."get_new_series?page=$page_no&per_page=$per_page";
		$response = curl_call($url);
		$response = json_decode($response);


		if(null != $response->series){
			foreach($response->series as $key=>$s){

				$start_date_DM = date("M j", strtotime($s->start_date));
				$end_date_DM = date("M j", strtotime($s->end_date));

				$response->series[$key]->start_date_DM = $start_date_DM;
				$response->series[$key]->end_date_DM = $end_date_DM;

				foreach ($s->videos as $key1 => $v) {
					# code...
					$v->seo_url = base_url()."highlights/".$v->video->id."/".$v->video->match_obj->series_id."/".seo_url($v->video->title)."/";

					$response->series[$key]->videos[$key1] = $v;
				}
			}
		}


		if ($this->input->is_ajax_request()){
			$response = json_encode($response);
			echo $response;
		} else {
			return $response;
		}

	}

	function timeline2($video_id, $series_id, $seokey=false){

		$this->session->set_userdata("ret_url", $seokey);
		$this->check_session();
		$this->check_subscription();

		$this->data["video_allowed"] = in_pakistan();

		// $url = API_PATH."get/video/timeline/related/$video_id?per_page=12";
		// $url = API_PATH."get_videos_by_series?series_id=$series_id&video_id=$video_id";

		$response = $this->more_related_videos($video_id, $series_id, 1);
		// display($response);
		// exit;


		$title = @$response->vf->title;
		if (empty($title)){
			$title = $response->vf->video->title;
		}


		$social = new stdClass();

		$social->link = base_url()."highlights/".$response->vf->id."/".$series_id."/".seo_url($response->vf->title)."/";

		$social->title = $response->vf->title;

		if (!empty($response->vf->ball->commentary)){
			$social->description = $response->vf->ball->commentary;
		} else {
			if (!empty($response->vf->desc)){
				$social->description = $response->vf->desc;
			} else {
				$social->description = $title;
			}

		}
		$social->thumb_file = $response->vf->large_image;
		$this->data["social"] = $social;

		$this->data["confirm_view"] = base_url()."ajax/confirm_timeline_view/".$video_id;

		$this->data["inner_page"] = $this->load->view('april19/videos/timeline_video2', $this->data, true);

		$this->data["video_page"] = TRUE;


		$this->data["current_series"] = $response->series;
		$this->data["related_videos"] = $response->videos;
		$this->data["current_page"] = $response->current_page;
		$this->data["total_pages"] = $response->total_pages;
		$this->data["current_video"] = $response->vf;
		$this->data["current_video"]->social = $social;






		$response = $this->more_series(1);
		$this->data["series"] = $response->series;
		$this->data["main_series"] = $this->data["series"][0];

		$this->data["page"] = $this->load->view('april19/videos/list3', $this->data, true);


		$this->data["add_video_id_to_body"] = true;
		$this->data["new_video_page"] = true;
		$this->load->view('april19/template', $this->data);
	}

}

/* End of file video.php */
/* Location: ./application/controllers/welcome.php */
