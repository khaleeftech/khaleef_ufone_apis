<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Squad extends My_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	function index($series_id, $team_id){
		
		$this->check_session();
		//$this->check_subscription();
		
		$url = API_PATH."get_series_squad?series_id=$series_id&team_id=$team_id";
		$response = curl_call($url);
		$response = json_decode($response);
		
		$this->data["squad"] = $response->teams_squad;
		$this->data["content_heading"] = $response->series->title;
		$this->data["inner_page"] = $this->load->view('squad/list', $this->data, true);
		$this->data["page_heading"] = $response->team->title;
		$this->data["banner_heading"] = "CricBoom";
		$this->data["banner_tagline"] = "&nbsp;";
		$this->data["banner_img_class"] = "back-img-t20";
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		
		$this->load->view('template', $this->data);
		
	}

	
}
