<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campaign extends CI_Controller {
	
	public $data = array();
	function __construct(){
		parent::__construct();
		
	}
	
	public function qualifier(){

		$user = $this->xession->get("user");
		if (!$user){
			$server = $_SERVER;
			if (!empty($server["HTTP_MSISDN"])){
				$header_number = $server["HTTP_MSISDN"];
				$url = SUB_PATH."get_user_info?web_user=1&phone=".$header_number;
				$response = curl_call($url);
				$response = json_decode($response);
				if (!empty($response->user) && in_array($response->user->subscribe_status, array(1,3))){
					$this->xession->set("user", $response->user);
					$this->data["phone_no"] = $response->user->phone;
					$this->data["udid"] = $response->user->udid;
					$this->purhcase_stream($header_number);
					redirect(base_url());
				} else {
					$params = array();
					$params["phone"] = $header_number;
					$params["web_user"] = 1;
					$params["service_id"] = 65;
					
					$url = SUB_PATH."appstart?";
					$url = $url.http_build_query($params);
					$response_appstart = curl_call($url);
					$response_appstart = json_decode($response_appstart);
					
					$this->xession->set("user", $response_appstart->user);
					$this->data["phone_no"] = $response_appstart->user->phone;
					$this->data["udid"] = $response_appstart->user->udid;
					
					
					$params["udid"] = $response_appstart->user->udid;
						
					$url = SUB_PATH."send_pin_sms?".http_build_query($params);
					curl_call($url);
					
					$url = SUB_PATH."api_hit_subscribe?";
					$url = $url.http_build_query($params);
					curl_call($url);
					$this->purhcase_stream($header_number);
					redirect(base_url());
				}
			} else {
				//$this->xession->set(base_url());
				//redirect("login");
				redirect(base_url());
			}
			
		} else {
			$this->data["phone_no"] = $user->phone;
			$this->data["udid"] = $user->udid;
			redirect(base_url());
		}
	}
	
	function purhcase_stream($msisdn){
		$params = array();
		$params["time"] = time();
		$params["sender"] = $msisdn;
		$params["message"] = "W - Ad Campaign";
		$params["shortcode"] = "704421";
		$params = http_build_query($params);
		
		$ch = curl_init('http://api.cricboom.com/userapi/incoming_stream_sms_zain');
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
        curl_close($ch);
	}


}
