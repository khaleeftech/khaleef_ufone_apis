<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Figures extends My_Controller {

	function __construct(){
		parent::__construct();
    // if (empty($this->input->server('PHP_AUTH_USER')))
    // {
    //     header('HTTP/1.0 401 Unauthorized');
    //     header('HTTP/1.1 401 Unauthorized');
    //     header('WWW-Authenticate: Basic realm="My Realm"');
    //     echo 'You must login to use this service'; // User sees this if hit cancel
    //     die();
    //  }
    //
    //  $username = $this->input->server('radmin');
    //  $password = $this->input->server('@dmIn');


    $valid_passwords = array ("admin" => "@dmIn");
    $valid_users = array_keys($valid_passwords);
    $user = @$_SERVER['PHP_AUTH_USER'];
    $pass = @$_SERVER['PHP_AUTH_PW'];
    $validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);
    if (!$validated) {
      header('WWW-Authenticate: Basic realm="Restricted Area"');
      header('HTTP/1.0 401 Unauthorized');
      die ("Restricted Area");
    }

    $this->load->model('figure');
	}

	function index($page_no=1){


    $data = [];
    date_default_timezone_set("Asia/Karachi");
    $data["graph"] = $this->get_revenue();
    $review =  json_decode($this->figure->get_overview(), true)[0];
      //  echo "<pre>";
      // print_r($review);
      // echo "</pre>";exit;
    $data["app_registers"] = number_format($review["app_registrations"]);
    //$data["wap_registers"] = '0';
      $data["wap_registers"] = number_format($review["wap_registrations"]);
    $data["active_subs"] = number_format($review["active_subs"]);
    $data["free_subs"] = number_format($review["free_subs"]);


    $data["access"] = number_format($review["app_accessed_today"]);
    $data["cc_unsub"] = number_format($review["cc_unsub"]);
    $data["user_unsub"] = number_format($review["user_unsub"]);
    $data["low_unsub"] = number_format($review["low_unsub"]);
    $data["purged_today"] = number_format($review["purged_today"]);
    $data["nopin_today"] = number_format($review["nopin_today"]);
    $data["billed_today"] = $review["billed_today"];
    $data["total_amount"] = $review["total_amount"];
		

    $this->load->view('figures/pages/index', $data);
	}

	function detailed_report(){
		$this->load->view('figures/pages/detailed_report');
	}


  function get_revenue($from = '', $to = ''){
    if($this->input->is_ajax_request()){

      if( date(strtotime($from) > date(strtotime($to)) )){
        echo json_encode(array("status"=> '6'));
        return;
      }

      echo $this->figure->get_revenue($from, $to);
    }else{
      return $this->figure->get_revenue($from, $to);
    }
  }

  function get_revenue_by_month($month, $year){
    echo $this->figure->get_revenue_by_month($month, $year);
  }


	function get_report($from = '', $to = ''){
    if($this->input->is_ajax_request()){

      if( date(strtotime($from) > date(strtotime($to)) )){
        echo json_encode(array("status"=> '6'));
        return;
      }

			$data["data"] = json_decode($this->figure->get_report($from, $to));

      echo $this->load->view('figures/pages/detailed_report_table', $data, true);
    }else{
      echo $this->figure->get_report($from, $to);
    }
  }


}
