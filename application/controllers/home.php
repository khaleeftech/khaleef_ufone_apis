<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Home extends My_Controller {

	function __construct(){
		parent::__construct();
	}

	public function x_index(){
		//$this->check_session();
		//$this->check_subscription();

		$path = API_PATH."home?telco=".CONTENT_TELCO."&web_user=1";

		$response = curl_call($path);
		$response = json_decode($response);

		$news_response = json_decode($this->more_news(1,4));

		// print_r($news_response);
		// exit;

		// $this->data["news"] = $response->news;
		$this->data["news"] = $news_response;
		$this->data["timeline_videos"] = $response->timeline_videos;


		// $this->memcached_library->delete('cricwick_session_news');
		$this->memcached_library->set('cricwick_session_news',$news_response,172800); // 2days
		$this->memcached_library->set('cricwick_session_videos',$response->timeline_videos,172800); // 2days

		$path = BACKEND."live_matches?telco=ufone&web_user=1";
		$response = curl_call($path);
		$scoring = json_decode($response);
		$this->data["live_matches"] = $scoring->live_matches;
		// $this->data["live_matches"] = [];

		$this->data["upcoming_fixtures"] = $scoring->upcoming_matches;
		// $this->data["upcoming_fixtures"] = [];





		$this->data["recent_matches"] = $scoring->recent_matches;

		foreach($this->data["recent_matches"] as $key=>$result){

			$local = format_date_newserver($result->match_start);
			//$local_time = $local['date']." ".$local["time"];
			$this->data["recent_matches"][$key]->local_time = $local;

			$seokey = $result->team_1->team->name." vs ".$result->team_2->team->name;
			$this->data["recent_matches"][$key]->seo_url = base_url()."match/$result->id/".seo_url($seokey)."/";

			$this->data["recent_matches"][$key]->team_1->team->flag_url = $this->data["recent_matches"][$key]->team_1->team->full_flag_url;
			$this->data["recent_matches"][$key]->team_2->team->flag_url = $this->data["recent_matches"][$key]->team_2->team->full_flag_url;
		}

		$teams = array();
		foreach($scoring->live_matches as $LiveMatch){
			if (!empty($LiveMatch->innings)){
				$inning = $LiveMatch->innings[0];
				if ($inning->batting_team_id == $LiveMatch->team_1_id){
					$teams[$inning->batting_team_id] = $LiveMatch->team_1->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $LiveMatch->team_2->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_1->team;
				}
			}
		}

		$this->data["teams"] = $teams;
		$this->memcached_library->set("liveMatchTeams", $teams, 21600); //Six Hours


		// header('Content-Type: json');
		// echo json_encode($this->data["live_matches"]);
		// exit;

		$this->load->view('april19/home_page', $this->data);
	}


	public function index(){
		// echo BACKEND;exit;
		// $this->data["psl18_slug"] = "";
		// if($this->uri->segment(1) == 'psl18'){
		// 	$this->data["psl18_slug"] = "psl18";
		// }
		//
		// if( null !== $this->input->get('q') ){
		// 	$phone = $this->input->get('q');
		//
		// 	if (!empty($phone)){
		// 		$phone = international_number($phone);
		// 		$url = SUB_PATH."appstart?web_user=1&telco=stc&phone=".$phone;
		// 		$response = curl_call($url);
		// 		$response = json_decode($response);
		// 		if (!empty($response->user) && (in_array($response->user->subscribe_status, array(1,3)) || $response->user->free_secs_remaining>0)){
		// 			$this->xession->set("user", $response->user);
		// 			redirect(base_url());
		// 			exit;
		// 		}
		// 	}
		//
		// }}


		// $this->data["news"] = $news_response;
		$this->data["news"] = "";
		$this->data["twitter"] = "";
		// $this->data["timeline_videos"] = $response->timeline_videos;
		$this->data["timeline_videos"] = "";


		// $this->memcached_library->set('cricwick_session_news',$news_response,172800); // 2days
		// $this->memcached_library->set('cricwick_session_videos',$response->timeline_videos,172800); // 2days

		// $this->memcached_library->delete('cricwick_zain_session_videos');

		// $scoring = [];
		// // $scoring = $this->memcached_library->get('cricwick_zain_home_matches');
		// if(empty($scoring)){
		// 	// echo "<h1>NOT FOUND: </h1>";
		// 	$path = BACKEND."live_matches?telco=ufone&web_user=1";
		// 	$response = curl_call($path);
		// 	$scoring = $response;
		// 	if(!empty($scoring)){
		// 		$this->memcached_library->set('cricwick_zain_home_matches',$scoring,60);
		// 	}
			
		// 	// $this->memcached_library->set('cricwick_zain_home_matches',$scoring,180);
		// }else{
		// 	// echo "<h1>FOUND: </h1>";
		// }

		$scoring = [];
		$path = BACKEND."live_matches?telco=ufone&web_user=1";
		$response = curl_call($path);

		if(!empty(json_decode($response))){
			$this->memcached_library->set('cricwick_ufone_home_live_matches',$response,86400);
		}else{
			$response = $this->memcached_library->get('cricwick_ufone_home_live_matches');
		}

		$scoring = $response;

		$scoring = mb_convert_encoding($scoring, "UTF-8");
		$scoring = json_decode($scoring);

    $this->data["twitter"] = [];

		$this->data["live_matches"] = $scoring->live_matches;
		$this->data["upcoming_fixtures"] = $scoring->upcoming_matches;
		$this->data["recent_matches"] = $scoring->recent_matches;





		foreach($this->data["recent_matches"] as $key=>$result){

			$local = format_date_newserver($result->match_start);
			$this->data["recent_matches"][$key]->local_time = $local;

			$seokey = $result->team_1->team->name." vs ".$result->team_2->team->name;
			$this->data["recent_matches"][$key]->seo_url = base_url()."match/$result->id/".seo_url($seokey)."/";

			$this->data["recent_matches"][$key]->team_1->team->flag_url =  $this->data["recent_matches"][$key]->team_1->team->full_flag_url;
			$this->data["recent_matches"][$key]->team_2->team->flag_url =  $this->data["recent_matches"][$key]->team_2->team->full_flag_url;
		}

		$teams = array();

		foreach($scoring->live_matches as $LiveMatch){
			if (!empty($LiveMatch->innings)){
				$inning = $LiveMatch->innings[0];
				if ($inning->batting_team_id == $LiveMatch->team_1_id){
					$teams[$inning->batting_team_id] = $LiveMatch->team_1->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $LiveMatch->team_2->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_1->team;
				}
			}
		}

		$this->data["teams"] = $teams;
		$this->memcached_library->set("liveMatchTeams", $teams, 21600); //Six Hours

		$this->load->view('april19/home_page', $this->data);
	}


	public function sub(){

		if ($this->input->is_ajax_request()){
			$xession = $this->xession->get("user");
			$params = array();
			$params["pincode"] = $xession->pin;
			$params["service_id"] = 65;
			$params["web_user"] = 1;
			$params["phone"] = $this->data["phone_no"];
			$url = SUB_PATH."confirm/pin?";
			$url = $url.http_build_query($params);
			$response = curl_call($url);
			$response = json_decode($response);
			if (!empty($response->hit_subscribe)){
				$params["udid"] = $this->data["udid"];
				$url = SUB_PATH."api_hit_subscribe?";
				$url = $url.http_build_query($params);
				curl_call($url);
				sleep(10);
			}
			unset($params["udid"]);
			$url = SUB_PATH."appstart?";
			$url = $url.http_build_query($params);
			echo $response = curl_call($url);
			exit;
		}

		$user_status = $this->appstart();

		$this->data["sub_msg"] = "";
		if ($user_status->user->subscribe_status==3){
			$this->data["sub_msg"] = "Please recharge your account to get access of Cricwick Services!";
		} else if ($user_status->user->subscribe_status==1){
			$redirect(base_url());
			return;
		} else {
			$this->data["sub_msg"] = "Please subscribe now to get access for Cricwick Services!";
		}
		$this->data["phone_number"] = $this->data["phone_no"];
		$this->data["page"] = $this->load->view('login/sub', $this->data, true);
		$this->data["page_heading"] = "Subscribe";
		$this->load->view('template', $this->data);
	}

	public function lowbalance(){
		/*
		if ($this->input->is_ajax_request()){
			$xession = $this->xession->get("user");
			$params = array();
			$params["pincode"] = $xession->pin;
			$params["service_id"] = 65;
			$params["web_user"] = 1;
			$params["phone"] = $this->data["phone_no"];
			$url = SUB_PATH."confirm/pin?";
			$url = $url.http_build_query($params);
			$response = curl_call($url);
			$response = json_decode($response);
			if (!empty($response->hit_subscribe)){
				$params["udid"] = $this->data["udid"];
				$url = SUB_PATH."api_hit_subscribe?";
				$url = $url.http_build_query($params);
				curl_call($url);
				sleep(10);
			}
			unset($params["udid"]);
			$url = SUB_PATH."appstart?";
			$url = $url.http_build_query($params);
			echo $response = curl_call($url);
			exit;
		}
		*/
		$user_status = $this->appstart();

		$this->data["sub_msg"] = "";
		if ($user_status->user->subscribe_status==3){
			$this->data["sub_msg"] = "Sorry! Due to low balance, we have suspended your subscription temporarily. Please recharge your account to enjoy CricWick Services";
		} else {
			redirect(base_url());
		}
		$this->data["phone_number"] = $this->data["phone_no"];
		$this->data["page"] = $this->load->view('login/lowbalance', $this->data, true);
		$this->data["page_heading"] = "Low Balance";
		$this->load->view('template', $this->data);
	}

	function ball2ball($match_id=false){
		$this->xession->set("ball2ballPlayers",false);
		if (!$match_id){
			redirect(base_url());
			return;
		}
		$players = array();
		$url = API_PATH."get_match_details?match_id=$match_id";
		$match_response = curl_call($url);
		$match_response = json_decode($match_response);
		foreach($match_response->match->fixture->Playing_11->{"Team A"} as $player){
			$players[$player->id] = $player;
		}
		foreach($match_response->match->fixture->Playing_11->{"Team B"} as $player){
			$players[$player->id] = $player;
		}
		$this->data["match_details"] = $match_response->match;
		$this->xession->set("ball2ballPlayers",$players);
		$this->xession->set("ball2ballMatchId",$match_id);

		$url = API_PATH."get_comp_match_commentry?match_id=$match_id";
		//$url = base_url()."test.json";
		$response = curl_call($url);
		$response = json_decode($response);

		$this->data["first_inning"] = false;
		$this->data["second_inning"] = false;
		if (count($response->inns)==1){
			$this->data["current_inning"] = $response->inns[0];
			$this->data["first_inning"] = $response->inns[0]->inn;
		} else {
			$this->data["current_inning"] = $response->inns[1];
			$this->data["first_inning"] = $response->inns[0]->inn;
			$this->data["second_inning"] = $response->inns[1]->inn;
		}
		$this->data["players"] = $players;
		$this->data["batsman"] = $this->data["current_inning"]->inn->batsmen_on_pitch;
		$this->data["bowlers"] = $this->data["current_inning"]->inn->bowlers_on_spell;
		$this->data["inner_page"] = $this->load->view("live/ball2ball", $this->data, true);
		$this->data["page_heading"] = "Live Score";

		$this->data["banner_heading"] = str_replace(",", "", $match_response->match->fixture->series->title);
		$series_start = format_date($match_response->match->fixture->series->start_date, "");
		$series_end = format_date($match_response->match->fixture->series->end_date, "");
		$this->data["banner_tagline"] = $series_start["date"]." to ".$series_end["date"];

		$this->data["banner_img_class"] = "back-img";
		$this->data["page"] = $this->load->view('template_wd_left_col', $this->data, true);
		$this->data["add_ball_css"] = true;
		$this->load->view('template', $this->data);


	}

	function scorecard($match_id=false){

		//$this->xession->set("ball2ballPlayers",false);
		if (!$match_id){
			redirect(base_url());
			return;
		}
		$url = BACKEND."$match_id";
		$match_response = curl_call($url);
		$match_response = json_decode($match_response);


		$this->data["match"] = $match_response;


		$teams = array();
		if (!empty($match_response->innings)){
			$inning = $match_response->innings[0];
			if ($inning->batting_team_id == $match_response->team_1_id){
				$teams[$inning->batting_team_id] = $match_response->team_1->team;
				$teams[$inning->fielding_team_id] = $match_response->team_2->team;
			} else {
				$teams[$inning->batting_team_id] = $match_response->team_2->team;
				$teams[$inning->fielding_team_id] = $match_response->team_1->team;
			}
			$teams[$inning->batting_team_id]->flag_url = $teams[$inning->batting_team_id]->full_flag_url;
			$teams[$inning->fielding_team_id]->flag_url = $teams[$inning->fielding_team_id]->full_flag_url;
		}
		$this->data["teams"] = $teams;

		$this->data["current_inning"] = count($match_response->innings)-1;

		// $this->data["inner_page"] = $this->load->view("scorecard", $this->data);
		$this->data["inner_page"] = $this->load->view("april19/scorecard", $this->data);

		return;
		$this->data["page_heading"] = "Scorecard";
		$this->data["banner_heading"] = str_replace(",", "", $match_response->match->fixture->series->title);
		$series_start = format_date($match_response->match->fixture->series->start_date, "");
		$series_end = format_date($match_response->match->fixture->series->end_date, "");
		$this->data["banner_tagline"] = $series_start["date"]." to ".$series_end["date"];
		$this->data["banner_img_class"] = "back-img";
		$this->data["page"] = $this->load->view('template_wd_left_col', $this->data, true);
		$this->data["add_ball_css"] = true;
		$this->load->view('template', $this->data);


	}



	function test(){
		$server = $_SERVER;
		echo json_encode($server);
	}

	function get_home_videos(){
		if($this->input->is_ajax_request()){


				$response = [];
				$response = $this->memcached_library->get('cricwick_ufone_home_videos');
				if(empty($response)){
					$path = API_PATH."home?web_user=1";
					$response = curl_call($path);
					$this->memcached_library->set('cricwick_ufone_home_videos', $response, 600);
				}
				$response = mb_convert_encoding($response, "UTF-8");
				$response = json_decode($response);

				$data = [];
				$data["timeline_videos"] = $response->timeline_videos;
				echo $this->load->view('april19/home_videos', $data, true);

		}
	}

	function get_home_news(){

		if($this->input->is_ajax_request()){

				// $this->memcached_library->delete('cricwick_zain_home_news');
				$response = [];
				$response = $this->memcached_library->get('cricwick_ufone_home_news');

				if(empty($response)){
					$url = BACKEND."news?page=1&per_page=4&telco=".CONTENT_TELCO;
					$response = curl_call($url);
					$response = json_decode($response);

					foreach($response->data as $key=>$item){
						$item->created_at = date('F j, Y', strtotime($item->created_at));

						$item->seo_url = base_url()."news-update/".$item->id."/".seo_url($item->title)."/";
						$response->news[$key] = $item;

						$item->stripped_body = mb_substr(strip_tags($item->body),0, 180).'... ';
						$item->stripped_body_XS = mb_substr(strip_tags($item->body),0, 230).'... ';

					}

					$response = json_encode($response);

					$this->memcached_library->set('cricwick_ufone_home_news', $response, 600);
				}else{
				}

				$response = mb_convert_encoding($response, "UTF-8");
				$response = json_decode($response);

				$data = [];
				$data["news"] = $response;
				echo $this->load->view('april19/home_news', $data, true);

		}
	}

	function clean(){
		$this->memcached_library->flush();
	}

	function info(){
		sleep(60);
		phpinfo();
	}

}
