<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Ajax extends My_Controller {

	public $data = array();
	function __construct(){
		parent::__construct();
		$this->load->library("memcached_library");
	}

	public function testCache(){
		$players = $this->memcached_library->get("liveMatchPlayers");
		$teams = $this->memcached_library->get("liveMatchTeams");

		echo json_encode(array("teams"=>$teams,"players"=>$players));
		exit;

	}

	public function updateHomeLiveMatchScore(){

		$path = BACKEND."live_matches?telco=mobilink";
		//$path = "http://staging.cric-live.net:8080/api/live_matches_temp";
		$response = curl_call($path);


		if(!empty(json_decode($response))){
			$this->memcached_library->set('cricwick_ufone_home_live_matches',$response,86400);
		}else{
			$response = $this->memcached_library->get('cricwick_ufone_home_live_matches');
		}
		
		$response = json_decode($response);

		$teams = array();
		foreach($response->live_matches as $LiveMatch){
			if (!empty($LiveMatch->innings)){
				$inning = $LiveMatch->innings[0];
				if ($inning->batting_team_id == $LiveMatch->team_1_id){
					$teams[$inning->batting_team_id] = $LiveMatch->team_1->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $LiveMatch->team_2->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_1->team;
				}
			}
		}
		$this->memcached_library->set("liveMatchTeams", $teams, 21600); //Six Hours

		$responseArray = array();
		$responseArray["live_matches"] = array();

		foreach($response->live_matches as $live){
			//$players = $this->memcached_library->get("liveMatchPlayers".$live);
			//$teams = $this->memcached_library->get("liveMatchTeams");

			$match_status = new stdClass();
			$match_status->match_id = $live->id;
			$match_status->format = $live->format;
			$match_status->batting = new stdClass();
			$match_status->bowling = new stdClass();
			//$match_status->latest_balls = '<div class="b-grey ball-circle"></div>';
			$match_status->latest_balls = "";

			$innings = $live->innings;
			$current_inning = count($innings)-1;

			$batting_team_id = $innings[$current_inning]->batting_team_id;
			$fielding_team_id = $innings[$current_inning]->fielding_team_id;

			$teamA = $teams[$batting_team_id];
			$teamB = $teams[$fielding_team_id];

			$batsman_onstrike = "";
			$batsman_offstrike = "";

			$bowler_onspell = "";
			$bowler_offspell = "";

			if (!empty($live->partnership_and_bowlers)){
				if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->string_state=="facing"){
					$batsman_onstrike = $live->partnership_and_bowlers->batsman_1;
				} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->string_state=="facing"){
					$batsman_onstrike = $live->partnership_and_bowlers->batsman_2;
				}

				if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->string_state=="batting"){
					$batsman_offstrike = $live->partnership_and_bowlers->batsman_1;
				} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->string_state=="batting"){
					$batsman_offstrike = $live->partnership_and_bowlers->batsman_2;
				}

				if (!empty($live->partnership_and_bowlers->bowler)){
					$bowler_onspell = $live->partnership_and_bowlers->bowler;
				}
				if (!empty($live->partnership_and_bowlers->last_bowler)){
					$bowler_offspell = $live->partnership_and_bowlers->last_bowler;
				}
			}


			if (empty($live->innings[$current_inning]->run_rate) || $live->innings[$current_inning]->run_rate==''){
				$live->innings[$current_inning]->run_rate = "0.0";
			}

			$live->innings[$current_inning]->overs = empty($live->innings[$current_inning]->overs)?"0.0":$live->innings[$current_inning]->overs;
			$live->innings[$current_inning]->runs = empty($live->innings[$current_inning]->runs)?0:$live->innings[$current_inning]->runs;
			$live->innings[$current_inning]->wickets = empty($live->innings[$current_inning]->wickets)?0:$live->innings[$current_inning]->wickets;

			if ($live->format!='Test'){
				$match_status->batting->score = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets;
				$match_status->batting->overs = $live->innings[$current_inning]->overs;
				$match_status->html_title = $match_status->batting->score." | ".$match_status->batting->overs."ov";
			} else {
				$batting_team_previous_innings = false;
        		foreach($innings as $inn){
        			if ($inn->batting_team_id==$batting_team_id && $inn->id!=$innings[$current_inning]->id){
        				$batting_team_previous_innings = $inn;
						break;
        			}
        		}
				if ($batting_team_previous_innings){
					$declared = $batting_team_previous_innings->declared?'d':'';
					$match_status->batting->previous_score = $batting_team_previous_innings->runs."/".$batting_team_previous_innings->wickets.$declared."<br>"."(".$batting_team_previous_innings->overs.")";
					$match_status->batting->score = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets;
					// $match_status->batting->score = $batting_team_previous_innings->runs."/".$batting_team_previous_innings->wickets.$declared." &amp;";
					$match_status->batting->overs = "(".$live->innings[$current_inning]->overs.")";
					// $match_status->batting->overs = "<strong>".$live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets."</strong> (".$live->innings[$current_inning]->overs." ov)";

					$match_status->html_title = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets." | ".$live->innings[$current_inning]->overs."ov";
				} else {
					$match_status->batting->score = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets;
					$match_status->batting->overs = "(".$live->innings[$current_inning]->overs." ov)";

					$match_status->html_title = $match_status->batting->score." | ".$match_status->batting->overs."ov";
				}
			}

			$match_status->batting->title = $teams[$live->innings[$current_inning]->batting_team_id]->name;
			$match_status->batting->title_small = $teams[$live->innings[$current_inning]->batting_team_id]->short_name;
			$match_status->batting->flag =  $teams[$live->innings[$current_inning]->batting_team_id]->full_flag_url;
			$match_status->batting->run_rate = $live->innings[$current_inning]->run_rate;
			$match_status->batting->color = $teams[$live->innings[$current_inning]->batting_team_id]->color_hex;

			$match_status->batting->id = $live->innings[$current_inning]->batting_team_id;

			$match_status->batting->onstrike = new stdClass();
			$match_status->batting->onstrike->short_name = "-";
			$match_status->batting->onstrike->score = "-";
			$match_status->batting->offstrike = new stdClass();
			$match_status->batting->offstrike->short_name = "-";
			$match_status->batting->offstrike->score = "-";

			if (!empty($batsman_onstrike)){
				$batsman_onstrike->batsman->balls_played = empty($batsman_onstrike->batsman->balls_played)?0:$batsman_onstrike->batsman->balls_played;

				$match_status->batting->onstrike->short_name = empty($batsman_onstrike->player->short_name)? '-': $batsman_onstrike->player->short_name."*";
				$match_status->batting->onstrike->name = empty($batsman_onstrike->player->name)?'-' : $batsman_onstrike->player->name."*";

				$match_status->batting->onstrike->score = empty($batsman_onstrike->batsman->runs_scored)? '0' : $batsman_onstrike->batsman->runs_scored;

				$match_status->batting->onstrike->balls = empty($batsman_onstrike->batsman->balls_played)? '0' : $batsman_onstrike->batsman->balls_played;

				$match_status->batting->onstrike->rate = empty($batsman_onstrike->batsman->strike_rate)? '0.0' : $batsman_onstrike->batsman->strike_rate;

			}

			if (!empty($batsman_offstrike)){
				// $batsman_offstrike->batsman->balls_played = empty($batsman_offstrike->batsman->balls_played)?0:$batsman_offstrike->batsman->balls_played;
				// $match_status->batting->offstrike->short_name = $batsman_offstrike->player->short_name;
				// $match_status->batting->offstrike->score = $batsman_offstrike->batsman->runs_scored ."(".$batsman_offstrike->batsman->balls_played.")";

				$batsman_offstrike->batsman->balls_played = empty($batsman_offstrike->batsman->balls_played)?0:$batsman_offstrike->batsman->balls_played;

				$match_status->batting->offstrike->short_name = empty($batsman_offstrike->player->short_name)? '-': $batsman_offstrike->player->short_name;
				$match_status->batting->offstrike->name = empty($batsman_offstrike->player->name)?'-' : $batsman_offstrike->player->name;

				$match_status->batting->offstrike->score = empty($batsman_offstrike->batsman->runs_scored)? '0' : $batsman_offstrike->batsman->runs_scored;

				$match_status->batting->offstrike->balls = empty($batsman_offstrike->batsman->balls_played)? '0' : $batsman_offstrike->batsman->balls_played;

				$match_status->batting->offstrike->rate = empty($batsman_offstrike->batsman->strike_rate)? '0.0' : $batsman_offstrike->batsman->strike_rate;

			}

			/*
			$req_over = !empty($live->innings[0]->latest_balls[0])?$live->innings[0]->latest_balls[0]->over_number:1;
      		$this_over_balls = array();
			foreach($live->innings[0]->latest_balls as $b){
				if ($b->over_number==$req_over){
					$this_over_balls[] = $b;
				} else {
					break;
				}
			}

			foreach($this_over_balls as $key=>$ball){
				if($key==0){
					$match_status->latest_balls = get_ball_score_status_timeline_scorecard($ball);
				} else {
					$match_status->latest_balls = get_ball_score_status_timeline_scorecard($ball).$match_status->latest_balls;
				}
			}
			*/
			if (!empty($live->partnership_and_bowlers) && !empty($live->partnership_and_bowlers->latest_balls)){
				foreach($live->partnership_and_bowlers->latest_balls as $ball){
					$match_status->latest_balls .= get_ball_score_status_timeline_scorecard($ball);
				}
			}

			if ($live->format!='Test'){
				if (count($live->innings)==1){
					$match_status->bowling->score = "";
					$match_status->bowling->overs = "";
					$match_status->bowling->title = "";
					$match_status->bowling->flag = "";
					$match_status->bowling->color = "";
					$match_status->tagline = ($teams[$live->toss_won_by_id]->short_name)." won the toss and choose to ".($live->chose_to=="Bowl"?"field":"bat")." first";
					$match_status->tagline_small = "Run Rate: ".$live->innings[$current_inning]->run_rate;
				} else {
					$match_status->bowling->score = $live->innings[$current_inning-1]->runs."/".$live->innings[$current_inning-1]->wickets;
					$match_status->bowling->overs = $live->innings[$current_inning-1]->overs;
					$match_status->bowling->run_rate = $live->innings[$current_inning-1]->run_rate;
					$match_status->bowling->title = $teams[$live->innings[$current_inning-1]->batting_team_id]->name;
					$match_status->bowling->title_small = $teams[$live->innings[$current_inning-1]->batting_team_id]->short_name;
					$match_status->bowling->flag =  $teams[$live->innings[$current_inning-1]->batting_team_id]->full_flag_url;
					$match_status->bowling->color = $teams[$live->innings[$current_inning-1]->batting_team_id]->color_hex;

					$match_status->tagline = "Need ".($live->innings[$current_inning]->runs_required+1-$live->innings[$current_inning]->runs)." runs from ".$live->innings[$current_inning]->balls_remaining." balls";
					$match_status->tagline_small = "Req ".($live->innings[$current_inning]->runs_required+1-$live->innings[$current_inning]->runs)." in ".$live->innings[$current_inning]->balls_remaining." balls @ RR:".$live->innings[$current_inning]->required_rate;
					if (!empty($live->match_result)){
						$match_status->tagline = $live->match_result;
						$match_status->tagline_small = $live->match_result;
					}
				}

			} else {

				$fielding_team_first_inning = false;
				$fielding_team_second_inning = false;
        		foreach($innings as $inn){
        			if ($inn->batting_team_id==$fielding_team_id){
        				if (!$fielding_team_first_inning){
        					$fielding_team_first_inning = $inn;
        				} else {
        					$fielding_team_second_inning = $inn;
        				}
        			}
        		}
				$declared_1 = $fielding_team_first_inning->declared?'d':'';
				$declared_2 = @$fielding_team_second_inning->declared?'d':'';
				if ($fielding_team_second_inning){
					$match_status->bowling->score = $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1." &amp;";
					$match_status->bowling->overs = $fielding_team_second_inning->runs."/".$fielding_team_second_inning->wickets.$declared_2;
				} else if ($fielding_team_first_inning) {
					$match_status->bowling->score = $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1;
					$match_status->bowling->overs = "(".$fielding_team_first_inning->overs.")";
				}

				$match_status->bowling->title = $teamB->name;
				$match_status->bowling->title_small = $teamB->short_name;
				$match_status->bowling->flag =  $teamB->full_flag_url;
				$match_status->bowling->color = $teamB->color_hex;

				$match_status->tagline = '<strong>'.$live->title."</strong>";
				if (!empty($live->day)){
					$match_status->tagline .= " | Day ".$live->day;
				}
				if (empty($live->break_type)){
					if (!empty($live->session)){
						$match_status->tagline .= " | Session ".$live->session;
					}
				} else {
					$match_status->tagline .= " | ".$live->break_type;
				}
			}


			$match_status->bowling->onspell = new stdClass();
			$match_status->bowling->onspell->short_name = "-";
			$match_status->bowling->onspell->status = "-";
			$match_status->bowling->offspell = new stdClass();
			$match_status->bowling->offspell->short_name = "-";
			$match_status->bowling->offspell->status = "-";

			if (!empty($bowler_onspell)){
				$match_status->bowling->onspell->short_name = $bowler_onspell->player->short_name."*";
				$match_status->bowling->onspell->name = $bowler_onspell->player->name."*";

				$match_status->bowling->onspell->status = $bowler_onspell->bowler->runs_given."/".$bowler_onspell->bowler->wickets_taken." (".$bowler_onspell->bowler->overs_bowled.")";
				$match_status->bowling->onspell->overs = $bowler_onspell->bowler->overs_bowled;
				$match_status->bowling->onspell->runs = $bowler_onspell->bowler->runs_given;
				$match_status->bowling->onspell->wickets = $bowler_onspell->bowler->wickets_taken;

				$match_status->bowling->onspell->tahir = $bowler_onspell->bowler;

			}

			if (!empty($bowler_offspell)){
				$match_status->bowling->offspell->short_name = $bowler_offspell->player->short_name;
				$match_status->bowling->offspell->status = $bowler_offspell->bowler->runs_given."/".$bowler_offspell->bowler->wickets_taken." (".$bowler_offspell->bowler->overs_bowled.")";

				$match_status->bowling->offspell->short_name = $bowler_offspell->player->short_name;
				$match_status->bowling->offspell->name = $bowler_offspell->player->name;

				$match_status->bowling->offspell->status = $bowler_offspell->bowler->runs_given."/".$bowler_offspell->bowler->wickets_taken." (".$bowler_offspell->bowler->overs_bowled.")";
				$match_status->bowling->offspell->overs = $bowler_offspell->bowler->overs_bowled;
				$match_status->bowling->offspell->runs = $bowler_offspell->bowler->runs_given;
				$match_status->bowling->offspell->wickets = $bowler_offspell->bowler->wickets_taken;
			}
			$responseArray["live_matches"][] = $match_status;
		}

		header('Content-Type: application/json');
		echo json_encode($responseArray);
	}

	public function old_updateHomeLiveMatchScore(){

		$path = BACKEND."live_matches?telco=ufone";
		//$path = "http://staging.cric-live.net:8080/api/live_matches_temp";
		$response = curl_call($path);
		$response = json_decode($response);

		$teams = array();
		foreach($response->live_matches as $LiveMatch){
			if (!empty($LiveMatch->innings)){
				$inning = $LiveMatch->innings[0];
				if ($inning->batting_team_id == $LiveMatch->team_1_id){
					$teams[$inning->batting_team_id] = $LiveMatch->team_1->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_2->team;
				} else {
					$teams[$inning->batting_team_id] = $LiveMatch->team_2->team;
					$teams[$inning->fielding_team_id] = $LiveMatch->team_1->team;
				}
			}
		}
		$this->memcached_library->set("liveMatchTeams", $teams, 21600); //Six Hours

		$responseArray = array();
		$responseArray["live_matches"] = array();

		foreach($response->live_matches as $live){
			//$players = $this->memcached_library->get("liveMatchPlayers".$live);
			//$teams = $this->memcached_library->get("liveMatchTeams");

			$match_status = new stdClass();
			$match_status->match_id = $live->id;
			$match_status->format = $live->format;
			$match_status->batting = new stdClass();
			$match_status->bowling = new stdClass();
			//$match_status->latest_balls = '<div class="b-grey ball-circle"></div>';
			$match_status->latest_balls = "";

			$innings = $live->innings;
			$current_inning = count($innings)-1;

			$batting_team_id = $innings[$current_inning]->batting_team_id;
			$fielding_team_id = $innings[$current_inning]->fielding_team_id;

			$teamA = $teams[$batting_team_id];
			$teamB = $teams[$fielding_team_id];

			$batsman_onstrike = "";
			$batsman_offstrike = "";

			$bowler_onspell = "";
			$bowler_offspell = "";

			if (!empty($live->partnership_and_bowlers)){
				if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="facing"){
					$batsman_onstrike = $live->partnership_and_bowlers->batsman_1;
				} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="facing"){
					$batsman_onstrike = $live->partnership_and_bowlers->batsman_2;
				}

				if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="batting"){
					$batsman_offstrike = $live->partnership_and_bowlers->batsman_1;
				} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="batting"){
					$batsman_offstrike = $live->partnership_and_bowlers->batsman_2;
				}

				if (!empty($live->partnership_and_bowlers->bowler)){
					$bowler_onspell = $live->partnership_and_bowlers->bowler;
				}
				if (!empty($live->partnership_and_bowlers->last_bowler)){
					$bowler_offspell = $live->partnership_and_bowlers->last_bowler;
				}
			}


			if (empty($live->innings[$current_inning]->run_rate) || $live->innings[$current_inning]->run_rate==''){
				$live->innings[$current_inning]->run_rate = "0.0";
			}

			$live->innings[$current_inning]->overs = empty($live->innings[$current_inning]->overs)?"0.0":$live->innings[$current_inning]->overs;
			$live->innings[$current_inning]->runs = empty($live->innings[$current_inning]->runs)?0:$live->innings[$current_inning]->runs;
			$live->innings[$current_inning]->wickets = empty($live->innings[$current_inning]->wickets)?0:$live->innings[$current_inning]->wickets;

			if ($live->format!='Test'){
				$match_status->batting->score = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets;
				$match_status->batting->overs = $live->innings[$current_inning]->overs;
				$match_status->html_title = $match_status->batting->score." | ".$match_status->batting->overs."ov";
			} else {
				$batting_team_previous_innings = false;
        		foreach($innings as $inn){
        			if ($inn->batting_team_id==$batting_team_id && $inn->id!=$innings[$current_inning]->id){
        				$batting_team_previous_innings = $inn;
						break;
        			}
        		}
				if ($batting_team_previous_innings){
					$declared = $batting_team_previous_innings->declared?'d':'';
					$match_status->batting->score = $batting_team_previous_innings->runs."/".$batting_team_previous_innings->wickets.$declared." &amp;";
					$match_status->batting->overs = "<strong>".$live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets."</strong> (".$live->innings[$current_inning]->overs." ov)";

					$match_status->html_title = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets." | ".$live->innings[$current_inning]->overs."ov";
				} else {
					$match_status->batting->score = $live->innings[$current_inning]->runs."/".$live->innings[$current_inning]->wickets;
					$match_status->batting->overs = "(".$live->innings[$current_inning]->overs." ov)";

					$match_status->html_title = $match_status->batting->score." | ".$match_status->batting->overs."ov";
				}
			}

			$match_status->batting->title = $teams[$live->innings[$current_inning]->batting_team_id]->name;
			$match_status->batting->title_small = $teams[$live->innings[$current_inning]->batting_team_id]->short_name;
			$match_status->batting->flag =  $teams[$live->innings[$current_inning]->batting_team_id]->full_flag_url;
			$match_status->batting->run_rate = $live->innings[$current_inning]->run_rate;
			$match_status->batting->color = $teams[$live->innings[$current_inning]->batting_team_id]->color_hex;

			$match_status->batting->id = $live->innings[$current_inning]->batting_team_id;

			$match_status->batting->onstrike = new stdClass();
			$match_status->batting->onstrike->short_name = "-";
			$match_status->batting->onstrike->score = "-";
			$match_status->batting->offstrike = new stdClass();
			$match_status->batting->offstrike->short_name = "-";
			$match_status->batting->offstrike->score = "-";

			if (!empty($batsman_onstrike)){
				$batsman_onstrike->batsman->balls_played = empty($batsman_onstrike->batsman->balls_played)?0:$batsman_onstrike->batsman->balls_played;

				$match_status->batting->onstrike->short_name = empty($batsman_onstrike->player->short_name)? '-': $batsman_onstrike->player->short_name."*";
				$match_status->batting->onstrike->name = empty($batsman_onstrike->player->name)?'-' : $batsman_onstrike->player->name."*";

				$match_status->batting->onstrike->score = empty($batsman_onstrike->batsman->runs_scored)? '0' : $batsman_onstrike->batsman->runs_scored;

				$match_status->batting->onstrike->balls = empty($batsman_onstrike->batsman->balls_played)? '0' : $batsman_onstrike->batsman->balls_played;

				$match_status->batting->onstrike->rate = empty($batsman_onstrike->batsman->strike_rate)? '0.0' : $batsman_onstrike->batsman->strike_rate;

			}

			if (!empty($batsman_offstrike)){
				// $batsman_offstrike->batsman->balls_played = empty($batsman_offstrike->batsman->balls_played)?0:$batsman_offstrike->batsman->balls_played;
				// $match_status->batting->offstrike->short_name = $batsman_offstrike->player->short_name;
				// $match_status->batting->offstrike->score = $batsman_offstrike->batsman->runs_scored ."(".$batsman_offstrike->batsman->balls_played.")";

				$batsman_offstrike->batsman->balls_played = empty($batsman_offstrike->batsman->balls_played)?0:$batsman_offstrike->batsman->balls_played;

				$match_status->batting->offstrike->short_name = empty($batsman_offstrike->player->short_name)? '-': $batsman_offstrike->player->short_name;
				$match_status->batting->offstrike->name = empty($batsman_offstrike->player->name)?'-' : $batsman_offstrike->player->name;

				$match_status->batting->offstrike->score = empty($batsman_offstrike->batsman->runs_scored)? '0' : $batsman_offstrike->batsman->runs_scored;

				$match_status->batting->offstrike->balls = empty($batsman_offstrike->batsman->balls_played)? '0' : $batsman_offstrike->batsman->balls_played;

				$match_status->batting->offstrike->rate = empty($batsman_offstrike->batsman->strike_rate)? '0.0' : $batsman_offstrike->batsman->strike_rate;

			}

			/*
			$req_over = !empty($live->innings[0]->latest_balls[0])?$live->innings[0]->latest_balls[0]->over_number:1;
      		$this_over_balls = array();
			foreach($live->innings[0]->latest_balls as $b){
				if ($b->over_number==$req_over){
					$this_over_balls[] = $b;
				} else {
					break;
				}
			}

			foreach($this_over_balls as $key=>$ball){
				if($key==0){
					$match_status->latest_balls = get_ball_score_status_timeline_scorecard($ball);
				} else {
					$match_status->latest_balls = get_ball_score_status_timeline_scorecard($ball).$match_status->latest_balls;
				}
			}
			*/
			if (!empty($live->partnership_and_bowlers) && !empty($live->partnership_and_bowlers->latest_balls)){
				foreach($live->partnership_and_bowlers->latest_balls as $ball){
					$match_status->latest_balls .= get_ball_score_status_timeline_scorecard($ball);
				}
			}

			if ($live->format!='Test'){
				if (count($live->innings)==1){
					$match_status->bowling->score = "";
					$match_status->bowling->overs = "";
					$match_status->bowling->title = "";
					$match_status->bowling->flag = "";
					$match_status->bowling->color = "";
					$match_status->tagline = ($teams[$live->toss_won_by_id]->short_name)." won the toss and choose to ".($live->chose_to=="Bowl"?"field":"bat")." first";
					$match_status->tagline_small = "Run Rate: ".$live->innings[$current_inning]->run_rate;
				} else {
					$match_status->bowling->score = $live->innings[$current_inning-1]->runs."/".$live->innings[$current_inning-1]->wickets;
					$match_status->bowling->overs = $live->innings[$current_inning-1]->overs;
					$match_status->bowling->run_rate = $live->innings[$current_inning-1]->run_rate;
					$match_status->bowling->title = $teams[$live->innings[$current_inning-1]->batting_team_id]->name;
					$match_status->bowling->title_small = $teams[$live->innings[$current_inning-1]->batting_team_id]->short_name;
					$match_status->bowling->flag =  $teams[$live->innings[$current_inning-1]->batting_team_id]->full_flag_url;
					$match_status->bowling->color = $teams[$live->innings[$current_inning-1]->batting_team_id]->color_hex;

					$match_status->tagline = "Need ".($live->innings[$current_inning]->runs_required+1-$live->innings[$current_inning]->runs)." runs from ".$live->innings[$current_inning]->balls_remaining." balls";
					$match_status->tagline_small = "Req ".($live->innings[$current_inning]->runs_required+1-$live->innings[$current_inning]->runs)." in ".$live->innings[$current_inning]->balls_remaining." balls @ RR:".$live->innings[$current_inning]->required_rate;
					if (!empty($live->match_result)){
						$match_status->tagline = $live->match_result;
						$match_status->tagline_small = $live->match_result;
					}
				}

			} else {

				$fielding_team_first_inning = false;
				$fielding_team_second_inning = false;
        		foreach($innings as $inn){
        			if ($inn->batting_team_id==$fielding_team_id){
        				if (!$fielding_team_first_inning){
        					$fielding_team_first_inning = $inn;
        				} else {
        					$fielding_team_second_inning = $inn;
        				}
        			}
        		}
				$declared_1 = $fielding_team_first_inning->declared?'d':'';
				$declared_2 = @$fielding_team_second_inning->declared?'d':'';
				if ($fielding_team_second_inning){
					$match_status->bowling->score = $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1." &amp;";
					$match_status->bowling->overs = $fielding_team_second_inning->runs."/".$fielding_team_second_inning->wickets.$declared_2;
				} else if ($fielding_team_first_inning) {
					$match_status->bowling->score = $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1;
					$match_status->bowling->overs = "(".$fielding_team_first_inning->overs.")";
				}

				$match_status->bowling->title = $teamB->name;
				$match_status->bowling->title_small = $teamB->short_name;
				$match_status->bowling->flag =  $teamB->full_flag_url;
				$match_status->bowling->color = $teamB->color_hex;

				$match_status->tagline = '<strong>'.$live->title."</strong>";
				if (!empty($live->day)){
					$match_status->tagline .= " | Day ".$live->day;
				}
				if (empty($live->break_type)){
					if (!empty($live->session)){
						$match_status->tagline .= " | Session ".$live->session;
					}
				} else {
					$match_status->tagline .= " | ".$live->break_type;
				}
			}


			$match_status->bowling->onspell = new stdClass();
			$match_status->bowling->onspell->short_name = "-";
			$match_status->bowling->onspell->status = "-";
			$match_status->bowling->offspell = new stdClass();
			$match_status->bowling->offspell->short_name = "-";
			$match_status->bowling->offspell->status = "-";

			if (!empty($bowler_onspell)){
				$match_status->bowling->onspell->short_name = $bowler_onspell->player->short_name."*";
				$match_status->bowling->onspell->name = $bowler_onspell->player->name."*";

				$match_status->bowling->onspell->status = $bowler_onspell->bowler->runs_given."/".$bowler_onspell->bowler->wickets_taken." (".$bowler_onspell->bowler->overs_bowled.")";
				$match_status->bowling->onspell->overs = $bowler_onspell->bowler->overs_bowled;
				$match_status->bowling->onspell->runs = $bowler_onspell->bowler->runs_given;
				$match_status->bowling->onspell->wickets = $bowler_onspell->bowler->wickets_taken;

				$match_status->bowling->onspell->tahir = $bowler_onspell->bowler;

			}

			if (!empty($bowler_offspell)){
				$match_status->bowling->offspell->short_name = $bowler_offspell->player->short_name;
				$match_status->bowling->offspell->status = $bowler_offspell->bowler->runs_given."/".$bowler_offspell->bowler->wickets_taken." (".$bowler_offspell->bowler->overs_bowled.")";

				$match_status->bowling->offspell->short_name = $bowler_offspell->player->short_name;
				$match_status->bowling->offspell->name = $bowler_offspell->player->name;

				$match_status->bowling->offspell->status = $bowler_offspell->bowler->runs_given."/".$bowler_offspell->bowler->wickets_taken." (".$bowler_offspell->bowler->overs_bowled.")";
				$match_status->bowling->offspell->overs = $bowler_offspell->bowler->overs_bowled;
				$match_status->bowling->offspell->runs = $bowler_offspell->bowler->runs_given;
				$match_status->bowling->offspell->wickets = $bowler_offspell->bowler->wickets_taken;
			}
			$responseArray["live_matches"][] = $match_status;
		}

		header('Content-Type: application/json');
		echo json_encode($responseArray);
	}

	function fetch_timeline($match_id, $page=1, $tag=false, $action=false, $timeline_id=false){
		if ($page!=0){
			$path = API_PATH_2."get_new_timeline_items?telco=ufone&match_id=$match_id";
		} else {
			if ($action=="next"){
				$path = API_PATH_2."fetch_new_timeline_next?telco=ufone&match_id=".$match_id."&timeline_id=".$timeline_id;
			} else if ($action=="previous"){
				$path = API_PATH_2."fetch_new_timeline_previous?telco=ufone&match_id=".$match_id."&timeline_id=".$timeline_id;
			}
		}

		if ($tag){
			$path .= "&tag_id=$tag";
		}
		$response = curl_call($path);
		$response = json_decode($response);
		foreach($response->timeline as $key=>$item){
			if(!empty($item->tweet)){
				/*
				if (!empty($item->tweet->urls)){
					$keys = array();
					$values = array();
					foreach($item->tweet->urls as $key=>$value){
						$keys[] = $key;
						$values[] = $value;
					}
					$item->tweet->text = str_replace($keys, $values, $item->tweet->text);
				}
				$item->tweet->created_at = date("M d, Y",strtotime($item->tweet->created_at));
				*/
			}
			if (!empty($item->news)){
				date_default_timezone_set("Asia/Karachi");
				//$date = date("d M, Y".$item->news->created_at);
				$item->news->details = word_limiter(strip_tags($item->news->details), 75);
				$item->news->created_at = date('F j, Y', $item->news->created_at/1000);
				$item->news->seo_url = base_url()."news-detail/".$item->news->id."/".seo_url($item->news->title)."/";
			}

			if (!empty($item->article)){
				$item->article->details = word_limiter(strip_tags($item->article->details), 75);
				$item->article->created_at = date('F j, Y', $item->article->created_at/1000);
				$item->article->seo_url = base_url()."post/".$item->article->id."/".seo_url($item->article->title)."/";
			}

			if (!empty($item->video)){

				$item->video->large_image = urldecode($item->video->large_image);
				$item->video->med_image = urldecode($item->video->med_image);
				$item->video->thumb_image = urldecode($item->video->thumb_image);
				$item->video->seo_url = seo_url($item->video->title);

			}

			$tags = [];
            $tag0 = [];
            $tag1 = [];
            $two_tags=0;
            
            if($item->ball->boundary_4){ $two_tags=1; $tag0["name"]="Four"; $tag0["color_code"]="#6db21e"; }
            if($item->ball->boundary_6){ $two_tags=1; $tag0["name"]="Six"; $tag0["color_code"]="#55acee";}
            if($item->ball->wicket){ $two_tags=1; $tag0["name"]="Out"; $tag0["color_code"]="#d71920";}
            
            $tag1 = array("name"=>"Ball", "color_code"=>"#878b96");
            if($two_tags){
                $tags[] = $tag0;
                $tags[] = $tag1;
            }else{
                $tags[] = $tag1;
            }
            $tags = json_decode(json_encode($tags));
            $item->tags = [];
            $item->tags = $tags;

			if (!empty($item->tags)){
				if ($item->tags[0]->name=="Four" || $item->tags[0]->name=="Six" || $item->tags[0]->name=="Out"){
					if (!empty($item->ball)){
						$item->tags[0]->customized_tag = "<b>".$item->ball->title."</b>&nbsp;|&nbsp;<b>".$item->tags[0]->name."</b>";
					} else {
						$item->tags[0]->customized_tag = "<b>".$item->tags[0]->name."</b>";
					}
				} else if ($item->tags[0]->name=="Ball"){
					$item->tags[0]->customized_tag = "<b>".$item->ball->title."</b>&nbsp;|&nbsp;".get_ball_score_status_timeline_tag($item->ball);
				}
			}

			$response->timeline[$key] = $item;
		}
		$response = json_encode($response);
		header("Content-type: text/json");
		echo $response;
	}

	function getBall2BallUpdate($last_ball_updated){

		$match_id = $this->xession->get("ball2ballMatchId");
		$this->data["players"] = $this->xession->get("ball2ballPlayers");

		$url = API_PATH."get_comp_match_commentry?match_id=$match_id";
		//$url = base_url()."test.json";
		$response = curl_call($url);
		$response = json_decode($response);
		if (count($response->inns)==1){
			$this->data["current_inning"] = $response->inns[0];
		} else {
			$this->data["current_inning"] = $response->inns[1];
		}
		$this->data["batsman"] = $this->data["current_inning"]->inn->batsmen_on_pitch;
		$this->data["bowlers"] = $this->data["current_inning"]->inn->bowlers_on_spell;

		$this->data["first_inning"] = false;
		$this->data["second_inning"] = false;
		if (count($response->inns)==1){
			$this->data["current_inning"] = $response->inns[0];
			$this->data["first_inning"] = $response->inns[0]->inn;
		} else {
			$this->data["current_inning"] = $response->inns[1];
			$this->data["first_inning"] = $response->inns[0]->inn;
			$this->data["second_inning"] = $response->inns[1]->inn;
		}

		$this->data["last_ball_updated"] = $last_ball_updated;

		$commentry_html = $this->load->view("live/ball2ball_ajax_response", $this->data, true);
		$batsman_bowler_top_card = $this->load->view("live/ball_2_ball_top_card_ajax_response", $this->data, true);
		$team_over_all_score_update = $this->load->view("live/ball2ball_team_overall_score_ajax_response", $this->data, true);

		$last_ball_updated = $this->data["current_inning"]->balls[0]->id;
		$response_array = array("status"=>1, "last_ball_updated"=>$last_ball_updated, "commentry_html"=>$commentry_html);
		$response_array["batsman_bowler_top_card"] = $batsman_bowler_top_card;
		$response_array["team_over_all_score_update"] = $team_over_all_score_update;

		echo json_encode($response_array);
	}

	function get_match_content_stats($match_id){
		$url = API_PATH."get_match_highlight_tags?match_id=".$match_id;
		$response = curl_call($url);
		$tags_stats = json_decode($response);
		$responseArray = array();
		$responseArray["t_six"] = 0;
		$responseArray["t_four"] = 0;
		$responseArray["t_out"] = 0;
		$responseArray["t_all"] = 0;
		$responseArray["t_boom"] = 0;
		$responseArray["t_total"] = 0;

		if (isset($tags_stats->{1})) {
			$responseArray["t_six"] = $tags_stats->{1};
			$responseArray["t_total"] += $responseArray["t_six"];
		}
		if (isset($tags_stats->{2})) {
			$responseArray["t_four"] = $tags_stats->{2};
			$responseArray["t_total"] += $responseArray["t_four"];
		}
		if (isset($tags_stats->{3})) {
			$responseArray["t_out"] = $tags_stats->{3};
			$responseArray["t_total"] += $responseArray["t_out"];
		}
		if (isset($tags_stats->{23})) {
			$responseArray["t_boom"] = $tags_stats->{23};
			$responseArray["t_total"] += $responseArray["t_boom"];
		}
		foreach($tags_stats as $key=>$val){
			if ($key!=1 && $key!=2 && $key!=3 && $key!=23){
				$responseArray["t_all"] += $val;
			}
		}
		$responseArray["t_total"] += $responseArray["t_all"];
		echo json_encode($responseArray);
	}

	function confirm_timeline_view($video_id){
		// $url = API_PATH."update_video_view/$video_id";

		$url = "http://contentserver.cricwick.net/userapi/update_only_video_views?id=$video_id";
		curl_call($url);
	}

	function related_video_rss($video_id){
		$url = API_PATH."get/video/timeline/related/$video_id?per_page=12";
		$response = curl_call($url);
		$response = json_decode($response);
		$responseArray = array();
		/*
		$xml = '<rss version="2.0" xmlns:jwplayer="http://rss.jwpcdn.com/"><channel>';
		foreach($response->related_videos as $related){
			$xml .= '<item>';
			$xml .= '<title>'.$related->title.'</title>';
			$xml .= '<link>'.base_url()."highlights/".$related->id."/".seo_url($related->title)."/".'</link>';
			$xml .= '<media:thumbnail url="'.$related->med_image.'"/>';
			$xml .= '<guid isPermalink="false">'.$related->id.'</guid>';
			$xml .= '</item>';
		}
		$xml .= '</channel></rss>';
		header("Content-type: text/xml");
		echo $xml;
		*/

		foreach($response->related_videos as $related){
			$array = array();
			$array["title"] = $related->title;
			$array["link"] = base_url()."highlights/".$related->id."/".seo_url($related->title)."/";
			$array["image"] = $related->med_image;
			$array["mediaid"] = $related->id;
			//$array["file"] = $related->video_file;
			$responseArray[] = $array;
		}
		header("Content-type: text/json");
		echo json_encode($responseArray);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
