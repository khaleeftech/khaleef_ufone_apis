<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Fixtures extends My_Controller {

	function __construct(){
		parent::__construct();
	}

	function index($page_no=1){

		//$this->check_session();
		//$this->check_subscription();

		$this->data["fixtures"] = json_decode($this->more_fixtures($page_no));
		$this->data["add_video_id_to_body"] = 1;

		$this->data["page_heading"] = "Fixtures";
		$this->data["inner_page"] = $this->load->view('april19/fixtures/list', $this->data, true);

		$this->data["page"] = $this->load->view("april19/template_wd_left_col", $this->data, true);

		$this->load->view('april19/template', $this->data);

	}

	function more_fixtures($page_no=1, $page_size=12){

		$url = BACKEND."upcoming_matches/$page_no/$page_size";
		$response = curl_call($url);
		$response = json_decode($response);
		foreach($response->matches as $key=>$fixture){
			$local = format_date_newserver2($fixture->match_start);
			//$local_time = $local['date']." ".$local["time"];
			$response->matches[$key]->local_time = $local;
			$response->matches[$key]->team_1->team->flag_url = $response->matches[$key]->team_1->team->full_flag_url;
			$response->matches[$key]->team_2->team->flag_url = $response->matches[$key]->team_2->team->full_flag_url;
		}
		$response = json_encode($response);
		if ($this->input->is_ajax_request()){
			echo $response;
		} else {
			return $response;
		}

	}

}
