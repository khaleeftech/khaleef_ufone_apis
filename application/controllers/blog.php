<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Blog extends My_Controller {
	
	function __construct(){
		parent::__construct();
	}
	 
	
	function index($page_no=1){
		
		$this->check_session();
		$this->check_subscription();
		
		$this->data["articles"] = json_decode($this->more_blog($page_no))->articles;
		$this->data["page_heading"] = "Blogs";
		$this->data["inner_page"] = $this->load->view('blog/list', $this->data, true);
		
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$this->load->view('template', $this->data);
		
	}
	
	function more_blog($page_no=1, $page_size=12){
		
		$url = API_PATH."get_articles?page=$page_no&per_page=$page_size";
		$response = curl_call($url);
		$response = json_decode($response);
		foreach($response->articles as $key=>$article){
			$article->details = trimer($article->details, 200);
			$article->created_at = date('F j, Y', $article->created_at/1000);
			$article->seo_url = base_url()."post/".$article->id."/".seo_url($article->title)."/";
			$response->articles[$key] = $article;
		}
		$response = json_encode($response);
		if ($this->input->is_ajax_request()){
			echo $response;
		} else {
			return $response;
		}
	}
	
	function post($article_id, $seokey=false){
		
		$this->check_session();
		$this->check_subscription();
		
		$url = API_PATH."get/article/".$article_id;
		$response = curl_call($url);
		$response = json_decode($response);
		$this->data["current_post"] = $response->article;
		$this->data["current_post"]->seo_url = base_url()."post/".$response->article->id."/".seo_url($response->article->title)."/";
		$this->data["more_posts"] = json_decode($this->more_blog(1, 12))->articles;
		$this->data["inner_page"] = $this->load->view('april19/blog/post', $this->data, true);
	
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		
		$this->data["add_video_id_to_body"] = true;
		
		$default_social = new stdClass();
		$default_social->link = current_url()."/";
		$default_social->title =  $response->article->title." - CricBoom.com";
		$default_social->descr = trimer($response->article->details,200);
		$default_social->image = $response->article->large_image;
		$this->data["og_tags"] = $default_social;
		
		$this->load->view('template', $this->data);
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */