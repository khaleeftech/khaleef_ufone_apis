<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Quiz extends My_Controller {
	
	function __construct(){
		parent::__construct();

	}
	
	function get_quiz_status(){
		$url = API_PATH."get_user_quiz_status?web_user=1&phone=".$this->data["phone_no"];
		$curl = curl_init($url);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl,CURLOPT_HEADER, 0 );
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($curl);
		curl_close ( $curl );
		$response = json_decode($response);
		return $response;
	}
	
	function check_quiz_subscription(){
		$response = $this->appstart();
		if ($response->user->subscribe_status_quiz!=1){
			redirect("quiz/subscribe_quiz");
			die();
		}
	}
	 
	function index(){
		
		$this->check_session();
		$this->check_quiz_subscription();
		$response = $this->get_quiz_status();
		
		if (!$response->joined_quiz){
			$response = $this->submit_quiz_to_server(1);
			$response = json_decode($response);
		} 
		
		$this->data["stats"] = $response->quiz_stats;
		$this->data["finished"] = $response->quiz_stats->finished;
		
		$this->data["inner_page"] = $this->load->view("quiz/play_quiz", $this->data, true);

		$this->data["page_heading"] = "Play Quiz";
		$this->data["banner_heading"] = "Pakistan Supper League";
		$this->data["banner_tagline"] = "4th Feb 2016 to  23rd Feb 2016";
		$this->data["banner_img_class"] = "back-img";
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		$this->load->view("template", $this->data);
	}
	
	function subscribe_quiz(){
		
		if ($this->input->is_ajax_request()){
			
			$xession = $this->xession->get("user");
			$params = array();
			
			$params["pincode"] = $xession->pin;
			$params["service_id"] = 63;
			$params["web_user"] = 1;
			$params["phone"] = $this->data["phone_no"];
			$url = API_PATH."confirm/pin?";
			$url = $url.http_build_query($params);
			
			$response = curl_call($url);
			$response = json_decode($response);
			
			if (!empty($response->hit_subscribe)){
				$params["udid"] = $this->data["udid"];
				$url = API_PATH."api_hit_subscribe?";
				$url = $url.http_build_query($params);
				curl_call($url);
				
				sleep(10);
			}
			unset($params["udid"]);
			$url = API_PATH."appstart?";
			$url = $url.http_build_query($params);
			echo $response = curl_call($url);
			exit;
		}
		$this->data["inner_page"] = $this->load->view("quiz/sub_quiz", $this->data, true);
		$this->data["page_heading"] = "Subscribe Quiz";
		$this->data["banner_heading"] = "Pakistan Supper League";
		$this->data["banner_tagline"] = "4th Feb 2016 to  23rd Feb 2016";
		$this->data["banner_img_class"] = "back-img";
		$this->data["page"] = $this->load->view("template_wd_left_col", $this->data, true);
		$this->load->view("template", $this->data);
	}
	
	function submit_ans(){
		$response = $this->get_quiz_status();
		$user_ans = $this->input->post("answer");
		$answer_status = "";
		foreach($response->quiz_stats->last_asked_question->answers as $answer){
			if ($answer->answer_pos==$user_ans){
				$answer_status = $answer->correct_answer;
				break;
			}
		}
		$server_response = $this->submit_quiz_to_server($user_ans);
		$server_response = json_decode($server_response);
		$answer_status = $answer_status?"1":"0";
		$array = array("status"=>$answer_status, "score"=>$server_response->quiz_stats->score,"rank"=>$server_response->quiz_stats->rank);
		echo json_encode($array);
	}
	
	private function submit_quiz_to_server($answer){
	
		$fields = array("web_user"=>1, "udid"=>$this->data["udid"], 'answer_pos'=>$answer);
		$url = API_PATH."submit_quiz_answer";
		$curl = curl_init($url);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl,CURLOPT_HEADER, 0 );
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($fields));
		$response = curl_exec($curl);
		curl_close ( $curl );
		return $response;
	}

}

/* End of file quiz.php */
/* Location: ./application/controllers/quiz.php */