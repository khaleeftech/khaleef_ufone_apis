<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/My_Controller.php';

class Articles extends My_Controller {

	function __construct(){
		parent::__construct();
	}


	function index($page_no=1){

		// $this->check_session();
		// $this->check_subscription();

		$this->data["articles"] = json_decode($this->more_articles($page_no))->data;
		// display_json(json_encode($this->data["articles"]));
		// exit;
		$this->data["page_heading"] = "Articles";
		$this->data["inner_page"] = $this->load->view('april19/articles/articles', $this->data, true);

		$this->data["page"] = $this->load->view("april19/template_wd_left_col", $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$this->load->view('april19/template', $this->data);

	}

	function more_articles($page_no=1, $page_size=6){

		$url = BACKEND."articles?page=$page_no&page_size=$page_size&telco=".CONTENT_TELCO;
		// $url = API_PATH."get_news?page_number=$page_no&page_size=$page_size";
		$response = curl_call($url);
		$response = json_decode($response);


		foreach($response->data as $key=>$item){
			$item->details = trimer(strip_tags($item->body), 150);
			$item->created_at = date('F j, Y', strtotime($item->created_at) );
			$item->seo_url = base_url()."article/".$item->id."/".seo_url($item->title)."/";
			$item->image =  $item->full_image;
			$item->tahir = $url;
			$response->data[$key] = $item;
		}
		$response = json_encode($response);
		if ($this->input->is_ajax_request()){
			echo $response;
			return;
		} else {
			return $response;
		}
	}

	function article_detail($news_id, $seokey=false){

		$this->check_session();
		$this->check_subscription();

		$url = BACKEND."articles/$news_id";
		// $url = API_PATH."get_single_news?id=$news_id";
		$curl = curl_init($url);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl,CURLOPT_HEADER, 0 );
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($curl);
		curl_close ( $curl );
		$response = json_decode($response);

		$this->data["current_article"] = [];
		if(!empty($response)){
			$this->data["current_article"] = $response;
			$this->data["current_article"]->seo_url = base_url()."article/".$response->id."/".seo_url($response->title)."/";
		}

		$this->data["more_articles"] = [];
		if(!empty($this->more_articles(1, 8))){
			$this->data["more_articles"] = json_decode($this->more_articles(1, 8))->data;
		}
		
		$this->data["inner_page"] = $this->load->view('april19/articles/article_detail', $this->data, true);
		$this->data["page"] = $this->load->view("april19/template_wd_left_col", $this->data, true);
		$this->data["add_video_id_to_body"] = true;
		$default_social = new stdClass();
		$default_social->link = current_url()."/";
		$default_social->title =  $response->title." - CricWick";
		$default_social->descr = trimer(strip_tags($response->body),200);
		$default_social->image =  $response->full_image;
		$this->data["og_tags"] = $default_social;
		$this->load->view('april19/template', $this->data);

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
