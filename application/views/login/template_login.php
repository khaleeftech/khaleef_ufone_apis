
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CricBoom</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta name="keywords" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <link href="<?php echo assets_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="<?php echo assets_url(); ?>css/fonts/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo assets_url(); ?>css/animate.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo assets_url(); ?>css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo assets_url(); ?>css/jquery.jscrollpane.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo assets_url(); ?>css/component.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo assets_url(); ?>css/style_dir.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="img/favicon.ico" />
    <link href="<?php echo assets_url(); ?>css/responsive.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="cric_wrapper">
	<section class="box-logo">
	  <header class="container">
	    <div class="content-logo col-md-12">
	      <div class="logo"> <img src="<?php echo assets_url(); ?>images/cricboom-logo.png" alt="" /> </div>
	      <?php if ($this->xession->get("user")){ ?>
	      <div class="bt-menu pull-right"><a href="javascript:;" class="menu"><span>&equiv;</span></a></div>
	      <div class="box-menu">
	        <nav id="cbp-hrmenu" class="cbp-hrmenu">
	          <ul id="menu">
	            <li><a class="lnk-menu" href="<?php echo base_url(); ?>">Home</a></li>
	            <li><a class="lnk-menu" href="<?php echo base_url("quiz"); ?>">PSL Quiz</a></li>
	            <li><a class="lnk-menu" href="<?php echo base_url("news"); ?>">News</a></li>
	            <li><a class="lnk-menu" href="<?php echo base_url("videos"); ?>">Videos</a></li>
	            <li><a class="lnk-menu" href="<?php echo base_url("logout"); ?>">Logout</a></li>
	          </ul>
	        </nav>
	      </div>
	      <?php } ?>
	    </div>
	  </header>
	</section>
	<section class="drawer mb30">
	<div class="col-md-12 size-img back-img">
		<div class="effect-cover">
			<h3 class="txt-advert animated">CricBoom</h3>
			<p class="txt-advert-sub">
				News - Match - Player
			</p>
		</div>
	</div>
	<div class="clear"></div>
	<div class="section_top_trns">
		<div class="container" style="margin-top: -60px;">
			<div class="top-match col-xs-12 col-md-12">
				<div class="news-content">
					<div class="row">
						<div class="col-md-12 ">
							<div class="cs-section-title no_style">
								<h2 class="no_style"><?php echo $page_heading; ?></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section id="login" class="container secondary-page">
		<div class="general general-results players">
			<?php echo $page; ?>
		</div>
		</section>
	</div>
	</section>  
</div>
<footer>
  <div class="col-md-12 content-footer">
	<p>&copy; <?php echo date("Y"); ?> cricwick.com. All rights reserved. </p>
  </div>
</footer>

<script src="<?php echo assets_url(); ?>js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="<?php echo assets_url(); ?>js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo assets_url(); ?>js/bootstrap.js" ></script> 
<!--MENU-->
<script src="<?php echo assets_url(); ?>js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo assets_url(); ?>js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="<?php echo assets_url(); ?>js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="<?php echo assets_url(); ?>js/custom.js" type="text/javascript"></script> 
<script>

var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	$("body").on("submit","#login-form", function(){
		$("#login-form #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/check_login/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					//alert(response.message);
					window.location = base_url;
				} else {
					alert(response.message);
					if (response.clear_phone){
						$("#phone").val("");
					}
					$("#password").val("");
					$("#login-form #btnSubmit").button("reset");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("submit","#register-phone", function(){
		$("#register-phone #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/register_mobile/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					alert(response.message);
					window.location = base_url+"login";
				} else {
					alert(response.message);
					$("#register-phone #btnSubmit").button("reset");
				}
				
			}
		});
		return false;
	});
	
	$("body").on("submit","#sub-now", function(){
		$("#btnSubmit").button("loading");		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'home/sub/',
			data: [],
			success: function(response) {
				if(response.user.subscribe_status==1){
					window.location = base_url;
				} else if(response.user.subscribe_status==2){
					
					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");	
					
				} else if(response.user.subscribe_status==3){
					
					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");	
				} else {
					alert("<?php echo lang("general_error"); ?>");
					$("#btnSubmit").button("reset");	
				}
			}
		});
		return false;
	});
	
	$("body").on("submit","#forget-pass", function(){
		$("#btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/forget_password/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					alert(response.message);
					window.location = base_url+"login";
				} else {
					alert(response.message);
					$("#btnSubmit").button("reset");
				}
				
			}
		});
		return false;
	});
	
	
});
 </script>
</body>
</html>
