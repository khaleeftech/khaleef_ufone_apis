<!-- LOGIN BOX -->
<div class="top-score-title right-score col-md-12 col-xs-12 pt20" style="height: 85vh;">
	<div class="col-xs-12">
		<div class="row">
			<div class="btn btn-group pull-right">
				<a href="<?php echo current_url()."?lang=english"; ?>" class="login btn btn-sm <?php echo ($this->xession->get("language")=='urdu')?'btn-default':"btn-primary"; ?>">English</a>
				<a href="<?php echo current_url()."?lang=urdu"; ?>" class="login btn btn-sm <?php echo ($this->xession->get("language")!='urdu')?'btn-default':"btn-primary"; ?>">اردو</a>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="cs-section-title">
				<h2><?php echo lang("register_or_login"); ?></h2>
			</div>
		</div>
	</div>
	<div class="col-md-12 login-page">
		<form method="post" class="login-form" id="phone-form">
			<div class="pt10"></div>
			
			<div class="name">
				<label for="name_login"><?php echo lang("enter_phone_number"); ?></label>
				<div class="clear">
				</div>
				<input id="phone" name="phone" type="text" placeholder="966xxxxxxxxx/05xxxxxxxx" value="<?php echo $phone_number; ?>"/>
				
			</div>
			<div id="login-submit">
				<input type="submit" id="btnSubmit" value="<?php echo lang("btn_continue"); ?>" data-loading-text="<?php echo lang("label_please_wait"); ?>"/>
			</div>
			<BR>
				<i>*Billing enabled by <img src="<?php echo base_url()."assets/img/logo_small.jpg" ?>" style="width: 45px;"/></i>
		</form>
	</div>
</div>
<!--Close Login-->


<div class="clear"></div>