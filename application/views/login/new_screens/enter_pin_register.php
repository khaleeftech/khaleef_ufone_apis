<!-- LOGIN BOX -->
<div class="top-score-title right-score col-md-12 col-xs-12 pt20" style="height: 85vh;">
	<div class="col-xs-12">
		<div class="row">
			<div class="btn btn-group pull-right">
				<a href="<?php echo current_url()."?lang=english"; ?>" class="login btn btn-sm <?php echo ($this->xession->get("language")=='urdu')?'btn-default':"btn-primary"; ?>">English</a>
				<a href="<?php echo current_url()."?lang=urdu"; ?>" class="login btn btn-sm <?php echo ($this->xession->get("language")!='urdu')?'btn-default':"btn-primary"; ?>">اردو</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="cs-section-title">
				<h2><?php echo lang("label_register"); ?></h2>
			</div>
		</div>
	</div>
	<div class="col-md-12 login-page">
		<form method="post" class="login-form" id="pin-form-register">
			<div class="pt10"><?php echo lang("login_wd_sms_pin"); ?><BR><BR></div>
			<div class="name">
				<label for="name_login"><?php echo lang("enter_pincode"); ?></label>
				<div class="clear">
				</div>
				<input id="pincode" name="pincode" type="text" placeholder="xxxx" value="" required=""/>
			</div>
			<div id="login-submit">
				<input type="submit" id="btnSubmit" value="<?php echo lang("label_subscribe"); ?>" data-loading-text="<?php echo lang("label_please_wait"); ?>"/>
			</div>
		</form>
	</div>
</div>
<!--Close Login-->


<div class="clear"></div>

<script>

var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	$("body").on("submit","#pin-form-register", function(e){
		$("#pin-form-register #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/confirm_pin_register/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					window.location = response.url;
				} else {
					$('.alert-popup .modal-body').html(response.message);
					$('.alert-popup').modal('show');
					$("#pin-form-register #btnSubmit").button("reset");
				}
			}
		});
		return false;
	});
});
	
</script>