<!-- LOGIN BOX -->
<div class="top-score-title right-score col-md-12 col-xs-12 pt20">
	<div class="row">
		<div class="col-md-12">
			<div class="cs-section-title">
				<h2>Login Now!</h2>
			</div>
		</div>
	</div>
	<div class="col-md-12 login-page">
		<form method="post" class="login-form" id="login-form">
			<div class="name">
				<label for="name_login">Mobile Number:</label>
				<div class="clear">
				</div>
				<input id="phone" name="phone" type="text" placeholder="966xxxxxxxxx/05xxxxxxxx" value="<?php echo $phone_number; ?>"/>
			</div>
			<div class="pwd">
				<label for="password_login">Pincode:</label>
				<div class="clear">
				</div>
				<input id="password" name="password" type="password" placeholder="****"/>
			</div>
			<div id="login-submit">
				<input type="submit" id="btnSubmit" value="Continue" data-loading-text="Please Wait..."/>
			</div>
			<div class="pt10"><a href="<?php echo base_url()."forget-password"; ?>">Forgot Pincode?</a></div>
			<div class="pt10"><a href="<?php echo base_url()."register-1"; ?>">New User? Subscribe Now!</a></div>
		</form>
	</div>
</div>
<!--Close Login-->


<div class="top-score-title right-score col-md-6 col-xs-12 pt20 hidden">
	<div class="row">
		<div class="col-md-12">
			<div class="cs-section-title">
				<h2>New User? Subscribe Now!</h2>
			</div>
		</div>
	</div>
	<div class="col-md-12 login-page">
		<form method="post" class="login-form" id="register-phone">
			<div class="name">
				<label for="pin">Mobile Number: </label>
				<div class="clear">
				</div>
				<input id="register_phone" name="register_phone" type="text" placeholder="966xxxxxxxxx/05xxxxxxxx">
			</div>
			<div id="login-submit">
				<input type="submit" id="btnSubmit" value="Subscribe!" data-loading-text="Please Wait...">
			</div>
		</form>
	</div>
</div>

<div class="clear"></div>