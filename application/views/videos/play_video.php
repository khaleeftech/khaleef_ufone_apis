<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<script>jwplayer.key="vkFxGOqRxYPKvhFSCw2QvpCcM3C7Aii/jSTZ/w==";</script>
<div class="top-score-title col-md-9">
	<div class="row">
		<div class="col-md-12">
			<div class="cs-section-title">
				<h2><?php echo $current_video->title; ?></h2>
			</div>
		</div>
	</div>
	<div class="col-md-12 news-video">
		<div class="row">
			<div id="videoElement"></div>
			<script type="text/javascript">
			var playerInstance = jwplayer("videoElement");
			var video_allowed = <?php echo $video_allowed; ?>;
			playerInstance.setup({
				playlist: <?php echo $playlist; ?>,
			    width: "100%",
			    aspectratio: "16:9",
			    controls: true,
			    autostart: true,
			    title: "<?php echo $current_video->title; ?>",
			    ga: {
					label: "title"
				},
			    sharing: {
			      sites: ['facebook','twitter','googleplus'],
			      link: "<?php echo base_url()."video/".$current_video->id."/".seo_url($current_video->title)."/"; ?>",
			   	}
			}).onPlay(function() {
				if (video_allowed==0){
					jwplayer("videoElement").stop();
					alert('Sorry, this content is not available in your country!');
				}
				
			});
			</script>
			<div class="cric-lockup-meta">
              <ul class="cric-lockup-meta-info mt5 pb5 pull-right views hidden">
                <li>&nbsp;</li>
                <li>&nbsp;</li>
              </ul>
            </div>
		</div>
	</div>
	<div class="video-desc hidden">
		<p class="video-arg"><?php echo $current_video->description; ?></p>
	</div>
	<div class="col-md-12 video-desc">
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<div class="cs-section-title">
						<h2>Related Videos</h2>
					</div>
				</div>
			</div>
			<div class="gallary col-md-12 pb20">
				<div class="row">
					<div class="row" id="videoListContainter">
						<?php foreach($more_videos as $video){ 
							if ($video->id!=$current_video->id){
							?>
							<div class="col-xs-6 col-sm-4 col-md-4 pt20">
								<a href="<?php echo base_url()."video/".$video->id."/".seo_url($video->title)."/"; ?>" >
									<img src="<?php echo $video->thumb_file; ?>" alt=""></a>
								<div class="cric-lockup-content">
									<h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom"><?php echo $video->title; ?></h3>
									<div class="cric-lockup-meta">
										<ul class="cric-lockup-meta-info pl5 mt0 pb5 hidden">
											<li>&nbsp;</li>
											<li>&nbsp;</li>
										</ul>
									</div>
								</div>
							</div>						
						<?php 
							}
						} 
						?>
					</div>
					<div class="clear"></div>
					<div class="pt20 pb20">
	                  <input type="button" id="loadMoreVidosList" data-page-to-load="2" data-loading-text="Please Wait..." class="btn-more full-width" value="Load More">
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>