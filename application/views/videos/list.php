<div class="wrapper-main">
	<section id="quiz" class="container secondary-page">
	<div class="general general-results players pt30">
		<div class="top-score-title col-md-9">
			<div class="col-md-12">
				<div class="cs-section-title">
					<h2><?php echo $specials[0]->title; ?></h2>
				</div>
			</div>
			<div class="col-md-12 news-video">
				<div id="videoElement">
					<a href="<?php echo $specials[0]->seo_url; ?>"><img width="100%" src="<?php echo $specials[0]->med_image; ?>" alt=""></a>
				</div>
				<div class="video-desc hidden">
					<span style="background: #fff !important;" class="entry-date pl10"><span class="entry-meta-date"><time><b>11.5</b>&nbsp;|&nbsp;<b>Out</b></time></span></span>
					<p class="video-arg">
						Sabbir Rahman to Ajay Lalcheta. Doesn't connect well with this one and Lalcheta sends it high into the air and square leg catches it easy.
					</p>
				</div>
				<div class="cric-lockup-meta">
					<ul class="cric-lockup-meta-info mt5 pb5 pull-right views">
						<li style="list-style:none"><?php echo $specials[0]->views; ?> views</li>
					</ul>
					
					<div class="socialmedia" tabindex="1000">
						<ul class="mt5 pl0">
							<li><a data-original-title="Facebook" class="social_sharing_link" title="Facebook" href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u=<?php echo $specials[0]->seo_url; ?>&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a></li>
							<li><a data-original-title="twitter" class="social_sharing_link" title="Tweet" href="http://www.twitter.com/share?text=<?php echo $specials[0]->title; ?>&url=<?php echo $specials[0]->seo_url; ?>"><i class="fa fa-twitter"></i></a></li>
							<li><a data-original-title="google-plus" class="a" target="_blank" title="Google Bookmark" href="https://plus.google.com/share?url=<?php echo $specials[0]->seo_url; ?>"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Left welcome-->
		<!--right column-->
		<div class="col-md-3 right-column">
			<aside class="top-score-title col-md-12 right-title pt20">
			<div class="widget element-size-100 widget_team">
				<div class="widget-section-title">
					<h2>related</h2>
				</div>
				<ul class="pt45">
					<?php 
						unset($specials[0]);
						foreach($specials as $key=>$special){
							if ($key>7){
								break;
							}
						?>
						<li>
						<figure><a href="<?php echo $special->seo_url; ?>"><img src="<?php echo $special->thumb_image; ?>" alt=""></a></figure>
						<div class="infotext">
							<a href="<?php echo $special->seo_url; ?>"><?php echo $special->title; ?></a>
							<p><?php echo $special->views; ?> views</p>
						</div>
						</li>
					<?php } ?>
				</ul>
			</div>
			</aside>
		</div>
	</div>
	<div class="general general-results players">
		<!--Left welcome-->
		<div class="top-score-title right-score col-md-12">
			<?php foreach($sections as $section){ ?>
			<div class="col-md-12">
				<div class="cs-section-title mb0">
					<h2><?php echo $section->label; ?></h2>
				</div>
			</div>
			<div class="gallary col-md-12 pb20">
				<div class="row" id="<?php echo $section->container_id; ?>">
					<?php foreach($section->items as $item){ ?>
						<div class="col-xs-6 col-sm-4 col-md-3 pt20">
							<a href="<?php echo $item->seo_url; ?>"><img src="<?php echo $item->med_image; ?>" alt=""></a>
							<div class="cric-lockup-content">
								<h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom"><?php echo $item->title; ?></h3>
								<div class="cric-lockup-meta">
									<ul class="cric-lockup-meta-info pl5 mt0 pb5">
										<li><?php echo $item->views; ?> views</li>
										<li class="hidden">5 days ago</li>
									</ul>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="clear"></div>
				<?php if ($section->current_page < $section->total_pages){ ?>
					<div class="pt20 pb20">
						<input type="button" data-page-to-load="2" data-filter="<?php echo $section->filter; ?>" data-container-id="<?php echo $section->container_id; ?>" value="Load More" data-loading-text="Please Wait..." class="btn-more btn-more-video-filter full-width" >
					</div>
				<?php } ?>
			</div>
			<?php } ?>

		</div>
	</div>
	</section>
</div>