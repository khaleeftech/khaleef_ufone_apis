<div class="top-score-title right-score col-md-9">
	<div class="cs-section-title">
		<h2>Squad for <?php echo $content_heading; ?></h2>
	</div>
	
	<?php foreach($squad as $item){ ?>
		<div class="col-md-3 cric-player">
			<a href="<?php echo base_url()."player/profile/".$item->player->id; ?>"><img alt="" src="<?php echo $item->player->dp; ?>">
				<p><?php echo $item->player->name; ?></p>
			</a>
		</div>
	<?php } ?>
</div>