<div class="team-1-name">
	<?php echo $first_inning->title; ?> 
	<span class="innings-1-score <?php echo empty($second_inning)?'innings-current':''; ?> "> 
	<?php echo $first_inning->runs."/".$first_inning->wickets." (".$first_inning->overs."/".$first_inning->total_overs.")"; ?>	
	</span> 
</div>

<?php if (!empty($second_inning)){ ?>
	<div class="team-2-name"> 
    	<?php echo $second_inning->title; ?>
    	<span class="innings-1-score innings-current"> 
    		<?php echo $second_inning->runs."/".$second_inning->wickets." (".$second_inning->overs."/".$second_inning->total_overs.")"; ?>
    	</span> 
	</div>
<?php } ?>

<?php if (empty($second_inning)){ ?>
	<div class="innings-requirement"> Run Rate: <?php echo $first_inning->run_rate; ?>  </div>
<?php } else { ?>
	<div class="innings-requirement">
		Run Rate: <?php echo $second_inning->run_rate; ?><BR>
		Req Rate: <?php echo $second_inning->req_rate; ?><BR>
		Need <?php echo $second_inning->runs_req; ?> Runs for <?php echo $second_inning->balls_rem; ?> Balls
	</div>
<?php } ?>