<?php 
  	$total_overs = count($current_inning->overs);
	$last_over = $total_overs - 1;
	
  	foreach($current_inning->balls as $key=>$ball){
  		
		if ($ball->id>$last_ball_updated){
			?>
			<div class="commentary-event border-t">
			  <div class="commentary-overs"><?php echo $ball->title; ?></div>
			  <div class="commentary-text hidden"><?php echo $ball->commentary.", ".($ball->runs_scored!=0?$ball->runs_scored>1?"$ball->runs_scored Runs":"Single Run":'No Run'); ?></div>
			  <div class="commentary-text"><?php echo $ball->commentary.get_ball_score_status_full($ball) ?></div>
			</div>
			<?php
		  	if ($ball->title==($ball->over_number-1).".1"){
		  		$over = $current_inning->overs[$last_over-1];
		  		?>
				<div class="end-of-over-info">
					<p>
						<span class="bold">End of Over <?php echo $last_over; ?></span> (<?php echo $over->total_runs; ?>/<?php echo $over->total_dismissal; ?>)
					</p>
					<ul class="large-block-grid-2 end-of-over-player-stat clearfix">
						<li class="eov-stat-batsmen">
						<ul class="end-of-over-batsmen">
							<li class="clearfix"><span class="large-10 medium-10 columns"> <?php echo $over->batsman1?$players[$over->batsman1]->short_name:''; ?> </span><span class="large-10 medium-10 columns"> <?php echo $over->batsman1_scorecard_summary; ?> </span></li>
							<li class="clearfix"><span class="large-10 medium-10 columns"> <?php echo $over->batsman2?$players[$over->batsman2]->short_name:''; ?> </span><span class="large-10 medium-10 columns"> <?php echo $over->batsman2_scorecard_summary; ?> </span></li>
						</ul>
						</li>
						<li class="eov-stat-bowlers">
							<ul class="end-of-over-bowlers">
								<li class="clearfix">
									<span class="large-11 medium-11 columns"> <?php echo $over->bowler1?$players[$over->bowler1]->short_name:''; ?> </span><span class="large-9 medium-9 columns"> <?php echo $over->bowler1_scorecard_summary; ?> </span>
								</li>
								<li class="clearfix">
									<span class="large-11 medium-11 columns"> <?php echo $over->bowler2?$players[$over->bowler2]->short_name:'';; ?> </span><span class="large-9 medium-9 columns"> <?php echo $over->bowler2_scorecard_summary; ?> </span>
								</li>
							</ul>
						</li>
					</ul>
				</div>
		  		<?php
		  		$last_over--;
		  	}
		} else {
			break;
		}
	?>
  <?php } ?>