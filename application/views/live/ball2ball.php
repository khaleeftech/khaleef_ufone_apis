<div class="top-score-title right-score col-md-9">
  <div class="top-score-title player-vs">
    <div class="">
      <div class="col-md-12 ">
        <h4><?php echo $match_details->title; ?>, <?php echo $match_details->fixture->title.": ".$match_details->fixture->series->title; ?></h4>
      </div>
    </div>
    <div class="main">
      <div class="tabs standard">
        <div class="tab-content">
          <div id="tab1" class="tab active"> 
            <!--<h3 class="tab-match-title">MATCH STATISTICS</h3>-->
            <div class="tabs standard">
              <ul class="tab-links-match">
                <li class="active"><a class="first-tabs" href="javascript:;"><i class="fa fa-list-alt"></i> Ball 2 Ball</a></li>
                <li><a class="first-tabs" href="<?php echo base_url()."scorecard/".$match_details->id; ?>"><i class="fa fa-list-alt"></i> Scorecard</a></li>
              </ul>
              <div class="tab-content">
                <div id="tab1a" class="tab active">
                  <div class="col-md-12">
                    <div class=" pt10 pb10">
                      <div class="innings-information" id="team_overall_score">
                        <div class="team-1-name">
                        	<?php echo $first_inning->title; ?> 
                        	<span class="innings-1-score <?php echo empty($second_inning)?'innings-current':''; ?> "> 
                        	<?php echo $first_inning->runs."/".$first_inning->wickets." (".$first_inning->overs."/".$first_inning->total_overs.")"; ?>	
                        	</span> 
                        </div>
                        
                        <?php if (!empty($second_inning)){ ?>
                        	<div class="team-2-name"> 
	                        	<?php echo $second_inning->title; ?>
	                        	<span class="innings-1-score innings-current"> 
	                        		<?php echo $second_inning->runs."/".$second_inning->wickets." (".$second_inning->overs."/".$second_inning->total_overs.")"; ?>
	                        	</span> 
                        	</div>
                        <?php } ?>
						
						<?php if (empty($second_inning)){ ?>
							<div class="innings-requirement"> Run Rate: <?php echo $first_inning->run_rate; ?>  </div>
						<?php } else { ?>
                        	<div class="innings-requirement">
                        		Run Rate: <?php echo $second_inning->run_rate; ?><BR>
                        		Req Rate: <?php echo $second_inning->req_rate; ?><BR>
                        		Need <?php echo $second_inning->runs_req; ?> Runs for <?php echo $second_inning->balls_rem; ?> Balls
                        	</div>
                        <?php } ?>
                      </div>
                      <div class="hidden"><a href="" class="share-score color-theme"><i class="fa fa-share-alt"></i></a></div>

                    </div>
                    <div style="position:relative;" class="scorecard" id="batsman_bowler_top_card">
                      <hr class="border-top-score-squad">
                      <table width="100%" class="score-table" >
                        <tr>
                          <td colspan="2">
                          	<table width="100%" class="batsmen">
                              <thead>
                                <tr>
                                  <th class="th-batsmen">Batsmen</th>
                                  <th class="th-r">R</th>
                                  <th class="th-b">B</th>
                                  <th class="th-4s">4s</th>
                                  <th class="th-6s">6s</th>
                                  <th class="th-sr">&nbsp;</th>
                                  <th class="th-this-bowler">SR</th>
                                </tr>
                              </thead>
                              <tbody>
                              	<?php 
                              	 $onstrike = 0;
								 $offstrike = 1;
								 if (!$batsman[0]->on_strike){
								 	$onstrike = 1;
								 	$offstrike = 0;
								 }
                              	?>
                                <tr class="row1">
								<?php if (!empty($batsman[$onstrike])){ ?>
								  <td class="batsman-1-name current-player"><?php echo $players[$batsman[$onstrike]->player]->name; ?></td>
								  <td class="bold"><?php echo $batsman[$onstrike]->runs; ?></td>
								  <td><?php echo $batsman[$onstrike]->balls; ?></td>
								  <td><?php echo $batsman[$onstrike]->boundary_4s; ?></td>
								  <td><?php echo $batsman[$onstrike]->boundary_6s; ?></td>
								  <td>&nbsp;</td>
								  <td><?php echo $batsman[$onstrike]->strike_rate; ?></td>
								<?php } else { ?>
								  <td class="batsman-1-name current-player">&nbsp;</td>
								  <td class="bold">&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								<?php } ?>
                                </tr>
                                <tr class="row1">
									<?php if (!empty($batsman[$offstrike])){ ?>
										<td class="batsman-1-name"><?php echo $players[$batsman[$offstrike]->player]->name; ?></td>
										<td class="bold"><?php echo $batsman[$offstrike]->runs; ?></td>
										<td><?php echo $batsman[$offstrike]->balls; ?></td>
										<td><?php echo $batsman[$offstrike]->boundary_4s; ?></td>
										<td><?php echo $batsman[$offstrike]->boundary_6s; ?></td>
										<td>&nbsp;</td>
										<td><?php echo $batsman[$offstrike]->strike_rate; ?></td>
									<?php } else { ?>
										<td class="batsman-1-name current-player">&nbsp;</td>
										<td class="bold">&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									<?php } ?>
                                </tr>
                              </tbody>
                            </table></td>
                        </tr>
                        <tr>
                          <td colspan="2"><table width="100%" class="bowlers">
                              <thead>
                                <tr>
                                  <th width="330" class="th-bowlers">Bowlers</th>
                                  <th width="57" class="th-o">O</th>
                                  <th width="57" class="th-m">M</th>
                                  <th width="57" class="th-r">R</th>
                                  <th width="57" class="th-w">W</th>
                                  <th width="37" class="th-o">&nbsp;</th>
                                  <th width="37" class="th-o">&nbsp;</th>
                                  <th width="57" class="th-0s">0s</th>
                                  <th width="57" class="th-4s">4s</th>
                                  <th width="61" class="th-6s">6s</th>
                                </tr>
                              </thead>
                              <tbody>
                              	<?php 
                              	$onspell = 0;
                              	$offspell = 1;
								if (!$bowlers[0]->on_spell_primary){
									$onspell = 1;
                              		$offspell = 0;
								}
                              	?>
                                <tr class="row1">
                                  <td class="bowler-1-name current-player"><?php echo $players[$bowlers[$onspell]->player]->name; ?></td>
                                  <td><?php echo $bowlers[$onspell]->overs; ?></td>
                                  <td><?php echo $bowlers[$onspell]->maiden; ?></td>
                                  <td><?php echo $bowlers[$onspell]->runs; ?></td>
                                  <td><?php echo $bowlers[$onspell]->wickets; ?></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td><?php echo $bowlers[$onspell]->dots_0s; ?></td>
                                  <td><?php echo $bowlers[$onspell]->boundary_4s; ?></td>
                                  <td><?php echo $bowlers[$onspell]->boundary_6s; ?></td>
                                </tr>
                                <tr class="row2">
                                <?php if (count($bowlers)>1){ ?>
                                  <td class="bowler-2-name "><?php echo $players[$bowlers[$offspell]->player]->name; ?></td>
                                  <td><?php echo $bowlers[$offspell]->overs; ?></td>
                                  <td><?php echo $bowlers[$offspell]->maiden; ?></td>
                                  <td><?php echo $bowlers[$offspell]->runs; ?></td>
                                  <td><?php echo $bowlers[$offspell]->wickets; ?></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td><?php echo $bowlers[$offspell]->dots_0s; ?></td>
                                  <td><?php echo $bowlers[$offspell]->boundary_4s; ?></td>
                                  <td><?php echo $bowlers[$offspell]->boundary_6s; ?></td>
                                <?php } else { ?>
                                  <td class="bowler-2-name ">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                <?php } ?>
                                </tr>
                              </tbody>
                            </table></td>
                        </tr>
                      </table>
                      <div class="more-match-stats">
                        <div class="recent-overs"><span class="head" > This Over </span>
                          <ul class="inline-list recent-over ml20" style="border-left: 1px solid">
                          	<?php
                          		$req_over = count($current_inning->overs);
                          		$this_over_balls = array();
								foreach($current_inning->balls as $b){
									if ($b->over_number==$req_over){
										$this_over_balls[] = $b;
									} else {
										break;
									}
								}
								$this_over_balls = array_reverse($this_over_balls);
								
								foreach($this_over_balls as $tob){
									?><li> <?php echo get_ball_score_status_small($tob); ?> </li><?php
								}
                          	?>
                          </ul>
                          <div class="cb"></div>
                        </div>
                        <hr class="secondary-hr">

                      </div>
                    </div>
                    <div id="commentary"> 
                      <!-- Commentary & Central Thin Colum --> 
                      
                      <!-- Commentary -->
                      <div class="commentary-block pt20" id="commentry_block">

					  <?php 
					  	$total_overs = count($current_inning->overs);
						$last_over = $total_overs - 1;
						$last_ball_id = "";
					  	foreach($current_inning->balls as $key=>$ball){
					  	 if ($key==0){
					  	 	$last_ball_id = $ball->id;
					  	 }	 
					  	if ($ball->over_number==$last_over){
					  		$over = $current_inning->overs[$last_over-1];
					  		?>
							<div class="end-of-over-info">
								<p>
									<span class="bold">End of Over <?php echo $last_over; ?></span> (<?php echo $over->total_runs; ?>/<?php echo $over->total_dismissal; ?>)
								</p>
								<ul class="large-block-grid-2 end-of-over-player-stat clearfix">
									<li class="eov-stat-batsmen">
									<ul class="end-of-over-batsmen">
										<li class="clearfix"><span class="large-10 medium-10 columns"> <?php echo $over->batsman1?$players[$over->batsman1]->short_name:''; ?> </span><span class="large-10 medium-10 columns"> <?php echo $over->batsman1_scorecard_summary; ?> </span></li>
										<li class="clearfix"><span class="large-10 medium-10 columns"> <?php echo $over->batsman2?$players[$over->batsman2]->short_name:''; ?> </span><span class="large-10 medium-10 columns"> <?php echo $over->batsman2_scorecard_summary; ?> </span></li>
									</ul>
									</li>
									<li class="eov-stat-bowlers">
										<ul class="end-of-over-bowlers">
											<li class="clearfix">
												<span class="large-11 medium-11 columns"> <?php echo $over->bowler1?$players[$over->bowler1]->short_name:''; ?> </span><span class="large-9 medium-9 columns"> <?php echo $over->bowler1_scorecard_summary; ?> </span>
											</li>
											<li class="clearfix">
												<span class="large-11 medium-11 columns"> <?php echo $over->bowler2?$players[$over->bowler2]->short_name:'';; ?> </span><span class="large-9 medium-9 columns"> <?php echo $over->bowler2_scorecard_summary; ?> </span>
											</li>
										</ul>
									</li>
								</ul>
							</div>
					  		<?php
					  		$last_over--;
					  	}
					  	?>
						<div class="commentary-event border-t">
						  <div class="commentary-overs"><?php echo $ball->title; ?></div>
						  <div class="commentary-text"><?php echo $ball->commentary.get_ball_score_status_full($ball) ?></div>
						</div>
  					  <?php } ?>
         


                        <div class="cb"></div>
                      </div>
                      <div class="large-4 medium-4 columns thin-incontent-block hide"> </div>
                      <div class=""> 
                        <!-- filters, comm feedback and social icons start-->
                        <div class="large-20 medium-20 columns filters-block">
                          <hr>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>

<script>


$( document ).ready(function(){
	    
	var recursive_call_id = setInterval(function() {
		 updateBall2Ball();
	}, 10000);
	
	window.onbeforeunload = function(e) {
	  clearInterval(recursive_call_id);
	  console.log("Clearing Interval");
	};

});

var base_url = "<?php echo base_url(); ?>";
var last_ball_updated = <?php echo $last_ball_id ?>;
function updateBall2Ball(){
	$.ajax({
		dataType: "json",
		url: base_url+'ajax/getBall2BallUpdate/'+last_ball_updated,
		data: {},
		success: function(response) {
			$("#commentry_block").prepend(response.commentry_html);
			$("#batsman_bowler_top_card").html(response.batsman_bowler_top_card);
			$("#team_overall_score").html(response.team_over_all_score_update);
			last_ball_updated = response.last_ball_updated;
		}
	});
}
</script>
