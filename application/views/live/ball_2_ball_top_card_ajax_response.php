
<hr class="border-top-score-squad">
<table width="100%" class="score-table" id="batsman_bowler_top_card">
	<tr>
	  <td colspan="2">
	  	<table width="100%" class="batsmen">
	      <thead>
	        <tr>
	          <th class="th-batsmen">Batsmen</th>
	          <th class="th-r">R</th>
	          <th class="th-b">B</th>
	          <th class="th-4s">4s</th>
	          <th class="th-6s">6s</th>
	          <th class="th-sr">&nbsp;</th>
	          <th class="th-this-bowler">SR</th>
	        </tr>
	      </thead>
	      <tbody>
	      	<?php 
	      	 $onstrike = 0;
			 $offstrike = 1;
			 if (!$batsman[0]->on_strike){
			 	$onstrike = 1;
			 	$offstrike = 0;
			 }
	      	?>
	        <tr class="row1">
	          <?php if (!empty($batsman[$onstrike])){ ?>
		          <td class="batsman-1-name current-player"><?php echo $players[$batsman[$onstrike]->player]->name; ?></td>
		          <td class="bold"><?php echo $batsman[$onstrike]->runs; ?></td>
		          <td><?php echo $batsman[$onstrike]->balls; ?></td>
		          <td><?php echo $batsman[$onstrike]->boundary_4s; ?></td>
		          <td><?php echo $batsman[$onstrike]->boundary_6s; ?></td>
		          <td>&nbsp;</td>
		          <td><?php echo $batsman[$onstrike]->strike_rate; ?></td>
	          <?php } else { ?>
	          	  <td class="batsman-1-name current-player">&nbsp;</td>
		          <td class="bold">&nbsp;</td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
	          <?php } ?>
	        </tr>
	        <tr class="row1">
				<?php if (!empty($batsman[$offstrike])){ ?>
					<td class="batsman-1-name"><?php echo $players[$batsman[$offstrike]->player]->name; ?></td>
					<td class="bold"><?php echo $batsman[$offstrike]->runs; ?></td>
					<td><?php echo $batsman[$offstrike]->balls; ?></td>
					<td><?php echo $batsman[$offstrike]->boundary_4s; ?></td>
					<td><?php echo $batsman[$offstrike]->boundary_6s; ?></td>
					<td>&nbsp;</td>
					<td><?php echo $batsman[$offstrike]->strike_rate; ?></td>
				<?php } else { ?>
					<td class="batsman-1-name current-player">&nbsp;</td>
					<td class="bold">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				<?php } ?>
	        </tr>
	      </tbody>
	    </table></td>
	</tr>
	<tr>
	  <td colspan="2"><table width="100%" class="bowlers">
	      <thead>
	        <tr>
	          <th width="330" class="th-bowlers">Bowlers</th>
	          <th width="57" class="th-o">O</th>
	          <th width="57" class="th-m">M</th>
	          <th width="57" class="th-r">R</th>
	          <th width="57" class="th-w">W</th>
	          <th width="37" class="th-o">&nbsp;</th>
	          <th width="37" class="th-o">&nbsp;</th>
	          <th width="57" class="th-0s">0s</th>
	          <th width="57" class="th-4s">4s</th>
	          <th width="61" class="th-6s">6s</th>
	        </tr>
	      </thead>
	      <tbody>
	      	<?php 
	      	$onspell = 0;
	      	$offspell = 1;
			if (!$bowlers[0]->on_spell_primary){
				$onspell = 1;
	      		$offspell = 0;
			}
	      	?>
	        <tr class="row1">
	          <td class="bowler-1-name current-player"><?php echo $players[$bowlers[$onspell]->player]->name; ?></td>
	          <td><?php echo $bowlers[$onspell]->overs; ?></td>
	          <td><?php echo $bowlers[$onspell]->maiden; ?></td>
	          <td><?php echo $bowlers[$onspell]->runs; ?></td>
	          <td><?php echo $bowlers[$onspell]->wickets; ?></td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td><?php echo $bowlers[$onspell]->dots_0s; ?></td>
	          <td><?php echo $bowlers[$onspell]->boundary_4s; ?></td>
	          <td><?php echo $bowlers[$onspell]->boundary_6s; ?></td>
	        </tr>
	        <tr class="row2">
	        <?php if (count($bowlers)>1){ ?>
	          <td class="bowler-2-name "><?php echo $players[$bowlers[$offspell]->player]->name; ?></td>
	          <td><?php echo $bowlers[$offspell]->overs; ?></td>
	          <td><?php echo $bowlers[$offspell]->maiden; ?></td>
	          <td><?php echo $bowlers[$offspell]->runs; ?></td>
	          <td><?php echo $bowlers[$offspell]->wickets; ?></td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td><?php echo $bowlers[$offspell]->dots_0s; ?></td>
	          <td><?php echo $bowlers[$offspell]->boundary_4s; ?></td>
	          <td><?php echo $bowlers[$offspell]->boundary_6s; ?></td>
	        <?php } else { ?>
	          <td class="bowler-2-name ">&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	          <td>&nbsp;</td>
	        <?php } ?>
	        </tr>
	      </tbody>
	    </table></td>
	</tr>
</table>
<div class="more-match-stats">
	<div class="recent-overs"><span class="head" > This Over </span>
		<ul class="inline-list recent-over ml20" style="border-left: 1px solid">
        	<?php
          		$req_over = count($current_inning->overs);
          		$this_over_balls = array();
				foreach($current_inning->balls as $b){
					if ($b->over_number==$req_over){
						$this_over_balls[] = $b;
					} else {
						break;
					}
				}
				$this_over_balls = array_reverse($this_over_balls);
				
				foreach($this_over_balls as $tob){
					?><li> <?php echo get_ball_score_status_small($tob); ?> </li><?php
				}
          	?>
		</ul>
		<div class="cb"></div>
    </div>
    <hr class="secondary-hr">
</div>