<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Jazz Cricket :: watch exlusive videos and highlights</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="theme-color" content="#" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="keywords" content="">
<link href="<?php echo base_url() ?>assets/promo/css/custom.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>assets/promo/css/bootstrap.css" rel="stylesheet">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88965851-3', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body class="brands-section">
<div id="wrapper"> <!-- top image1 and language btn-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row"> <a href="index-ar.html" class="language hidden"> <span> العربية </span> </a>
      <img src="<?php echo base_url() ?>assets/promo/images/banner-img-2.jpg?v=<?php echo VERSION; ?>" style="width:100%;" alt="" class="img-responsive">
    </div>
  </div>
  <!-- end top image1 and language btn-->

  <div class="container">
    <div class="row">
      <div class="latest-collection">
        <div class="latest-collection-inner">
			<div class="message hidden">
		 <div class="alert alert-danger ffamily" role="alert"> <strong>Oh snap!</strong> Enter correct values. </div>
         </div>
          <div class="latest-content">
            <div class="col-md-12 col-sm-12 col-xs-12">
		          <div class="row">
                <h3 class="ffamily text-center color-dark">
                  Join Us for
                  <span class="color-red">Free</span>
                </h3>
                <h4 class="ffamily text-center color-gray nomsisdn">Please Enter Your Mobile Number</h4>
		           <h5 class="ffamily text-center color-gray nomsisdn">This is only to verify if you’re a Jazz or Warid subscriber</h5>
             </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 center-block">
              <div class=" center-block text-center">
                <form id="form">

                    <input type="text" placeholder="03XX XXXXXXX" class="input-jazz nomsisdn" />
		                <button class="btn-continue" type="submit" >
                      <?php if ($msisdn): ?>
                        Continue
                        <input type="hidden" name="task" value="continue">
                      <?php else: ?>
                        Continue
                        <input type="hidden" name="task" value="phone">
                      <?php endif; ?>
                      <span class="pull-right">
                        <img src="<?php echo base_url() ?>assets/promo/images/arrow.png" alt="" class="img-responsive" style="width:15px;margin-top: 5px">
                      </span>
                    </button>

                </form>
              </div>
            </div>

          </div>
           </div>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 bg-color-light-gray">
				<div class="row"> <h4 class="ffamily text-center color-light-gray">Use Jazz Cricket App For FREE By Sending</h4>
					<h4 class="ffamily text-center color-red">SUB to 9876</h4> </div>
            </div>
  <div style="position: relative">
	 <div class="inner" style="position: absolute; bottom: 10px; width: 100%">
          <div class="copyrights"><strong class="copy">&copy; 2018. All rights reserved </strong> </div>
        </div>
             <img src="<?php echo base_url() ?>assets/promo/images/img-blur-client.jpg" alt="" class="img-responsive">

            </div>
</div>
<footer>
  <div class="lower-footer">
    <div class="container">
      <div class="row">


      </div>
    </div>
  </div>
</footer>
<!-- app direct model -->
<div aria-labelledby="gridModalLabel" tabindex="-1" id="directModal" role="dialog" class="modal fade" style="display: none">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <!--<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>-->
        <h2 id="gridModalLabel" class="modal-title color-success"><i class="icon-playlist"></i>Congratulations!</h2>
      </div>
      <div class="modal-body align-center">
        <div class="">
          <div>
            <h4>You are successfuly subscribed</h4>
            <p>After few seconds you will be <strong>proceeded</strong> to the portal. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end app direct model -->
<script src="<?php echo base_url() ?>assets/promo/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/promo/js/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>assets/promo/js/bootstrap.min.js"></script>

<script type="text/javascript">
  var m = "<?php echo $msisdn ?>";
  var a = "<?php echo $agency ?>";
  var visitor_id = "<?php echo $visitor_id; ?>";

  function success(){
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if(isAndroid) {
      window.location = "market://details?id=com.Khaleef.CricWickMobilink";
    }else{
      window.location = "<?php echo base_url(); ?>";
    }
  }

  $(document).ready(function(){
    if(m){
      $('.nomsisdn').hide();
    }

    $("#form").submit(function(e){
      e.preventDefault();
      var $form = $(this);
      var $task = $form.find('input[name=task]');
      var $input = $form.find('input[type=text]');
      var $btn = $form.find('button');

      if($task.val() == 'continue'){
        ga('send', 'event', 'ContinueButton: Header', 'Phone', m, 1);
        $btn.prop('disabled', true);
        $btn.html('please wait...');
        $.ajax({
          method: "POST",
          url: "<?php echo base_url() ?>"+"continue_subscription",
          data: "visitor_id="+visitor_id+"&agency="+a,
          dataType: "json",
          success: function(response){
            if(response.status == 1){
              //as
              ga('send', 'event', 'ResponseHeader: Already Sub', 'Phone', m, 1);
              $("#directModal").modal();
              setTimeout(function(){
                success();
              },2000);
            }
            else if(response.status == 2){
              //ns
              ga('send', 'event', 'ResponseHeader: New Sub', 'Phone', m, 1);
              $("#directModal").modal();
                setTimeout(function(){
                  success();
                },2000);
            }else{
              ga('send', 'event', 'ResponseHeader: Error', m, response.message, 1);
              $(".nomsisdn").show();
              $task.val('phone');
              $btn.html('Continue');
            }
          }
        });
        $btn.removeAttr('disabled');
        return false;
      }


      if($task.val() == 'phone'){
        var phone = $input.val().trim();
        if(phone){
          ga('send', 'event', 'ContinueButton: NoHeader', 'Phone', phone, 1);
          $btn.prop('disabled', true);
          $btn.html('please wait...');
          $.ajax({
            method: "POST",
            url: "<?php echo base_url() ?>"+"continue_subscription_phone",
            data: "phone="+phone,
            dataType: "json",
            success: function(response){
              if(response.status == 1){
                //ns
                ga('send', 'event', 'ResponseNoHeader: New Sub', 'Phone', phone, 1);
                $("#directModal").modal();
                setTimeout(function(){
                  success();
                },2000);
              }else if (response.status == 2) {
                ga('send', 'event', 'ResponseNoHeader: Already Sub', 'Phone', m, 1);
                $("#directModal").modal();
                setTimeout(function(){
                  success();
                },2000);
              }else{
                //err
                ga('send', 'event', 'ResponseNoHeader: Error', phone, response.message, 1);
                $btn.html('Continue');
                alert(response.message);
                $input.val('');
              }
              $btn.removeAttr('disabled');
            }
          });


        }
        return false;
      }

    });
  });
</script>

</body>
</html>
