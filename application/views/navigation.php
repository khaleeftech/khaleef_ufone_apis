<nav class="navbar navbar-inverse" role="navigation">
   <div class="container">
       <!-- Brand and toggle get grouped for better mobile display -->
       <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?php echo base_url(); ?>">
               <img src="<?php echo assets_url(); ?>images/logo2_n.png"></img>
           </a>
       </div>

       <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav navbar-nav">
               <li><a href="<?php echo base_url(); ?>">Home</a></li>
               <li class="<?php echo ($this->uri->segment(1)=='results')?"active":""; ?>"><a href="<?php echo base_url()."results/"; ?>">Results</a></li>
               <li class="<?php echo ($this->uri->segment(1)=='blog')?"active":""; ?> hidden"><a href="<?php echo base_url()."blog/"; ?>">Blog</a></li>
               <li class="<?php echo ($this->uri->segment(1)=='fixtures')?"active":""; ?>"><a href="<?php echo base_url()."fixtures/"; ?>">Schedules</a></li>
               <li class="<?php echo ($this->uri->segment(1)=='videos' || $this->uri->segment(1)=='highlights')?"active":""; ?>" ><a href="<?php echo base_url()."videos/"; ?>">Videos</a></li>
               <li class="<?php echo ($this->uri->segment(1)=='news')?"active":""; ?>"><a href="<?php echo base_url()."news/"; ?>">News</a></li>
               <?php
				$user = $this->xession->get("user");
				if ($user){
				?>
           			<li class=""><a href="<?php echo base_url()."logout/"; ?>">Logout</a></li>
           		<?php
				} else { ?>
					<li class=""><a href="<?php echo base_url()."login/"; ?>">Login</a></li>
				<?php	
				}
				?>
           </ul>
           <div class="ad-logo hidden">
               <img src="<?php echo assets_url(); ?>images/logo_partner.png"></img>
           </div>
           		
       </div>
       <?php /*
       <div class="ad-logo-sm hidden">
           <img src="<?php echo assets_url(); ?>images/logo_partner.png"></img>
       </div>
	    
	    */?>
       <?php
			$user = $this->xession->get("user");
			if ($user){
			?>
	        <div class="">
	   			<a href="javascript:;" class="user" phone="<?php echo $this->data["phone_no"]; ?>" sub_status="<?php echo $this->data["sub_status"] ?>"> 
	   				<i class="fa fa-user"></i>
	   			</a>
	     	</div>
		<?php } ?>
       <!-- /.navbar-collapse -->
   </div>
   <!-- /.container -->
   </nav>
   