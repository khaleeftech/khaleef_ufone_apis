<div class="top-score-title col-md-9">
	<div class="detail-blog pt20 pl10 pr10">
		<div class="post-option-panel">
			<div class="cs-section-title">
				<h2 class="mt0 mb0" id="hidettl"><?php echo $current_news->title; ?></h2>
			</div>		
			<div class="cs-seprator mb0">
				<div class="devider1"></div>
			</div>
			<img src="<?php echo $current_news->large_image; ?>" alt="" class="">
			<div class="desc_news"><?php echo $current_news->details; ?></div>
			<p class="desc_news important_news data">
				by <?php echo $current_news->author; ?> <i class="fa fa-calendar"></i><?php echo date("F j, Y", $current_news->created_at/1000); ?> 
			</p>
			<div class="col-md-12 ">
				<div class="detail-post pl0">
					<h5 class="mb10">Share Now</h5>
					<div class="socialmedia">
						<ul class="mt0">
							<li><a data-original-title="Facebook" class="addthis_button_facebook social_sharing_link" href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u=<?php echo $current_news->seo_url; ?>&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a></li>
							<li><a data-original-title="twitter" class="addthis_button_twitter social_sharing_link" href="http://www.twitter.com/share?text=<?php echo $current_news->title; ?>&url=<?php echo $current_news->seo_url; ?>"><i class="fa fa-twitter"></i></a></li>
							<li><a data-original-title="google-plus" class="addthis_button_google social_sharing_link" href="https://plus.google.com/share?url=<?php echo $current_news->seo_url; ?>"><i class="fa fa-google-plus google"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="">
		<div class="col-md-12 ">
			<div class="cs-section-title">
				<h2>More News</h2>
			</div>
		</div>
	</div>
	<div class="">
		<div class="">
		<?php foreach($more_news as $n){ 
			if ($n->id!=$current_news->id){
			?>
			<div class="col-md-12 col-xs-12 col-sm-4 news-page">
				<a href="<?php echo$n->seo_url; ?>"><img class="img-djoko" alt="" src="<?php echo $n->image_or_thumb_file; ?>"></a>
				<div class="col-md-10 data-news-pg">
					<h3 class="pt0 mb0 crop_to_one_line"><a href="<?php echo$n->seo_url; ?>"><?php echo $n->title; ?></a></h3>
					<p class="news-dd"><?php echo $n->created_at; ?></p>
					<div class="clear"></div>
					<p class="mt0" style="text-align: justify;"><?php echo $n->details; ?>
						<a class="read_more_anchor" href="<?php echo $n->seo_url; ?>"> Read More <i class="fa fa-angle-right"></i></a>
					</p>
					
				</div>
			</div>
		<?php }
		} ?>
		</div>
	</div>
	<!--Close comment-->
</div>