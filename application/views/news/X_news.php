<div id="c-calend" class="top-score-title right-score col-md-9">
	<!--TABS START-->
	<div class="top-score-title right-score col-md-12">
		<div class="row">
			<div class="col-md-12 ">
				<div class="cs-section-title">
					<h2><?php echo $page_heading; ?></h2>
				</div>
			</div>
		</div>
		
			
				
					
		<div id="results-container">
			<?php foreach($news as $n){ ?>
				<div class="col-md-12 col-xs-12 col-sm-12 news-page">
					<a href="<?php echo $n->seo_url; ?>"><img class="img-djoko" alt="" src="<?php echo $n->image_or_thumb_file; ?>"></a>
					<div class="col-md-10 col-sm-10 data-news-pg mb0">
						<h3 class="pt0 mb0 news-list crop_to_one_line"><a href="<?php echo $n->seo_url; ?>"><?php echo $n->title; ?></a></h3>
						<p class="news-dd news-list"><?php echo $n->created_at ?></p>
						<div class="clear"></div>
						<p class="mt0 news-list" style="text-align: justify;"><?php echo $n->details; ?>
							<a class="read_more_anchor" href="<?php echo $n->seo_url; ?>">Read More <i class="fa fa-angle-right"></i></a>
						</p>
					</div>
				</div>
			<?php } ?>
				
		</div>
		<div class="bottom-event-panel hidden">
			<a href="javascript:;" id="more-news-btn" data-page-to-load="2"> More News</a> 
		</div>
				
				
			
		
	</div>
	<!--TABS END-->
</div>
