
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cricwick</title>
<!--// Responsive //-->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<!--// Stylesheets //-->
<link href="<?php echo base_url(); ?>assets/css/redirect/bootstrap.css" rel="stylesheet" media="screen" />
<link href="<?php echo base_url(); ?>assets/css/redirect/style.css" rel="stylesheet" media="screen" />
<script src="<?php echo base_url("assets"); ?>/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
<![endif]-->
</head>
<body class="bg_direction">
<!-- Wrapper Start -->
<div class="wrapper"> 
  <!-- Header Start -->
  <header>
    <div class="custom-container">
      <div class="row"> 
        <!-- Logo Start -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="logo_direction"> <a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/logo_color.svg" alt="Cricwick" /></a> </div>
          <h3 class="text-center color_gray">Please wait, we are preparing your subscription!</h3>
        </div>
      </div>
    </div>
  </header>
  <div class="custom-container">
    <div class="row">
     <div class="screen"> 
      	<a href="index.html">
      		<img src="<?php echo base_url(); ?>assets/images/screen.png" alt="Cricwick" /></a> </div>
     </div>
  </div>
</div>
<!-- Wrapper End -->

<script>

var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	$.ajax({
		type: "POST",
		dataType: "json",
		url: base_url+'autosub/',
		data: [],
		success: function(response) {
			
			window.location = '<?php echo $playstore_url ?>';
			
		}
	});
});

</script>

</body>
</html>