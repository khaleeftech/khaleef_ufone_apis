<div class="top-score-title right-score col-md-9">
        <div class="row">
          <div class="col-md-12 ">
            <div class="cs-section-title">
              <h2>Subscribe for Quiz Service</h2>
            </div>
          </div>
        </div>
        
        <div class="col-md-12 quiz-page quiz-w-page">
 
        
          <div class="">
            <div class="col-md-12">
              <form method="post" class="quiz-form" id="subscribe-quiz">
  				<img src="<?php echo base_url("assets"); ?>/images/banner_02.png" alt="" style="width:100%" />
  				<BR>
              <div id="quiz-submit" class="pt20">
                  <input type="submit" id="btnSubmit" class="" value="Subscribe Now!" data-loading-text="Please Wait..."/>
              </div>

              </form>
            </div>
          </div>
        </div>
        <div id="answer-response"></div>
        <!--next section-->
        <div class="row">
          <div class="col-md-12 ">
            <div class="cs-section-title">
              <h2>Quiz Description</h2>
            </div>
          </div>
        </div>
        <div class="col-md-12 quiz-page quiz-w-page">
        <div class="score-next-time">
	        <div class="circle-ico">
	          <p><i class="fa fa-gift"></i></p>
	        </div>
	      </div>
        <section style="margin-top: -60px;" class="pl30">
          <div class="title">
            <h2><a href="">&nbsp;</a></h2>
          </div>
          <div class="blog-text">
            <p>
            	PSL Quiz is a grand opportunity for the cricket lovers to answer some simple questions and win heavy prizes including multiple 100SR Zain Vouchers and Samsung Note 3. It is an exciting experience to play the Quiz and 
            	get score which can enable you to be a part of the winners. So, you can test your cricket knowledge along with getting reward against your correct answers. These 
            	are very easy multiple choice questions. You just have to tap on the correct answer and move forward to the next questions. So, play and win with passion. Stay Tuned!
            </p>
            
            </div>
        </section>
            <div class="clear"></div>    
        </div>
      </div>