<div class="top-score-title right-score col-md-9">
        <div class="row">
          <div class="col-md-12 ">
            <div class="cs-section-title">
              <h2>You are in top <?php echo $stats->rank; ?>%</h2>
            </div>
          </div>
        </div>
        
        <div class="col-md-12 quiz-page quiz-w-page">
          <?php if (!$finished){ ?>
          <div class="score-next-time">
            <div class="circle-ico">
              <p>Q</p>
            </div>
          </div>
          <?php } ?>
          <div class="quiz-score">Score: <?php echo $stats->score; ?></div>
          <?php if (!$finished){ ?>
          <div class="logo-psl"><img src="<?php echo base_url("assets"); ?>/images/logo_psl.png" alt=""></div>
          <div class="col-md-11 col-md-offset-1  col-xs-offset-1">
          	
            <h3 class="quiz-w-title" style="margin-top: -10px">
            	<img class="ball-cric" src="<?php echo base_url("assets"); ?>/images/ball.png" alt="">
            	<span class="pl20"><?php echo $stats->last_asked_question->question_eng; ?></span>
            </h3>
            <br>
            <div class="col-md-11 col-md-offset-1  col-xs-offset-1">
            
              <form method="post" class="quiz-form" id="quiz-form">
              	<?php foreach($stats->last_asked_question->answers as $answer){ ?>
					<div class="radio">
					  <label>
					    <input type="radio" name="answer" value="<?php echo $answer->answer_pos; ?>">
					    <span class="pl20"><?php echo $answer->answer_value; ?></span>
					  </label>
					</div>
              <?php } ?>
              <div id="quiz-submit" class="pt20">
                  <input type="submit" id="btnSubmit" class="btn-ok" value="OK" data-loading-text="Please Wait..."/>
              </div>

              </form>
            </div>
            
          </div>
          <?php } else { ?>
          		<BR><BR>
		  		<img src="<?php echo base_url()."assets/images/quiz_finished.png" ?>" />
		  	<?php } ?>
        </div>
        <div class="answer-response correct" style="display: none;">
        	<img src="<?php echo base_url()."assets/images/quiz_correct.png"; ?>" />
        </div>
        <div class="answer-response incorrect" style="display: none;">
        	<img src="<?php echo base_url()."assets/images/quiz_incorrect.png"; ?>" />
        </div>
        <!--next section-->
        <div class="row">
          <div class="col-md-12 ">
            <div class="cs-section-title">
              <h2>Quiz Description</h2>
            </div>
          </div>
        </div>
        <div class="col-md-12 quiz-page quiz-w-page">
        <div class="score-next-time">
	        <div class="circle-ico">
	          <p><i class="fa fa-gift"></i></p>
	        </div>
	      </div>
        <section style="margin-top: -60px;" class="pl30">
          <div class="title">
            <h2><a href="">&nbsp;</a></h2>
          </div>
          <div class="blog-text">
            <p>
            	PSL Quiz is a grand opportunity for the cricket lovers to answer some simple questions and win heavy prizes including multiple 100SR Zain Vouchers and Samsung Note 3. It is an exciting experience to play the Quiz and 
            	get score which can enable you to be a part of the winners. So, you can test your cricket knowledge along with getting reward against your correct answers. These 
            	are very easy multiple choice questions. You just have to tap on the correct answer and move forward to the next questions. So, play and win with passion. Stay Tuned!
            </p>
            
            </div>
        </section>
            <div class="clear"></div>    
        </div>
      </div>