<div class="top-score-title right-score col-md-9">
	<div class="cs-section-title">
		<h2><?php echo $player->name; ?></h2>
	</div>
	<div class="col-md-12 cric-single-player">
		<img class="img-djoko thumbnail" src="<?php echo $player->dp; ?>" alt="">
		<div class="col-md-2 data-player">
			<p>Short Name:</p>
			<p>Born:</p>
			<p>Age:</p>
			<p>Playing Role:</p>
			<p>Batting style:</p>
			<p>Bowling style:</p>
		</div>
		<div class="col-md-7 data-player-2 ">
			<div class="row">
				<p><?php echo $player->short_name; ?></p>
				<p>Missing</p>
				<p>Missing</p>
				<p><?php echo $constants[$player->playing_role]; ?></p>
				<p><?php echo $player->batting_style; ?></p>
				<p><?php echo $player->bowling_style; ?></p>
			</div>
		</div>
	</div>
	<div class="col-md-12 cric-single-player skill-content">
		<div class="exp-skill">
			<p class="exp-title-pp">
				Batting SR
			</p>
			<div class="skills-pp">
				<div class="skillbar-pp clearfix" data-percent="65%">
					<div class="skillbar-title-pp">
						<span>Tests</span>
					</div>
					<div class="skillbar-bar-pp" style="width: 65%;">
					</div>
					<div class="skill-bar-percent-pp">
						86.97%
					</div>
				</div>
				<div class="skillbar-pp clearfix" data-percent="75%">
					<div class="skillbar-title-pp">
						<span>ODIs </span>
					</div>
					<div class="skillbar-bar-pp" style="width: 75%;">
					</div>
					<div class="skill-bar-percent-pp">
						117%
					</div>
				</div>
				<div class="skillbar-pp clearfix" data-percent="85%">
					<div class="skillbar-title-pp">
						<span>T20Is</span>
					</div>
					<div class="skillbar-bar-pp" style="width: 85%;">
					</div>
					<div class="skill-bar-percent-pp">
						149.37%
					</div>
				</div>
				<div class="skillbar-pp clearfix" data-percent="90%">
					<div class="skillbar-title-pp">
						<span>Twinty20</span>
					</div>
					<div class="skillbar-bar-pp" style="width: 90%;">
					</div>
					<div class="skill-bar-percent-pp">
						154.36%
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 cric-single-player skill-content">
		<div class="exp-skill">
			<p class="exp-title-pp">
				Bowling SR
			</p>
			<div class="skills-pp">
				<div class="skillbar-pp clearfix" data-percent="66.5%">
					<div class="skillbar-title-pp">
						<span>Tests</span>
					</div>
					<div class="skillbar-bar-pp" style="width: 66.5%;">
					</div>
					<div class="skill-bar-percent-pp">
						66.5%
					</div>
				</div>
				<div class="skillbar-pp clearfix" data-percent="44.7%">
					<div class="skillbar-title-pp">
						<span>ODIs </span>
					</div>
					<div class="skillbar-bar-pp" style="width: 44.7%;">
					</div>
					<div class="skill-bar-percent-pp">
						44.57%
					</div>
				</div>
				<div class="skillbar-pp clearfix" data-percent="21.7%">
					<div class="skillbar-title-pp">
						<span>T20Is</span>
					</div>
					<div class="skillbar-bar-pp" style="width: 21.7%;">
					</div>
					<div class="skill-bar-percent-pp">
						21.7%
					</div>
				</div>
				<div class="skillbar-pp clearfix" data-percent="51.3%">
					<div class="skillbar-title-pp">
						<span>First-class</span>
					</div>
					<div class="skillbar-bar-pp" style="width: 51.3%;">
					</div>
					<div class="skill-bar-percent-pp">
						51.3%
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 cric-single-player skill-content">
	    <div class="ppl-desc">
	      <p class="exp-title-pp">DESCRIPTION</p>
	      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
	        standard dummy text ever since the 1500s, when an unknown printer took a gallery of type and scrambled it to make a type specimen book.</p>
	      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
	        standard dummy text ever since the 1500s, when an unknown printer took a gallery of type and scrambled it to make a type specimen book.</p>
	    </div>
	  </div>
	
</div>