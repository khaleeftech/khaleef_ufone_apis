<!DOCTYPE html>
<html lang="en-US" >
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
<meta content="text/html;" http-equiv="Content-Type" />
<link rel="shortcut icon" href="<?php echo assets_url(); ?>images/fav/ico-16.png">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-57.png" sizes="57x57">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-72.png" sizes="72x72">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-114.png" sizes="114x114">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-144.png" sizes="144x144">
<meta charset="UTF-8">
<meta name="keywords" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta property="og:site_name" 		   content="CricBoom"/>
<meta property="fb:app_id" 			   content="430752913746475" />
<?php if (!empty($video_page)){ ?>
	<title><?php echo $current_video->social->title." - Cricwick"; ?></title>
	<meta property="og:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta property="og:title" content="<?php echo $current_video->social->title." - Cricwick"; ?>" />
	<meta property="og:description" content="<?php echo $current_video->social->description; ?>" />
	<meta property="og:image" content="<?php echo $current_video->social->thumb_file; ?>" />
	<?php /*
	<meta property="og:type" content="website" />
	<meta property="og:video:width" content="640" />
	<meta property="og:video:height" content="320" />
	<meta property="og:video:type" content="video/mp4" />
	<meta property="og:video:url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />
	<meta property="og:video:secure_url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />
	
	<meta name="twitter:card" content="player">
	<meta name="twitter:app:name:googleplay" content="CricBoom">
	<meta name="twitter:app:id:googleplay" content="com.Khaleef.CricBoom" />
	<meta name="twitter:player" content="<?php echo base_url(). "embed/video/".$current_video -> id; ?>" />
	<meta name="twitter:player:stream" content="<?php echo base_url()."embed/serve/".$current_video -> id; ?>" />
	<meta name="twitter:player:stream:content_type" content="video/mp4" />
	<meta name="twitter:player:width" content="480">
	<meta name="twitter:player:height" content="480">
	*/ ?>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta name="twitter:title" content="<?php echo $current_video->social->title." - Cricwick"; ?>">
	<meta name="twitter:description" content="<?php echo $current_video->social->description; ?>">
	<meta name="twitter:image" content="<?php echo $current_video->social->thumb_file; ?>">

<?php } else { ?>
	<title><?php echo $og_tags->title; ?></title>
	<meta property="og:url"  content="<?php echo @$og_tags->link; ?>" />
	<meta property="og:title" content="<?php echo $og_tags->title; ?>" />
	<meta property="og:description" content="<?php echo $og_tags->descr; ?>" />
	<meta property="og:image" content="<?php echo $og_tags->image; ?>" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo @$og_tags->link; ?>" />
	<meta name="twitter:title" content="<?php echo $og_tags->title; ?>">
	<meta name="twitter:description" content="<?php echo $og_tags->descr; ?>">
	<meta name="twitter:image" content="<?php echo $og_tags->image; ?>">
	
<?php } ?>

<style type="text/css">
	img.wp-smiley, img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
</style>
<link href="<?php echo assets_url();  ?>home/css/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' id='contact-form-7-css'  href="<?php echo assets_url(); ?>css/styles.css?v=<?php echo VERSION; ?>" type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href="<?php echo assets_url(); ?>css/style.css?v=<?php echo VERSION; ?>" type='text/css' media='all' />
<link rel='stylesheet' id='theme-font-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href="<?php echo assets_url(); ?>css/font-awesome.min.css?92bc46" type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href="<?php echo assets_url(); ?>css/magnific-popup.css?92bc46" type='text/css' media='all' />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/custom.css?v=<?php echo VERSION; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/scorecard.css?v=<?php echo VERSION; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/custom-front.css?v=<?php echo VERSION; ?>" />
<script type='text/javascript' src="<?php echo assets_url(); ?>js/js/jquery-1.10.2.js" ></script>
<script type='text/javascript' src='<?php echo assets_url(); ?>js/jquery-migrate.min.js'></script>
<link href="<?php echo assets_url(); ?>css/cric_css/style_dir.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/owl.carousel.css" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/owl.theme.css" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/component.css" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/widget.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<?php if (empty($remove_responsive)){ ?>
<link href="<?php echo assets_url(); ?>css/cric_css/responsive.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<?php } ?>
<link href="<?php echo assets_url(); ?>css/cric_css/style_dir_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/style_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/layout.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/bootstrap-select.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<script>jwplayer.key="2HZsFtRYlzQZJpY2SmrCGnAe6fBk5mGtGN8MgA==";</script>
<?php
$language = $this->input->get("lang");
if (!$language){
	$language = $this->xession->get("language");
}
if ($language=='urdu' && $this->uri->segment(1)==login){
	?>
	<link rel='stylesheet' href='<?php echo assets_url(); ?>css/style_ur.css?v=<?php echo time(); ?>' type='text/css' media='all' />
	<?php
} 
?>
<style id='theme-style-inline-css' type='text/css'>
@media only screen and (max-width: 768px) {
.login.btn-default {
    padding: 5px 10px !important;
}

	.jw-icon-next, .jw-icon-playlist, .jw-icon-prev{
	    display: none !important;
	}
</style>
<?php if (ENVIRONMENT=='production'){ ?>
<!-- Google Analytics Code -->


<!-- Google Analytics Code -->
<?php } ?>
</head>
<body class="" <?php echo !empty($add_video_id_to_body)?'id="videos_page"':''; ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=430752913746475";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		<div class="wrapper-main hide-sm-screen">
			<?php $this->load->view("navigation"); ?>
			
			<?php echo $page; ?>
			
			<?php $this->load->view("footer"); ?>
		</div>
		

	<script type='text/javascript' src="<?php echo assets_url(); ?>js/functions.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/jquery.magnific-popup.min.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/bootstrap.min.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/bootstrap-select.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/js/own/owl.carousel.js" ></script>

	<script>window.twttr = (function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0],
	    t = window.twttr || {};
	  if (d.getElementById(id)) return t;
	  js = d.createElement(s);
	  js.id = id;
	  js.src = "https://platform.twitter.com/widgets.js";
	  fjs.parentNode.insertBefore(js, fjs);
	 
	  t._e = [];
	  t.ready = function(f) {
	    t._e.push(f);
	  };
	 
	  return t;
	}(document, "script", "twitter-wjs"));</script>

<script>

var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	
	$("body").on("click","#loadMoreVidosList", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).button("loading");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_videos/'+page_to_load,
			data: [],
			success: function(response) {
				response.timeline_videos.forEach(function(video) {
					
					html = '<div class="col-xs-6 col-sm-4 col-md-4 pt20"><a href="'+video.seo_url+'" >';
					html = html + '<img src="'+video.med_image+'" alt=""></a>';
					html = html + '<div class="cric-lockup-content"><h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom">'+video.title+'</h3>';
					html = html + '<div class="cric-lockup-meta">';
					html = html + '<ul class="cric-lockup-meta-info pl5 mt0 pb5">';
					html = html + '<li>'+video.views+' views</li>';
					html = html + '<li class="hidden">5 days ago</li>';
					html = html + '</ul></div></div></div>';
					
					$("#videoListContainter").append(html);
				});
				
				if (response.current_page==response.total_pages){
					btnRef.hide();
					btnRef.button("reset");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.button("reset");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("click",".btn-more-video-filter", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		var containerId = $(this).attr("data-container-id");
		var filter = $(this).attr("data-filter");
		$(this).button("loading");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_filter_videos/'+filter+'/'+page_to_load,
			data: [],
			success: function(response) {
				response.timeline_videos.forEach(function(item) {
					
					html = '<div class="col-xs-6 col-sm-4 col-md-3 pt20">';
					html = html + '<a href="'+item.seo_url+'"><img src="'+item.med_image+'" alt=""></a>';
					html = html + '<div class="cric-lockup-content">';
					html = html + '<h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom">'+item.title+'</h3>';
					html = html + '<div class="cric-lockup-meta">';
					html = html + '<ul class="cric-lockup-meta-info pl5 mt0 pb5">';
					html = html + '<li>'+item.views+' views</li>';
					html = html + '<li class="hidden">5 days ago</li>';
					html = html + '</ul>';
					html = html + '</div>';
					html = html + '</div>';
					html = html + '</div>';
					
					$("#"+containerId).append(html);
				});
				
				if (response.current_page==response.total_pages){
					btnRef.hide();
					btnRef.button("reset");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.button("reset");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("click","#loadMoreVidosRelatedTimeline", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		var video_id = $(this).attr("data-video-id");
		$(this).button("loading");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_timeline_related_videos/'+video_id+'/'+page_to_load,
			data: [],
			success: function(response) {
				response.related_videos.forEach(function(video) {
					
					html = '<div class="col-xs-6 col-sm-4 col-md-4 pt20"><a href="'+video.seo_url+'" >';
					html = html + '<img src="'+video.med_image+'" alt=""></a>';
					html = html + '<div class="cric-lockup-content"><h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom">'+video.title+'</h3>';
					html = html + '<div class="cric-lockup-meta">';
					html = html + '<ul class="cric-lockup-meta-info pl5 mt0 pb5">';
					html = html + '<li>'+video.views+' views</li>';
					html = html + '<li class="hidden">5 days ago</li>';
					html = html + '</ul></div></div></div>';
					
					$("#videoListContainter").append(html);
				});
				
				if (response.current_page==response.total_pages){
					btnRef.hide();
					btnRef.button("reset");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.button("reset");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("click","#more-results-btn", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'results/more_results/'+page_to_load,
			data: [],
			success: function(response) {
				response.matches.forEach(function(result) {
	
					var html = '<div class="col-md-12 results-block" onclick="window.location=\''+result.seo_url+'\'">';
					html = html + '<div class="row"><div class="col-md-2 col-xs-3"><div class="row"><div class="time">';
					html = html + result.local_time.date+'<br><span>'+result.local_time.month+'</span></div></div></div><div class="col-md-10 col-xs-9">';
					html = html + '<article class="cs-upcomming-fixtures"><div class="calendar-date">';
					html = html + '<figure><a href="'+result.seo_url+'"><img src="'+result.team_1.team.flag_url+'" alt="" class="result-img"></a></figure></div>';
					html = html + '<div class="text"><h2 ><a href="'+result.seo_url+'" class="hidden-xs">'+result.team_1.team.name+'</a>';
					html = html + '<span>VS</span><a href="'+result.seo_url+'" class="hidden-xs">'+result.team_2.team.name+'</a></h2>';
					html = html + '<ul class="post-option mt0 hidden-xs"><li>'+result.match_result+'</li></ul>';
					html = html + '</div><div class="match-result">';
					html = html + '<figure><a href="'+result.seo_url+'"><img src="'+result.team_2.team.flag_url+'" alt="" class="result-img"></a></figure>';
					html = html + '</div></article>';
					html = html + '<ul class="post-option mt0 visible-xs"><li>'+result.match_result+'</li></ul>';
					html = html + '</div></div></div>';					

					$("#results-container").append(html);
					
				});
				
				if (response.page_no==response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Results");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Results");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("click","#more-fixtures-btn", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'fixtures/more_fixtures/'+page_to_load,
			data: [],
			success: function(response) {
				response.matches.forEach(function(fixture) {
					
					//var teamASquadLink = base_url+'squad/'+fixture.series.id+"/"+fixture.teamA.id;
					//var teamBSquadLink = base_url+'squad/'+fixture.series.id+"/"+fixture.teamB.id;
					
					var html = '<div class="col-md-12" style="border-bottom: 6px solid #efefef;" >';
					html = html + '<div class="row"><div class="col-md-2 col-xs-3"><div class="row"><div class="time">';
					html = html + fixture.local_time.date+'<br><span>'+fixture.local_time.month+'</span><br><span>'+fixture.local_time.time+'</span>';
					html = html + '</div></div></div><div class="col-md-10 col-xs-9">';
					html = html + '<article class="cs-upcomming-fixtures"><div class="calendar-date">';
					html = html + '<figure><a href="javascript:;"><img src="'+fixture.team_1.team.flag_url+'" alt="" class="result-img"></a></figure></div>';
					html = html + '<div class="text"><h2 ><a href="javascript:;" class="hidden-xs">'+fixture.team_1.team.name+'</a>';
					html = html + '<span>VS</span><a href="javascript:;" class="hidden-xs">'+fixture.team_2.team.name+'</a></h2>';
					html = html + '<ul class="post-option mt0 hidden-xs"><li>'+fixture.title+'<BR>'+fixture.series.title+'</li></ul>';
					html = html + '</div><div class="match-result">';
					html = html + '<figure><a href="javascript:;"><img src="'+fixture.team_2.team.flag_url+'" alt="" class="result-img"></a></figure>';
					html = html + '</div></article>';
					html = html + '<ul class="post-option mt0 visible-xs"><li>'+fixture.title+'<BR>'+fixture.series.title+'</li></ul>';
					html = html + '</div></div></div>';	
					$("#fixtures-container").append(html);	
					
				});
				
				if (response.page_number>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Fixtures");
					
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Fixtures");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("click","#more-blogs-btn", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'blog/more_blog/'+page_to_load,
			data: [],
			success: function(response) {
				response.articles.forEach(function(article) {
					
					var html = '<div class="col-md-12 pt20 pl0 pr0">';
					html = html + '<div class="cs-blog blog-lrg mb20"><div class="cs-media">';
					html = html + '<figure><a href="'+article.seo_url+'"><img src="'+article.med_image+'" alt=""></a></figure>';
					html = html + '</div><section><div class="title">';
					html = html + '<h2 class="mt0"><a href="'+article.seo_url+'">'+article.title+'</a></h2>';
					html = html + '<ul class="post-option pl0"><li class="time-xs"><time>'+article.created_at+'</time></li></ul>';
					html = html + '</div><div class="blog-text">';
					html = html + '<p class="mt0">'+article.details+'</p>';
					html = html + '<div class="cs-seprator"><div class="devider1"></div></div>';
					html = html + '<ul class="post-option pl0">';
					html = html + '<li class="time-xs"><i class="fa fa-user"></i>By<a href="javascript:;"> '+article.author+'</a></li>';
					html = html + '</ul>';
					html = html + '<a href="'+article.seo_url+'" class="btnshare addthis_button_compact at300m"><i class="fa fa-share-square-o"></i></a>';
					html = html + '</div></section></div></div>';
				
					$("#results-container").append(html);	
					
				});
				
				if (response.current_page>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Articles");
					
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Articles");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("click",".social-icons-timeline a", function (e){
		e.preventDefault();	
		var url = jQuery(this).prop("href");
		var popup = window.open(url, "_blank", "scrollbars=1,resizable=1,height=300,width=450");
		return false;
	});
	
	$("body").on("click",".user", function (e){
		//e.preventDefault();	
		var phone = $(this).attr("phone");
		var sub_status = $(this).attr("sub_status");
		var msg = "Welcome <b>"+phone+"</b><BR>";
		msg = msg + "Subscription Status: ";
		if (sub_status==1){
			msg = msg + "Active";
		} else if (sub_status==3){
			msg = msg + "Suspended";
		} else {
			msg = msg + "Trial";
		}
		$('.alert-popup .modal-body').html(msg);
		$('.alert-popup').modal('show');
	});
	
	$("body").on("click",".social_sharing_link", function (e){
		e.preventDefault();	
		var url = jQuery(this).prop("href");
		var popup = window.open(url, "_blank", "scrollbars=1,resizable=1,height=300,width=450");
		return false;
	});
	
	
	$("body").on("submit","#login-form", function(){
		$("#login-form #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/check_login/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					//alert(response.message);
					window.location = response.url;
				} else {
					alert(response.message);
					if (response.clear_phone){
						$("#phone").val("");
					}
					$("#password").val("");
					$("#login-form #btnSubmit").button("reset");
				}
				
			}
		});
		
		return false;
	});
	
	$("body").on("submit","#phone-form", function(e){
		$("#phone-form #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/validate_phone_number/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					window.location='<?php echo base_url()."login/step2"; ?>';
				} else {
					$('.alert-popup .modal-body').html(response.message);
					$('.alert-popup').modal('show');
					$("#phone-form #btnSubmit").button("reset");
				}
			}
		});
		return false;
	});
	
	/*
	$("body").on("submit","#register-phone", function(){
		$("#register-phone #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/register_mobile/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					alert(response.message);
					window.location = base_url+"login";
				} else {
					alert(response.message);
					$("#register-phone #btnSubmit").button("reset");
				}
				
			}
		});
		return false;
	});
	*/
	$("body").on("submit","#regsiter-confirm-pin", function(){
		$("#regsiter-confirm-pin #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/confirm_pin_register/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					window.location = response.url;
				} else {
					alert(response.message);
					$("#register-phone #btnSubmit").button("reset");
				}
				
			}
		});
		return false;
	});
	
	$("body").on("submit","#sub-now", function(){
		$("#btnSubmit").button("loading");		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'home/sub/',
			data: [],
			success: function(response) {
				if(response.user.subscribe_status==1){
					window.location = base_url;
				} else if(response.user.subscribe_status==2){
					
					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");	
					
				} else if(response.user.subscribe_status==3){
					
					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");	
				} else {
					alert("<?php echo lang("general_error"); ?>");
					$("#btnSubmit").button("reset");	
				}
			}
		});
		return false;
	});
	
	$("body").on("submit","#forget-pass", function(){
		$("#btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/forget_password/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					alert(response.message);
					window.location = base_url+"login";
				} else {
					alert(response.message);
					$("#btnSubmit").button("reset");
				}
				
			}
		});
		return false;
	});
	
});
	
</script>

</body>
</html>