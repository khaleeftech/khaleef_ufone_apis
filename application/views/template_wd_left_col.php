<?php if ($video_page && 0){ ?>
<nav class="navbar navbar-inverse sub-navbar pt0 pb0" role="navigation">
	<div class="container pl10 pr0">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			<span class="">Filter Videos</span>
			<span class="fa fa-angle-down pull-right"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
			<ul class="nav navbar-nav">
				<li><a href="javascript:;">Most Viewed</a></li>
				<li><a href="javascript:;">Popular</a></li>
				<li><a href="javascript:;">Laugh</a></li>
				<li class="active"><a href="javascript:;">Worldcup T20</a></li>
				<li><a href="javascript:;">Asia Cup</a></li>
				<li><a href="javascript:;">Sixex</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
<!-- /.container -->
</nav>
<?php } ?>
<div class="wrapper wrapper-main">
  <section id="quiz" class="container secondary-page <?php echo ($video_page && 0)?"pt40 pl0 pr0":"pt20"; ?>">
    <div class="general general-results players">
    <!--Left welcome-->
      <?php echo $inner_page; ?>
      <!--Left welcome-->

      <!--right column-->
      <div class="col-md-3 right-column">
        <div class="top-score-title col-md-12 right-title">
          <div class="row">
            <div class="col-md-12 ">
              <div class="cs-section-title">
                <h2>Latest News</h2>
              </div>
            </div>
          </div>
          <div class="right-content">
          	<?php
						$news = mb_convert_encoding($this->memcached_library->get('cricwick_zain_home_news'), "UTF-8");
						$news = json_decode($news);
						$videos = mb_convert_encoding($this->memcached_library->get('cricwick_zain_home_videos'), "UTF-8");
						$videos = json_decode($videos)->timeline_videos;
						if(isset($news) && !empty($news)){
							$news = $news->data[0];
					}
          	?>
            <p class="news-title-right"><?php echo $news->title; ?></p>
            <p class="txt-right"><?php echo substr($news->details,0,200)."..."; ?>
            	<a class="read_more_anchor" href="<?php echo $news->seo_url; ?>"> Read More <i class="fa fa-angle-right"></i></a>
            </p>

          </div>
        </div>
        <div class="clear"></div>

        <div class="top-score-title col-md-12 right-title">
          <div class="row">
            <div class="col-md-12 ">
              <div class="cs-section-title">
                <h2>Recent Videos</h2>
              </div>
            </div>
          </div>
          <ul class="right-last-photo">
          	<?php foreach($videos as $video){ ?>
            <li class="side_panel_video" onclick="window.location='<?php echo base_url()."highlights/".$video->id."/".seo_url($video->title)."/"; ?>'">
              <div class="jm-item second">
                <div class="jm-item-wrapper">
                  <div class="jm-item-image"> <img src="<?php echo $video->med_image; ?>" alt="">
                  </div>
                </div>
              </div>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </section>

</div>


<script>
var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	$("body").on("submit","#quiz-form", function(){
		$(".answer-response").fadeOut();
		$("#btnSubmit").removeClass("btn-ok");
		$("#btnSubmit").button("loading");

		if($('input:radio:checked').length > 0){
			$.ajax({
				type: "POST",
				dataType: "json",
				url: base_url+'quiz/submit_ans',
				data: $(this).serializeArray(),
				success: function(response) {

					var img = "";
					if (response.status==1){
						$(".answer-response.correct").fadeIn();
					} else {
						$(".answer-response.incorrect").fadeIn();
					}
					//$("#btnSubmit").button("reset");
					//$("#btnSubmit").addClass("btn-ok");

					setTimeout(function(){
						window.location = base_url+'quiz';
					}, 1000);

				}
			});
		} else {

			$("#btnSubmit").button("reset");
			$("#btnSubmit").addClass("btn-ok");
			alert("Please select your answer!");
		}



		return false;
	});

	$("body").on("submit","#subscribe-quiz", function(){
		$("#btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'quiz/subscribe_quiz/',
			data: [],
			success: function(response) {
				if(response.user.subscribe_status_quiz==1){

					window.location = base_url+"quiz/";

				} else if(response.user.subscribe_status_quiz==2){

					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");

				} else if(response.user.subscribe_status_quiz==3){

					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");
				} else {

					alert("<?php echo lang("general_error"); ?>");
					$("#btnSubmit").button("reset");
				}

			}
		});
		return false;
	});
});


 </script>
