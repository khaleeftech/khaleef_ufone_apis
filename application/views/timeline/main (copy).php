<?php
	$teamA = "";
	$teamB = "";
	$innings = $live->innings;
	//count($innings);
	$first_batting_team_id = $innings[0]->batting_team;
	if ($live->fixture->teamA->id == $first_batting_team_id){
		$teamA = $live->fixture->teamA;
		$teamB = $live->fixture->teamB;
	} else {
		$teamA = $live->fixture->teamB;
		$teamB = $live->fixture->teamA;
	}
	
	$onstrike = 0;
	$offstrike = 1;
	$onspell = 0;
	$offspell = 1;
	
	$batsman = "";
	$bowlers = "";
	$batsman = @$innings[0]->batsmen_on_pitch;
	$bowlers = @$innings[0]->bowlers_on_spell;
	
	if (!empty($batsman[0]->on_strike)){
		$onstrike = 1;
		$offstrike = 0;
	}
	
	if (!!empty($bowlers[0]->on_spell_primary)){
		$onspell = 1;
		$offspell = 0;
	}
	
	$banner = empty($live->fixture->banner_file)?$default_banner:$live->fixture->banner_file;
	
	?>
<?php if ($is_live){ ?>
<div class="image-wrapper" style="background-image: url(<?php echo $banner; ?>); ">
	<div class="vs-circle"><span>vs</span></div>
	<div class="top-banner-scorecard" id="liveMatch<?php echo $live->id; ?>">
	    <div class="first-team-container batting">
	        <h3 class="teamtitle hidden-xs full"> <?php echo $teamA->title; ?>* </h3>
	        <h3 class="teamtitle hidden-md hidden-sm hidden-lg mini"> <?php echo $teamA->short_name; ?>* </h3>
	        <div class="first-team-content">
	            <div class="current-score-first-row">
	                <div class="score-container">
	                    <span class="current-score teamscore"> <?php echo $innings[0]->runs."/".$innings[0]->wickets; ?> </span>
	                    <div class="clearfix"></div>
	                    <span class="current-overs teamovers"> (<?php echo $innings[0]->overs; ?> ov) </span>
	                </div>
	                <div class="rr-container">
	                    <span class="run-rate hidden-xs"> CRR: <?php echo $innings[0]->run_rate; ?> </span>
	                </div>
	                
	                <div class="flag-container">
                    	<img src="<?php echo $teamA->flag; ?>">
	                </div>
	                
	            </div>
	            <div class="current-score-second-row">
	                <span class="player-name active-bold onstrike"> <?php echo !empty($batsman[$onstrike])?$players[$batsman[$onstrike]->player]->short_name."*":"-"; ?> </span>
	                <div class="clearfix visible-xs"></div>
	                <span class="player-score active-bold onstrike-score"> <?php echo !empty($batsman[$onstrike])?$batsman[$onstrike]->runs."(".$batsman[$onstrike]->balls.")":"-"; ?> </span>
	                <div class="clearfix"></div>
	                <span class="player-name offstrike"> <?php echo !empty($batsman[$offstrike])?$players[$batsman[$offstrike]->player]->short_name:"-"; ?> </span>
	                <div class="clearfix visible-xs"></div>
	                <span class="player-score offstrike-score"> <?php echo !empty($batsman[$offstrike])?$batsman[$offstrike]->runs."(".$batsman[$offstrike]->balls.")":"-"; ?> </span>
	            </div>
	            <div class="current-score-third-row tagline">
	            	<?php if (!empty($live->match_result)){ ?>
	            		<p><?php echo $live->match_result; ?></p>
	            	<?php } else if (count($innings)==1){ ?>
						<p><?php echo ($live->toss_won_by==$live->fixture->teamA->id?$live->fixture->teamA->title:$live->fixture->teamB->title)." won the toss and choose to ".($live->choose_to=="F"?"field":"bat")." first"; ?></p>
					<?php } else if (empty($live->match_result)){ ?>
						<p><?php echo $innings[0] ->title." require another ".$innings[0]->runs_req." runs from ".$innings[0]->balls_rem." balls"; ?></p>
					<?php } ?>
	            </div>
	        </div>
	    </div>
	    <div class="second-team-container bowling">
	        <h3 class="teamtitle hidden-xs full"> <?php echo $teamB->title; ?> </h3>
	        <h3 class="teamtitle hidden-md hidden-sm hidden-lg mini"> <?php echo $teamB->short_name; ?> </h3>
	        <div class="second-team-content">
	            <div class="current-score-first-row">
	            	<div class="flag-container">
                    	<img src="<?php echo $teamB->flag; ?>">
	                </div>
	                <div class="score-container">
	                    <span class="current-score teamscore"> <?php echo !empty($innings[1])?$innings[1]->runs."/".$innings[1]->wickets:"&nbsp;"; ?> </span>
	                    <div class="clearfix"></div>
	                    <span class="current-overs teamovers"> <?php echo !empty($innings[1])?" (".$innings[1]->overs." ov) ":"&nbsp;"; ?> </span>
	                </div>
	            </div>
	            <div class="current-score-second-row">
	                <span class="player-name active-bold onspell"> <?php echo !empty($bowlers[$onspell])?$players[$bowlers[$onspell]->player]->short_name."*":"-"; ?> </span>
	                <div class="clearfix visible-xs"></div>
	                <span class="player-score active-bold onspell-status"> <?php echo !empty($bowlers[$onspell])?$bowlers[$onspell]->runs."/".$bowlers[$onspell]->wickets." (".$bowlers[$onspell]->overs." ov)":"-"; ?> </span>
	                <div class="clearfix"></div>
	                <span class="player-name offspell"> <?php echo !empty($bowlers[$offspell])?$players[$bowlers[$offspell]->player]->short_name:"-"; ?> </span>
	                <div class="clearfix visible-xs"></div>
	                <span class="player-score offspell-status"> <?php echo !empty($bowlers[$offspell])?$bowlers[$offspell]->runs."/".$bowlers[$offspell]->wickets." (".$bowlers[$offspell]->overs." ov)":"-"; ?> </span>
	            </div>
	            <div class="current-score-third-row latest_balls">
			        <?php 
			        	$latest_balls = '&nbsp;';
			            foreach($live->innings[0]->latest_balls as $key=>$ball){
			            	if ($key==0){
			            		$latest_balls = get_ball_score_status_timeline_scorecard($ball);
			            	}
							if ($key>5){
								break;
							} else {
								$latest_balls .= get_ball_score_status_timeline_scorecard($ball);
							}
						}
						echo $latest_balls;
					?>
	                
	            </div>
	
	        </div>
	    </div>
	</div>
</div>
<?php } else { ?>
<div class="image-wrapper end_match" style="background-image: url(<?php echo $banner; ?>);">
	<div class="match_result">
		<h2>
			<?php echo $teamA->title." (".@$innings[0]->runs."/".@$innings[0]->wickets.")"; ?>
			<span class="visible-xs"><div class="cleafix"></div></span><span>vs</span><span class="visible-xs"><div class="cleafix"></div></span>
			<?php echo $teamB->title." (".@$innings[1]->runs."/".@$innings[1]->wickets.")"; ?>
		</h2>
		<h3><?php echo $live->match_result; ?></h3>
	</div>
</div>
<?php } ?>
<div class="clearfix"></div>
<div class="timeline-main-menu">
    <a href="javascript:;" class="switch sc" data-src="scorecard_area">Scorecard</a>
    <a href="javascript:;" class="switch tm active" data-src="timeline_area" class="">Match Timeline</a>
    <a href="javascript:;" class="switch mf" data-src="match_fact_area">Match Facts</a>
    <a href="javascript:;" class="hidden">Articles</a>
    <a href="javascript:;" class="hidden">Predict & Win</a>
</div>
	<div id="site-container" class="clearfix" style="background-color: #eaebed; min-height: 500px">
		<div class="scorecard_area custom-content-divs" style="display: none"><div class="container"></div></div>
		<div class="match_fact_area custom-content-divs" style="display: none"></div>
		<div class="timeline_area custom-content-divs">
			<div class="filter-icons">
				<a href="javascript:;" class="btn-filter-n" data-tag-id="23" data-page="1" data-count="<?php echo @$tags_stats["t_boom"]; ?>">
					<div class="boom">BooM</div>
				</a>
			    <a href="javascript:;" class="btn-filter-n" data-tag-id="2" data-page="1" data-count="<?php echo @$tags_stats["t_four"]; ?>">
			    	<div class="four">4s</div>
			    </a>
			   	<a href="javascript:;" class="btn-filter-n" data-tag-id="0" data-page="1" data-count="<?php echo @$tags_stats["t_all"]; ?>" >
			   		<div class="all active">ALL</div>
			   	</a>
			    <a href="javascript:;" class="btn-filter-n" data-tag-id="1" data-page="1" data-count="<?php echo @$tags_stats["t_six"]; ?>">
			    	<div class="sIx">6s</div>
			    </a>
			    <a href="javascript:;" class="btn-filter-n" data-tag-id="3" data-page="1" data-count="<?php echo @$tags_stats["t_out"]; ?>">
			    	<div class="wicket">W</div>
			    </a>
			</div>
			<div class="container">
				<div id="content" role="main">
					<div id="loadingDiv">
						<img src="<?php echo assets_url()."images/loading.gif"; ?>" />
					</div>
				</div>
				<div class="nextPageLoader text-center" style="display: none;"><img src="<?php echo assets_url()."images/ajax-loader.gif"; ?>"> Loading</div>
			</div>
		</div>
	</div>
<style>
.jw-icon-fullscreen{
	display: none;
}
</style>
<script>
var base_url = "<?php echo base_url(); ?>";
var match_id = "<?php echo $match_id; ?>";
var current_tag = 0;
var page_to_load = 0;
var add_social_icon = true;
var activate_player = false;
var sharing_url = "";
var sharing_title = "";
var video_allowed = <?php echo $video_allowed; ?>;
var load_on_scroll = true;
var loadingImage = '<div id="loadingDiv"><img src="<?php echo assets_url()."images/loading.gif"; ?>" /></div>';
var top_item_id = 0;
var bottom_item_id = 0;
var first_item_of_timeline = 0;
var current_available_items = <?php echo $this->data["tags_stats"]["t_total"]; ?>;

$( document ).ready(function(){
	
	<?php if (!empty($is_live)){ ?>
		recursive_call_id = setInterval(function() {
			  updateLiveMatches();
		}, 10000);
		
		recursive_call_id_stats = setInterval(function() {
			  updateLiveMatchFilterStats();
		}, 10000);
		
		window.onbeforeunload = function(e) {
		  clearInterval(recursive_call_id);
		  clearInterval(recursive_call_id_stats);
		  console.log("Clearing Interval");
		};
		
	<?php } ?>
	

	$(window).scroll(function() {
	  if($(window).scrollTop() + $(window).height() == $(document).height()) {
	  	  if ($(".switch.tm").hasClass("active")){
	  	  	if (load_on_scroll){
	  	  		load_on_scroll = false;
	  	  		get_next_page_current_timeline();
	  	  	}
	  	  }
	      
	  }
	});
	
	$(".btn-filter-n").on("click",function(){
		var tag_id = $(this).attr("data-tag-id");

		var url = base_url+'ajax/fetch_timeline/'+match_id;
		$(this).siblings().children().removeClass("active");
		$(this).children().addClass("active");
		if (tag_id!=0){
			url = url + "/1/"+tag_id;
			$(this).children(".label").html("");
		} else {
			$(".btn-filter-n").children(".label").html("");
		}
		current_tag = tag_id;
		refresh_timeline(url);
	});
	
	$(".switch").on("click",function(){
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
		
		if ($(this).hasClass("sc")){ //scorecard clicked
			var container = $(this).attr("data-src");
			$("."+container+" .container").html(loadingImage);
			$(".custom-content-divs").hide();
			$("."+container).fadeIn();
			var url = base_url+'home/scorecard/'+match_id;
			$.ajax({
				dataType: "html",
				url: url,
				data: {},
				success: function(response) {
					$("."+container+" .container").html(response);
				},
				error: function (e){
					$("."+container+" .container").html('<div class="alert alert-danger">Scorecard not available right now, please try again later!</div>');
				}
			});
		} else if ($(this).hasClass("tm")){
			var container = $(this).attr("data-src");
			//$("."+container+" .container").html(loadingImage);
			$(".custom-content-divs").hide();
			$("."+container).fadeIn();
		}
	});
	
	refresh_timeline(base_url+'ajax/fetch_timeline/'+match_id);
	
});

function get_next_page_current_timeline() {
	if (page_to_load!=0){
		$(".nextPageLoader").show();
		var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/previous/'+bottom_item_id;
		
		$.ajax({
			dataType: "json",
			url: url,
			data: {},
			success: function(response) {
				$(".nextPageLoader").hide();
				
				append_timeline(response,"append");
				/*
				if (response.current_page < response.total_pages){
					page_to_load = response.current_page+1;
				} else {
					page_to_load = 0;
				}
				*/
				if (bottom_item_id == response.first_timeline_id){
					page_to_load = 0
				} else {
					page_to_load = 1;
				}
				load_on_scroll = true;
			},
			error: function (e){
				
			}
		});
	} else {
		console.log("page to load is zero");
	}
	
}

function refresh_timeline(url_to_call){
	
	$("#content").html(loadingImage);
	
	$.ajax({
		dataType: "json",
		url: url_to_call,
		data: {},
		success: function(response) {
			$("#content").html('<ul id="timeline" class="clearfix"></ul>');
			append_timeline(response,"append");
			if (response.current_page < response.total_pages){
				page_to_load = 2;
			} else {
				page_to_load = 0;
			}
			load_on_scroll = true;
		},
		error: function (e){
			$("#content").html('<div class="alert alert-danger">Timeline not available right now, please try again later!</div>');
		}
	});
}

function append_timeline(response,toDo){
	response.timeline.forEach(function(item) {
		add_social_icon = true;
		activate_player = false;
		playlist = new Array();
		if (item.tags.length>0){
			var contentType = item.tags[0].name;
			
			var boxToPlace = '<li class="animated fadeInUp">';
			boxToPlace = boxToPlace + '<article  class="post type-post status-publish format-quote has-post-thumbnail hentry category-category-trio tag-post-format tag-quotes post_format-post-format-quote has_thumb clearfix">';
			
			if (item.tags.length>0){
				
				boxToPlace = boxToPlace + '<span class="entry-date" style="background: '+item.tags[0].color_code+' !important;"><span class="entry-meta-date">';
				
				if (item.tags[0].name!='Ball' && item.tags[0].name!='Six' && item.tags[0].name!='Four' && item.tags[0].name!='Out'){
					boxToPlace = boxToPlace + '<time>'+item.tags[0].name+'</time></span></span>';
				} else {
					boxToPlace = boxToPlace + '<time>'+item.tags[0].customized_tag+'</time></span></span>';
				}
				
			}
			if (!$.isEmptyObject(item.video)){
				boxToPlace = boxToPlace + '<div class="hentry-box">';
				boxToPlace = boxToPlace + '<h3 class="image-title">'+item.video.title+'</h3>';
				//boxToPlace = boxToPlace + '<video controls poster="'+item.video.med_image+'">';
				//boxToPlace = boxToPlace + '<source src="'+item.video.video_file+'" />';
				//boxToPlace = boxToPlace + '</video>';
				boxToPlace = boxToPlace + '<div webkitAllowFullScreen mozallowfullscreen allowFullScreen id="video'+item.video.id+'"></div>';
				
				if (!$.isEmptyObject(item.ball)){
					boxToPlace = boxToPlace + '<p class="image-description">'+item.ball.commentary+'</p>';
				} else {
					boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
				}

				if (!$.isEmptyObject(item.video.pre_ad)){
					var file = {file: item.video.pre_ad.video_file, image: item.video.med_image};
					playlist.push(file);
				}
				
				qualities = new Array();
				var i = parseInt("1");
				var qualityItem = "";
				item.video.qualities.forEach(function(q) {
					if (i==2){
						qualityItem = {file: q.video_file, label: q.height+"P", "default": true};
					} else {
						qualityItem = {file: q.video_file, label: q.height+"P"};
					}
					i++;
					qualities.push(qualityItem);
				});
				if (!$.isEmptyObject(item.video.video_file)){
					qualityItem = {file: item.video.video_file, label: "Source"};
					qualities.push(qualityItem);
				}
				file = {
					sources: qualities,
					image: item.video.med_image
				};
				playlist.push(file);
				
				if (!$.isEmptyObject(item.video.post_ad)){
					file = {file: item.video.post_ad.video_file, image: item.video.med_image};
					playlist.push(file);
				}
				//console.log(playlist);
				activate_player = true;
				sharing_url = base_url+'highlights/'+item.video.id+'/'+item.video.seo_url+"/";
				sharing_title = item.video.title;
				
			} else if (!$.isEmptyObject(item.image)){
				boxToPlace = boxToPlace + '<div class="hentry-box">';
				boxToPlace = boxToPlace + '<h3 class="image-title">'+item.title+'</h3>';
				boxToPlace = boxToPlace + '<img class="icc-image" src="'+item.image+'"/>';
				//boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
				if (!$.isEmptyObject(item.ball)){
					boxToPlace = boxToPlace + '<p class="image-description">'+item.ball.commentary+'</p>';
				} else {
					boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
				}
				add_social_icon = false;
			
			} else {
				
				if (contentType=='Ball' || contentType=='Four' || contentType=='Six' || contentType=='Out' ){
					boxToPlace = boxToPlace + '<div class="hentry-box no-padding">';
					boxToPlace = boxToPlace + '<div class="entry-quote-white commentry"><p>';
					boxToPlace = boxToPlace + item.ball.commentary;
					boxToPlace = boxToPlace + '</p></div>';
					add_social_icon = false;
					
				} else if (contentType=='News'){
					
					boxToPlace = boxToPlace + '<div class="hentry-box">';
					boxToPlace = boxToPlace + '<h3 class="news-title">'+item.news.title+'</h3>';
					boxToPlace = boxToPlace + '<p class="news-sub-title">By Cricboom Staff</p>';
					boxToPlace = boxToPlace + '<p class="news-sub-title">March 08, 2016</p>';
					boxToPlace = boxToPlace + '<p class="news-description">'+item.news.details+'</p>';
					boxToPlace = boxToPlace + '<div class="news-image"><img class="icc-image" src="'+item.news.med_image+'"/></div>';
					add_social_icon = false;
	
				
				} else if (contentType=='Article'){
					
					boxToPlace = boxToPlace + '<div class="hentry-box">';
					boxToPlace = boxToPlace + '<h3 class="article-title">'+item.title+'</h3>';
					boxToPlace = boxToPlace + '<p class="article-sub-title">By Cricboom Staff</p>';
					boxToPlace = boxToPlace + '<p class="article-sub-title">March 08, 2016</p>';
					boxToPlace = boxToPlace + '<div class="article-image"><img class="icc-image" src="'+item.article.med_image+'"/></div>';
					boxToPlace = boxToPlace + '<p class="article-details">'+item.article.details+'</p>';
					add_social_icon = false;
										
				} else if (contentType=='Tweet'){
					boxToPlace = boxToPlace + '<div class="hentry-box">';
					boxToPlace = boxToPlace + '<blockquote class="twitter-tweet" lang="es">';
					boxToPlace = boxToPlace + '<p lang="en">'+item.tweet.text+'</p>';
					boxToPlace = boxToPlace + '— '+item.tweet.user.name+' (@'+item.tweet.user.screen_name+')';
					boxToPlace = boxToPlace + '<a href="https://twitter.com/'+item.tweet.user.screen_name+'/status/'+item.tweet.id+'"> '+item.tweet.created_at+'</a>';
					boxToPlace = boxToPlace + '</blockquote>';
					add_social_icon = false;
				} else {
					boxToPlace = boxToPlace + '<div class="hentry-box no-padding">';
					boxToPlace = boxToPlace + '<blockquote class="entry-quote-white commentry"><p>';
					boxToPlace = boxToPlace + item.desc;
					boxToPlace = boxToPlace + '</p></blockquote>';
					add_social_icon = false;
				}
				
			}
			
			if (add_social_icon){
				boxToPlace = boxToPlace + '<div class="social-icons-timeline" class="clearfix">';
				boxToPlace = boxToPlace + '<a href="https://plus.google.com/share?url='+sharing_url+'"><i class="fa fa-google-plus"></i></a>';
				boxToPlace = boxToPlace + '<a href="#" class="hidden"><i class="fa fa-instagram"></i></a>';
				boxToPlace = boxToPlace + '<a href="http://www.twitter.com/share?text='+sharing_title+'&url='+sharing_url+'"><i class="fa fa-twitter"></i></a>';
				boxToPlace = boxToPlace + '<a href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u='+sharing_url+'&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a>';
				boxToPlace = boxToPlace + '</div></div></article></li>';
			}
			
	
		}
		
		if(toDo=='append'){
			$("#timeline").append(boxToPlace);
			bottom_item_id = item.id;
			
			if (top_item_id==0){
				top_item_id = item.id;
			}
			//console.log(bottom_item_id);
		} else  if (toDo=='prepend'){
			$("#timeline").prepend(boxToPlace);
			top_item_id = item.id;
		}
		
		if (activate_player){
			var playerInstance = jwplayer("video"+item.video.id);
			playerInstance.setup({
			    playlist: playlist,
			    width: "100%",
			    controls: true
			}).onPlay(function() {
				if (video_allowed==0){
					jwplayer("video"+item.video.id).stop();
					alert('Sorry, this content is not available in your country!');
				}
				
			});
		}
		
	});
}

function updateLiveMatchFilterStats(){
	
	$.ajax({
		dataType: "json",
		url: base_url+"ajax/get_match_content_stats/"+match_id,
		data: {},
		success: function(response) {
			/*
			current_all = $(".btn-filter.all").attr("data-count");
			current_out = $(".btn-filter.wicket").attr("data-count");
			current_four = $(".btn-filter.four").attr("data-count");
			current_six = $(".btn-filter.six").attr("data-count");
			
			if (response.t_all>current_all){
				$(".btn-filter.all .label").html(response.t_all-current_all);
				$(".btn-filter.all").attr("data-count",response.t_all);
			}
			if (response.t_out>current_out){
				$(".btn-filter.wicket .label").html(response.t_out-current_out);
				$(".btn-filter.wicket").attr("data-count",response.t_out);
			}
			if (response.t_four>current_four){
				$(".btn-filter.four .label").html(response.t_four-current_four);
				$(".btn-filter.four").attr("data-count",response.t_four);
			}
			if (response.t_six>current_six){
				$(".btn-filter.six1 .label").html(response.t_six-current_six);
				$(".btn-filter.six1").attr("data-count",response.t_six);
			}
			*/
			if (current_available_items < response.t_total){
				var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/next/'+top_item_id;
				$.ajax({
					dataType: "json",
					url: url,
					data: {},
					success: function(response) {
						append_timeline(response,"prepend");
					},
					error: function (e){
						
					}
				});
				current_available_items = response.t_total;
				console.log("Top Auto Refreshed");
				//console.log(url);
			}
			
			
		}
	});
}

function updateLiveMatches(){
	$.ajax({
		dataType: "json",
		url: base_url+'ajax/updateHomeLiveMatchScore',
		data: {},
		success: function(response) {
			response.live_matches.forEach(function(live) {
				
				$("#liveMatch"+live.match_id+" .batting .onstrike").html(live.batting.onstrike.short_name);
				$("#liveMatch"+live.match_id+" .batting .onstrike-score").html(live.batting.onstrike.score);
				$("#liveMatch"+live.match_id+" .batting .teamscore").html(live.batting.score);
				$("#liveMatch"+live.match_id+" .batting .teamflag").attr("src",live.batting.flag);
				$("#liveMatch"+live.match_id+" .batting .offstrike").html(live.batting.offstrike.short_name);
				$("#liveMatch"+live.match_id+" .batting .offstrike-score").html(live.batting.offstrike.score);
				var overs = '('+live.batting.overs+' ov)';
				$("#liveMatch"+live.match_id+" .batting .teamovers").html(overs);
				$("#liveMatch"+live.match_id+" .batting .teamtitle.full").html(live.batting.title+'*');
				$("#liveMatch"+live.match_id+" .batting .teamtitle.mini").html(live.batting.title_small+'*');
				
				if (live.bowling.score!=''){
					$("#liveMatch"+live.match_id+" .bowling .teamscore").html(live.bowling.score);
					$("#liveMatch"+live.match_id+" .bowling .teamflag").attr("src",live.bowling.flag);
					$("#liveMatch"+live.match_id+" .bowling .teamtitle.full").html(live.bowling.title);
					$("#liveMatch"+live.match_id+" .bowling .teamtitle.mini").html(live.bowling.title_small);
					overs = '('+live.bowling.overs+' ov)';
					$("#liveMatch"+live.match_id+" .bowling .teamovers").html(overs);
				}
				
				$("#liveMatch"+live.match_id+" .bowling .onspell").html(live.bowling.onspell.short_name);
				$("#liveMatch"+live.match_id+" .bowling .onspell-status").html(live.bowling.onspell.status);
				
				$("#liveMatch"+live.match_id+" .bowling .offspell").html(live.bowling.offspell.short_name);
				$("#liveMatch"+live.match_id+" .bowling .offspell-status").html(live.bowling.offspell.status);
				
				var tagline = '<p>'+live.tagline+'</p>';
				$("#liveMatch"+live.match_id+" .tagline").html(tagline);
				$("#liveMatch"+live.match_id+" .bowling .latest_balls").html(live.latest_balls);
			});
		}
	});
}

 </script>
