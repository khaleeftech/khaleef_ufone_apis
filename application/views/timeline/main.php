<?php
	$teamA = "";
	$teamB = "";
	$innings = $live->innings;
	$current_inning = count($innings)-1;
	$batting_team_id = $innings[$current_inning]->batting_team_id;
	$fielding_team_id = $innings[$current_inning]->fielding_team_id;

	$teamA = $teams[$batting_team_id];
	$teamB = $teams[$fielding_team_id];
	
	$batsman_onstrike = "";
	$batsman_offstrike = "";

	$bowler_onspell = "";
	$bowler_offspell = "";

	if (!empty($live->partnership_and_bowlers)){
		if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="facing"){
			$batsman_onstrike = $live->partnership_and_bowlers->batsman_1;
		} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="facing"){
			$batsman_onstrike = $live->partnership_and_bowlers->batsman_2;
		}

		if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="batting"){
			$batsman_offstrike = $live->partnership_and_bowlers->batsman_1;
		} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="batting"){
			$batsman_offstrike = $live->partnership_and_bowlers->batsman_2;
		}

		if (!empty($live->partnership_and_bowlers->bowler)){
			$bowler_onspell = $live->partnership_and_bowlers->bowler;
		}
		if (!empty($live->partnership_and_bowlers->last_bowler)){
			$bowler_offspell = $live->partnership_and_bowlers->last_bowler;
		}
	}
	
	$banner = empty($live->banner_file_name)?$default_banner:BACKEND."../".$live->banner_url;

	$innings[$current_inning]->runs = empty($innings[$current_inning]->runs)?0:$innings[$current_inning]->runs;
	$innings[$current_inning]->wickets = empty($innings[$current_inning]->wickets)?0:$innings[$current_inning]->wickets;
	$innings[$current_inning]->overs = empty($innings[$current_inning]->overs)?"0.0":$innings[$current_inning]->overs;
	$innings[$current_inning]->run_rate = empty($innings[$current_inning]->run_rate)?"0.0":$innings[$current_inning]->run_rate;
	
?>

<?php if ($is_live){ ?>
<div class="image-wrapper" style="background-image: url(<?php echo $banner; ?>); ">
	<div class="vs-circle"><span>vs</span></div>
	<div class="top-banner-scorecard" id="liveMatch<?php echo $live->id; ?>">
		<div class="hidden-md hidden-lg hidden-sm" style="background-color: gray; height: 40px; margin-top: -10px;">
	       <h2 class="mt5 mb0 pt5 pl5 pull-left" style="color:white;font-size:18px;"><?php echo $teamA->short_name; ?> vs <?php echo $teamB->short_name; ?></h2>
	       <?php if (!empty($live_stream)){ ?>
	       	<button class="btn pull-right cs-bg-color" style="margin-top:3px;margin-right:10px" onclick="window.location='<?php echo base_url()."livestream"; ?>'">Watch Live!</button>
	       <?php } ?>
	    </div>
	    <div class="first-team-container batting">
	        <h3 class="teamtitle hidden-xs full" style="background: <?php echo $teamA->color_hex; ?>;"> <?php echo $teamA->name; ?>* </h3>
	        <h3 class="teamtitle hidden-xs hidden-md hidden-sm hidden-lg mini" style="background: <?php echo $teamA->color_hex; ?>;"> <?php echo $teamA->short_name; ?>* </h3>
	        <div class="first-team-content">
	            <div class="current-score-first-row">
	                <div class="score-container">
	                	<?php if ($live->format!='Test'){ ?>
		                    <span class="current-score teamscore"> <?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?> </span>
		                    <div class="clearfix"></div>
		                    <span class="current-overs teamovers"> (<?php echo $innings[$current_inning]->overs; ?> ov) </span>
	                    <?php } else { ?>
	                    	<?php
	                    		$batting_team_previous_innings = false;
	                    		foreach($innings as $inn){
	                    			if ($inn->batting_team_id==$batting_team_id && $inn->id!=$innings[$current_inning]->id){
	                    				$batting_team_previous_innings = $inn;
										break;
	                    			}
	                    		}
								if ($batting_team_previous_innings){
									$declared = $batting_team_previous_innings->declared?'d':'';
									?>
									<span class="current-score teamscore" style="color:#b1b1b1; font-weight:normal"> <?php echo $batting_team_previous_innings->runs."/".$batting_team_previous_innings->wickets.$declared; ?> &amp;</span>
				                    <div class="clearfix"></div>
				                    <span class="current-overs teamovers"><strong><?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?></strong>&nbsp;(<?php echo $innings[$current_inning]->overs; ?> ov) </span>
									<?php
								} else {
									?>
									<span class="current-score teamscore"> <?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?> </span>
				                    <div class="clearfix"></div>
				                    <span class="current-overs teamovers"> (<?php echo $innings[$current_inning]->overs; ?> ov) </span>
									<?php
								}
	                    	?>
	                    <?php } ?>
	                </div>
	                <div class="rr-container">
	                    <span class="run-rate hidden-xs <?php echo $live->format=='Test'?'hidden':''; ?>"> CRR: <?php echo $innings[$current_inning]->run_rate; ?> </span>
	                </div>
	                
	                <div class="flag-container">
                    	<img src="<?php echo $teamA->flag_url; ?>" class="teamflag">
	                </div>
	                
	            </div>
	            <div class="current-score-second-row">
	                <span class="player-name active-bold onstrike"> <?php echo !empty($batsman_onstrike)?$batsman_onstrike->player->short_name."*":"-"; ?> </span>
	                
	                <span class="player-score active-bold onstrike-score"> <?php echo !empty($batsman_onstrike)?$batsman_onstrike->batsman->runs_scored."(".$batsman_onstrike->batsman->balls_played.")":"-"; ?> </span>
	                <div class="clearfix"></div>
	                <span class="player-name offstrike"> <?php echo !empty($batsman_offstrike)?$batsman_offstrike->player->short_name:"-"; ?> </span>
	                
	                <span class="player-score offstrike-score"> <?php echo !empty($batsman_offstrike)?$batsman_offstrike->batsman->runs_scored."(".$batsman_offstrike->batsman->balls_played.")":"-"; ?> </span>
	            </div>
	            <div class="current-score-third-row tagline">
	            	<?php if ($live->format!='Test'){ ?>
		            	<?php if (count($innings)==1){ ?>
							<p class="hidden-xs"><?php echo $teams[$live->toss_won_by_id]->short_name." won the toss and choose to ".($live->chose_to=="Bowl"?"field":"bat")." first"; ?></p>
							<p class="visible-xs">Run Rate: <?php echo $innings[$current_inning]->run_rate; ?></p>
						<?php } else { ?>
							<p class="hidden-xs"><?php echo "Need ".($innings[$current_inning]->runs_required+1-$innings[$current_inning]->runs)." runs from ".$innings[$current_inning]->balls_remaining." balls"; ?></p>
							<p class="visible-xs"><?php echo "Req ".($innings[$current_inning]->runs_required+1-$innings[$current_inning]->runs)." in ".$innings[$current_inning]->balls_remaining." balls @ RR:".$innings[$current_inning]->required_rate; ?></p>
						<?php } ?>
					<?php } else { ?>
						<strong><?php echo $live->title; ?></strong><?php echo !empty($live->day)?" | Day $live->day":''; ?><?php echo !empty($live->break_type)?" | $live->break_type":" | Session $live->session"; ?>
					<?php } ?>
	            </div>
	        </div>
	    </div>
	    <div class="second-team-container bowling">
	        <h3 class="teamtitle hidden-xs full" style="background: <?php echo $teamB->color_hex; ?>;"> <?php echo $teamB->name; ?> </h3>
	        <h3 class="teamtitle hidden-xs hidden-md hidden-sm hidden-lg mini" style="background: <?php echo $teamB->color_hex; ?>;"> <?php echo $teamB->short_name; ?> </h3>
	        <div class="second-team-content">
	            <div class="current-score-first-row">
	            	<div class="flag-container">
                    	<img src="<?php echo $teamB->flag_url; ?>" class="teamflag">
	                </div>
	                <div class="score-container">
	                	<?php if ($live->format!='Test'){ ?>
		                    <span class="current-score teamscore"> <?php echo !empty($innings[$current_inning-1])?$innings[$current_inning-1]->runs."/".$innings[$current_inning-1]->wickets:"&nbsp;"; ?> </span>
		                    <div class="clearfix"></div>
		                    <span class="current-overs teamovers"> <?php echo !empty($innings[$current_inning-1])?" (".$innings[$current_inning-1]->overs." ov) ":"&nbsp;"; ?> </span>
	                    <?php } else { ?>
	                    	<?php
	                    		$fielding_team_first_inning = false;
								$fielding_team_second_inning = false;
	                    		foreach($innings as $inn){
	                    			if ($inn->batting_team_id==$fielding_team_id){
	                    				if (!$fielding_team_first_inning){
	                    					$fielding_team_first_inning = $inn;
	                    				} else {
	                    					$fielding_team_second_inning = $inn;
	                    				}
	                    			}
	                    		}
								$declared_1 = $fielding_team_first_inning->declared?'d':'';
								$declared_2 = @$fielding_team_second_inning->declared?'d':'';
								if ($fielding_team_second_inning){
									
									?>
									<span class="current-score teamscore" style="color:#b1b1b1; font-weight:normal"> <?php echo $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1; ?> &amp;</span>
				                    <div class="clearfix"></div>
				                    <span class="current-overs teamovers" style="color:#b1b1b1; font-weight:normal"><?php echo $fielding_team_second_inning->runs."/".$fielding_team_second_inning->wickets.$declared_2; ?></span>
									<?php
								} else if ($fielding_team_first_inning) {
									?>
									<span class="current-score teamscore" style="color:#b1b1b1; font-weight:normal"> <?php echo $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1; ?> </span>
				                    <div class="clearfix"></div>
				                    <span class="current-overs teamovers" style="color:#b1b1b1; font-weight:normal"> (<?php echo $fielding_team_first_inning->overs; ?> ov) </span>
									<?php
								} else { ?>
									<span class="current-score teamscore" style="color:#b1b1b1; font-weight:normal"> - </span>
				                    <div class="clearfix"></div>
				                    <span class="current-overs teamovers" style="color:#b1b1b1; font-weight:normal"> - </span>
				                    <?php
								}
	                    	?>
	                    	
	                    <?php } ?>
	                </div>
	            </div>
	            <div class="current-score-second-row">
	                <span class="player-name active-bold onspell"> <?php echo !empty($bowler_onspell)?$bowler_onspell->player->short_name."*":"-"; ?> </span>
	                
	                <span class="player-score active-bold onspell-status"> <?php echo !empty($bowler_onspell)?$bowler_onspell->bowler->runs_given."/".$bowler_onspell->bowler->wickets_taken." (".$bowler_onspell->bowler->overs_bowled.")":"-"; ?> </span>
	                <div class="clearfix"></div>
	                <span class="player-name offspell"> <?php echo !empty($bowler_offspell)?$bowler_offspell->player->short_name:"-"; ?> </span>
	                
	                <span class="player-score offspell-status"> <?php echo !empty($bowler_offspell)?$bowler_offspell->bowler->runs_given."/".$bowler_offspell->bowler->wickets_taken." (".$bowler_offspell->bowler->overs_bowled.")":"-"; ?> </span>
	            </div>
	            <div class="current-score-third-row latest_balls">
			        <?php 
			        $latest_balls = "";
			        /*
	       				$req_over = !empty($live->innings[0]->latest_balls[0])?$live->innings[0]->latest_balls[0]->over_number:1;
                  		$this_over_balls = array();
						foreach($live->innings[0]->latest_balls as $b){
							if ($b->over_number==$req_over){
								$this_over_balls[] = $b;
							} else {
								break;
							}
						}
						//$this_over_balls = array_reverse($this_over_balls);
			        	$latest_balls = '&nbsp;';
			            //foreach($live->innings[0]->latest_balls as $key=>$ball){
			            foreach($this_over_balls as $key=>$ball){	
					
			            	if ($key==0){
			            		$latest_balls = get_ball_score_status_timeline_scorecard($ball);
			            	}  else {
								$latest_balls = get_ball_score_status_timeline_scorecard($ball).$latest_balls;
							}
						}
						echo $latest_balls;
						*/
						if (!empty($live->partnership_and_bowlers) && !empty($live->partnership_and_bowlers->latest_balls)){
							foreach($live->partnership_and_bowlers->latest_balls as $ball){
								$latest_balls .= get_ball_score_status_timeline_scorecard($ball);
							}
						}
						if (empty($latest_balls)){
							?>
							&nbsp;&nbsp;
							<?php
						} else {
							echo $latest_balls;
						}
						
					?>
	            </div>
	
	        </div>
	    </div>
	</div>
</div>
<?php } else { ?>
<div class="image-wrapper end_match" style="background-image: url(<?php echo $banner; ?>);">
	<div class="match_result">
		<?php if ($live->format!='Test'){ ?>
			<h2>
				<?php echo $teamA->name." (".@$innings[$current_inning]->runs."/".@$innings[$current_inning]->wickets.")"; ?>
				<span class="visible-xs"><div class="cleafix"></div></span><span>vs</span><span class="visible-xs"><div class="cleafix"></div></span>
				<?php echo $teamB->name." (".@$innings[$current_inning-1]->runs."/".@$innings[$current_inning-1]->wickets.")"; ?>
			</h2>
		<?php } else { ?>
			<?php 
				$teamA_first = false;
				$teamA_second = false;
				$teamB_first = false;
				$teamB_second = false;
				foreach($innings as $inn){
        			if ($inn->batting_team_id==$batting_team_id){
        				if (!$teamA_first){
        					$teamA_first = $inn;
        				} else {
        					$teamA_second = $inn;
        				}
        			} else {
        				if (!$teamB_first){
        					$teamB_first = $inn;
        				} else {
        					$teamB_second = $inn;
        				}
        			}
        		}
			?>
			<h2>
				<?php 
					$declared_1 = $teamA_first->declared?'d':'';
					$declared_2 = $teamA_second->declared?'d':'';
					echo $teamA->name." (".@$teamA_first->runs."/".@$teamA_first->wickets.$declared_1; 
					if ($teamA_second){
						echo " & ".@$teamA_second->runs."/".@$teamA_second->wickets.$declared_2;
					}
					echo ")";
				?>
				<span class="visible-xs"><div class="cleafix"></div></span><span>vs</span><span class="visible-xs"><div class="cleafix"></div></span>
				<?php 
					$declared_1 = $teamB_first->declared?'d':'';
					$declared_2 = $teamB_second->declared?'d':'';
					echo $teamB->name." (".@$teamB_first->runs."/".@$teamB_first->wickets.$declared_1; 
					if ($teamB_second){
						echo " & ".@$teamB_second->runs."/".@$teamB_second->wickets.$declared_2;
					}
					echo ")";
				?>
			</h2>
		<?php } ?>
		<h3><?php echo $live->match_result; ?></h3>
	</div>
</div>
<?php } ?>

<div class="clearfix"></div>

<div class="timeline-main-menu">
	<div class="col-lg-6 col-xs-6 col-md-6">
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-md-6 pull-right">
				<div class="row">
					<style>
                   .timeline-main-menu .sc, .timeline-main-menu .tm {
                            background-color: #b1b1b1  !important;
                        }
                        .timeline-main-menu .sc:hover, .timeline-main-menu .sc.active, .timeline-main-menu .tm:hover, .timeline-main-menu .tm.active{
                            background: #07569e  none repeat scroll 0 0 !important;
                            border: medium none;
                            color: #fff !important;
                            text-decoration: none;
                        }
					</style>
					<a href="javascript:;" class="switch mt0 mb0 tm active" data-src="timeline_area" style="width: 100%; text-align: center; margin-left: 0px; height: 44px; padding-top: 11px !important;">Timeline</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-xs-6 col-md-6">
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-md-6">
				<div class="row">
					<a href="javascript:;" class="switch mt0 mb0 sc" data-src="scorecard_area" style="width:100%">Scorecard</a>
					<?php if (!empty($live_stream)){ ?>
                    	<button class="btn pull-right cs-bg-color visible-md visible-lg" style="margin-top: 3px; margin-right: 10px; position: absolute; right: -255px; top: 2px;" onclick="window.location='<?php echo base_url()."livestream"; ?>'">Watch Live!</button>
                	<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

	<div id="site-container" class="clearfix" style="background-color: #eaebed; min-height: 500px">
		<div class="scorecard_area custom-content-divs" style="display: none"><div class="container"></div></div>
		<div class="match_fact_area custom-content-divs" style="display: none"></div>
		<?php if (!empty($live_stream) && !$stream_allowed && 0){ ?>
			<div class="live_stream_area custom-content-divs" >
				<div class="container">
					<br>
					<div class="col-sm-12">
						<img class="buy_stream_anchor" src="<?php echo base_url()."assets/ads/buy_live_stream_4sar.jpg"; ?>"><BR>
						<div class="buy_stream">
							<a href="javascript:;" class="btn btn-success buy_stream_anchor" data-loading-text="Please wait...">Watch Live Stream Now!</a>
						</div>
					</div>
				</div>
			</div>

		<?php }  ?>
		<div class="timeline_area custom-content-divs" >
			<div class="filter-icons hidden">
				<a href="javascript:;" class="btn-filter-n" data-tag-id="23" data-page="1" data-count="<?php echo @$tags_stats["t_boom"]; ?>">
					<div class="boom">BooM</div>
				</a>
			    <a href="javascript:;" class="btn-filter-n" data-tag-id="2" data-page="1" data-count="<?php echo @$tags_stats["t_four"]; ?>">
			    	<div class="four">4s</div>
			    </a>
			   	<a href="javascript:;" class="btn-filter-n" data-tag-id="0" data-page="1" data-count="<?php echo @$tags_stats["t_all"]; ?>" >
			   		<div class="all active">ALL</div>
			   	</a>
			    <a href="javascript:;" class="btn-filter-n" data-tag-id="1" data-page="1" data-count="<?php echo @$tags_stats["t_six"]; ?>">
			    	<div class="sIx">6s</div>
			    </a>
			    <a href="javascript:;" class="btn-filter-n" data-tag-id="3" data-page="1" data-count="<?php echo @$tags_stats["t_out"]; ?>">
			    	<div class="wicket">W</div>
			    </a>
			</div>
			<div class="timeline-filter-menu">
				<div class="col-xs-offset-3 col-md-6">
					<span class="hidden-xs">Filters: </span>
					<a class=" btn-filters btn-filter-n gray active" data-tag-id="0" data-page="1" data-count="<?php echo @$tags_stats["t_all"]; ?>">All</a>
					<a class=" btn-filters btn-filter-n gray" data-tag-id="1" data-page="1" data-count="<?php echo @$tags_stats["t_six"]; ?>">6s</a>
					<a class=" btn-filters btn-filter-n gray" data-tag-id="2" data-page="1" data-count="<?php echo @$tags_stats["t_four"]; ?>">4s</a>
					<a class=" btn-filters btn-filter-n gray" data-tag-id="3" data-page="1" data-count="<?php echo @$tags_stats["t_out"]; ?>">W</a>
				</div>
			</div>
			<div class="clearfix"></div>
			
			<div class="container">
				<div id="content" role="main">
					<div id="loadingDiv">
						<img src="<?php echo assets_url()."images/loading.gif"; ?>" />
					</div>
				</div>
				<div class="nextPageLoader text-center" style="display: none;"><img src="<?php echo assets_url()."images/ajax-loader.gif"; ?>"> Loading</div>
			</div>
		</div>	
	</div>
<style>
.jw-icon-fullscreen{
	display: none;
}
#live_stream_player .jw-icon-fullscreen{
	display: inline;
}
.EmbeddedTweet{
	max-width: 100% !important;
}
.cs-bg-color.active.focus, .cs-bg-color.active:focus, .cs-bg-color.focus, .cs-bg-color:active.focus, .cs-bg-color:active:focus, .cs-bg-color:focus, .cs-bg-color:hover{
	color: #FFF;
	outline: none !important;
}
</style>
<script>
var base_url = "<?php echo base_url(); ?>";
var match_id = "<?php echo $match_id; ?>";
var match_stream_id = "<?php echo $match_stream_id; ?>";
var current_tag = 0;
var page_to_load = 0;
var add_social_icon = true;
var activate_player = false;
var sharing_url = "";
var sharing_title = "";
var video_allowed = <?php echo $video_allowed; ?>;
var load_on_scroll = true;
var loadingImage = '<div id="loadingDiv"><img src="<?php echo assets_url()."images/loading.gif"; ?>" /></div>';
var top_item_id = 0;
var bottom_item_id = 0;
var first_item_of_timeline = 0;
var current_available_items = <?php echo $this->data["tags_stats"]["t_total"]; ?>;
var match_title = '<?php echo " | ".$teamA->name." vs ".$teamB->name." | Cricwick"; ?>';

$( document ).ready(function(){

	var default_title = '<?php echo empty($innings[$current_inning]->runs)?"0/":$innings[$current_inning]->runs."/"; ?>';
	default_title = default_title + '<?php echo empty($innings[$current_inning]->wickets)?0:$innings[$current_inning]->wickets; ?>';
	default_title = default_title + " | "+'<?php echo empty($innings[$current_inning]->overs)?"0.0":$innings[$current_inning]->overs; ?>'+'ov';

	$('title').html(default_title+match_title);
	
	<?php if (!empty($is_live)){ ?>
		<?php if (empty($live_stream) || !$stream_allowed ){ ?>
			recursive_call_id = setInterval(function() {
				  updateLiveMatches();
			}, 10000);
		<?php } ?>
		recursive_call_id_stats = setInterval(function() {
			  updateLiveMatchFilterStats();
		}, 10000);
		
		window.onbeforeunload = function(e) {
		  clearInterval(recursive_call_id);
		  clearInterval(recursive_call_id_stats);
		  console.log("Clearing Interval");
		};
		
	<?php } ?>
	
	$('.selectpicker').on('loaded.bs.select', function (e) {
		//console.log("adasd");
	  	$(".bootstrap-select .btn-default").addClass("active");
	});
	
	

	$(window).scroll(function() {
	  if($(window).scrollTop() + $(window).height() == $(document).height()) {
	  	  if ($(".switch.tm").hasClass("active")){
	  	  	if (load_on_scroll){
	  	  		load_on_scroll = false;
	  	  		get_next_page_current_timeline();
	  	  	}
	  	  }
	      
	  }
	});
	
	$(".btn-filter-n").on("click",function(){
		load_on_scroll = false;
		var tag_id = $(this).attr("data-tag-id");

		var url = base_url+'ajax/fetch_timeline/'+match_id;
		$(".btn-filter-n").removeClass("active");
		$(this).addClass("active");
		if (tag_id!=0){
			url = url + "/1/"+tag_id;
			//$(this).children(".label").html("");
		} else {
			//$(".btn-filter-n").children(".label").html("");
		}
		current_tag = tag_id;
		refresh_timeline(url);
		load_on_scroll = true;
	});
	
	$("body").on("click",".buy_stream_anchor",function(){
		if (confirm("You will be charged with 4 SAR for live streaming. Are you sure you want to continue")){
			
			btnRef = $('.btn.btn-success.buy_stream_anchor');
			btnRef.button("loading");
			var url = base_url+'timeline/purchase_live_stream/'+match_stream_id;
			$.ajax({
				dataType: "json",
				url: url,
				data: {},
				success: function(response) {
					if (response.status==1){
						window.location='<?php echo base_url()."livestream"; ?>';
					} else {
						alert(response.message);
						btnRef.button("reset");
					}
				}
			});
		}
		return false;
	});
	$("body").on("click",".scorecard-tabs",function(e){
		e.preventDefault();
		$(".scorecard-tabs").parent().removeClass("active");
		//$("#tab1").removeClass("active");
		$(".tab").removeClass("active");
		$(this).parent().addClass("active");
		$($(this).attr("tabid")).addClass("active");
		return false;
	});
	
	$("body").on("change","#timeline_filter",function(e){
		$(".switch.sc").removeClass("active");
		var container ="timeline_area";
		$(".custom-content-divs").hide();
		$("."+container).fadeIn();
		
		load_on_scroll = false;
		var tag_id = $(this).val();
		if (tag_id=="-1"){
			return false;
		}

		var url = base_url+'ajax/fetch_timeline/'+match_id;
		//$(this).siblings().children().removeClass("active");
		//$(this).children().addClass("active");
		if (tag_id!=0){
			url = url + "/1/"+tag_id;
		} 
		current_tag = tag_id;
		refresh_timeline(url);
		load_on_scroll = true;
		$(".switch.tm").addClass("active");
		$(this).siblings(0).addClass("active");
		
	});
	
	$(".switch").on("click",function(){
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
		
		if ($(this).hasClass("sc")){ //scorecard clicked
			$('#timeline_filter').siblings(0).removeClass("active");
			$(".switch.tm").removeClass("active");
			//$('#timeline_filter').val("-1");
			//$('#timeline_filter option[value=-1]').attr('selected','selected');
			//$('.filter-option').html('Timeline');
			var container = $(this).attr("data-src");
			$("."+container+" .container").html(loadingImage);
			$(".custom-content-divs").hide();
			//$(".live_stream_area .container").html("");
			$("."+container).fadeIn();
			var url = base_url+'home/scorecard/'+match_id;
			$.ajax({
				dataType: "html",
				url: url,
				data: {},
				success: function(response) {
					$("."+container+" .container").html(response);
				},
				error: function (e){
					$("."+container+" .container").html('<div class="alert alert-danger">Scorecard not available right now, please try again later!</div>');
				}
			});
		} else if ($(this).hasClass("tm")){
			var container = $(this).attr("data-src");
			//$("."+container+" .container").html(loadingImage);
			$(".switch.sc").removeClass("active");
			//$(".bootstrap-select .btn-default").addClass("active");
			$(".custom-content-divs").hide();
			//$(".live_stream_area .container").html("");
			$("."+container).fadeIn();
			
		} else if ($(this).hasClass("live_stream")){
			var container = $(this).attr("data-src");
			
			<?php /*if ($stream_allowed) { ?>
				$("."+container+" .container").html('<BR><div class="col-sm-12" style="margin-bottom: 15px;"><div id="live_stream_player"></div></div>');
			<?php } else { ?>
				var subimg = '<br><div class="col-sm-12"><img class="" src="<?php echo base_url()."assets/ads/buy_live_stream_4sar.jpg"; ?>"><BR>';
				subimg = subimg + '<div class="buy_stream"><a href="javascript:;" class="btn btn-success buy_stream_anchor" data-loading-text="Please wait...">Click Here to Purchase Stream</a></div></div>';
				$("."+container+" .container").html(subimg);
			<?php }*/ ?>
			$(".custom-content-divs").hide();
			$("."+container).fadeIn();
			
			<?php if ($stream_allowed) {/* ?>
			var playerInstance = jwplayer("live_stream_player");
			playerInstance.setup({
				sources: <?php echo $live_sources ?>,
				androidhls: 'true',
			    width: "100%",
			    aspectratio: "16:9",
			    controls: true,
			    primary: "flash",
			    autostart: true
			});
			<?php */} ?>
		}
	});
	
	refresh_timeline(base_url+'ajax/fetch_timeline/'+match_id);
	
	
	
});

function get_next_page_current_timeline() {
	if (page_to_load!=0){
		$(".nextPageLoader").show();
		var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/previous/'+bottom_item_id;
		
		$.ajax({
			dataType: "json",
			url: url,
			data: {},
			success: function(response) {
				$(".nextPageLoader").hide();
				
				append_timeline(response,"append");
				/*
				if (response.current_page < response.total_pages){
					page_to_load = response.current_page+1;
				} else {
					page_to_load = 0;
				}
				*/
				if (bottom_item_id == response.first_timeline_id){
					page_to_load = 0
				} else {
					page_to_load = 1;
				}
				load_on_scroll = true;
			},
			error: function (e){
				
			}
		});
	} else {
		console.log("page to load is zero");
	}
	
}

function refresh_timeline(url_to_call){
	
	$("#content").html(loadingImage);
	
	$.ajax({
		dataType: "json",
		url: url_to_call,
		data: {},
		success: function(response) {
			$("#content").html('<ul id="timeline" class="clearfix"></ul>');
			append_timeline(response,"append");
			if (response.current_page < response.total_pages){
				page_to_load = 2;
			} else {
				page_to_load = 0;
			}
			load_on_scroll = true;
		},
		error: function (e){
			$("#content").html('<div class="alert alert-danger">Timeline not available right now, please try again later!</div>');
		}
	});
}

function append_timeline(response,toDo){
	
	response.timeline.forEach(function(item) {
		add_social_icon = true;
		activate_player = false;
		activate_twitter = false;
		playlist = new Array();
		if (item.tags.length>0){
			var contentType = item.tags[0].name;
			
			var boxToPlace = '<li class="animated fadeInUp" data-timeline-id="'+item.id+'">';
			boxToPlace = boxToPlace + '<article  class="post type-post status-publish format-quote has-post-thumbnail hentry category-category-trio tag-post-format tag-quotes post_format-post-format-quote has_thumb clearfix">';
			
			if (item.tags.length>0){
				
				boxToPlace = boxToPlace + '<span class="entry-date" style="background: '+item.tags[0].color_code+' !important;"><span class="entry-meta-date">';
				
				if (item.tags[0].name!='Ball' && item.tags[0].name!='Six' && item.tags[0].name!='Four' && item.tags[0].name!='Out'){
					boxToPlace = boxToPlace + '<time>'+item.tags[0].name+'</time></span></span>';
				} else {
					boxToPlace = boxToPlace + '<time>'+item.tags[0].customized_tag+'</time></span></span>';
				}
				
			}
			if (!$.isEmptyObject(item.video)){
				boxToPlace = boxToPlace + '<div class="hentry-box" data-viewed="0">';
				boxToPlace = boxToPlace + '<h3 class="image-title">'+item.video.title+'</h3>';
				//boxToPlace = boxToPlace + '<video controls poster="'+item.video.med_image+'">';
				//boxToPlace = boxToPlace + '<source src="'+item.video.video_file+'" />';
				//boxToPlace = boxToPlace + '</video>';
				boxToPlace = boxToPlace + '<div id="video'+item.video.id+'"></div>';
				
				if (!$.isEmptyObject(item.ball)){
					boxToPlace = boxToPlace + '<p class="image-description">'+item.ball.commentary+'</p>';
				} else {
					boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
				}

				if (!$.isEmptyObject(item.video.pre_ad)){
					var file = {file: item.video.pre_ad.video_file, image: item.video.med_image};
					playlist.push(file);
				}
				
				qualities = new Array();
				var i = parseInt("1");
				var qualityItem = "";
				item.video.qualities.forEach(function(q) {
					if (i==2){
						qualityItem = {file: q.video_file, label: q.height+"P", "default": true};
					} else {
						qualityItem = {file: q.video_file, label: q.height+"P"};
					}
					i++;
					qualities.push(qualityItem);
				});
				if (!$.isEmptyObject(item.video.video_file)){
					qualityItem = {file: item.video.video_file, label: "720P"};
					qualities.push(qualityItem);
				}
				file = {
					sources: qualities,
					image: item.video.med_image
				};
				playlist.push(file);
				
				if (!$.isEmptyObject(item.video.post_ad)){
					file = {file: item.video.post_ad.video_file, image: item.video.med_image};
					playlist.push(file);
				}
				//console.log(playlist);
				activate_player = true;
				sharing_url = base_url+'highlights/'+item.video.id+'/'+item.video.seo_url+"/";
				sharing_title = item.video.title;
				
			} else if (!$.isEmptyObject(item.image)){
				boxToPlace = boxToPlace + '<div class="hentry-box">';
				boxToPlace = boxToPlace + '<h3 class="image-title">'+item.title+'</h3>';
				boxToPlace = boxToPlace + '<img class="icc-image" src="'+item.image+'"/>';
				//boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
				if (!$.isEmptyObject(item.ball)){
					boxToPlace = boxToPlace + '<p class="image-description">'+item.ball.commentary+'</p>';
				} else {
					boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
				}
				add_social_icon = false;
			
			} else {
				
				if (contentType=='Ball' || contentType=='Four' || contentType=='Six' || contentType=='Out' ){
					boxToPlace = boxToPlace + '<div class="hentry-box no-padding">';
					boxToPlace = boxToPlace + '<div class="entry-quote-white commentry"><p>';
					boxToPlace = boxToPlace + item.ball.commentary;
					boxToPlace = boxToPlace + '</p></div>';
					add_social_icon = false;
					
				} else if (contentType=='News'){
					
					boxToPlace = boxToPlace + '<div class="hentry-box">';
					boxToPlace = boxToPlace + '<h3 class="news-title">'+item.news.title+'</h3>';
					boxToPlace = boxToPlace + '<p class="news-sub-title">By '+item.news.author+'</p>';
					boxToPlace = boxToPlace + '<p class="news-sub-title">'+item.news.created_at+'</p>';
					boxToPlace = boxToPlace + '<p class="news-description">'+item.news.details+'</p>';
					boxToPlace = boxToPlace + '<div class="news-image"><img class="icc-image" src="'+item.news.med_image+'"/></div>';
					add_social_icon = false;
	
				
				} else if (contentType=='Article'){
					
					boxToPlace = boxToPlace + '<div class="hentry-box">';
					boxToPlace = boxToPlace + '<h3 class="article-title">'+item.title+'</h3>';
					boxToPlace = boxToPlace + '<p class="article-sub-title">By '+item.article.author+'</p>';
					boxToPlace = boxToPlace + '<p class="article-sub-title">'+item.article.created_at+'</p>';
					boxToPlace = boxToPlace + '<div class="article-image"><img class="icc-image" src="'+item.article.med_image+'"/></div>';
					boxToPlace = boxToPlace + '<p class="article-details">'+item.article.details+'</p>';
					//boxToPlace = boxToPlace + '<a class="read-more-article" href="'+item.article.seo_url+'"> Read Full Article> </a>';
					add_social_icon = false;
										
				} else if (contentType=='Tweet'){
					boxToPlace = boxToPlace + '<div id="twitter-feed-'+item.id+'">';
					/*
					boxToPlace = boxToPlace + '<div class="hentry-box" id="twitter-feed-'+item.id+'">';
					boxToPlace = boxToPlace + '<blockquote class="twitter-tweet" lang="es">';
					boxToPlace = boxToPlace + '<p lang="en">'+item.tweet.text+'</p>';
					boxToPlace = boxToPlace + '— '+item.tweet.user.name+' (@'+item.tweet.user.screen_name+')';
					boxToPlace = boxToPlace + '<a href="https://twitter.com/'+item.tweet.user.screen_name+'/status/'+item.desc+'"> '+item.tweet.created_at+'</a>';
					boxToPlace = boxToPlace + '</blockquote>';
					*/
					add_social_icon = false;
					activate_twitter = true;
				} else {
					boxToPlace = boxToPlace + '<div class="hentry-box no-padding">';
					boxToPlace = boxToPlace + '<blockquote class="entry-quote-white commentry"><p>';
					boxToPlace = boxToPlace + item.title;
					boxToPlace = boxToPlace + '</p></blockquote>';
					add_social_icon = false;
				}
				
			}
			
			add_social_icon = false; // Hide Social sharing buttons
			if (add_social_icon){
				boxToPlace = boxToPlace + '<div class="social-icons-timeline" class="clearfix">';
				boxToPlace = boxToPlace + '<a href="https://plus.google.com/share?url='+sharing_url+'"><i class="fa fa-google-plus"></i></a>';
				boxToPlace = boxToPlace + '<a href="#" class="hidden"><i class="fa fa-instagram"></i></a>';
				boxToPlace = boxToPlace + '<a href="http://www.twitter.com/share?text='+sharing_title+'&url='+sharing_url+'"><i class="fa fa-twitter"></i></a>';
				boxToPlace = boxToPlace + '<a href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u='+sharing_url+'&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a>';
				boxToPlace = boxToPlace + '</div></div></article></li>';
			}

		}
		
		if(toDo=='append'){
			$("#timeline").append(boxToPlace);
			bottom_item_id = item.id;
			
			if (top_item_id==0){
				top_item_id = item.id;
			}
			//console.log(bottom_item_id);
		} else  if (toDo=='prepend'){
			$("#timeline").prepend(boxToPlace);
			top_item_id = item.id;
		}
		
		if (activate_player){
			//console.log("activating"+item.video.id);
			var playerInstance = jwplayer("video"+item.video.id);
			playerInstance.setup({
			    playlist: playlist,
			    width: "100%",
			    controls: true,
			    preload: "metadata",
			    title: item.video.title,
				ga: {
					label: "title"
				}
			}).onPlay(function() {
				if (video_allowed==0){
					jwplayer("video"+item.video.id).stop();
					alert('Sorry, this content is not available in your country!');
				} else {
					if (!$("#video"+item.video.id).parent().attr("data-view-confirmed")){
						jQuery.ajax({
							dataType: "json",
							url: base_url+'ajax/confirm_timeline_view/'+item.video.id,
							data: {},
							success: function(response) {
								
							}
						});
						$("#video"+item.video.id).parent().attr("data-view-confirmed",1);
					}
				}
				
			});
		}
		
		if (activate_twitter){
			twttr.widgets.createTweet(
			  item.desc,
			  document.getElementById('twitter-feed-'+item.id),
			  {
			    width: "100%"
			  })
			  .then(function (el) {
			    
			    $('iframe').contents().find("head")
      				.append($("<style type='text/css'>  .EmbeddedTweet{max-width: 100% !important;}  </style>"));
			  });
		}
		
	});
}

function updateLiveMatchFilterStats(){
	/*
	$.ajax({
		dataType: "json",
		url: base_url+"ajax/get_match_content_stats/"+match_id,
		data: {},
		success: function(response) {
			
			current_all = $(".btn-filter.all").attr("data-count");
			current_out = $(".btn-filter.wicket").attr("data-count");
			current_four = $(".btn-filter.four").attr("data-count");
			current_six = $(".btn-filter.six").attr("data-count");
			
			if (response.t_all>current_all){
				$(".btn-filter.all .label").html(response.t_all-current_all);
				$(".btn-filter.all").attr("data-count",response.t_all);
			}
			if (response.t_out>current_out){
				$(".btn-filter.wicket .label").html(response.t_out-current_out);
				$(".btn-filter.wicket").attr("data-count",response.t_out);
			}
			if (response.t_four>current_four){
				$(".btn-filter.four .label").html(response.t_four-current_four);
				$(".btn-filter.four").attr("data-count",response.t_four);
			}
			if (response.t_six>current_six){
				$(".btn-filter.six1 .label").html(response.t_six-current_six);
				$(".btn-filter.six1").attr("data-count",response.t_six);
			}
			
			if (current_available_items < response.t_total){
				var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/next/'+top_item_id;
				$.ajax({
					dataType: "json",
					url: url,
					data: {},
					success: function(response) {
						append_timeline(response,"prepend");
					},
					error: function (e){
						
					}
				});
				current_available_items = response.t_total;
				console.log("Top Auto Refreshed");
				//console.log(url);
			}			
		}
	});
	*/
	var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/next/'+top_item_id;
	$.ajax({
		dataType: "json",
		url: url,
		data: {},
		success: function(response) {
			append_timeline(response,"prepend");
			console.log("Top Auto Refreshed");
		},
		error: function (e){
			
		},timeout: 8000
	});
}

function updateLiveMatches(){
	var match_updated = false;
	$.ajax({
		dataType: "json",
		url: base_url+'ajax/updateHomeLiveMatchScore',
		data: {},
		success: function(response) {
			response.live_matches.forEach(function(live) {
				
				if (live.match_id==match_id){
					$("#liveMatch"+live.match_id+" .batting .onstrike").html(live.batting.onstrike.short_name);
					$("#liveMatch"+live.match_id+" .batting .onstrike-score").html(live.batting.onstrike.score);
					$("#liveMatch"+live.match_id+" .batting .teamscore").html(live.batting.score);
					$("#liveMatch"+live.match_id+" .batting .run-rate").html("CRR: "+live.batting.run_rate);
					$("#liveMatch"+live.match_id+" .batting .teamflag").attr("src",live.batting.flag);
					$("#liveMatch"+live.match_id+" .batting .offstrike").html(live.batting.offstrike.short_name);
					$("#liveMatch"+live.match_id+" .batting .offstrike-score").html(live.batting.offstrike.score);
					var overs = '';
					if (live.format!='Test'){
						overs = '('+live.batting.overs+' ov)';
					} else {
						overs = live.batting.overs;
					}
					
					$("#liveMatch"+live.match_id+" .batting .teamovers").html(overs);
					$("#liveMatch"+live.match_id+" .batting .teamtitle.full").html(live.batting.title+'*');
					$("#liveMatch"+live.match_id+" .batting .teamtitle.mini").html(live.batting.title_small+'*');
					$("#liveMatch"+live.match_id+" .batting .teamtitle").css("background",live.batting.color);
					
					if (live.bowling.score!=''){
						$("#liveMatch"+live.match_id+" .bowling .teamscore").html(live.bowling.score);
						$("#liveMatch"+live.match_id+" .bowling .teamflag").attr("src",live.bowling.flag);
						$("#liveMatch"+live.match_id+" .bowling .teamtitle.full").html(live.bowling.title);
						$("#liveMatch"+live.match_id+" .bowling .teamtitle.mini").html(live.bowling.title_small);
						$("#liveMatch"+live.match_id+" .bowling .teamtitle").css("background",live.bowling.color);
						if (live.format!='Test'){
							overs = '('+live.bowling.overs+' ov)';
						} else {
							overs = live.bowling.overs;
						}
						$("#liveMatch"+live.match_id+" .bowling .teamovers").html(overs);
					}
					
					$("#liveMatch"+live.match_id+" .bowling .onspell").html(live.bowling.onspell.short_name);
					$("#liveMatch"+live.match_id+" .bowling .onspell-status").html(live.bowling.onspell.status);
					
					$("#liveMatch"+live.match_id+" .bowling .offspell").html(live.bowling.offspell.short_name);
					$("#liveMatch"+live.match_id+" .bowling .offspell-status").html(live.bowling.offspell.status);
					
					if (live.format!='Test'){
						var tagline = '<p>'+live.tagline+'</p>';
						var tagline_small = '<p>'+live.tagline_small+'</p>';
						$("#liveMatch"+live.match_id+" .tagline .hidden-xs").html(tagline);
						$("#liveMatch"+live.match_id+" .tagline .visible-xs").html(tagline_small);
						if (live.latest_balls!=""){
							$("#liveMatch"+live.match_id+" .bowling .latest_balls").html(live.latest_balls);
						}
					} else {
						$("#liveMatch"+live.match_id+" .tagline").html(live.tagline);
					}
					
					$('title').html(live.html_title+match_title);
		
					match_updated = true;
				} 
				
			});
			
			if (!match_updated){
				window.location = "<?php echo $post_match_url; ?>";
			}
		},timeout: 8000
	});
}

 </script>
