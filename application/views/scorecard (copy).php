
<div class="top-score-title right-score col-md-9" >
	<div class="top-score-title player-vs">

		<div class="main">
			<div class="tabs standard">
				<div class="tab-content">
					<div id="tab1" class="tab active">
						<!--<h3 class="tab-match-title">MATCH STATISTICS</h3>-->
						<div class="tabs standard">
			
							<div class="tab-content">
								<div id="tab1a" class="tab">
									&nbsp;
								</div>
								<div id="tab2a" class="tab active">
									<div class="col-md-12">
										<!-- Match Information -->
										<div class=" pt10 pb10">
											<div class="innings-information">
												<div class="team-1-name">
						                        	<?php echo $first_inning->title; ?> 
						                        	<span class="innings-1-score <?php echo empty($second_inning)?'innings-current':''; ?> "> 
						                        	<?php echo $first_inning->runs."/".$first_inning->wickets." (".$first_inning->overs."/".$first_inning->total_overs.")"; ?>	
						                        	</span> 
						                        </div>
						                        
						                        <?php if (!empty($second_inning)){ ?>
						                        	<div class="team-2-name"> 
							                        	<?php echo $second_inning->title; ?>
							                        	<span class="innings-1-score innings-current"> 
							                        		<?php echo $second_inning->runs."/".$second_inning->wickets." (".$second_inning->overs."/".$second_inning->total_overs.")"; ?>
							                        	</span> 
						                        	</div>
						                        <?php } ?>
												<div class="innings-requirement">
													<?php
													if ($match_details->toss_won_by==$match_details->fixture->teamA->id){
														echo $match_details->fixture->teamA->title;
													} else {
														echo $match_details->fixture->teamB->title;
													}
													?>
													won the toss and chose to <?php echo $match_details->choose_to=="F"?'Field':'Bat'; ?> first.
													<BR>
													<?php
													if ($match_details->playing_state=="E"){
														echo $match_details->match_result;
													} else {
														?>
														<?php if (empty($second_inning)){ ?>
															<div class="innings-requirement"> Run Rate: <?php echo $first_inning->run_rate; ?>  </div>
														<?php } else { ?>
								                        	<div class="innings-requirement">
								                        		Run Rate: <?php echo $second_inning->run_rate; ?><BR>
								                        		Req Rate: <?php echo $second_inning->req_rate; ?><BR>
								                        		Need <?php echo $second_inning->runs_req; ?> Runs for <?php echo $second_inning->balls_rem; ?> Balls
								                        	</div>
								                        <?php } ?>
								                    <?php
													}
													?>
												</div>
											</div>
											<div>
												<a href="" class="share-score color-theme hidden"><i class="fa fa-share-alt"></i></a>
											</div>
										</div>
										<!-- End -->
										<div class="row">
											<hr class="secondary-hr">
											<div class="large 20 columns">
												<table width="100%" class="batting-table innings">
												<tbody>
												<tr class="tr-heading">
													<th colspan="2" scope="col" class="th-innings-heading">
														<?php echo $first_inning->title; ?> <span class="normal"> (<?php echo $first_inning->overs; ?> Overs) </span>
													</th>
													<th scope="col" class="th-r">R</th>
													<th scope="col" class="th-b">B</th>
													<th scope="col" class="th-4s">4s</th>
													<th scope="col" class="th-6s">6s</th>
													<th scope="col" class="th-sr hide-sr">SR</th>
												</tr>
												<?php foreach($first_inning->batsmen_summary as $batsman){ ?>
												<tr>
													<td class="batsman-name">
														<a href="javascript:;" class="playerName"><?php echo $players[$batsman->player]->short_name; ?></a>
													</td>
													<td class="dismissal-info"><?php echo $batsman->is_out?$batsman->out_details:"(not out)"; ?></td>
													<td class="bold"><?php echo $batsman->runs; ?></td>
													<td class=""><?php echo $batsman->balls; ?></td>
													<td class=""><?php echo $batsman->boundary_4s; ?></td>
													<td class=""><?php echo $batsman->boundary_6s; ?></td>
													<td class="hide-sr"><?php echo $batsman->strike_rate; ?></td>
												</tr>
												<?php } ?>
												<tr class="extra-wrap">
													<td class="extra">
														Extras
													</td>
													<td class="extra-details">
														<?php
															$string = array();
															if ($first_inning->extra_leg_bye>0){
																$string[] = "lb ".$first_inning->extra_leg_bye;
															}
															if ($first_inning->extra_bye>0){
																$string[] = "b ".$first_inning->extra_bye;
															}
															if ($first_inning->wide_ball>0){
																$string[] = "wd ".$first_inning->wide_ball;
															}
															if ($first_inning->no_ball>0){
																$string[] = "nb".$first_inning->no_ball;
															}
															if (!empty($string)){
																echo "(".implode(", ",$string).")";
															} else {
																echo "-";
															}
														?>
													</td>
													<td class="bold">
														<?php echo $first_inning->total_extras; ?>
													</td>
													<td class="bold"></td>
													<td class="bold"></td>
													<td class="bold"></td>
													<td class="bold"></td>
												</tr>
												<tr class="total-wrap">
													<td class="total">
														<b>Total</b>
													</td>
													<td class="total-details">
														(<?php echo $first_inning->wickets; ?> wickets, <?php echo $first_inning->overs; ?> overs)
													</td>
													<td class="bold">
														<b><?php echo $first_inning->runs; ?></b>
													</td>
													<td class="rpo" colspan="3">
														(<?php echo $first_inning->run_rate; ?> runs per over)
													</td>
													<td class="hidden-xs"></td>
												</tr>
												</tbody>
												</table>
												<div class="more-match-stats">
													<hr class="secondary-hr">
													<div class="to-bat hidden">
														<p>
															<span class="head">Did not bat</span><span class="bold"><a href=" " target="" class="playerName color-theme" title="view the player profile for Kane Richardson">KW Richardson</a></span>, <span class="bold"><a href=" " target="" class="playerName color-theme" title="view the player profile for Nathan Lyon">NM Lyon</a></span>
														</p>
													</div>
													<hr class="secondary-hr hidden">
													<div class="fow">
														<p>
															<span class="head">Fall of wickets</span>
															<a href="javascript:;" class="fowLink" title="">
															<?php 
																$wickets = array();
																foreach($first_inning->fall_of_wickets as $key=>$wicket){
																	$wickets[] = "<span>$wicket->wicket_order-$wicket->team_end_score (".$players[$wicket->player_dismissed]->short_name.", $wicket->team_end_over ov)</span>";
																}
																$wickets = implode(", ", $wickets);
																echo $wickets;
															?>
															</a>
														</p>
													</div>
												</div>
												<table width="100%" class="bowling-table">
												<tbody>
												<tr class="tr-heading">
													<th scope="col" class="th-innings-heading">Bowling</th>
													<th scope="col" class="th-o" title="overs bowled"><b>O</b></th>
													<th scope="col" class="th-m" title="maidens bowled"><b>M</b></th>
													<th scope="col" class="th-r" title="runs conceded"><b>R</b></th>
													<th scope="col" class="th-w" title="wickets taken"><b>W</b></th>
													<th scope="col" class="th-w" title="dot balls bowled"><b>0s</b></th>
													<th scope="col" class="th-w" title="boundary fours conceded"><b>4s</b></th>
													<th scope="col" class="th-w" title="boundary sixes conceded"><b>6s</b></th>
												</tr>
												<?php foreach($first_inning->bowlers_summary as $bowler){ ?>
													<tr>
														<td class="bowler-name">
															<a href="javascript:;" target="" class="playerName" title=""><?php echo $players[$bowler->player]->short_name; ?></a>
														</td>
														<td><?php echo $bowler->overs; ?></td>
														<td><?php echo $bowler->maiden; ?></td>
														<td><?php echo $bowler->runs; ?></td>
														<td><?php echo $bowler->wickets; ?></td>
														<td><?php echo $bowler->dots_0s; ?></td>
														<td><?php echo $bowler->boundary_4s; ?></td>
														<td><?php echo $bowler->boundary_6s; ?></td>
													</tr>
												<?php } ?>
												</tbody>
												</table>
												<?php if (!empty($second_inning)){ ?>
												<table width="100%" class="batting-table innings">
												<tbody>
												<tr class="tr-heading">
													<th colspan="2" scope="col" class="th-innings-heading">
														<?php echo $second_inning->title; ?> <span class="normal"> (<?php echo $second_inning->overs; ?> Overs) </span>
													</th>
													<th scope="col" class="th-r" title="runs scored">R</th>
													<th scope="col" class="th-b" title="balls faced">B</th>
													<th scope="col" class="th-4s" title="boundary fours">4s</th>
													<th scope="col" class="th-6s" title="boundary sixes">6s</th>
													<th scope="col" class="th-sr" title="">SR</th>
												</tr>
												<?php foreach($second_inning->batsmen_summary as $batsman){ ?>
												<tr>
													<td class="batsman-name">
														<a href="javascript:;" class="playerName"><?php echo $players[$batsman->player]->short_name; ?></a>
													</td>
													<td class="dismissal-info"><?php echo $batsman->is_out?$batsman->out_details:"(not out)"; ?></td>
													<td class="bold"><?php echo $batsman->runs; ?></td>
													<td class=""><?php echo $batsman->balls; ?></td>
													<td class=""><?php echo $batsman->boundary_4s; ?></td>
													<td class=""><?php echo $batsman->boundary_6s; ?></td>
													<td class=""><?php echo $batsman->strike_rate; ?></td>
												</tr>
												<?php } ?>
												
												<tr class="extra-wrap">
													<td class="extra">
														Extras
													</td>
													<td class="extra-details">
														<?php
															$string = array();
															if ($second_inning->extra_leg_bye>0){
																$string[] = "lb ".$second_inning->extra_leg_bye;
															}
															if ($second_inning->extra_bye>0){
																$string[] = "b ".$second_inning->extra_bye;
															}
															if ($second_inning->wide_ball>0){
																$string[] = "wd ".$second_inning->wide_ball;
															}
															if ($second_inning->no_ball>0){
																$string[] = "nb".$second_inning->no_ball;
															}
															if (!empty($string)){
																echo "(".implode(", ",$string).")";
															} else {
																echo "-";
															}
														?>
													</td>
													<td class="bold">
														<?php echo $second_inning->total_extras; ?>
													</td>
													<td class="bold"></td>
													<td class="bold"></td>
													<td class="bold"></td>
													<td class="bold"></td>
												</tr>
												<tr class="total-wrap">
													<td class="total">
														<b>Total</b>
													</td>
													<td class="total-details">
														(<?php echo $second_inning->wickets; ?> wickets, <?php echo $second_inning->overs; ?> overs)
													</td>
													<td class="bold">
														<b><?php echo $second_inning->runs; ?></b>
													</td>
													<td class="rpo" colspan="4">
														(<?php echo $second_inning->run_rate; ?> runs per over)
													</td>
												</tr>
												</tbody>
												</table>
												<hr class="secondary-hr">
												<div class="more-match-stats">
													<div class="fow">
														<p>
															<span class="head">Fall of wickets</span>
															<a href=" " class="fowLink" title="">
																<?php 
																$wickets = array();
																foreach($second_inning->fall_of_wickets as $key=>$wicket){
																	$wickets[] = "<span>$wicket->wicket_order-$wicket->team_end_score (".$players[$wicket->player_dismissed]->short_name.", $wicket->team_end_over ov)</span>";
																}
																$wickets = implode(", ", $wickets);
																echo $wickets;
															?>
															</a>
														</p>
													</div>
												</div>
												<table width="100%" class="bowling-table">
												<tbody>
												<tr class="tr-heading">
													<th scope="col" class="th-innings-heading">
														Bowling
													</th>
													<th scope="col" class="th-o" title="overs bowled"><b>O</b></th>
													<th scope="col" class="th-m" title="maidens bowled"><b>M</b></th>
													<th scope="col" class="th-r" title="runs conceded"><b>R</b></th>
													<th scope="col" class="th-w" title="wickets taken"><b>W</b></th>
													<th scope="col" class="th-w" title="dot balls bowled"><b>0s</b></th>
													<th scope="col" class="th-w" title="boundary fours conceded"><b>4s</b></th>
													<th scope="col" class="th-w" title="boundary sixes conceded"><b>6s</b></th>
												</tr>
												<?php foreach($second_inning->bowlers_summary as $bowler){ ?>
													<tr>
														<td class="bowler-name">
															<a href="javascript:;" target="" class="playerName" title=""><?php echo $players[$bowler->player]->short_name; ?></a>
														</td>
														<td><?php echo $bowler->overs; ?></td>
														<td><?php echo $bowler->maiden; ?></td>
														<td><?php echo $bowler->runs; ?></td>
														<td><?php echo $bowler->wickets; ?></td>
														<td><?php echo $bowler->dots_0s; ?></td>
														<td><?php echo $bowler->boundary_4s; ?></td>
														<td><?php echo $bowler->boundary_6s; ?></td>
													</tr>
												<?php } ?>
												</tbody>
												</table>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>