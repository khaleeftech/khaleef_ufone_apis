<div id="c-article" class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container">

  <div class="cs-seprator mt30 hidden-xs">
    <div class="devider1"> </div>
  </div>

  <h1 class='hidden-xs' style="margin-bottom:0px!important;"><?php echo $page_heading; ?></h1>
  <h1 class="visible-xs f20"><?php echo $page_heading; ?></h1>




    <div id="results-container" class="col-md-12 col-xs-12  padding-md-0 mt-xs-10">
      <div class="row-xs-10">

        <?php foreach($articles as $key => $a){ ?>
          <?php if ($key==1) { ?>
          <div class="col-md-12 col-sm-12 col-xs-12 " style="padding-bottom:10px;">
            <div class="video-container">

              <div class="video-thumbnail" style="height:300px;">
                <a href="<?php echo $a->seo_url; ?>">
                  <img src="<?php echo $a->image; ?>" class="img-home-video img-responsive">
                </a>
                <div class="overlay"> </div>
                <figcaption>
                  <h2 class="mb0 one-liner"><?php echo $a->title; ?></h2>
                  <p class="mb0 mt0">
                    <?php echo $a->created_at; ?>
                    <a href="" class="icon-share hidden">
                      <img src="<?php echo assets_url(); ?>images/share-article.svg" alt="">
                    </a>
                  </p>

                </figcaption>
              </div>

            </div>
          </div>
          <?php } ?>
        <?php if($key>1) { ?>
          <div class="col-md-6 col-sm-6 col-xs-12 " >
            <div class="video-container">

              <div class="video-thumbnail" style="height:220px;">
                <a href="<?php echo $a->seo_url; ?>">
                  <img src="<?php echo $a->image; ?>" class="img-home-video img-responsive">
                </a>
                <div class="overlay"> </div>
                <figcaption>
                  <h2 class="mb0 one-liner"><?php echo $a->title; ?></h2>
                  <p class="mb0 mt0">
                    <?php echo $a->created_at; ?>
                    <a href="" class="icon-share hidden">
                      <img src="<?php echo assets_url(); ?>images/share-article.svg" alt="">
                    </a>
                  </p>

                </figcaption>
              </div>

              <div class="video-content">
                <p class="pt10 color-gray mb40">
                  <?php echo $a->details; ?>
                  <a href="<?php echo $a->seo_url; ?>" class="more-arrow"><img src="<?php echo assets_url(); ?>images/arrow-right.png"></a>
                </p>
              </div>


            </div>
          </div>
        <?php } ?>
        <?php } ?>
      </div>
    </div>


    <div class="bottom-event-panel">
      <a href="javascript:;" id="more-articles-btn" data-page-to-load="2"> More Articles</a>
    </div>



</div>
