<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<div class="wrapper-main">
    <!-- Page Content -->
    <div class="container">

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container" style="position: relative; z-index: 1">
            <h1 class="text-capitalize">
              <?php echo $current_video->video->title; ?>
            </h1>
            <div class="col-md-12 news-video videos-full-xs">

              <div id="videoElement"></div>
        			<script type="text/javascript">
        			var video_allowed = <?php echo $video_allowed; ?>;
        			var playerInstance = jwplayer("videoElement");
        			var base_url = "<?php echo base_url(); ?>";
        			var view_confirmed = false;
        			playerInstance.setup({
        			    playlist: <?php echo $playlist; ?>,
        			    width: "100%",
        			    aspectratio: "16:9",
        			    controls: true,
        			    autostart: true,
        			    title: "<?php echo $current_video->video->title; ?>",
        			    sharing: {
        			      sites: ['facebook','twitter','googleplus'],
        			      link: "<?php echo $social->link; ?>",
        			   	},
        			   	related: {
        					file: base_url+"ajax/related_video_rss/MEDIAID",
        					onclick: "link",
        					oncomplete: 'show'
        				},
        				ga: {
        					label: "title"
        				}
        			}).onPlay(function() {
        				if (video_allowed==0){
        					jwplayer("videoElement").stop();
        					alert('Sorry, this content is not available in your country!');
        				} else {
        					if (!view_confirmed){
        						jQuery.ajax({
        							dataType: "json",
        							url: "<?php echo $confirm_view; ?>",
        							data: {},
        							success: function(response) {

        							}
        						});
        						view_confirmed = true;
        					}
        				}

        			});
        			</script>
        			<?php
        				$video_desc = "";
        				if (!empty($current_video->ball)){
        					$video_desc = $current_video->ball->commentary;
        				} else if (!empty($current_video->desc)){
        					$video_desc = $current_video->desc;
        				}
        			?>
        			<div class="video-desc <?php echo empty($video_desc)?"hidden":""; ?>">
        				<?php if (!empty($current_video->ball)){ ?>
        				<span style="background: #fff !important; display: none;" class="entry-date pl10">
        					<span class="entry-meta-date"><time><b><?php echo $current_video->ball->title; ?></b>&nbsp;|&nbsp;<b>Out</b></time></span>
        				</span>
        				<?php } ?>

        				<p class="video-arg"><?php echo $video_desc; ?></p>
        			</div>

            </div>
        </div>

        <!--right column-->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 right-column" style="position: relative;z-index: 2">
            <div class="top-score-title col-md-12 right-title">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="widget element-size-100 widget_team mb0">
                            <div class="cs-seprator mt30 visible-xs mb10">
                                <div class="devider1"></div>
                            </div>
                            <div class="hidden-xs">
                                <h1>&nbsp;</h1>
                            </div>
                            <ul class="">
                              <?php
                    						foreach($more_videos as $key=>$video){
                    							if ($key>6){
                    								break;
                    							}
                    						?>
                                <li>
                                    <figure>
                                        <a href="<?php echo base_url()."highlights/".$video->id."/".seo_url($video->title)."/"; ?>" >
                                          <img src="<?php echo $video->med_image; ?>" alt="" class="img-responsive">
                                        </a>
                                    </figure>
                                    <div class="infotext">
                                      <a href="<?php echo base_url()."highlights/".$video->id."/".seo_url($video->title)."/"; ?>">
                                        <?php echo $video->title; ?>
                                      </a>
                                      <p><?php echo $video->views; ?> views</p>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                
                    					<?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>





        <div class="" id="videos-section-container">


        <?php foreach($sections as $key => $section){ ?>
            <div class="col-lg-12 crk-trending-videos-container crk-trending-videos-section " <div class="col-md-3 col-sm-3 col-xs-6 " >
              <div class="cs-seprator">
                <div class="devider1"></div>
              </div>
              <h1><?php echo $section->label; ?></h1>

              <div class="col-md-12" style="padding-left:0; padding-right:0;">

                <?php foreach($section->items as $item){ ?>
                  <div class="col-md-3 col-sm-3 col-xs-6 " href="<?php echo $item->seo_url; ?>" >
                    <div class="video-container">
                      <a href="<?php echo base_url()."highlights/".$item->id."/".seo_url($item->title)."/"; ?>">
                        <div class="video-thumbnail">
                          <img src="<?php echo $item->med_image; ?>" class="img-home-video img-responsive">
                          <img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">
                          <div class="overlay"> </div>
                          <figcaption>
                            <!-- <h2 class="mb0">Pending1 v Pending2</h2> -->
                            <!-- <p class="mb0">Pending objects in home api</p> -->
                            <p class="mb0">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                              <?php echo $item->views; ?></p>
                          </figcaption>
                            </div>
                        <div class="video-content">
                          <p class="video-description crop_to_one_line">
                            <?php echo $item->title; ?>
                          </p>
                          <!-- <p class="video-description crop_to_one_line">In timeline_videos objects required: team1, team2 & series</p> -->
                        </div>
                      </a>
                    </div>
                  </div>
                <?php } ?>




        			</div>
              <div class=" clearfix" style=" margin-bottom:20px;"></div>
            </div>

        <?php } ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clear"></div>
</div>


<script type="text/javascript">
  var heightSet = 0;
  $(window).load(function(){
    setVideosHeight()
  });
  $(window).resize(function(){
    if(heightSet == 1){
      setVideosHeight()
    }
  });

  function setVideosHeight(){
    for(var j=0; j < $('.crk-trending-videos-section').length; j++){
      $container = $('.crk-trending-videos-section').eq(j);
      var constntHeight = 1000;
      var minHeight = constntHeight;
      $container.find('.video-thumbnail').each(function(e){
        $thumbnail = $(this);
        var thumbHeight = $thumbnail.height();
        if(thumbHeight < minHeight && thumbHeight > 0 && thumbHeight < constntHeight){
          minHeight = $thumbnail.height();
        }
      });
      $container.find('.video-thumbnail').height(minHeight);
      heightSet = 1;
    }
  }
</script>
