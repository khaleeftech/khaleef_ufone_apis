<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<div class="wrapper-main">
    <!-- Page Content -->
    <div class="container">

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container" style="position: relative;z-index: 1;">
            <h1 class="text-capitalize"><?php echo $specials[0]->title; ?></h1>
            <div class="col-md-12 news-video videos-full-xs">
                <!-- <div id="videoElement">
                  <a href="<?php //echo $specials[0]->seo_url; ?>">
                    <img width="100%" src="<?php //echo $specials[0]->med_image; ?>" alt="">
                  </a>
                </div> -->

                <div id="videoElement"></div>
          			<script type="text/javascript">
          			var video_allowed = <?php echo $video_allowed; ?>;
          			var playerInstance = jwplayer("videoElement");
          			var base_url = "<?php echo base_url(); ?>";
          			var view_confirmed = false;
          			playerInstance.setup({
                    playlist: [
                      {
                        "sources": [
                          <?php foreach ($specials[0]->qualities as $q): ?>
                          {
                            "default": "<?php echo($q->height == 144)?'false' :'true' ; ?>",
                            "file": "<?php echo $q->video_file; ?>",
                            "label": "<?php echo $q->height.'P'; ?>"
                            // "type": "hls"
                          },
                          <?php endforeach; ?>
                        ]
                      }
                    ],
          			    width: "100%",
          			    aspectratio: "16:9",
          			    controls: true,
          			    autostart: true,
          			    title: "<?php echo $current_video->video->title; ?>",
          			    sharing: {
          			      sites: ['facebook','twitter','googleplus'],
          			      link: "<?php echo $social->link; ?>",
          			   	},
          			   	related: {
          					file: base_url+"ajax/related_video_rss/MEDIAID",
          					onclick: "link",
          					oncomplete: 'show'
          				},
          				ga: {
          					label: "title"
          				}
          			}).onPlay(function() {
          				if (video_allowed==0){
          					jwplayer("videoElement").stop();
          					alert('Sorry, this content is not available in your country!');
          				} else {
          					if (!view_confirmed){
          						jQuery.ajax({
          							dataType: "json",
          							url: "<?php echo $confirm_view; ?>",
          							data: {},
          							success: function(response) {

          							}
          						});
          						view_confirmed = true;
          					}
          				}

          			});
          			</script>



            </div>
        </div>

        <!--right column-->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 right-column" style="position: relative; z-index: 2;">
            <div class="top-score-title col-md-12 right-title">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="widget element-size-100 widget_team mb0">
                            <div class="cs-seprator mt30 visible-xs mb10">
                                <div class="devider1"></div>
                            </div>
                            <div class="hidden-xs">
                                <h1>&nbsp;</h1>
                            </div>
                            <ul class="">
                              <?php
                    						unset($specials[0]);
                    						foreach($specials as $key=>$special){
                    							if ($key>6){
                    								break;
                    							}
                    						?>
                                <li>
                                    <figure>
                                        <a href="<?php echo $special->seo_url; ?>">
                                          <img src="<?php echo $special->thumb_image; ?>" alt="" class="img-responsive">
                                        </a>
                                    </figure>
                                    <div class="infotext">
                                      <a href="<?php echo $special->seo_url; ?>">
                                        <?php echo $special->title; ?>
                                      </a>
                                      <p><?php echo $special->views; ?> views</p>
                                    </div>
                                </li>
                                <li class="divider"></li>
                    					<?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>


        <?php if (isset($rising_star)): ?>
          
          <div class="col-lg-12 crk-trending-videos-container">

            <div class="cs-seprator">
              <div class="devider1"> </div>
            </div>

            <h1>News</h1>

            <div class="customNavigation">
              <a class="next-trending" href="<?php echo base_url().'news/' ?>"> View All</a>
            </div>


            <?php foreach($news as $key=>$n){
              if($key>3){
                break;
              }
              ?>
              <?php if (($key)%2==0){ ?>
                <div class="col-md-12 <?php echo $key>0?'hidden-xs':''; ?>" style="">
              <?php } ?>
                <div class="col-xs-12 col-md-6 col-sm-6">
                  <div class="news-page">
                    <div class="col-md-6 col-sm-6 pl0 pr10 pb10">
                      <img src="<?php echo $n->image_or_thumb_file; ?>" alt="" class="img-djoko img-home-item" style="width:100%">
                    </div>
                    <div class="col-md-6 data-news-pg pl10 pr0">
                      <h4 class="pt0 mb0 mt0 crop_to_one_line"><?php echo $n->title; ?></h4>
                      <p class="news-dd">
                        <i>by <?php echo $n->author; ?></i>
                      </p>
                      <p class="news-dd"><?php echo date('F j, Y', $n->created_at/1000); ?></p>
                      <div class="clear">
                      </div>
                      <p class="mt0"><?php echo substr(strip_tags($n->details),0, 200); ?>
                        <!-- <a class="read_more_anchor" href="<?php //echo base_url()."news-update/$n->id/".seo_url($n->title)."/"; ?>"> Read More <i class="fa fa-chevron-right"></i></a> -->
                      </p>
                    </div>
                  </div>
                </div>
              <?php if (($key)%2!=0){ ?>
                </div>
              <?php } ?>
            <?php } ?>


          </div> <!-- col-lg-12 -->
        

        <?php else: ?>

        <div class="" id="videos-section-container">


        <?php foreach($sections as $key => $section){ ?>

            <div class="col-lg-12 crk-trending-videos-container crk-trending-videos-section " id="yo-<?php echo $key; ?>">
              <div class="cs-seprator">
                <div class="devider1"></div>
              </div>
              <h1><?php echo $section->label; ?></h1>

              <div class="col-md-12" style="padding-left:0; padding-right:0;">

                <?php foreach($section->items as $item){ ?>
                  <div class="col-md-3 col-sm-3 col-xs-6 " href="<?php echo $item->seo_url; ?>">
                    <div class="video-container">
                      <div class="video-thumbnail">
                        <a href="<?php echo $item->seo_url; ?>">
                        <img src="<?php echo $item->med_image; ?>" class="img-home-video img-responsive">
                        <img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">
                        <div class="overlay"> </div>
                        <figcaption>
                          <!-- <h2 class="mb0">Pending1 v Pending2</h2> -->
                          <!-- <p class="mb0">Pending objects in home api</p> -->
                          <p class="mb0">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                            <?php echo $item->views; ?></p>
                        </figcaption>
                      </a>
                      </div>
                      <div class="video-content">
                        <p class="video-description crop_to_one_line">
                          <a href="<?php echo $item->seo_url; ?>">
                            <?php echo $item->title; ?>
                          </a>
                        </p>
                        <!-- <p class="video-description crop_to_one_line">In timeline_videos objects required: team1, team2 & series</p> -->
                      </div>
                    </div>
                  </div>
                <?php } ?>




        			</div>
              <div class=" clearfix" style=" margin-bottom:20px;"></div>
            </div>

        <?php } ?>
        </div>

      <?php endif ?>

        <div class="clearfix"></div>
    </div>
    <div class="clear"></div>


    <?php if (isset($rising_star)): ?>
      <div class="adds-hrz" style="padding: 0px;"> <a href="" target="_blank"><img src="<?php echo assets_url(); ?>images/rising-star.jpg" alt=""></a> </div>
    <?php endif ?>

</div>


<script type="text/javascript">
  var heightSet = 0;
  $(window).load(function(){
    setVideosHeight()
  });
  $(window).resize(function(){
    if(heightSet == 1){
      setVideosHeight()
    }
  });

  function setVideosHeight(){
    for(var j=0; j < $('.crk-trending-videos-section').length; j++){
      $container = $('.crk-trending-videos-section').eq(j);
      var constntHeight = 1000;
      var minHeight = constntHeight;
      $container.find('.video-thumbnail').each(function(e){
        $thumbnail = $(this);
        var thumbHeight = $thumbnail.height();
        if(thumbHeight < minHeight && thumbHeight > 0 && thumbHeight < constntHeight){
          minHeight = $thumbnail.height();
        }
      });
      $container.find('.video-thumbnail').height(minHeight);
      heightSet = 1;
    }
  }
</script>
