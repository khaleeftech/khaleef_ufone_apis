<?php if (@$video_page && 0){ ?>
<nav class="navbar navbar-inverse sub-navbar pt0 pb0" role="navigation">
	<div class="container pl10 pr0">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			<span class="">Filter Videos</span>
			<span class="fa fa-angle-down pull-right"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
			<ul class="nav navbar-nav">
				<li><a href="javascript:;">Most Viewed</a></li>
				<li><a href="javascript:;">Popular</a></li>
				<li><a href="javascript:;">Laugh</a></li>
				<li class="active"><a href="javascript:;">Worldcup T20</a></li>
				<li><a href="javascript:;">Asia Cup</a></li>
				<li><a href="javascript:;">Sixex</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
<!-- /.container -->
</nav>
<?php } ?>
<div class="wrapper wrapper-main">
  <section id="quiz" class="container secondary-page <?php echo (@$video_page && 0)?"pt40 pl0 pr0":"pt20"; ?>">
    <div class="general general-results players">
    <!--Left welcome-->
      <?php echo $inner_page; ?>
      <!--Left welcome-->

      <!--right column-->
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 right-column hidden-xs">
        <div class="top-score-title col-md-12 right-title mb10">

          <?php
          // $this->memcached_library->delete("cricwick_session_news");
            $news = mb_convert_encoding($this->memcached_library->get('cricwick_ufone_home_news'), "UTF-8");
						$news = json_decode($news);
            $videos = mb_convert_encoding($this->memcached_library->get('cricwick_ufone_home_videos'), "UTF-8");
            
						$videos = json_decode($videos)->timeline_videos;
						if(isset($news) && !empty($news)){
							$news = $news->data[0];
						}

          ?>


          <?php if (isset($videos) && !empty($videos)): ?>


          <div class="row aside">
            <div class="col-md-12 col-xs-12">
              <div class="widget element-size-100 widget_team">

                <div class="cs-seprator mt30 padding-md-0">
                  <div class="devider1"></div>
                </div>

                <div class="">
                  <h2>Featured Videos</h2>
                </div>

                <ul class="">
                  <?php
                    foreach($videos as $key=>$video){
                      if($key > 3){break;}

                  ?>

                  <li>
                    <figure>
                      <a href="<?php echo base_url().'highlights/'.$video->id.'/'.$video->match_obj->series_id.'/'.seo_url($video->title).'/'; ?>">
                        <img src="<?php echo $video->med_image; ?>" alt="" class="img-responsive">
                      </a>
                    </figure>
                    <div class="infotext">
                      <a href="<?php echo base_url().'highlights/'.$video->id.'/'.$video->match_obj->series_id.'/'.seo_url($video->title).'/'; ?>" title="<?php echo $video->title; ?>">
                        <?php echo $video->title; ?>
                      </a>
                      <p><?php echo $video->views ?> views</p>
                    </div>
                  </li>
				  <li class="divider"></li>
                  <?php } ?>

              </ul>

              </div>
            </div>
          </div>
      <?php endif; ?>


      	<?php if (isset($news) && !empty($news)): ?>


          <div class="row aside">
            <div class="col-md-12">
              <div class="right-content p15" style="background-color:#f88300!important;">
          			<h2><?php echo $news->title; ?></h2>
          			<!-- <h5><?php //echo $news->title; ?></h5> -->
                <p class="">
					<?php echo substr(strip_tags($news->body),0, 180); ?>...
                </p>
          			<h5>
	                  <a href="<?php echo $news->seo_url; ?>">Read More</a>
	                </h5>

              </div>
            </div>
          </div> <!-- row 2nd -->

      <?php endif ?>




      </div>
      <!--right column-->


    </div>
  </section>

</div>



<script>
var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	$("body").on("submit","#quiz-form", function(){
		$(".answer-response").fadeOut();
		$("#btnSubmit").removeClass("btn-ok");
		$("#btnSubmit").button("loading");

		if($('input:radio:checked').length > 0){
			$.ajax({
				type: "POST",
				dataType: "json",
				url: base_url+'quiz/submit_ans',
				data: $(this).serializeArray(),
				success: function(response) {

					var img = "";
					if (response.status==1){
						$(".answer-response.correct").fadeIn();
					} else {
						$(".answer-response.incorrect").fadeIn();
					}
					//$("#btnSubmit").button("reset");
					//$("#btnSubmit").addClass("btn-ok");

					setTimeout(function(){
						window.location = base_url+'quiz';
					}, 1000);

				}
			});
		} else {

			$("#btnSubmit").button("reset");
			$("#btnSubmit").addClass("btn-ok");
			alert("Please select your answer!");
		}



		return false;
	});

	$("body").on("submit","#subscribe-quiz", function(){
		$("#btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'quiz/subscribe_quiz/',
			data: [],
			success: function(response) {
				if(response.user.subscribe_status_quiz==1){

					window.location = base_url+"quiz/";

				} else if(response.user.subscribe_status_quiz==2){

					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");

				} else if(response.user.subscribe_status_quiz==3){

					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");
				} else {

					alert("<?php echo lang("general_error"); ?>");
					$("#btnSubmit").button("reset");
				}

			}
		});
		return false;
	});
});


 </script>
