<div id="c-calend" class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container">

  <div class="cs-seprator mt30 hidden-xs">
    <div class="devider1"> </div>
  </div>

	<h1 class='hidden-xs'><?php echo $page_heading; ?></h1>
  <h1 class="visible-xs f20"><?php echo $page_heading; ?></h1>

  <div class="col-md-12 fixtures  mt-xs-10">
    <div class="row-5-xs">
      <div class="tabs animated-slide-2 matches-tbs">
        <div class="tab-content">
          <div id="results" class="tab active">
            <div class="event event-listing event-samll">


              <div id="fixtures-container" class="remove-cursor">
                <?php foreach($fixtures->matches as $fixture): ?>
                <?php
                  $flag1 = assets_url().'images/flag-placeholder.jpg';
                  $flag2 = assets_url().'images/flag-placeholder.jpg';
                  if(!empty($fixture->team_1->team->flag_url) && (strpos($fixture->team_1->team->flag_url, 'missing.png') === false && strpos($fixture->team_1->team->flag_url, 'placeholder.jpg') === false) ){
                    $flag1 = $fixture->team_1->team->flag_url;
                  }
                  if(!empty($fixture->team_2->team->flag_url) && (strpos($fixture->team_2->team->flag_url, 'missing.png') === false && strpos($fixture->team_2->team->flag_url, 'placeholder.jpg') === false) ){
                    $flag2 = $fixture->team_2->team->flag_url;
                  }
                ?>

                <div class="col-md-12 col-xs-12 event-fixture-box">
                    <div class="">

                       <div class="col-xs-12 visible-xs  pb20">

                        <h2 class="fw-normal text-left  m0">
                          <a class="color-grey-lighter f16" href="javascript:;">
                           <?php echo $fixture->local_time->date; ?> <?php echo $fixture->local_time->month; ?> | <?php echo $fixture->local_time->time; ?>
                          </a>
                        </h2>

                        <h4 class="m0 lh28 fw-normal text-left">
                          <a class="color-grey-light mt-5" href="javascript:;">
                            <?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle')?$fixture->title:'' ; ?><?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle' && !empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle' )?': ' :'' ; ?><?php echo(!empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle')?$fixture->series->short_name:'' ; ?>
                          </a>
                        </h4>

                      </div>


                      <div class="col-md-2 col-sm-3 col-xs-6 ">
                        <div class="row">

                          <div class="team-first">
                            <ul style="text-align: center;">

                              <li>
                                <a href="javascript:;">
                                  <img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
                                </a>
                              </li>

                              <li>
                                <h4 class="color-jazz-grey-darker mb0 one-liner2"><?php echo $fixture->team_1->team->short_name; ?></h4>
                              </li>


                            </ul>
                          </div>

                        </div>
                      </div>


                      <div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">
                        <article class="pb0">
                          <div class="text-center">

                            <h2 class="fw-normal ">
                              <a class="color-grey-lighter" href="javascript:;"><?php echo $fixture->local_time->date; ?> <?php echo $fixture->local_time->month; ?> | <?php echo $fixture->local_time->time; ?></a>
                            </h2>

                            <h4 class="m0 lh28 fw-normal ">
                              <a class="color-grey-light" href="javascript:;">
                                <?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle')?$fixture->title:'' ; ?><?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle' && !empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle' )?': ' :'' ; ?><?php echo(!empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle')?$fixture->series->short_name:'' ; ?>
                              </a>
                             </h4>

                            <h1 class="post-option mt0 pb0">
                              <span class="color-grey f18">
                                <?php if (isset($fixture->venue) && (!empty($fixture->venue->stadium_name) && !empty($fixture->venue->title))): ?>
                                  at <?php echo $fixture->venue->stadium_name.', '.$fixture->venue->title; ?>
                                <?php endif; ?>
                              </span>
                            </h1>
                          </div>

                        </article>

                      </div>

                      <div class="col-md-2 col-sm-3 col-xs-6 ">
                        <div class="row">
                          <div class="team-second">
                            <ul style="text-align: center;">
                              <li>
                                <a href="javascript:;">
                                  <img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
                                </a>
                              </li>

                              <li>
                                <h4 class="color-jazz-grey-darker mb0 one-liner2"><?php echo $fixture->team_2->team->short_name; ?></h4>
                              </li>


                              </ul>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-8 col-xs-12 visible-xs">
                        <article class="mt20">
                          <div class="text-center">


                            <h1 class="post-option mt0">
                              <span class="color-grey f16">
                                <?php if (isset($fixture->venue) && (!empty($fixture->venue->stadium_name) && !empty($fixture->venue->title))): ?>
                                  at <?php echo $fixture->venue->stadium_name.', '.$fixture->venue->title; ?>
                                <?php endif; ?>
                              </span>
                            </h1>
                          </div>

                        </article>

                      </div>

                    </div>
                  </div>

                <?php endforeach; ?>
              </div><!-- #fixtures-container -->


              <div class="bottom-event-panel">
                <a href="javascript:;" id="more-fixtures-btn" data-page-to-load="2"> More Fixtures</a>
              </div><!-- bottom-event-panel -->



            </div><!-- event event-listing event-samll -->
          </div><!-- #results -->
        </div><!-- tab-content -->
      </div><!-- tabs animated-slide-2 matches-tbs -->
    </div><!-- row-5-xs -->
  </div><!-- col-md-12  -->

</div> <!-- c-calend -->
