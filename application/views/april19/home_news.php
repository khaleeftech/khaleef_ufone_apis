<?php foreach($news->data as $key=>$n){
  if($key>3){
    break;
  }
  ?>
  <?php if (($key)%2==0){ ?>
    <div class="col-md-12 col-xs-12  padding-md-0  <?php echo $key>0?'':'mt-xs-5'; ?>" style="">
  <?php } ?>

  <div class="row-xs-10">

    <div class="col-xs-12 col-md-6 col-sm-6" onclick="window.location='<?php echo $n->seo_url; ?>'">
      <div class="news-page">
        <div class="col-md-6 col-sm-6 pl0 pr10 pb10 image-news-pg">
          <img src="<?php echo  $n->full_image; ?>" alt="" class="img-djoko img-home-item" style="width:100%">
        </div>
        <div class="col-md-6 data-news-pg pl10 pr0">
          <h4 class="pt0 mb0 mt0 crop_to_one_line"><?php echo $n->title; ?></h4>
          <p class="news-dd hidden">
            <i>by <?php echo $n->by; ?></i>
          </p>
          <p class="news-dd"><?php echo date('F j, Y', strtotime($n->created_at)); ?></p>
          <div class="clear">
          </div>
          <p class="mt0">
            <span class="hidden-xs">
                      <?php echo substr(strip_tags($n->body),0, 180); ?>...
                    </span>
                    <span class="visible-xs">
                      <?php echo substr(strip_tags($n->body),0, 230); ?>...
                    </span>
            <!-- <a class="read_more_anchor" href="<?php //echo base_url()."news-update/$n->id/".seo_url($n->title)."/"; ?>"> Read More <i class="fa fa-chevron-right"></i></a> -->
          </p>
        </div>

        <a href="<?php echo $n->seo_url; ?>" class="more-arrow">
          <img src="<?php echo assets_url(); ?>images/arrow-right.png">
        </a>
      </div>
    </div>

  </div>
  <?php if (($key)%2!=0){ ?>
    </div>
  <?php } ?>
<?php } ?>
