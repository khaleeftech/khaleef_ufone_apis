
        <div class="top-score-title col-md-12 right-title mb10">

          <?php
            $videos = mb_convert_encoding($this->memcached_library->get('cricwick_ufone_home_videos'), "UTF-8");
              if(empty($videos)){
                  // $path = API_PATH."home?web_user=1";
                  // $videos = curl_call($path);
                  // $this->memcached_library->set('cricwick_ufone_home_videos', $videos, 600);
                }
              $videos = mb_convert_encoding($videos, "UTF-8");
						$videos = json_decode($videos)->timeline_videos;
            
          ?>


          <?php if (isset($videos) && !empty($videos)): ?>


          <div class="row aside">
            <div class="col-md-12 col-xs-12">
              <div class="widget element-size-100 widget_team">

                <ul class="">
                  <?php
                    foreach($videos as $key=>$video){
                      if($key > 7){break;}

                  ?>

                  <li style="width=100%; padding-bottom:7px;">
                    <figure style="width:152px;">
                      <a href="<?php echo base_url().'highlights/'.$video->id.'/'.$video->match_obj->series_id.'/'.seo_url($video->title).'/'; ?>">
                        <img src="<?php echo $video->med_image; ?>" alt="" class="img-responsive" style="width: 152px; height:76px;">
                      </a>
                    </figure>
                    <div class="infotext" style="padding-left: 170px; padding-top:8px;">
                      <a href="<?php echo base_url().'highlights/'.$video->id.'/'.$video->match_obj->series_id.'/'.seo_url($video->title).'/'; ?>" title="<?php echo $video->title; ?>" style="font-size:15px;">
                        <?php echo $video->title; ?>
                      </a>
                      <p><?php echo $video->views ?> views</p>
                    </div>
                  </li>
				  <li class="divider"></li>
                  <?php } ?>

              </ul>

              </div>
            </div>
          </div>
      <?php endif; ?>





      </div>