
<?php
$count1 = 1;
if(!empty($upcoming_matches)){$count1--;}
if(count($recent_matches) == 1){$count1=0;}
for($r=$count1; $r>=0; $r--):

if (!empty($recent_matches)): ?>

<?php 

	 $flag1 = assets_url().'images/flag-placeholder.jpg';
        $flag2 = assets_url().'images/flag-placeholder.jpg';

        if(!empty($recent_matches[$r]->team_1->team->flag_url) && (strpos($recent_matches[$r]->team_1->team->flag_url, 'missing.png') === false && strpos($recent_matches[$r]->team_1->team->flag_url, 'placeholder.jpg') === false)){
          $flag1 =  $recent_matches[$r]->team_1->team->full_flag_url;
        }
        if(!empty($recent_matches[$r]->team_2->team->flag_url) && (strpos($recent_matches[$r]->team_2->team->flag_url, 'missing.png') === false && strpos($recent_matches[$r]->team_2->team->flag_url, 'placeholder.jpg') === false)){
          $flag2 =  $recent_matches[$r]->team_2->team->full_flag_url;
        }

        $team_1_a_score = '';
				$team_2_a_score = '';
				$team_1_b_score = '';
        $team_2_b_score = '';

        $team_1_a_overs = '';
				$team_2_a_overs = '';
				$team_1_b_overs = '';
        $team_2_b_overs = '';

        $team_1_class = '';
        $team_2_class = '';

        $is_test = '';

        if(strtolower($recent_matches[$r]->format)  ==  'test' ){
          $is_test = 1;
        }

        if($recent_matches[$r]->match_won_by_id == $recent_matches[$r]->team_1_id){
          $team_1_class = 'match-won';
        }
        if($recent_matches[$r]->match_won_by_id == $recent_matches[$r]->team_2_id){
          $team_2_class = 'match-won';
        }

        foreach ($recent_matches[$r]->innings as $keyI => $i) {
					if($keyI <= 1){
					if($i->batting_team_id == $recent_matches[$r]->team_1_id){
						
							$team_1_a_score = ($i->declared) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d" : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets));
							$team_1_a_overs = "(".$i->overs." ov)";
					}else{
						$team_2_a_score = ($i->declared) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d" : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets));
						$team_2_a_overs = "(".$i->overs." ov)";
					}
				}else{

					if($i->batting_team_id == $recent_matches[$r]->team_1_id){
						$team_1_b_score = ($i->declared) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d" : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets));
						$team_1_b_overs = "(".$i->overs." ov)";
					}else{
						$team_2_b_score = ($i->declared) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d" : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets));
						$team_2_b_overs = "(".$i->overs." ov)";
					}

					}

				}

?>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

			<div class="event event-listing event-samll">
		<div id="fixtures-container">
			
		<h2 class="visible-xs">Result</h2>
			<div class="col-md-12 col-xs-12 event-fixture-box">
			<div class="result" style=" ">Result</div>
			
		<div class="">
		<div class="col-xs-12  pb20">
		<h2 class="fw-normal text-left  m0">
			<a class="color-grey-lighter f16" href="<?php echo $recent_matches[$r]->seo_url; ?>">
				<?php
		 			$start_date =  format_date_newserver2($recent_matches[$r]->match_start); 
		 			echo $start_date["date"]." ";
		 			echo $start_date["month"];

	 			?>
			</a>
		</h2>

		<h4 class="m0 lh28 fw-normal text-left"> 
			<a class="color-grey-light mt-5" href="<?php echo $recent_matches[$r]->seo_url; ?>">
				<?php echo $recent_matches[$r]->title; ?>
			</a> 
		</h4>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 ">
		<div class="row">
			<div class="team-first">
		<ul style="text-align: center;">
		   <li>
		   	<a href="<?php echo $recent_matches[$r]->seo_url; ?>"> 
		   		<img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
		   	</a>
		   </li>
		   <li>
		   	<h4 class="color-jazz-grey-darker mb0 <?php echo $team_1_class; ?>">
		   		<?php echo $recent_matches[$r]->team_1->team->name; ?>
		   	</h4>
		   </li>
		   <li>
		   	<h4 class="color-jazz-grey-darker mt0 mb0 <?php echo $team_1_class; ?>">
				 <span class="f12"> <?php echo $team_1_a_score; ?> </span>
				 <?php if(!empty($team_1_b_score)): ?>
					 <span class="color-jazz-grey-darker f12" style="padding-left:5px; padding-right:5px;"> & </span>
					 <span class="f12"> <?php echo $team_1_b_score; ?> </span>
			<?php endif; ?>
				 </h4>
		   </li>
		   <li>
		   	<span class="f12 <?php echo ((!empty($team_1_b_score)) ? "pr10" : ""); ?>">
		   		<?php echo $team_1_a_overs ?>
		   	</span>
				 <span class="f12">
				 	<?php echo $team_1_b_overs; ?>
				 </span>
		   </li>

		</ul> </div>
		</div>
		</div>


		<div class="col-md-6 col-sm-6 col-xs-6 ">
		<div class="row">
			<div class="team-second">
		<ul style="text-align: center;">
		   <li>
		   	<a href="<?php echo $recent_matches[$r]->seo_url; ?>"> 
		   		<img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
		   	</a>
		   	</li>
		   <li>
		   	<h4 class="color-jazz-grey-darker mb0 <?php echo $team_2_class; ?>">
		   		<?php echo $recent_matches[$r]->team_2->team->name; ?>
		   	</h4>
		   </li>
		   <li>
			   	<h4 class="color-jazz-grey-darker mt0 mb0 <?php echo $team_2_class; ?>">
					 <span class="f12"> <?php echo $team_2_a_score; ?> </span>
				 <?php if(!empty($team_2_b_score)): ?>

					 <span class="color-jazz-grey-darker f12" style="padding-left:5px; padding-right:5px;"> & </span>
					 <span class="f12"> <?php echo $team_2_b_score; ?> </span>
			<?php endif; ?>
					 </h4>
			</li>
		   <li><span class="f12 <?php echo ((!empty($team_2_b_score)) ? "pr10" : ""); ?>"><?php echo $team_2_a_overs ?></span><span class="f12"><?php echo $team_2_b_overs ?></span></li>

		</ul> </div>
		</div>
		</div>

		<div class="col-md-12 col-xs-12">
		<article class="mt20">
		  <div class="text-center">


			<h1 class="post-option mt0">
			  <span class="color-grey f16"><?php echo($recent_matches[$r]->match_result)? $recent_matches[$r]->match_result : '&nbsp;'; ?> </span>
			</h1>
		  </div>

		</article>

		</div>

		</div>
		</div>
			
			
			
			

			
		</div>

		</div>
</div>
	
<?php endif ?>
<?php endfor ?>













																<?php 
																																if(!empty($upcoming_matches)){

                              		$count = 1;
                              		if( !empty($recent_matches) ){$count--; $upcoming_matches = array_reverse($upcoming_matches);}

                              		for($u=$count; $u>=0; $u--):
                              			
                          			if(!empty($upcoming_matches[$u]->team_1->team->flag_url) && (strpos($upcoming_matches[$u]->team_1->team->flag_url, 'missing.png') === false && strpos($upcoming_matches[$u]->team_1->team->flag_url, 'placeholder.jpg') === false)){
								          $flag1 =  $upcoming_matches[$u]->team_1->team->full_flag_url;
								        }
								        if(!empty($upcoming_matches[$u]->team_2->team->flag_url) && (strpos($upcoming_matches[$u]->team_2->team->flag_url, 'missing.png') === false && strpos($upcoming_matches[$u]->team_2->team->flag_url, 'placeholder.jpg') === false)){
								          $flag2 =  $upcoming_matches[$u]->team_2->team->full_flag_url;
								        }
                              	?>
                              			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								  
                               		<div class="event event-listing event-samll">
									<div id="fixtures-container">
							  		
									<h2 class="visible-xs">Upcoming</h2>
							 		<div class="col-md-12 col-xs-12 event-fixture-box">
							 		<div class="upcoming" style=" ">Upcoming</div>


						<div class="remove-cursor">
							<div class="col-xs-12  pb20">
								 <h2 class="fw-normal text-left  m0">
								 	<a class="color-grey-lighter f16" href="javascript:;">
								 		<?php
								 			$start_date =  format_date_newserver2($upcoming_matches[$u]->match_start); 
								 			echo $start_date["date"]." ";
								 			echo $start_date["month"];
								 			echo " | ";
								 			echo $start_date["time"];

							 			?>
								 	</a>
								  </h2>

								<h4 class="m0 lh28 fw-normal text-left"> 
									<a class="color-grey-light mt-5" href="javascript:;">
										<?php echo $upcoming_matches[$u]->title; ?>
									</a> 
								</h4>

							</div>

						    <div class="col-md-6 col-sm-6 col-xs-6 ">
								<div class="row">
									<div class="team-first">
							   <ul style="text-align: center;">
								   <li>
									   	<a href="javascript:;"> 
								   			<img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
								   		</a>
							   		</li>

								   <li>
							   			<h4 class="color-jazz-grey-darker mb0"><?php echo $upcoming_matches[$u]->team_1->team->name; ?></h4>
							   		</li>

								   <li class=" ">
								   	<h4 class="color-jazz-grey-darker mt0 mb0">&nbsp;</h4>
								   </li>

								   <li class=" ">
								   	<span class="f12">&nbsp;</span>
								   </li>

							   </ul> </div>
								</div>
						    </div>
								  

								  <div class="col-md-6 col-sm-6 col-xs-6 ">
									<div class="row">
										<div class="team-second">
								   <ul style="text-align: center;">
									   <li><a href="javascript:;"> <img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
									   <li><h4 class="color-jazz-grey-darker mb0 "><?php echo $upcoming_matches[$u]->team_2->team->name; ?></h4></li>
									   <li class=" "><h4 class="color-jazz-grey-darker mt0 mb0 ">&nbsp;</h4></li>
									   <li class=" "><span class="f12">&nbsp;</span></li>

								   </ul> </div>
									</div>
								  </div>

								  <div class="col-md-12 col-xs-12">
									<article class="mt20">
									  <div class="text-center">


										<h1 class="post-option mt0">
										  <span class="color-grey f16">
										  	<?php if(isset($upcoming_matches[$u]->venue) && !empty($upcoming_matches[$u]->venue) ){ 
										  		echo "".$upcoming_matches[$u]->venue->stadium_name.", ".$upcoming_matches[$u]->venue->title;} ?>
										  </span>
										</h1>
									  </div>

									</article>

								  </div>

								</div>
							  </div>
							  		
							  		
							  		
							  		

							  		
									</div>
									
								  </div>
								  </div>

                              <?php endfor;  ?>
												<?php } ?>