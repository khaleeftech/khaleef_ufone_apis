  <div class="clearfix"></div>


  <div id="psl" class="">
  <div id="site-container" class="clearfix">

    <div class="timeline_area custom-content-divs">
      <div class="clearfix"></div>

      <div class="container">
        <div id="content" role="main">
          <div role="tabpanel" class="tab-pane" id="scorecard">

              <div class="inplay-filters inplay-filters-margin pt-15 ">
                    <div class="">
                      <ul class="nav nav-tabs nav-side-tab psl-series" role="tablist" style=" ">
                        <li role="presentation" class="active">
                        	<a href="#summary" aria-controls="summary" role="tab" data-toggle="tab" class="f-16 pt-15 psl-tab">
                        		<span class="">Summary</span>
                        	</a>
                        </li>
                        <li role="presentation" class="hidden">
                        	<a href="#videos" aria-controls="videos" role="tab" data-toggle="tab" class=" f-16 pt-15 psl-tab">
                        		<span class="">Videos</span>
                        	</a>
                        </li>
                        <li role="presentation">
                        	<a href="#schedules" aria-controls="schedules" role="tab" data-toggle="tab" class=" f-16 pt-15 psl-tab">
                        		<span class="">Schedules</span>
                        	</a>
                        </li>
												<?php if(!empty($points_table)) : ?>
                        <li role="presentation">
                        	<a href="#pointstable" aria-controls="pointstable" role="tab" data-toggle="tab" class=" f-16 pt-15 psl-tab">
                        		<span class="">Points Table</span>
                        	</a>
                        </li>
												<?php endif;?>
                      </ul>
                    </div>
                  </div>


                    <div id="tab-height" style="width: 100%; min-height: 476px;">
                      <div class="tab-content pl-15 pr-15 pt15">
                       <!-- summary tab-->
                        <div role="tabpanel" class="tab-pane active" id="summary">
                          <div class="row">


                            <div class="col-md-12 col-xs-12 mt10">
                              <div class="row">

                              		<?php $this->load->view('april19/psl/psl_home_fixtures', array("recent_matches"=>$recent_matches, "upcoming_matches"=>$upcoming_matches)); ?>

                              		<?php $this->load->view('april19/psl/psl_home_news', $news ); ?>

								<div class="clearfix"></div>
                              </div>
                            </div>



                          </div>
                        </div>
                       <!-- end summary tab-->
                       <!-- videos tab-->
                        <div role="tabpanel" class="tab-pane" id="videos">
                         <?php //$this->load->view('april19/psl/psl_videos'); ?>
                        </div>
                       <!-- end videos tab-->
                       <!-- Schedules tab-->
                        <div role="tabpanel" class="tab-pane" id="schedules">
                          <?php //$this->load->view('april19/psl/psl_schedules'); ?>
                        </div>
                       <!-- end Schedules tab-->
                       <!-- points table tab-->
                        <div role="tabpanel" class="tab-pane" id="pointstable">
                          <?php //$this->load->view('april19/psl/psl_points.php'); ?>
                        </div>
                       <!-- end points table tab-->

                      </div>
                    </div>



          </div>
        </div>

      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
	var loadingImage = '<div id="loadingDiv"><img src="<?php echo assets_url()."images/loading.gif"; ?>" /></div>';
	var base_url = "<?php echo base_url(); ?>";
	window['ajax_schedules'] = false;
	window['ajax_pointstable'] = false;
	var pleasewait = "Please wait...";
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.psl-tab').click(function(){
			var $this = $(this);
			var role = $this.attr('aria-controls');

			if(role == 'summary'){
				return true;
			}

			var $container = $("#"+role);
				$container.find('#loadingDiv').remove();

			var container = $container.html().trim();
			console.log(container);

			var url = base_url+'psl/'+role;
			if(!container){

				if(!window['ajax_'+role]){
          window['ajax_'+role] = true;
					$container.html(loadingImage);
					$.ajax({
						type: "POST",
						dataType: "html",
						url: url,
						data: [],
						success: function(response) {
							$container.find('#loadingDiv').hide().remove();
							$container.append(response);
              window['ajax_'+role] = false;
						}
					});
				}

			}else{
				console.log('there is data');
			}

		});



		$("body").on("click",".psl-more",function(){
			var $this = $(this);
			var role = $this.attr('data-role');
			var container = $this.attr('data-container');
			var btnText = $this.html();
			$this.html(pleasewait);

			var page_to_load = $this.attr('data-page-to-load');
			var url = base_url+'psl/'+role+"/"+page_to_load;

			$.ajax({
				type: "POST",
				dataType: "html",
				url: url,
				data: [],
				success: function(response) {
					if(response.trim()){
						$("#"+container).append(response);
						$this.attr('data-page-to-load', parseInt(page_to_load)+1);
					}else{
						$this.parent().remove();
					}

					$this.html(btnText);
          resizeDiv();
				}
			});
		});
	});
</script>

<script>
  $(document).ready(function(){
    resizeDivs();
    });

    $(window).resize(resizeDivs);

    function resizeDivs() {
    vpw = $(window).width();
    vph = $(window).height();
      vph= vph - 202;
    $('#tab-height').css({'min-height': vph + 'px'});
    $('.tab-height-videos').css({'min-height': vph + 'px'});
}
  </script>
