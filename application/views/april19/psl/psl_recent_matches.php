<?php foreach ($recent_matches as $key => $value): ?>

	<?php 
		$title = $value->team_1->team->name." vs ".$value->team_2->team->name;
		$value->seo_url = base_url()."match/".$value->id."/".seo_url($title)."/";
		$flag1 = assets_url().'images/flag-placeholder.jpg';
        $flag2 = assets_url().'images/flag-placeholder.jpg';

        if(!empty($value->team_1->team->flag_url) && (strpos($value->team_1->team->flag_url, 'missing.png') === false && strpos($value->team_1->team->flag_url, 'placeholder.jpg') === false)){
          $flag1 =  $value->team_1->team->full_flag_url;
        }
        if(!empty($value->team_2->team->flag_url) && (strpos($value->team_2->team->flag_url, 'missing.png') === false && strpos($value->team_2->team->flag_url, 'placeholder.jpg') === false)){
          $flag2 =  $value->team_2->team->full_flag_url;
        }

        $team_1_a_score = '';
				$team_2_a_score = '';
				$team_1_b_score = '';
        $team_2_b_score = '';

        $team_1_a_overs = '';
				$team_2_a_overs = '';
				$team_1_b_overs = '';
        $team_2_b_overs = '';

        $team_1_class = '';
        $team_2_class = '';

        $is_test = '';

        if(strtolower($value->format)  ==  'test' ){
          $is_test = 1;
        }

        if($value->match_won_by_id == $value->team_1_id){
          $team_1_class = 'match-won';
        }
        if($value->match_won_by_id == $value->team_2_id){
          $team_2_class = 'match-won';
        }

				foreach ($value->innings as $keyI => $i) {
					if($keyI <= 1){
					if($i->batting_team_id == $value->team_1_id){
						
							$team_1_a_score = ($i->declared) ? ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d") : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)));
							$team_1_a_overs = "(".$i->overs." ov)";

					}else{
						$team_2_a_score = ($i->declared) ? ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d") : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)));
						$team_2_a_overs = "(".$i->overs." ov)";
					}
				}else{

						if($i->batting_team_id == $value->team_1_id){
							$team_1_b_score = ($i->declared) ? ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d") : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)));
							$team_1_b_overs = "(".$i->overs." ov)";
						}else{
							$team_2_b_score = ($i->declared) ? ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." d") : (($i->is_followed_on) ? (empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)." f" : ((empty($i->runs) ? "0" : $i->runs)."/".((empty($i->wickets))? "0" : $i->wickets)));
							$team_2_b_overs = "(".$i->overs." ov)";
					}


					}
					// echo $team_1_a_score."<br>";
					// echo $team_1_b_score."<br>";

					// echo $team_2_a_score."<br>";

					// echo $team_2_b_score;


				}

    ?>
	

<div class="col-md-12 col-xs-12 event-fixture-box">
<!-- <div class="result hidden-lg" style=" ">Result</div> -->
	<div class="">
	 <div class="col-xs-12 visible-xs  pb20">
	 <h2 class="fw-normal text-left  m0">
	 	<a class="color-grey-lighter f16" href="<?php echo $value->seo_url; ?>">
	 		<?php
	 			$start_date =  format_date_newserver2($value->match_start); 
	 			echo $start_date["date"]." ";
	 			echo $start_date["month"];

 			?>
	 	</a>
	  </h2>

			<h4 class="m0 lh28 fw-normal text-left"> <a class="color-grey-light mt-5" href="<?php echo $value->seo_url; ?>">
				<?php echo $value->title; ?>
			</a> 
			 </h4>
		</div>



	  <div class="col-md-2 col-sm-3 col-xs-6 ">
		<div class="row">
			<div class="team-first">
			   <ul style="text-align: center;">
				   <li><a href="<?php echo $value->seo_url; ?>"> <img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
				   <li><h4 class="color-jazz-grey-darker mb0 <?php echo $team_1_class; ?>"><?php echo $value->team_1->team->name; ?></h4></li>
					 <li>
		   	<h4 class="color-jazz-grey-darker mt0 mb0 <?php echo $team_1_class; ?>">
					 <span class="f12"> <?php echo $team_1_a_score; ?> </span>
					 <?php if(!empty($team_1_b_score)): ?>
					 <span class="color-jazz-grey-darker f12" style="padding-left:5px; padding-right:5px;"> & </span>
					 <span class="f12"> <?php echo $team_1_b_score; ?> </span>
					 <?php endif; ?>

		   	</h4>
		   </li>
		   <li>
		   	<span class="f12 <?php echo ((!empty($team_1_b_score)) ? "pr10" : ""); ?>">
		   		<?php echo $team_1_a_overs ?>
		   	</span>
				 <span class="f12">
				 	<?php echo $team_1_b_overs; ?>
				 </span>
		   </li>

			   </ul> 
		</div>
		</div>
	  </div>
	  <div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">
		<article class="mt10">
		  <div class="text-center">

			  <h2 class="fw-normal "><a class="color-grey-lighter" href="<?php echo $value->seo_url; ?>"><?php
	 			$start_date =  format_date_newserver2($value->match_start); 
	 			echo $start_date["date"]." ";
	 			echo $start_date["month"];

 			?></a>
			  </h2>

			<h4 class="m0 lh28 fw-normal "> 
				<a class="color-grey-light" href="<?php echo $value->seo_url; ?>">
					<?php echo $value->title; ?>
				</a> 
			 </h4>
			<h1 class="post-option mt0">
			  <span class="color-grey f18"><?php echo($value->match_result)? $value->match_result : '&nbsp;'; ?>   </span>
			</h1>
		  </div>

		</article>

	  </div>

	  <div class="col-md-2 col-sm-3 col-xs-6 ">
		<div class="row">
			<div class="team-second">
	   <ul style="text-align: center;">
		   <li><a href="<?php echo $value->seo_url; ?>"> <img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
		   <li><h4 class="color-jazz-grey-darker mb0 <?php echo $team_2_class; ?>"><?php echo $value->team_2->team->name; ?></h4></li>
		   <li>
			   	<h4 class="color-jazz-grey-darker mt0 mb0 <?php echo $team_2_class; ?>">
			   	<span class="f12"> <?php echo $team_2_a_score; ?> </span>
					 <?php if(!empty($team_2_b_score)): ?>

					 <span class="color-jazz-grey-darker f12" style="padding-left:5px; padding-right:5px;"> & </span>
					 <span class="f12"> <?php echo $team_2_b_score; ?> </span>
			<?php endif; ?>
					 </h4>
			</li>
		   <li><span class="f12 <?php echo ((!empty($team_2_b_score)) ? "pr10" : ""); ?>"><?php echo $team_2_a_overs ?></span><span class="f12"><?php echo $team_2_b_overs ?></span></li>

	   </ul> </div>
		</div>
	  </div>

	  <div class="col-md-8 col-xs-12 visible-xs">
		<article class="mt20">
		  <div class="text-center">


			<h1 class="post-option mt0">
			  <span class="color-grey f16"><?php echo($value->match_result)? $value->match_result : '&nbsp;'; ?></span>
			</h1>
		  </div>

		</article>

	  </div>

	</div>
  </div>
  <?php endforeach ?>
