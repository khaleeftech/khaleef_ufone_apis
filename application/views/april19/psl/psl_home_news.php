<?php if (!empty($news)): ?>

  <div class="col-lg-12 col-xs-12 crk-trending-videos-container">
    <div class="cs-seprator" style=" margin-bottom:10px; padding: 0">
  	<div class="devider1"> </div>
    </div>
    <h1 class="f20">News <a class="next-trending" href="<?php echo base_url('news'); ?>"> View All</a></h1>


		<?php $count = count($news);
		if(($count)%2!=0){ ?>
			<div class="col-md-12 col-xs-12  padding-md-0 mt-xs-5 ">
  			<div class=" row-xs-10">
		<?php } ?>

    <?php foreach($news as $key=>$n){
  	if($key>3){
  		break;
  	}
  	?>
  	<?php if (($key)%2==0 && ($count)%2==0){ ?>
  		<div class="col-md-12 col-xs-12  padding-md-0 mt-xs-5 ">
  			<div class=" row-xs-10">
  	<?php } ?>


  		<div class="col-xs-12 col-md-6 col-sm-6">
  			<div class="news-page">
  				<div class="col-md-6 col-sm-6">
  					<a href="<?php echo $n->seo_url; ?>">
  						<img src="<?php echo  $n->full_image; ?>" alt="" class="img-djoko img-home-item" style="width:100%">
  					</a>
  				</div>
  				<div class="col-md-6 data-news-pg pl5">
  	  				<h4 class="pt0 mb0 mt0 crop_to_one_line"><?php echo $n->title; ?></h4>

    					<p class="news-dd"><?php echo date('F j, Y', strtotime($n->created_at)); ?></p>
  	  				<div class="clear"> </div>
  	  				<p class="">
  	  					<span class="hidden-xs">
  		                  <?php echo substr(strip_tags($n->body),0, 160); ?>...
  		                </span>
  		                <span class="visible-xs">
  		                  <?php echo substr(strip_tags($n->body),0, 230); ?>...
  		                </span>
  	  				</p>

  				</div>
  				<a href="<?php echo $n->seo_url; ?>" class="more-arrow">
  					<img src="<?php echo assets_url(); ?>images/arrow-right.png">
  				</a>

  			</div>
  		</div>


  	<?php if (($key)%2!=0 || (count($news) == 1)){ ?>
  			</div>
  		</div>
  	<?php } ?>
  <?php } ?>


  </div>

<?php endif; ?>
