<div class="row">
                            <div class="col-md-12 col-xs-12">
                             <div class="row">
								 <table width="100%" cellspacing="0" cellpadding="0" class="table table-striped mb0">
									 <thead>
									  <tr>
										<td class="color-jazz-grey-darker text-left pl0 color-black f16"> <?php echo $series_title; ?> 
										</td>
									  </tr>
									  </thead>
								 </table>
                              <table width="100%" cellspacing="0" cellpadding="0" class="table table-striped">
                                 <thead class="bg-batting-head">
                                  <tr class="hidden-xs">
                                    <th class="text-left">Teams</th>
                                    <th width="6%" align="right">Mat</th>
                                    <th width="6%" align="right">Won</th>
                                    <th width="6%" align="right">Lost</th>
                                    <th width="6%" align="right">Tied</th>
                                    <th width="6%" nowrap="nowrap" align="right">No Result</th>
                                    <th width="6%" align="right" class="hidden">Aban</th>
                                    <th width="6%" nowrap="nowrap" align="right" class="hidden">Bonus Pts</th>
                                    <th width="6%" align="right">Points</th>
                                    <th width="6%" nowrap="nowrap" align="right">Net RR</th>
                                  </tr>

                                  <tr class="visible-xs">
                                    <th class="text-left pl5"><span>Teams</span></th>
                                    <th width="10" align="right"><span>M</span></th>
                                    <th width="10" align="right"><span>W</span></th>
                                    <th width="10" align="right"><span>L</span></th>
                                    <th width="10" align="right"><span>T</span></th>
                                    <th width="10" align="right"><span>NR</span></th>
                                    <th width="10" align="right" class="hidden-xs"><span>Ab</span></th>
                                    <th width="10" align="right" class="hidden-xs"><span>BPts</span></th>
                                    <th width="10" align="right"><span>Pts</span></th>
                                    <th width="10" align="right"><span>NRR</span></th>
                                  </tr>

								  </thead>
                                <tbody>


                                  <?php foreach ($points_table as $key => $value): ?>
                                    <tr>
                                    <td class="text-left"> <?php echo $value->team; ?> </td>
                                    <td align="right"> <?php echo $value->match_played ?> </td>
                                    <td align="right"> <?php echo $value->match_won ?> </td>
                                    <td align="right"> <?php echo $value->match_lost ?> </td>
                                    <td align="right"> <?php echo $value->match_tied ?> </td>
                                    <td align="right"> <?php echo $value->match_no_result ?> </td>
                                    <td align="right" class="hidden-xs hidden"> <?php echo $value->match_played ?> </td>
                                    <td align="right" class="hidden-xs hidden"> <?php echo $value->match_played ?> </td>
                                    <td align="right"> <?php echo $value->points ?> </td>
                                    <td align="right"> <?php echo $value->net_run_rate ?> </td>
                                  </tr>
                                  <?php endforeach ?>
                                </tbody>
                              </table>
                              </div>
                            </div>

                          </div>
