<div class="row">
                            <div class="col-md-12 col-xs-12 " style="overflow: hidden;">
                              <div class="row">
								<div class="">
    
		 
    
    
    <div class=" clearfix mb-xs-20"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="cs-seprator p0-xs" style=" margin-bottom:10px; padding: 0">
        <div class="devider1"></div>
      </div>
      <h1>
     	<span class="hidden-xs">The Ashes, 2017-18</span>
     	<span class="f15 visible-xs">The Ashes, 2017-18</span> 
     	
     	<span class="f16 color-jazz-grey-light hidden-xs">Jan 03 - Jan 08</span>  </h1>
     	<span class="f13 color-jazz-grey-light visible-xs">Jan 03 - Jan 08</span>
      
      
      <!--carousel 1-->
      
      <div id="trending" class="owl-carousel owl-theme" style="opacity: 0; display: block;">
        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 450px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 25px;"><div class="item" href="videos-detail.html">
                <div class="video-container">
                  
                      <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/669fe0fa37b6df832be3cb539ff9527c_1515822870.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 18 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption>
                </div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9069/bbl-27th-match-adelaide-strikers-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/5c6a2a49d22d703e3d12d259cde7e4b7_1515818927.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 72 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9043/bbl-27th-match-melbourne-renegades-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/b2d8ba24421eb65e9f4056272a504f48_1515817631.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 2 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9039/harris-outstanding-innings/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Harris Outstanding Innings!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9015/2nd-odi-pak-vs-aus-full-match-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">2nd ODI - Pak vs Aus - Full Match Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9012/2nd-odi-pak-vs-aus-pakistan-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">2nd ODI - Pak vs Aus - Pakistan Innings Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9001/hafeez-s-outstanding-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Hafeez's Outstanding Innings Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/8981/2nd-odi-pak-vs-aus-australia-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="owl-controls clickable" style="display: block;">
          <div class="owl-pagination">
            <div class="owl-page active"><span class=""></span></div>
            <div class="owl-page"><span class=""></span></div>
          </div>
        </div></div></div></div>
         
         
         
              
              
          
              
                      
              
         
        
      <div class="owl-controls clickable" style="display: block;"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
      <div class=" clearfix" style=" margin-bottom:10px;"></div>
      <!--end carousel 1-->
      <h1>
     	<span class="hidden-xs">West Indies Tour of New Zealand, 2017-18</span>
     	<span class="f15 visible-xs">WI Tour of NZ, 2017-18</span> 
     	
     	<span class="f16 color-jazz-grey-light hidden-xs">Nov 25 - Jan 03</span>  </h1>
     	<span class="f13 color-jazz-grey-light visible-xs">Nov 25 - Jan 03</span>
      
      
      <!---->
      <div id="trending1" class="owl-carousel owl-theme" style="opacity: 0; display: block;">
        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 450px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 25px;"><div class="item" href="videos-detail.html">
                <div class="video-container">
                  
                      <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/669fe0fa37b6df832be3cb539ff9527c_1515822870.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 18 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption>
                </div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9069/bbl-27th-match-adelaide-strikers-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/5c6a2a49d22d703e3d12d259cde7e4b7_1515818927.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 72 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9043/bbl-27th-match-melbourne-renegades-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/b2d8ba24421eb65e9f4056272a504f48_1515817631.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 2 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9039/harris-outstanding-innings/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Harris Outstanding Innings!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9015/2nd-odi-pak-vs-aus-full-match-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">2nd ODI - Pak vs Aus - Full Match Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9012/2nd-odi-pak-vs-aus-pakistan-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">2nd ODI - Pak vs Aus - Pakistan Innings Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9001/hafeez-s-outstanding-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Hafeez's Outstanding Innings Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/8981/2nd-odi-pak-vs-aus-australia-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="owl-controls clickable" style="display: block;">
          <div class="owl-pagination">
            <div class="owl-page active"><span class=""></span></div>
            <div class="owl-page"><span class=""></span></div>
          </div>
        </div></div></div></div>
         
         
         
              
              
          
              
                      
              
         
        
      <div class="owl-controls clickable" style="display: block;"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
      
      <div class=" clearfix" style=" margin-bottom:10px;"></div>
		<h1>
     	<span class="hidden-xs">Pakistan Tour of New Zealand , 2018</span>
     	<span class="f15 visible-xs">Pak Tour of NZ , 2018</span> 
     	
     	<span class="f16 color-jazz-grey-light hidden-xs">Nov 23 - Jan 08</span>  </h1>
     	<span class="f13 color-jazz-grey-light visible-xs">Nov 23 - Jan 08</span>
      
      <!---->
      
      <div id="trending2" class="owl-carousel owl-theme" style="opacity: 0; display: block;">
        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 450px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 25px;"><div class="item" href="videos-detail.html">
                <div class="video-container">
                  
                      <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/669fe0fa37b6df832be3cb539ff9527c_1515822870.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 18 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption>
                </div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9069/bbl-27th-match-adelaide-strikers-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/5c6a2a49d22d703e3d12d259cde7e4b7_1515818927.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 72 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9043/bbl-27th-match-melbourne-renegades-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/b2d8ba24421eb65e9f4056272a504f48_1515817631.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 2 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9039/harris-outstanding-innings/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Harris Outstanding Innings!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9015/2nd-odi-pak-vs-aus-full-match-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">2nd ODI - Pak vs Aus - Full Match Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9012/2nd-odi-pak-vs-aus-pakistan-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">2nd ODI - Pak vs Aus - Pakistan Innings Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/9001/hafeez-s-outstanding-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Hafeez's Outstanding Innings Highlights!</p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="item" href="http://cric.sa.zain.com/highlights/8981/2nd-odi-pak-vs-aus-australia-innings-highlights/">
                <div class="video-container">
                  <div class="video-thumbnail"> <img src="http://content.cricwick.net/thumbnails/98adb3c9a402a228aab3e77e6603f41f_1515815460.jpg" class="img-home-video img-responsive"> <a href="videos-detail.html"><img class="video-play-icon img-home-video" src="assets/images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span> <figcaption>
                <h2 class="mb0 hidden-xs">2nd Match - NZ vs AUS</h2>
                <h2 class="mb0 visible-xs">NZ vs AUS</h2>
              </figcaption></div>
                  <div class="video-content">
                    <p class="video-description">Outstanding batting performance never </p>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 25px;"><div class="owl-controls clickable" style="display: block;">
          <div class="owl-pagination">
            <div class="owl-page active"><span class=""></span></div>
            <div class="owl-page"><span class=""></span></div>
          </div>
        </div></div></div></div>
         
         
         
              
              
          
              
                      
              
         
        
      <div class="owl-controls clickable" style="display: block;"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
      
      <a href="" id="more-fixtures-btn" data-page-to-load="2" class="bottom-event-panel mt10 text-center more-series">  More Series</a>
		
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    </div>
    
    
  </div>  
                              </div>
                            </div>
                          </div>