<?php foreach($timeline_videos as $key=>$video){
  if($key>=6){
    break;
  }
  ?>

  <div class="col-md-4 col-sm-4 col-xs-6 " href="<?php echo base_url()."highlights/".$video->id."/".$video->match_obj->series_id."/".seo_url($video->title)."/"; ?>">
    <div class="video-container"  >
      <div class="video-thumbnail">
        <img src="<?php echo $video->med_image; ?>"  class="img-home-video img-responsive">
        <img class="video-play-icon img-home-video" src="<?php echo base_url() ?>assets/april19/images/play.png">
        <div class="overlay"> </div>
        <span class="video-view-count hidden"> <?php echo $video->views; ?>  views </span>
        <span class="video-duration fa fa-clock-o hidden"> 05:56 </span>

        <figcaption>
          <h2 class="mb0">
            <?php echo (!empty($video->match_obj->title))?$video->match_obj->title.' - ' :'' ; ?>
            <?php echo $video->match_obj->team_1.' vs '.$video->match_obj->team_2; ?>
        </h2>
        </figcaption>

          </div>

      <div class="video-content">
          <p class="video-description one-liner"><?php echo $video->title; ?></p>
        </div>

    </div>
  </div>

<?php } ?>
