<div class="top-score-title col-md-9">

		<div id="results-container">
			<?php foreach($articles as $article){ ?>
				<div class="col-md-12 pt20 pl0 pr0">
					<!-- Blog Lrg Start -->
					<div class="cs-blog blog-lrg mb20">
						<div class="cs-media">
							<figure><a href="<?php echo $article->seo_url; ?>"><img src="<?php echo $article->med_image; ?>" alt=""></a></figure>
						</div>
						<section>
						<div class="title">
							<h2 class="mt0"><a href="<?php echo $article->seo_url; ?>"><?php echo $article->title; ?></a></h2>
							<ul class="post-option pl0"><li class="time-xs"><time datetime="2014-03-16"><?php echo $article->created_at; ?></time></li></ul>
						</div>
						<div class="blog-text">
							<p class="mt0"><?php echo $article->details; ?></p>
							<div class="cs-seprator">
								<div class="devider1">
								</div>
							</div>
							<ul class="post-option pl0">
								<li class="time-xs"><i class="fa fa-user"></i>By<a href="javascript:;"> <?php echo $article->author; ?></a></li>
								<li class="time-xs hidden"><i class="fa fa-list"></i><a rel="tag" href="">Worldcup News</a>, <a rel="tag" href="">Top Stories</a></li>
							</ul>
							<a href="<?php echo $article->seo_url; ?>" class="btnshare addthis_button_compact at300m"><i class="fa fa-share-square-o"></i></a>
						</div>
						</section>
					</div>
					<!-- Blog Lrg End -->
				</div>
			<?php } ?>
				
		</div>
		<div class="bottom-event-panel">
			<a href="javascript:;" id="more-blogs-btn" data-page-to-load="2"> More Articles</a> 
		</div>
				
			
	<!--TABS END-->
</div>
