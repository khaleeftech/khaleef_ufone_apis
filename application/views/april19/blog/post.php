<div class="top-score-title col-md-9 pt20">
	<div class="detail-blog pl10 pr10">
		<div class="col-md-12">
			<div class="post-option-panel">
				<ul class="post-option">
	              <li class="post_time">
	                <time><?php echo date("F j, Y", $current_post->created_at/1000); ?> </time>
	              </li>
	            </ul>
	            <div class="cs-section-title">
	              <h2 id="hidettl" class="mt0 mb0"><?php echo $current_post->title; ?></h2>
	            </div>
	            <div class="cs-seprator mb0">
	              <div class="devider1"></div>
	            </div>
	            <ul class="post-option mt0">
	              <li class="post_time"> <i class="fa fa-user"></i>By <a href="javascript:;"> <?php echo $current_post->author; ?> </a></li>
	            </ul>
			</div>
			<figure class="detailpost blog-editor"><img src="<?php echo $current_post->large_image; ?>" alt=""></figure>
		</div>
		<div class="col-md-12">
			<div class="rich_editor_text blog-editor pl0"><?php echo $current_post->details; ?></div>
		</div>
		<div class="col-md-12 ">
			<div class="detail-post pl0">
				<h5 class="mb10">Share Now</h5>
				<div class="socialmedia">
					<ul class="mt0">
						<li><a data-original-title="Facebook" class="addthis_button_facebook social_sharing_link" href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u=<?php echo $current_post->seo_url; ?>&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a></li>
						<li><a data-original-title="twitter" class="addthis_button_twitter social_sharing_link" href="http://www.twitter.com/share?text=<?php echo $current_post->title; ?>&url=<?php echo $current_post->seo_url; ?>"><i class="fa fa-twitter"></i></a></li>
						<li><a data-original-title="google-plus" class="addthis_button_google social_sharing_link"><i class="fa fa-google-plus google" href="https://plus.google.com/share?url=<?php echo $current_post->seo_url; ?>"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-12 mt20">
		<div class="cs-section-title">
			<h2>More Blogs</h2>
		</div>
	</div>

		<?php foreach($more_posts as $post){ 
			if ($post->id!=$current_post->id){
			?>
			<div class="col-md-4 col-sm-4 pl5 pr5">
				<div class="cs-blog cs-blog-grid">
					<div class="cs-media">
                      <figure> <a href="<?php echo $post->seo_url; ?>">
                      	<img src="<?php echo $post->thumb_image; ?>" alt="blog-grid"></a> </figure>
                    </div>
                    <section>
                    	<div class="title">
                    		<ul class="post-option">
                    			<li class="post_time">
	                                <time><?php echo $post->created_at; ?></time>
	                              </li>
                    		</ul>
                    		<h4 class=""><a href="<?php echo $post->seo_url; ?>"><?php echo $post->title; ?></a></h4>
                    	</div>
                    	<div class="blog_text">
                    		<p><?php echo character_limiter(strip_tags($post->details), 250); ?></p>
                    		<ul class="post-option">
                              <li>By <a rel="tag" href="javascript:;"><?php echo $post->author; ?></a></li>
                            </ul>
                    	</div>
                    </section>
                    <div class="cs-seprator">
                      <div class="devider1"></div>
                    </div>
				</div>
			</div>
		<?php }
		} ?>
	<!--Close comment-->
</div>