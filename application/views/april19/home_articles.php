  <?php if (!empty($articles)) : ?>
  
  <?php foreach ($articles->data as $key => $art) { ?>
        <?php if($key==1) { ?>
        <!-- <h1><?php echo $key;?></h1> -->
        <?php       $date = new DateTime($art->created_at);
                    $date = $date->format("F d"); 
        ?>
    <div class="col-md-12 col-xs-12 padding-md-0 mt-xs-5 no-xs-padding" onclick="window.location='<?php echo base_url()."article/".$art->id."/".seo_url($art->title)."/" ?>'">
        <div class="col-md-12 col-sm-12 col-xs-12 pr-0 no-xs-padding">
          <div class="video-container">
            <div class="video-thumbnail"> <div style="padding-bottom: 40%; background-image: url(<?php echo $art->full_image; ?>)" class="sixteen-by-nine"></div>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 18 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
                <h1 style="font-weight: bold!important;" class="heading-lg"> <?php echo $art->title; ?> </h1>
                <h2 style="font-weight: bold!important;" class="mb0 heading-sm"><?php echo $art->by; ?></h2>
              </figcaption>
            </div>
          </div>
        </div>
    </div>
        <?php } ?>
      <?php if($key>1) { ?> 
      <div style="margin-top: 20px!important;" class="col-md-12 col-xs-12 padding-md-0 mt-xs-5 hide-xs-screen" onclick="window.location='<?php echo base_url()."article/".$art->id."/".seo_url($art->title)."/" ?>'">
        <div class="col-md-5 col-sm-5 col-xs-5 pr-0">
          <div class="video-container">
            <div class="video-thumbnail"> <div style="padding-bottom: 50%; background-image: url(<?php echo $art->full_image; ?>)" class="sixteen-by-nine"></div>
              
            </div>
          </div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-7 pr-0">
          <div class="container-description-lg">
            <p><?php echo $date; ?></p>
            <p><?php echo $art->title; ?></p>
            <p><?php echo $art->summary; ?></p>
          </div>
        </div>
        
      </div>
      
      <?php } ?>
      <?php if ($key>2) {
          break;
      } ?>
     <?php } ?>
   <?php endif ?>
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    