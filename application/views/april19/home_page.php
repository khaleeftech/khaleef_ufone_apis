<!DOCTYPE html>
<html lang="en">
<head>
<title>Ufone Cricket - Enjoy live cricket and watch key cricket moments</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Tune in to Ufone Cricket and never miss a cricket action again. Enjoy live and exclusive coverage of all cricket events. ">

<!-- Bootstrap Core CSS -->
<link href="<?php echo assets_url(); ?>home/css/bootstrap.min.css" rel="stylesheet">


<!-- Custom CSS -->
<link href="<?php echo assets_url(); ?>home/css/half-slider.css?v=13.5" rel="stylesheet">
<link href="<?php echo assets_url(); ?>home/css/custom.css?v=13.5" rel="stylesheet">
<link href="<?php echo assets_url(); ?>css/custom-front.css?v=13.5>" rel="stylesheet">
<link href="<?php echo assets_url(); ?>home/css/owl.carousel.min.css" rel="stylesheet">
<link href="<?php echo assets_url(); ?>home/css/owl.theme.default.min.css" rel="stylesheet">
<link href="<?php echo assets_url(); ?>home/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo assets_url(); ?>css/cric_css/style_asim.css?v=13.5" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/layout.css?v=13.5" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">

<!-- <!-- <link href="<?php echo assets_url() ?>home/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<!-- <link href="<?php echo assets_url() ?>home/css/half-slider.css?v=<?php echo VERSION; ?>" rel="stylesheet"> -->
<!-- <link href="<?php echo assets_url() ?>home/css/ufone-styles.css?v=<?php echo VERSION; ?>" rel="stylesheet"> -->
<!-- <link href="<?php echo assets_url1() ?>home/css/custom.css?v=<?php echo VERSION; ?>" rel="stylesheet"> -->
<link href="<?php echo assets_url1() ?>css/custom-front.css?v=<?php echo VERSION; ?>>" rel="stylesheet">
<!-- <link href="<?php echo assets_url1() ?>home/css/owl.carousel.css" rel="stylesheet"> -->
<!-- <link href="<?php echo assets_url1() ?>home/css/owl.theme.css" rel="stylesheet"> -->
<!-- <link href="<?php echo assets_url1() ?>home/css/font-awesome.min.css" rel="stylesheet"> -->
<!-- <link href="<?php echo assets_url1() ?>css/cric_css/style_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" /> -->
 <!-- <link href="<?php echo assets_url1() ?>css/layout.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" /> -->

<link href="<?php echo assets_url() ?>css/dev.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<!-- <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">-->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119615143-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-119615143-1');
</script>

<!-- Google Analytics Code -->



<style type="text/css">
.twitter .twitter-tweet .SandboxRoot .Embeddedtweet .MediaCard .MediaCard-media{
    display: none!important;
  }

  li span img {
    padding-top: 12px!important;
    opacity: 0.4;
  }
  .navbar li a {
    padding-top: 0px!important;
    color: #9C9DA1!important;
        height: 30px;
    padding-bottom: 0px;
  }
  li.active a {

  color: #9C9DA1!important;
}
li.active span img {

  color: #070707!important;
}
a:focus, a:hover{

}

      .bcolor {
        color: #363636!important;
      }
      .nocolor {
        color: #79787A;

      }
.sc-row .overflow {

      white-space: nowrap;
    /* width: 50px; */
    overflow: hidden;
    text-overflow: ellipsis;
    margin-left: 5px;
}
.overflow1 {
  white-space: nowrap;
     width: 130px;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-left: 5px;
}
.score-overflow {
      overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

@media only screen and (max-width: 600px) {
    span.score-overflow {
      font-size: 17px;
    }
}

</style>

</head>

<body id="videos_page">
<?php $this->load->view("april19/navigation"); ?>

<div class="hidden-sm hidden-md hidden-lg hidden" style="max-width: 100%;"> <img src="<?php echo assets_url(); ?>ads/top_app_mob_banner.jpg" class="playstore_link" style="max-width: 100%;"/> </div>
<div class="wrapper" style="background-color: #f88300!important;">
  <div class="wrapper-sc">
    <div class="owl-carousel owl-theme">


    <?php if(!empty($recent_matches)): ?>
    <?php foreach ($recent_matches as $key => $recent_match) { ?>
      <?php if ($key == 0) { ?>

      <div class="item xs-none" onclick="window.location='<?php echo $recent_match->seo_url; ?>'">
        <div class="scorecard">
          <div class="sc-title">
            <?php echo $recent_match->series->title; ?>
          </div>

          <?php
            if(!empty($recent_match->team_1->team->flag_url) && (strpos($recent_match->team_1->team->flag_url, 'missing.png') === false && strpos($recent_match->team_1->team->flag_url, 'placeholder.jpg') === false)){
                      $flag1 = $recent_match->team_1->team->flag_url;
                    }
                    if(!empty($recent_match->team_2->team->flag_url) && (strpos($recent_match->team_2->team->flag_url, 'missing.png') === false && strpos($recent_match->team_2->team->flag_url, 'placeholder.jpg') === false)){
                      $flag2 = $recent_match->team_2->team->flag_url;
                    }

                    $team_1_a_score = '';
                    $team_2_a_score = '';

                    $team_1_a_overs = '';
                    $team_2_a_overs = '';

                    $team_1_class = '';
                    $team_2_class = '';

                    $is_test = '';

                    if(strtolower($recent_match->format)  ==  'test' ){
                      $is_test = 1;
                    }

                    if($recent_match->match_won_by_id == $recent_match->team_1_id){
                      $team_1_class = 'winner-team';
                      $team_1_dot = 'winner-dot';
                        $team_2_class = '';
                      $team_2_dot = '';
                    }
                    if($recent_match->match_won_by_id == $recent_match->team_2_id){
                      $team_2_class = 'winner-team';
                      $team_2_dot = 'winner-dot';
                      $team_1_class = '';
                      $team_1_dot = '';
                    }

                    foreach ($recent_match->innings as $keyI => $i) {
                      if($keyI <= 1){
                        if($i->batting_team_id == $recent_match->team_1_id){
                          $team_1_a_score = $i->runs."/".$i->wickets;
                          $team_1_a_overs = "(".$i->overs." overs)";
                        }else{
                            $team_2_a_score = $i->runs."/".$i->wickets;
                            $team_2_a_overs = "(".$i->overs." overs)";
                        }
                      }else{

                        if($i->batting_team_id == $recent_match->team_1_id){
                          $team_1_b_score = $i->runs."/".$i->wickets;
                          $team_1_b_overs = "(".$i->overs." overs)";
                        }else{
                            $team_2_b_score = $i->runs."/".$i->wickets;
                            $team_2_b_overs = "(".$i->overs." overs)";
                        }

                      }

                    }
                    $match_start = new DateTime($recent_match->match_start);
                    $date = $match_start->format("d F");

                  ?>

          <div class="sc-row border-btm">
            <div><span class="<?php echo $team_1_dot; ?>"></span><span class="<?php echo $team_1_class; ?>"><?php echo $recent_match->team_1->team->short_name; ?></span></div>
            <span><?php echo $team_1_a_score; ?><?php echo $team_1_a_overs; ?></span>
            <img src="<?php echo $recent_match->team_1->team->flag_url ?>" />
          </div>
          <div class="sc-row">
          <div>
          <span class="<?php echo $team_2_dot; ?>"></span>
            <span class="<?php echo $team_2_class; ?>"><?php echo $recent_match->team_2->team->short_name; ?></span></div>
            <span><?php echo $team_2_a_score; ?><?php echo $team_2_a_overs; ?></span>
            <img src="<?php echo $recent_match->team_2->team->flag_url; ?>" />
          </div>
          <div class="sc-row">
            <span><?php echo $date; ?></span>
            <span class="overflow"><?php echo $recent_match->match_result; ?></span>
          </div>
        </div>
      </div>

     <?php } ?>
    <?php } ?>
    <?php endif ?>

    <?php if (!empty($live_matches)) : ?>
    <?php foreach ($live_matches as $key => $live_match) { ?>
    <?php
      $team1 = "";
      $team2 = "";
      $innings_team1 = "";
      $innings_team2 = "";
      $title = $live_match->team_1->team->name." vs ".$live_match->team_2->team->name;
            $match_url = base_url()."live/".$live_match->id."/".seo_url($title)."/";



            $flag1 = assets_url().'images/flag-placeholder.jpg';
          $flag2 = assets_url().'images/flag-placeholder.jpg';

          if(!empty($live_match->team_1->team->flag_url) && (strpos($live_match->team_1->team->flag_url, 'missing.png') === false && strpos($live_match->team_1->team->flag_url, 'placeholder.jpg') === false)){
            $flag1 = $live_match->team_1->team->full_flag_url;
          }
          if(!empty($live_match->team_2->team->flag_url) && (strpos($live_match->team_2->team->flag_url, 'missing.png') === false && strpos($live_match->team_2->team->flag_url, 'placeholder.jpg') === false)){
            $flag2 = $live_match->team_2->team->full_flag_url;
          }


          if (!empty($live_match->innings[0])) {
            if($live_match->innings[0]->batting_team_id == $live_match->team_1_id){
              $team1 = $live_match->team_1;
              $team2 = $live_match->team_2;
              $innings_team1 = $live_match->innings[0];
              $innings_team2 = @$live_match->innings[1];

            }
            else if ($live_match->innings[0]->batting_team_id == $live_match->team_2_id){
              $team1 = $live_match->team_2;
              $team2 = $live_match->team_1;

              $innings_team1 = @$live_match->innings[0];
              $innings_team2 = @$live_match->innings[1];
            }
            if (!empty($live_match->innings[2]) && $live_match->innings[2]->is_followed_on == true) {
                $innings2_team2 = $live_match->innings[2];
                $innings2_team1 = @$live_match->innings[3];
              }

            else {
                $innings2_team1 = $live_match->innings[2];
                $innings2_team2 = @$live_match->innings[3];
              }
          }
          // echo $innings2_team2->runs;

    ?>


    <div class="item" onclick="window.location='<?php echo base_url()."live/".$live_match->id."/".seo_url($title)."/" ?>'">
        <div class="scorecard">
          <div class="sc-title">
            <?php echo $live_match->series->title; ?>
          </div>
          <?php if (!empty($innings_team1)) : ?>
            <div class="sc-row <?php if($innings_team1->wickets < 10 && $innings_team1->declared == false) { echo 'bcolor'; } else { echo 'nocolor'; } ?>">
          <?php else : ?>
            <div class="sc-row">
          <?php endif ?>
          <?php if (empty($team1)) : ?>
            <span><?php echo $live_match->team_1->team->short_name; ?></span>
            <?php if (empty($innings_team1)) :  ?>
              <span>-</span>
            <?php else :?>
            <span><?php echo $innings_team1->runs; ?>/<?php echo $innings_team1->wickets; ?>(<?php echo $innings_team1->overs; ?>)</span>
            <?php endif ?>
            <img src="<?php echo $live_match->team_1->team->flag_url; ?>" />

          <?php  else : ?>

            <span><?php echo $team1->team->short_name; ?></span>
            <?php if (empty($innings_team1)) :  ?>
              <span>-</span>
            <?php else :?>
              <?php if ($innings_team1->declared == true || $innings_team1->wickets == 10) { ?>
              <span class="score-overflow" style="margin-left: 5px;"><?php echo $innings_team1->runs; ?>/<?php echo $innings_team1->wickets; ?> d</span>

              <?php } else { ?>
                <span class="score-overflow" style="margin-left: 5px;"><?php echo $innings_team1->runs; ?>/<?php echo $innings_team1->wickets; ?>(<?php echo $innings_team1->overs; ?>)</span>
              <?php } ?>
            <?php endif ?>



            <?php if (!empty($innings2_team1)) :  ?>
              &
            <span class="score-overflow"><?php echo $innings2_team1->runs; ?>/<?php echo $innings2_team1->wickets; ?>(<?php echo $innings2_team1->overs; ?>)</span>
            <?php endif ?>


            <img src="<?php echo $team1->team->flag_url; ?>" />
          <?php endif ?>
          </div>


          <?php if (!empty($innings_team2)) : ?>
            <div class="sc-row <?php if($innings_team2->wickets < 10 || @$innings2_team2->wickets < 10) { echo 'bcolor'; } else { echo 'nocolor'; } ?>">
          <?php else : ?>
            <div class="sc-row">
          <?php endif ?>
          <?php if (empty($team2)) : ?>
            <span><?php echo $live_match->team_2->team->short_name; ?></span>
            <?php if (empty($innings_team2)) :  ?>
              <span>-</span>
            <?php else :?>
            <span><?php echo $innings_team2->runs; ?>/<?php echo $innings_team2->wickets; ?>(<?php echo $innings_team2->overs; ?>)</span>
            <?php endif ?>
            <img src="<?php echo $live_match->team_2->team->flag_url; ?>" />

          <?php  else : ?>
            <span><?php echo $team2->team->short_name; ?></span>
            <?php if (empty($innings_team2)) :  ?>
              <span>-</span>
            <?php else :?>
           <?php if ($innings_team2->declared == true || $innings_team2->wickets == 10) { ?>
              <span class="score-overflow" style="margin-left: 5px;"><?php echo $innings_team2->runs; ?>/<?php echo $innings_team2->wickets; ?></span>

              <?php } else { ?>
                <span class="score-overflow" style="margin-left: 5px;"><?php echo $innings_team2->runs; ?>/<?php echo $innings_team2->wickets; ?>(<?php echo $innings_team2->overs; ?>)</span>
              <?php } ?>
            <?php endif ?>

            <?php if (!empty($innings2_team2)) :  ?>
              &
              <?php if ($innings2_team2->is_followed_on == true): ?>

            <span class="score-overflow" ><?php echo $innings2_team2->runs; ?>/<?php echo $innings2_team2->wickets; ?>(<?php echo $innings2_team2->overs; ?>) f</span>
            <?php else : ?>

            <span class="score-overflow"><?php echo $innings2_team2->runs; ?>/<?php echo $innings2_team2->wickets; ?>(<?php echo $innings2_team2->overs; ?>)</span>

            <?php endif ?>
            <?php endif ?>
            <img src="<?php echo $team2->team->flag_url; ?>" />
          <?php endif ?>
          </div>
          <div class="sc-row no-padding-left">
            <div>
              <div class="live-tag">
                <span style="color: #fff; line-height: 50px; font-size: 19px;">Live Now</span>
              </div>
              <div class="live-tag-triangle"></div>
            </div>
            <?php if (!empty($live_match->break_type)): ?>
              <span class="overflow1 text-right"><?php echo @$live_match->break_type; ?> (Day <?php echo @$live_match->day; ?>)</span>
            <?php else : ?>
            <span style="color: rgba(0,0,0,0.7);">View Match &nbsp;<i class="fa fa-angle-right" style="font-weight: bold;"></i></span>
            <?php endif ?>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php endif ?>

    <?php if(!empty($upcoming_fixtures)): ?>
    <?php foreach ($upcoming_fixtures as $key => $upcoming_fixture) { ?>


    <?php $match_start = new DateTime($upcoming_fixture->match_start);
          $date = $match_start->format("d F");
    ?>
      <div class="item">
        <div class="scorecard">
          <div class="sc-title">
            <?php echo $upcoming_fixture->series->title; ?>
          </div>
          <div class="sc-row-lg">
            <div class="text-center">
              <img src="<?php echo $upcoming_fixture->team_1->team->flag_url; ?>" />
              <span><?php echo $upcoming_fixture->team_1->team->short_name; ?></span>
            </div>
            <div class="text-center">
              <img src="<?php echo $upcoming_fixture->team_2->team->flag_url; ?>" />
              <span><?php echo $upcoming_fixture->team_2->team->short_name; ?></span>
            </div>
          </div>
          <div class="sc-row">
            <span><?php echo $date; ?></span>
            <span class="overflow">Upcoming</span>
          </div>
        </div>
      </div>
      <?php if ($key>3) {
        break;
      } ?>
    <?php } ?>
    <?php endif ?>

    <?php if(!empty($recent_matches)): ?>
    <?php foreach (array_reverse($recent_matches) as $key => $recent_match) { ?>
      <?php if ($key < count(array_reverse($recent_matches))-1) { ?>
      <div class="item" onclick="window.location='<?php echo $recent_match->seo_url; ?>'">
        <div class="scorecard">
          <div class="sc-title">
            <?php echo $recent_match->series->title; ?>
          </div>

          <?php
            if(!empty($recent_match->team_1->team->flag_url) && (strpos($recent_match->team_1->team->flag_url, 'missing.png') === false && strpos($recent_match->team_1->team->flag_url, 'placeholder.jpg') === false)){
                      $flag1 = $recent_match->team_1->team->flag_url;
                    }
                    if(!empty($recent_match->team_2->team->flag_url) && (strpos($recent_match->team_2->team->flag_url, 'missing.png') === false && strpos($recent_match->team_2->team->flag_url, 'placeholder.jpg') === false)){
                      $flag2 = $recent_match->team_2->team->flag_url;
                    }

                    $team_1_a_score = '';
                    $team_2_a_score = '';

                    $team_1_a_overs = '';
                    $team_2_a_overs = '';

                    $team_1_class = '';
                    $team_2_class = '';

                    $is_test = '';

                    if(strtolower($recent_match->format)  ==  'test' ){
                      $is_test = 1;
                    }

                    if($recent_match->match_won_by_id == $recent_match->team_1_id){
                      $team_1_class = 'winner-team';
                      $team_1_dot = 'winner-dot';
                        $team_2_class = '';
                      $team_2_dot = '';
                    }
                    if($recent_match->match_won_by_id == $recent_match->team_2_id){
                      $team_2_class = 'winner-team';
                      $team_2_dot = 'winner-dot';
                      $team_1_class = '';
                      $team_1_dot = '';
                    }

                    foreach ($recent_match->innings as $keyI => $i) {
                      if($keyI <= 1){
                        if($i->batting_team_id == $recent_match->team_1_id){
                          $team_1_a_score = $i->runs."/".$i->wickets;
                          $team_1_a_overs = "(".$i->overs." overs)";
                        }else{
                            $team_2_a_score = $i->runs."/".$i->wickets;
                            $team_2_a_overs = "(".$i->overs." overs)";
                        }
                      }else{

                        if($i->batting_team_id == $recent_match->team_1_id){
                          $team_1_b_score = $i->runs."/".$i->wickets;
                          $team_1_b_overs = "(".$i->overs." overs)";
                        }else{
                            $team_2_b_score = $i->runs."/".$i->wickets;
                            $team_2_b_overs = "(".$i->overs." overs)";
                        }

                      }

                    }
                    $match_start = new DateTime($recent_match->match_start);
                    $date = $match_start->format("d F");

                  ?>

          <div class="sc-row border-btm">
            <div><span class="<?php echo $team_1_dot; ?>"></span><span class="<?php echo $team_1_class; ?>"><?php echo $recent_match->team_1->team->short_name; ?></span></div>
            <span><?php echo $team_1_a_score; ?><?php echo $team_1_a_overs; ?></span>
            <img src="<?php echo $recent_match->team_1->team->flag_url ?>" />
          </div>
          <div class="sc-row">
          <div>
          <span class="<?php echo $team_2_dot; ?>"></span>
            <span class="<?php echo $team_2_class; ?>"><?php echo $recent_match->team_2->team->short_name; ?></span></div>
            <span><?php echo $team_2_a_score; ?><?php echo $team_2_a_overs; ?></span>
            <img src="<?php echo $recent_match->team_2->team->flag_url; ?>" />
          </div>
          <div class="sc-row">
            <span><?php echo $date; ?></span>
            <span class="overflow"><?php echo $recent_match->match_result; ?></span>
          </div>
        </div>
      </div>

     <?php } ?>
    <?php } ?>
    <?php endif ?>
    </div>
  </div>
  <!-- <h1><?php echo assets_url(); ?></h1>
  <!-- div class="banner-side-right">

    <ul class="nav nav-tabs nav-side" id="myTabs" role="tablist">
      <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Live</a></li>
      <li role="presentation"><a href="#results" role="tab" id="results-tab" data-toggle="tab" aria-controls="results">Results</a></li>
      <li role="presentation"><a href="#fixture" role="tab" id="fixture-tab" data-toggle="tab" aria-controls="fixture">Fixtures</a></li>
    </ul>
    <div class="tab-content nav-side-content" id="myTabContent">
      <div class="tab-pane fade in active" role="tabpanel" id="home" aria-labelledby="home-tab">
        <ul class="tab-banner">
          <li class="">
            <h5><a href="#pole"  data-toggle="modal" data-target="#pole">POLL FOR THE WINNER</a></h5>
            <p class="color-light-gray">Sydney Sixers 46/1 (6/20 ov) v Hobart Hurricanes</p>
          </li>
          <li class="">
            <h5><a href="">S. Sixers vs H. Hurricanes</a></h5>
            <p class="color-light-gray">Sydney Sixers 46/1 (6/20 ov) v Hobart Hurricanes</p>
          </li>
          <li class="">
            <h5><a href="">S. Sixers vs H. Hurricanes</a></h5>
            <p class="color-light-gray">Sydney Sixers 46/1 (6/20 ov) v Hobart Hurricanes</p>
          </li>
          <li class=""> <a href="#responsive"  data-toggle="modal" data-target="#responsive" class="fw-normal btn-more-tb">
            <h5 class="text-center more-tb"> More </h5>
            </a> </li>
        </ul>
      </div>
      <div class="tab-pane fade" role="tabpanel" id="results" aria-labelledby="results-tab">
        <ul class="tab-banner">
          <li class="">
            <h5>4th ODI: NZ v PK - Jan 16, 2018</h5>
            <p class="color-light-gray">New Zealand won by 5 wickets (with 25 balls remaining)</p>
          </li>
          <li class="">
            <h5>3rd ODI: NZ v PK - Jan 13, 2018</h5>
            <p class="color-light-gray">New Zealand won by 183 runs (with 45 balls remaining)</p>
          </li>
          <li class="">
            <h5>2nd ODI: NZ v PK - Jan 9, 2018</h5>
            <p class="color-light-gray">New Zealand won by 8 wickets (with 7 balls remaining)</p>
          </li>
          <li class=""> <a href="" class="fw-normal btn-more-tb">
            <h5 class="text-center more-tb"> More </h5>
            </a> </li>
        </ul>
      </div>
      <div class="tab-pane fade" role="tabpanel" id="fixture" aria-labelledby="fixture-tab">
        <ul class="tab-banner">
          <li class="">
            <h5>5th ODI: NZ v PK - 2018</h5>
            <p class="color-light-gray">Fri Jan 19 (50 ovs) 11:00 local (22:00 GMT -1d | 03:00 PKT) </p>
          </li>
          <li class="">
            <h5>1st T20: NZ v PK - 2018</h5>
            <p class="color-light-gray">Mon Jan 22 (20 ovs) 16:00 local (03:00 GMT | 08:00 PKT) </p>
          </li>
          <li class="">
            <h5>2nd T20: NZ v PK - 2018</h5>
            <p class="color-light-gray">Thu Jan 25 (20 ovs) 19:00 local (06:00 GMT | 11:00 PKT) </p>
          </li>
          <li class=""> <a href="" class="fw-normal btn-more-tb">
            <h5 class="text-center more-tb"> More </h5>
            </a> </li>
        </ul>
      </div>
    </div>
  </div> -->
</div>

<div class="wrapper-main">
  <!-- <div class="adds-hrz hidden" style="padding: 0px;"> <a href="" target="_blank"><img src="<?php echo assets_url(); ?>ads/vbox.jpg" alt=""/></a> </div> -->



  <!-- Page Content -->
  <div class="container">

    <div class="col-lg-12 crk-trending-videos-container">
    <!--html ad-->
     <div style="display: block; height: 100px; width: 100%; overflow: hidden; position: relative;margin-top: 10px;" class="hidden">
   <div id="adkit-container" style="width: 1280px; height: 100px; position: absolute;overflow: hidden; top: 0px; left: 0px; background-color: rgb(255, 255, 255);">
   <div id="image1" style="position: absolute; width: 1280px; height: 100px; top: 0px; left: 0px; z-index: 1; transform: rotate(0deg); font-family: Arial, Helvetica, sans-serif; visibility: visible; opacity: 1; overflow: hidden; word-break: normal; font-size: 0px;"><img src="<?php echo assets_url(); ?>images/pepsi/1280x100.jpg" style="width: 100%; height: 100%;"></div>
   <div id="image2" style="position: absolute; width: 210px; height: 148px; top: 1px; left: 678px; z-index: 2; transform: rotate(0deg); font-family: Arial, Helvetica, sans-serif; visibility: visible; opacity: 1; overflow: hidden; word-break: normal; font-size: 0px;"><img src="<?php echo assets_url(); ?>images/pepsi/hand.png" style="width: 100%; height: 100%;"></div>
   <div id="image3" style="position: absolute; width: 47px; height: 47px; top: 2px; left: 742px; z-index: 3; transform: rotate(0deg); font-family: Arial, Helvetica, sans-serif; visibility: visible; opacity: 1; overflow: hidden; word-break: normal; font-size: 0px;"><img src="<?php echo assets_url(); ?>images/pepsi/ball.png" style="width: 100%; height: 100%;"></div>
   <div id="image4" style="position: absolute; width: 165px; height: 75px; top: 14px; left: 305px; z-index: 4; transform: rotate(0deg); font-family: Arial, Helvetica, sans-serif; visibility: visible; opacity: 1; overflow: hidden; word-break: normal; font-size: 0px;"><img src="<?php echo assets_url(); ?>images/pepsi/pepsi.png" style="width: 100%; height: 100%;"></div>
   <div id="hotspot1" style="position: absolute; width: 1280px; height: 100px; top: 0px; left: 0px; z-index: 5; transform: rotate(0deg); cursor: pointer; visibility: visible;" class="adkit-hotspot-7a8d5f9c-d50b-c66f-d2c6-732934bebacf"></div></div>
   </div>
    <!-- end html ad-->

      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>
      <h1 style="font-weight: bold!important; color: #000;" class="f20">Latest Video Highlights <a class="next-trending" href="<?php echo base_url().'videos/' ?>"> View All </a> </h1>

      <!---->
      <div id="home-video" class="col-md-12 row-xs-10 padding-md-0 mt-xs-5">
        <div class="pleasewait text-center">
          <img src="<?php echo assets_url(); ?>images/loading.gif" alt="">
        </div>

        <div class=" clearfix" style=" margin-bottom:20px;"></div>

    </div>

    <div class="clearfix"></div>
    <!--<div class="col-lg-12">
      <div class="adds-hrz mt20 "> <a href="javascript:;" class="playstore_link"><img src="<?php echo assets_url(); ?>ads/add1.jpg" alt=""/></a> </div>
    </div>-->
  </div>
  <div class="clear"></div>
  <!-- <a href="javascript:;" class="playstore_link" style="display: block;"><img src="<?php echo assets_url(); ?>ads/add1.jpg" alt="" class="img-responsive" style="width: 100%;padding: 0;"></a> --> </div>

<div class="wrapper-main">

  <div class="container">

    <div class="col-lg-12 crk-trending-videos-container">

      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>
      <h1 style="font-weight: bold!important; color: #000;" class="f20">
            England v Pakistan Test Series 2018          
        <a class="next-trending" href="<?php echo base_url().'videos/' ?>"> View All </a>
      </h1>

      <!---->
      <div id="featured-series" class="col-md-12 row-xs-10 padding-md-0 mt-xs-5">
        <div class="pleasewait text-center">
          <img src="<?php echo assets_url(); ?>images/loading.gif" alt="">
        </div>
      </div>

    </div>

    <!--<div class="col-lg-12">
      <div class="adds-hrz mt20 "> <a href="javascript:;" class="playstore_link"><img src="<?php echo assets_url(); ?>ads/add1.jpg" alt=""/></a> </div>
    </div>-->

</div>
</div>


<?php if (isset($twitter->tweet) && !empty($twitter->tweet)): ?>
<div class="wrapper-main">
  <div class="container">
    <div class="col-lg-12">
      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>



        <div id="twitter" class="col-md-12 row-xs-10 padding-md-0 mt-xs-5">
          <!-- <div class="pleasewait text-center">
            <img src="<?php echo assets_url(); ?>images/loading.gif" alt="">
          </div> -->
          <?php foreach ($twitter->tweet as $key => $twt) { ?>
          <?php if ($key>2) { break; }  ?>
          <div class="col-lg-4 col-md-4 col-sm-6 twitter">
            <?php
            // echo $twt->body;
            // echo strip_tags($twt->body, '<span><div><blockquote><a><p>');
              $html = preg_replace('#<div class="MediaCard-media">(.*?)</div>#', '', $twt->body);
              echo $html;
            ?>
          </div>

          <?php } ?>
        </div>


      <div class=" clearfix" style=" margin-bottom:20px;"></div>

    </div>

    </div>
</div>
<?php endif ?>


<div class="wrapper-main">

  <div class="container">

      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>
    <div class="col-lg-8 col-xs-12 crk-trending-videos-container">

      <h1 style="font-weight: bold!important; color: #000;margin-top: 10px;" class="f20 container-hd">Featured Articles</h1>
      <div id="home-articles">
        <div class="pleasewait text-center">
          <img src="<?php echo assets_url(); ?>images/loading.gif" alt="">
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 crk-trending-videos-container">

      <h1 style="font-weight: bold!important; color: #000;margin-top: 10px;" class="f20 container-hd">Featured Videos</h1>

      <!---->
      <div id="featured-videos" class="col-md-12 col-sm-12 row-xs-10 padding-md-0 mt-xs-5">
        <div class="pleasewait text-center">
          <img src="<?php echo assets_url(); ?>images/loading.gif" alt="">
        </div>

      </div>
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    </div>
    <div class="clearfix"></div>
    <!--<div class="col-lg-12">
      <div class="adds-hrz mt20 "> <a href="javascript:;" class="playstore_link"><img src="<?php echo assets_url(); ?>ads/add1.jpg" alt=""/></a> </div>
    </div>-->
  </div>

</div>

<div class="wrapper-main">
  <div class="container">
    <div class="col-lg-12 crk-trending-videos-container">


      <div class="cs-seprator">
        <div class="devider1"> </div>
      </div>
      <h1 style="font-weight: bold!important; color: #000;" class="f20">News <a class="next-trending" href="<?php echo base_url().'news/' ?>"> View All</a></h1>
      <div id="home-news-container" class="col-md-12 col-xs-12  padding-md-0 mt-xs-5" >
       <div class="pleasewait text-center">
          <img src="<?php echo assets_url(); ?>images/loading.gif" alt="">
        </div>
    </div>
  </div>
</div>


<!-- /.container -->

<!--/wrapper-->


<!--/wrapper-->
<div class="modal fade" id="pole" tabindex="-3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-pole" style="">
    <div class="modal-content">
      <button type="button" class="close color-jazz-grey" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="event event-listing event-samll">
          <div id="fixtures-container">
            <!--vote-->
            <div class="col-md-12 col-xs-12 event-fixture-box">
              <div class="">
                <div class="col-xs-12  p20">
                  <h2 class="fw-normal text-left  m0"><a class="color-grey-lighter f16 " href="">29 January</a> </h2>
                  <h4 class="m0 lh28 fw-normal text-left mt-5">Match Prediction </h4>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 ">
                  <div class="row">
                    <div class="team-first vs">
                      <ul style="text-align: center;">
                        <li><a href=""> <img src="<?php echo assets_url(); ?>images/Afghanistan.png?1515590515" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
                        <li>
                          <h4 class="color-jazz-grey-darker mb0">Afghanistan</h4>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 ">
                  <div class="row">
                    <div class="team-second">
                      <ul style="text-align: center;">
                        <li><a href=""> <img src="<?php echo assets_url(); ?>images/aus.png" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
                        <li>
                          <h4 class="color-jazz-grey-darker mb0 match-won">Australia</h4>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 col-xs-12">
                  <article class="mt20">
                    <div class="text-center">
                      <h1 class="post-option mt0"> <span class="color-grey f16">VOTE: Who will win this match? </span> </h1>
                    </div>
                  </article>
                </div>
              </div>
            </div>
            <!--result-->

            <div class="col-md-12 col-xs-12 event-fixture-box mt10">
              <div class="">
                <div class="col-xs-12  pt20 pl20">
                  <h4 class="m0 lh28 fw-normal text-left mt-5">VOTE: Result </h4>
                </div>
                <div class="col-md-12 col-xs-12">
                  <article class="mt20">
                    <div class="text-center" style="width: 200px; margin: 0 auto; min-height: 200px">
                      <div id="pieChart" class="chart" style="width: 200px; height: 200px"></div>
                    </div>
                  </article>
                </div>
                <div class="col-xs-12  p10 pl0 pr0">
                  <div class="row">
                    <div class="col-md-6 col-xs-6 side-left">
                      <div class="left-color"></div>
                      <span class="pl5"> Afghanistan (30%)</span></div>
                    <div class="col-md-6 col-xs-6 side-right">
                      <div class="right-color"></div>
                      <span class="pl5"> Australia (70%)</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



</div></div>

<?php $this->load->view('footer'); ?>

</body>


<!-- jQuery -->
<script src="<?php echo assets_url(); ?>home/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo assets_url(); ?>home/js/bootstrap.min.js"></script>
<!-- <script src="<?php echo assets_url(); ?>home/js/jquery.mobile.custom.js"></script>  -->
<script src="<?php echo assets_url(); ?>home/js/owl.carousel.min.js"></script>
<script src="<?php echo assets_url(); ?>home/js/pole.js"></script>


<!-- Script to Activate the Carousel -->

<script>
    $('body').on('click','.playstore_link', function (e){


      sUsrAg = navigator.userAgent;
        if ((sUsrAg.indexOf("Android") > -1)) {
          ga('send', 'event', 'APP Banner', 'Click', 'Android', 1);
          window.open("market://details?id=com.Khaleef.CricWickMobilink");
      } else {
      	ga('send', 'event', 'APP Banner', 'Click', 'Non-Android', 1);
        win = window.open('https://play.google.com/store/apps/details?id=com.Khaleef.CricWickMobilink', '_blank');
        win.focus();
      }
    });
    $("body").on("click",".user", function (e){
		//e.preventDefault();
		var phone = $(this).attr("phone");
		var sub_status = $(this).attr("sub_status");
		var msg = "Welcome <b>"+phone+"</b><BR>";
		msg = msg + "Subscription Status: ";
		if (sub_status==1){
			msg = msg + "Active";
		} else if (sub_status==3){
			msg = msg + "Suspended";
		} else {
			msg = msg + "Trial";
		}
		$('.alert-popup .modal-body').html(msg);
		$('.alert-popup').modal('show');
	});

        // $('.carousel').carousel({
        //     interval: 10000 //changes the speed
        // });


        $(document).ready(function() {

      	$('.item  .img-home-item').on('click', function(event){
		    var link = $(this).parent().parent().attr("href");
		    if (link!='javascript:;'){
		    	window.location = link;
		    }
			});

			$('.video-container .img-home-video').on('click', function(event){
			    var link = $(this).parent().parent().parent().attr("href");
			    if (link!='javascript:;'){
			    	window.location = link;
			    } else {
			    	console.log("error");
			    }
			});

			$('.video-container .video-content .video-description').on('click', function(event){
			    var link = $(this).parent().parent().parent().attr("href");
			    if (link!='javascript:;'){
			    	window.location = link;
			    }
			});

			$('.item .data-news-pg').on('click', function(event){
			    var link = $(this).parent().attr("href");
			    console.log(link);
			    if (link!='javascript:;'){
			    	window.location = link;
			    }
			});
      $('.owl-carousel').owlCarousel({
          loop: true,
          margin: 10,
          responsiveClass: true,
          responsive: {
            0: {
              items: 1,
              nav: false
            },
            600: {
              items: 2,
              nav: false
            },
            1000: {
              items: 3,
              nav: true,
              navText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"],
              margin: 10
            },
            1600: {
              items: 4,
              nav: true,
              navText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"],
              margin: 10
            }
          }
      })
      //Uncomment to Move one slide right
      //$('.owl-carousel').trigger('to.owl.carousel', 1);


            // var trending = $("#trending");

            // trending.owlCarousel({
            //   items : 4, //10 items above 1000px browser width
            //   itemsDesktop : [1000,3], //5 items between 1000px and 901px
            //   itemsDesktopSmall : [900,2], // betweem 900px and 601px
            //   itemsTablet: [600,2], //2 items between 600 and 0
            //   itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
            // });

            // // Custom Navigation Events
            // $(".next-trending").click(function(){
            // trending.trigger('owl.next');
            // });

            // $(".prev-trending").click(function(){
            // trending.trigger('owl.prev');
            // });

            // var news = $("#news");

            // news.owlCarousel({
            //   items : 2, //10 items above 1000px browser width
            //   itemsDesktop : [1000,2], //5 items between 1000px and 901px
            //   itemsDesktopSmall : [900,2], // betweem 900px and 601px
            //   itemsTablet: [600,2], //2 items between 600 and 0
            //   itemsMobile : [400,1] // itemsMobile disabled - inherit from itemsTablet option
            // });

            // // Custom Navigation Events
            // $(".next-news").click(function(){
            // news.trigger('owl.next');
            // });

            // $(".prev-news").click(function(){
            // news.trigger('owl.prev');
            // });

        });

    </script>
</body>
</html>
<!-- <h1><?php echo base_url(); ?></h1> -->

<script type="text/javascript">

$(document).ready(function(){


        $('body').on('click', '.item  .img-home-item', function(event){
            var link = $(this).parent().parent().attr("href");
            if (link!='javascript:;'){
              window.location = link;
            }
        });

        $('body').on('click', '.video-container .img-home-video', function(event){
            var link = $(this).parent().parent().parent().attr("href");
            if (link!='javascript:;'){
              window.location = link;
            } else {
              console.log("error");
            }
        });

        $('body').on('click', '.video-container .video-content .video-description', function(event){
            var link = $(this).parent().parent().parent().attr("href");
            if (link!='javascript:;'){
              window.location = link;
            }
        });

        $('body').on('click', '.item .data-news-pg', function(event){
            var link = $(this).parent().attr("href");
            console.log(link);
            if (link!='javascript:;'){
              window.location = link;
            }
        });

    get_home_videos();
    // get_home_news();
    // get_featured_series();
    // get_featured_videos();
    // get_home_articles();
    // get_home_twitter();

          <?php if(!empty($live_matches)): ?>
            if($(window).width() < 600){
              console.log('move to livematch: ' + $(window).width());
              $('.owl-carousel').trigger('to.owl.carousel', 1);
            }
            console.log('live match!!');
          <?php endif; ?>
});
        function get_home_twitter() {
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url().'get_home_twitter' ?>",
                    dataType: "html",
                    success: function(response){
                        $("#twitter .pleasewait").fadeOut();
                        $("#twitter").html(response);
                    }
                });
            }

        function get_home_articles() {
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url().'get_home_articles' ?>",
                    dataType: "html",
                    success: function(response){
                        $("#home-articles .pleasewait").fadeOut();
                        $("#home-articles").html(response);

                    }
                });
            }

        function get_featured_videos() {
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url().'get_featured_videos' ?>",
                    dataType: "html",
                    success: function(response){
                        $("#featured-videos .pleasewait").fadeOut();
                        $("#featured-videos").html(response);
                        get_home_articles();
                    }
                });
            }
      function get_featured_series() {
          $.ajax({
              method: "POST",
              url: "<?php echo base_url().'get_featured_series' ?>",
              dataType: "html",
              success: function(response){
                  $("#featured-series .pleasewait").fadeOut();
                  $("#featured-series").html(response);
                  get_featured_videos();
              }
          });
      }
      function get_home_videos(){
        $.ajax({
          method: "POST",
          url: "<?php echo base_url().'get_home_videos'  ?>",
          dataType: "html",
          success: function(response){
            if(response){
              $("#home-video .pleasewait").fadeOut();
              $("#home-video").html(response);
              
            }
            get_home_news();
          }
        });
      }

      function get_home_news(){
        $.ajax({
          method: "POST",
          url: "<?php echo base_url().'get_home_news'  ?>",
          dataType: "html",
          success: function(response){
            if(response){
              $("#home-news-container .pleasewait").fadeOut();
              $("#home-news-container").html(response);
              
            }
            get_featured_series();
          }
        });
      }

      $("body").on("click",".user, .user1", function (e){
    //e.preventDefault();
    var phone = $(this).attr("phone");
    var sub_status = $(this).attr("sub_status");
    var msg = "Welcome <b>"+phone+"</b><BR>";
    msg = msg + "Subscription Status: ";
    if (sub_status==1){
      msg = msg + "Active";
    } else if (sub_status==3){
      msg = msg + "Suspended";
    } else {
      msg = msg + "Trial";
    }
    $('.alert-popup .modal-body').html(msg);
    $('.alert-popup').modal('show');
  });
    </script>
