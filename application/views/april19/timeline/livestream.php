<?php
	$teamA = "";
	$teamB = "";
	$innings = $live->innings;
	$current_inning = count($innings)-1;
	$batting_team_id = $innings[$current_inning]->batting_team_id;
	$fielding_team_id = $innings[$current_inning]->fielding_team_id;

	$teamA = $teams[$batting_team_id];
	$teamB = $teams[$fielding_team_id];

	$batsman_onstrike = "";
	$batsman_offstrike = "";

	$bowler_onspell = "";
	$bowler_offspell = "";

	if (!empty($live->partnership_and_bowlers)){
		if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="facing"){
			$batsman_onstrike = $live->partnership_and_bowlers->batsman_1;
		} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="facing"){
			$batsman_onstrike = $live->partnership_and_bowlers->batsman_2;
		}

		if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="batting"){
			$batsman_offstrike = $live->partnership_and_bowlers->batsman_1;
		} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="batting"){
			$batsman_offstrike = $live->partnership_and_bowlers->batsman_2;
		}

		if (!empty($live->partnership_and_bowlers->bowler)){
			$bowler_onspell = $live->partnership_and_bowlers->bowler;
		}
		if (!empty($live->partnership_and_bowlers->last_bowler)){
			$bowler_offspell = $live->partnership_and_bowlers->last_bowler;
		}
	}

	$banner = empty($live->banner_file_name)?$default_banner:BACKEND."../".$live->banner_url;

	$innings[$current_inning]->runs = empty($innings[$current_inning]->runs)?0:$innings[$current_inning]->runs;
	$innings[$current_inning]->wickets = empty($innings[$current_inning]->wickets)?0:$innings[$current_inning]->wickets;
	$innings[$current_inning]->overs = empty($innings[$current_inning]->overs)?"0.0":$innings[$current_inning]->overs;
	$innings[$current_inning]->run_rate = empty($innings[$current_inning]->run_rate)?"0.0":$innings[$current_inning]->run_rate;

?>
<style type="text/css">
	@media(max-width: 767px) {
	    .container{
	    	padding-right: 0px;
	    }
	}
</style>
<div class="wrapper wrapper-main">
  <section id="quiz" class="container secondary-page">
    <div class="general general-results players">
    	<div class="cs-section-title" style="margin-bottom: 0px;">
			<h2 class=""><?php echo $live->team_1->team->name." vs ".$live->team_2->team->name; ?></h2>

		</div>

	<div class="" style="margin-bottom: 15px;">
		<div id="live_stream_player"></div>
		<script type="text/javascript">
			var playerInstance = jwplayer("live_stream_player");
			var stream_allowed = <?php echo $stream_allowed; ?>;
			playerInstance.setup({
        // file: "<?php //echo $live->live_stream_url; ?>",

            "playlist": [
              {
                "sources": [
                  {
                    "default": false,
                    "file": "<?php echo $live->live_stream_url; ?>",
                    "label": "0",
                    "type": "hls",
                    "preload": "none"
                  }
                ]
              }
            ],
            "primary": "html5",
            "hlshtml": true,
            "autostart": true,
            width: "100%",
  			    aspectratio: "16:9",
  			    controls: true,
  			    title: "<?php echo "Live Stream: ".$live->team_1->team->name." vs ".$live->team_2->team->name; ?>",
  			    ga: {
    					label: "title"
    				}
			}).onPlay(function() {
				if (stream_allowed==0){
					jwplayer("live_stream_player").stop();
					//alert('Sorry, this content is not available in your country!');
					$(".buy_stream_anchor").click();
				}
			});
		</script>
	</div>

<?php if (!$stream_allowed){ ?>
	<div style="margin-bottom: 15px;">
		<div class="col-sm-12">
			<img class="buy_stream_anchor" src="<?php echo base_url()."assets/ads/buy_live_stream_4sar.jpg"; ?>">
			<BR>

			<div class="buy_stream hidden">
				<a href="javascript:;" class="btn btn-success buy_stream_anchor buy_stream_anchor_btn" data-loading-text="Please wait...">
				Click Here to Purchase Stream
				</a>
			</div>
		</div>
	</div>
<?php
}
?>
<div class="col-xs-12 col-sm-12 visible-xs visible-sm pt15" style="background-color:#b5b5b5 ; padding-bottom:15px">
	<div class="col-xs-12 col-sm-12 visible-xs visible-sm" style="background-color:#eeeeee ">
	   <div class="col-xs-12 col-sm-12 pl0 pr0">
	     <div class="row">
	       <div class="second-team-content batting">
	         <div class="current-score-first-row">
	         <div class="flag-container" style="width:50px; float:left">
	         	<img src="<?php echo $teamA->flag_url; ?>" class="teamflag">
         	 </div>
	           <div class="pull-left active-bold ml20 mt10"><span class="teamtitle full"><?php echo $teamA->name; ?></span></div>
	           <div class="score-container active-bold text-right"> <span class="teamscore"> <?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?> </span>
	             <div class="clearfix"></div>
	             <span class="teamovers"> (<?php echo $innings[$current_inning]->overs; ?> ov) </span>
	           </div>
	           <div class="clearfix"></div>
	         </div>
	       </div>
	     </div>
	   </div>
	   <hr>
	   <div class="col-xs-12 col-sm-12 pl0 pr0">
	     <div class="row">
	       <div class="first-team-content bowling">
	         <div class="current-score-first-row">
	           	   <?php

	           	   $runs = "-";
				   $overs = "-";
				   if (count($live->innings)>1){
						$runs = $innings[$current_inning-1]->runs."/".$innings[$current_inning-1]->wickets;
					   	$overs = "(".$innings[$current_inning-1]->overs." ov)";
				   }

	           	   ?>
		           <div class="flag-container" style="width:50px; float:left">
		           	<img src="<?php echo $teamB->flag_url; ?>" class="teamflag">
		           </div>
		           <div class="pull-left ml20 mt10"><span class="teamtitle full"><?php echo $teamB->name; ?></span></div>
		           <div class="score-container pull-right text-right"> <span class=" teamscore"> <?php echo $runs; ?> </span>
		             <div class="clearfix"></div>
		             <span class=" teamovers"> <?php echo $overs; ?> </span> </div>
		           <div class="clearfix"></div>

	           <div>
	             <?php if (count($innings)==1){ ?>
					<p class="tagline"><?php echo $teams[$live->toss_won_by_id]->short_name." won the toss and choose to ".($live->chose_to=="Bowl"?"field":"bat")." first"; ?></p>
				<?php } else { ?>
					<p class="tagline"><?php echo "Need ".($innings[$current_inning]->runs_required+1-$innings[$current_inning]->runs)." runs from ".$innings[$current_inning]->balls_remaining." balls"; ?></p>
				<?php } ?>
	             <span class="">Batting</span>
	             <div class="clearfix"></div>
	             <span class="player-name active-bold onstrike"> <?php echo !empty($batsman_onstrike)?$batsman_onstrike->player->short_name."*":"-"; ?></span>
	             <span class="player-score active-bold onstrike-status pull-right"> <?php echo !empty($batsman_onstrike)?$batsman_onstrike->batsman->runs_scored."(".$batsman_onstrike->batsman->balls_played.")":"-"; ?> </span>
	             <div class="clearfix"></div>
	             <span class="player-name offstrike"> <?php echo !empty($batsman_offstrike)?$batsman_offstrike->player->short_name:"-"; ?> </span>
	             <span class="player-score offstrike-status pull-right"> <?php echo !empty($batsman_offstrike)?$batsman_offstrike->batsman->runs_scored."(".$batsman_offstrike->batsman->balls_played.")":"-"; ?> </span>
	           </div>
	         </div>
	       </div>
	     </div>
	   </div>
	</div>
</div>

</div>
</section>
</div>



<script>
var base_url = "<?php echo base_url(); ?>";
var match_id = "<?php echo $match_id; ?>";
var match_stream_id = "<?php echo $match_stream_id; ?>";

function purhcase_stream(){
		$("body").off("click",".buy_stream_anchor");
		if (confirm("You will be charged with 4 SAR for live streaming. Are you sure you want to continue")){
			$('.alert-popup .modal-body').html("Please wait while we process your request...");
			$('.alert-popup').modal('show');
			$('.alert-popup .btn-confirm').addClass("disabled");
			//$(".buy_stream_anchor_btn").button("loading");
			//btnRef = $(".buy_stream_anchor_btn");
			var url = base_url+'timeline/purchase_live_stream/'+match_stream_id;
			$.ajax({
				dataType: "json",
				url: url,
				data: {},
				success: function(response) {
					if (response.status==1){
						window.location.reload(false);
					} else {
						//alert(response.message);
						//$('.alert-popup').modal('hide');
						$('.alert-popup .modal-body').html(response.message);
						$('.alert-popup').modal('show');
						$('.alert-popup .btn-confirm').removeClass("disabled");
						//btnRef.button("reset");
					}
					$("body").on("click",".buy_stream_anchor",purhcase_stream);
				}
			});
		}
		return false;
}

function updateLiveMatches(){
	var match_updated = false;
	$.ajax({
		dataType: "json",
		url: base_url+'ajax/updateHomeLiveMatchScore',
		data: {},
		success: function(response) {
			response.live_matches.forEach(function(live) {

				if (live.match_id==match_id){
					$(".player-name.onstrike").html(live.batting.onstrike.short_name);
					$(".player-score.onstrike-status").html(live.batting.onstrike.score);
					//$("#liveMatch"+live.match_id+" .batting .run-rate").html("CRR: "+live.batting.run_rate);

					$(".batting .teamscore").html(live.batting.score);
					$(".batting .teamflag").attr("src",live.batting.flag);

					$(".player-name.offstrike").html(live.batting.offstrike.short_name);
					$(".player-score.offstrike-status").html(live.batting.offstrike.score);

					var overs = '('+live.batting.overs+' ov)';

					$(".batting .teamovers").html(overs);
					$(".batting .teamtitle.full").html(live.batting.title);
					//$("#liveMatch"+live.match_id+" .batting .teamtitle.mini").html(live.batting.title_small+'*');
					//$("#liveMatch"+live.match_id+" .batting .teamtitle").css("background",live.batting.color);

					if (live.bowling.score!=''){
						$(".bowling .teamscore").html(live.bowling.score);
						$(".bowling .teamflag").attr("src",live.bowling.flag);
						$(".bowling .teamtitle.full").html(live.bowling.title);
						//$("#liveMatch"+live.match_id+" .bowling .teamtitle.mini").html(live.bowling.title_small);
						//$("#liveMatch"+live.match_id+" .bowling .teamtitle").css("background",live.bowling.color);
						overs = '('+live.bowling.overs+' ov)';
						$(".bowling .teamovers").html(overs);
					}

					$(".tagline ").html(live.tagline);

					//match_updated = true;
				}

			});
			/*
			if (!match_updated){
				window.location = "<?php echo $post_match_url; ?>";
			}
			*/
		},timeout: 6000
	});
}

$( document ).ready(function(){
	$("body").on("click",".buy_stream_anchor",purhcase_stream);
	recursive_call_id = setInterval(function() {
		  updateLiveMatches();
	}, 10000);
	window.onbeforeunload = function(e) {
	  	clearInterval(recursive_call_id);
		console.log("Clearing Interval");
	};
});
</script>
