<h1 class="featured-title" style="font-weight: bold!important; color: #000;" class="f20">
            <?php echo $timeline_videos[0]->match_obj->series_name; ?>          
        <a class="next-trending" href="<?php echo base_url().'videos/' ?>"> View All </a>
</h1>
<?php foreach ($timeline_videos as $key => $video) { ?>

  <?php if ($key==1) { ?>
      <div class="col-md-8 col-sm-8 col-xs-12 pr-0">
          <div class="video-container " >
            <div class="video-thumbnail"> <div style="padding-bottom: 40%; background-image: url(<?php echo $video->med_image; ?>)" class="sixteen-by-nine"></div> <a href="<?php echo base_url()."highlights/".$video->id."/".$video->match_obj->series_id."/".seo_url($video->title)."/"; ?>"><img class="play-icon-ov" src="<?php echo assets_url(); ?>images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 18 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
                <h1 style="font-weight: bold!important;" class="heading-lg"> <?php echo $video->title; ?> </h1>
                <h2 style="font-weight: bold!important;" class="mb0 heading-sm"><?php echo $video->match_obj->title; ?></h2>
              </figcaption>
            </div>
          </div>
        </div>
  <?php } ?>
  <?php if ($key<=3 && $key>1) { ?>
        <div class="col-md-4 col-sm-4 col-xs-6 pb-5 xs-screen-adjust" href="<?php echo base_url()."highlights/".$video->id."/".$video->match_obj->series_id."/".seo_url($video->title)."/"; ?>">
          <div class="video-container">
            <div class="video-thumbnail"> <div style="background-image: url(<?php echo $video->med_image; ?>)" class="sixteen-by-nine"></div> <a href="<?php echo base_url()."highlights/".$video->id."/".$video->match_obj->series_id."/".seo_url($video->title)."/"; ?>"><img class="play-icon-ov-sm" src="<?php echo assets_url(); ?>images/play.png"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 72 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
               <!--  <h1 style="font-weight: bold!important;" class="heading-lg"> <?php echo $video->title; ?> </h1>
                <h2 style="font-weight: bold!important;" class="mb0 heading-sm"><?php echo $video->match_obj->title; ?></h2> -->
              </figcaption>
              
            </div>
          </div>
        </div>
  <?php } ?>
  <?php if ($key<=5 && $key>3) { ?>
        <div class="col-md-6 col-sm-6 col-xs-6 pt-5 pr-0 hide-xs-screen" href="<?php echo base_url()."highlights/".$video->id."/".$video->match_obj->series_id."/".seo_url($video->title)."/"; ?>">
          <div class="video-container">
            <div class="video-thumbnail"> <div style="background-image: url(<?php echo $video->med_image; ?>)" class="sixteen-by-nine"></div> <a href="<?php echo base_url()."highlights/".$video->id."/".$video->match_obj->series_id."/".seo_url($video->title)."/"; ?>"></a>
              <div class="overlay"> </div>
              <span class="video-view-count hidden"> 4 views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
              <figcaption>
                <h2 style="margin-bottom: 5px!important; text-transform: uppercase;" class="mb0 heading-sm"><?php echo $video->match_obj->title; ?></h2>
                <h1 style="font-weight: bold!important;margin-bottom: 10px!important;" class="heading-lg"> <?php echo $video->title; ?> </h1>
              </figcaption>
            </div>
          </div>
        </div>
  <?php } ?>
  <?php if ($key>5) {
    break;
  } ?>
<?php } ?>