<div class="wrapper-main mtb10 bg-login" style="height: 100%;">

  <!-- LOGIN BOX -->
  <div class="col-md-12 col-xs-12 pt20">
    <div class="col-xs-12 hidden">
      <div class="row">
        <div class="btn btn-group pull-right"> <a href="http://cricket.jazz.com.pk/login?lang=english" class="login btn btn-sm btn-primary">English</a> <a href="http://cricket.jazz.com.pk/login?lang=urdu" class="login btn btn-sm btn-default">اردو</a> </div>
      </div>
    </div>


      <div class="col-md-10 col-xs-12 col-xs-offset-1 pl0 pt40">
        <div class="">
          <h4 class="color-white fw-normal text-uppercase login-title">Register | Login Now!</h4>
        </div>
      </div>

    <div class=" col-md-10 col-xs-12 col-xs-offset-1 login-page pr0">
      <div class="col-md-6 col-sm-6 col-xs-12">


        <form method="post" class="login-form" id="pin-form-register">
          <div class="left-panel-login name" style="">
            <div class="pt10">We have sent PIN Code to your mobile number <?php echo $pin_phone; ?> through SMS!</div>
            <label for="name_login">
              <?php echo lang("enter_pincode"); ?>
            </label>
            <input id="pincode" name="pincode" type="text" placeholder="xxxx" value="" class="input-number" required=""/>

            <div id="login-submit">
      				<input type="submit" class="btn-continue" id="btnSubmit" value="<?php echo lang("label_subscribe"); ?>" data-loading-text="<?php echo lang("label_please_wait"); ?>"/>
      			</div>


    			  <div class="mt10 color-red message" style="display:none;">
    			  	*Please enter valid number
    			  </div>

          </div>
        </form>



      </div>
      <div class="col-md-6 col-sm-6  col-xs-12 img-login">
		  <a href="javascript:;" class="playstore_link">
        <img src="<?php echo assets_url(); ?>images/img-download.jpg?v=<?php echo VERSION ?>" alt="" class="img-responsive">
      </a>
      </div>
    </div>
  </div>
  <!--Close Login-->


</div>
<div class="clearfix"></div>

<script>

var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){
	$("body").on("submit","#pin-form-register", function(e){
    $('#pin-form-register .message').hide();
		$("#pin-form-register #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/confirm_pin_register/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					window.location = response.url;
				} else {
					// $('.alert-popup .modal-body').html(response.message);
					// $('.alert-popup').modal('show');

          $('#pin-form-register .message').html(response.message);
					$('#pin-form-register .message').show();

					$("#pin-form-register #btnSubmit").button("reset");
				}
			}
		});
		return false;
	});
});

</script>
