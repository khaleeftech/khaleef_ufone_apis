<!DOCTYPE html>
<html lang="en">

<head>
	<title><?php echo $og_tags->title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $og_tags->descr; ?>">
    <link rel="shortcut icon" href="<?php echo assets_url(); ?>images/fav/ico-16.png">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-144.png" sizes="144x144">

    <meta property="og:site_name" 		   content="CricWick"/>
	<meta property="fb:app_id" 			   content="430752913746475" />
	<?php if (!empty($video_page)){ ?>

	<meta property="og:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta property="og:title" content="<?php echo $current_video->social->title; ?>" />
	<meta property="og:description" content="<?php echo $current_video->social->description; ?>" />
	<meta property="og:image" content="<?php echo $current_video->social->thumb_file; ?>" />
	<?php /*
	<meta property="og:type" content="website" />
	<meta property="og:video:width" content="640" />
	<meta property="og:video:height" content="320" />
	<meta property="og:video:type" content="video/mp4" />
	<meta property="og:video:url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />
	<meta property="og:video:secure_url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />

	<meta name="twitter:card" content="player">
	<meta name="twitter:app:name:googleplay" content="CricBoom">
	<meta name="twitter:app:id:googleplay" content="com.Khaleef.CricBoom" />
	<meta name="twitter:player" content="<?php echo base_url(). "embed/video/".$current_video -> id; ?>" />
	<meta name="twitter:player:stream" content="<?php echo base_url()."embed/serve/".$current_video -> id; ?>" />
	<meta name="twitter:player:stream:content_type" content="video/mp4" />
	<meta name="twitter:player:width" content="480">
	<meta name="twitter:player:height" content="480">
	*/ ?>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta name="twitter:title" content="<?php echo $current_video->social->title; ?>">
	<meta name="twitter:description" content="<?php echo $current_video->social->description; ?>">
	<meta name="twitter:image" content="<?php echo $current_video->social->thumb_file; ?>">

	<?php } ?>
	<style>
		.user {
		   top: 20px !important;
		}
		@media(max-width: 767px) {
			.user {
			   top: 7px !important;
			}
		}
	</style>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo assets_url();  ?>home/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo assets_url();  ?>home/css/half-slider.css?v=<?php echo VERSION; ?>" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/custom.css?v=<?php echo VERSION; ?>" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>css/custom-front.css?v=<?php echo VERSION; ?>>" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/owl.theme.css" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo assets_url();  ?>css/cric_css/style_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo assets_url(); ?>css/layout.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

	<meta name="google-site-verification" content="vFrwbZ1fAeGzbO4yDWsPPW0q1FH8-3F7KOUTk7S5lOk" />
	<meta name="msvalidate.01" content="73818B487022F81E4006E2364DA72CCA" />

	<meta property="og:url"  content="<?php echo current_url(); ?>" />
	<meta property="og:title" content="<?php echo $og_tags->title; ?>" />
	<meta property="og:description" content="<?php echo $og_tags->descr; ?>" />
	<meta property="og:image" content="<?php echo base_url()."assets/images/default_banner_icc.jpg"; ?>" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo current_url(); ?>" />
	<meta name="twitter:title" content="<?php echo $og_tags->title; ?>">
	<meta name="twitter:description" content="<?php echo $og_tags->descr; ?>">
	<meta name="twitter:image" content="<?php echo base_url()."assets/images/default_banner_icc.jpg"; ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.<?php echo assets_url();  ?>home/js/1.4.2/respond.min.js"></script>
    <![endif]-->



		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-88034149-3"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119615143-1');
		</script>

		<!-- Google Analytics Code -->


<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<script>jwplayer.key="2HZsFtRYlzQZJpY2SmrCGnAe6fBk5mGtGN8MgA==";</script>

<style media="screen">
  #videos_page .col-lg-1, #videos_page  .col-lg-10, #videos_page  .col-lg-11, #videos_page  .col-lg-12, #videos_page  .col-lg-2, #videos_page  .col-lg-3, #videos_page  .col-lg-4, #videos_page  .col-lg-5, #videos_page  .col-lg-6, #videos_page  .col-lg-7, #videos_page  .col-lg-8, #videos_page  .col-lg-9, #videos_page  .col-md-1, #videos_page  .col-md-10, #videos_page  .col-md-11, #videos_page  .col-md-12, #videos_page  .col-md-2, #videos_page  .col-md-3, #videos_page  .col-md-4,
  #videos_page  .col-md-5, #videos_page  .col-md-6, #videos_page  .col-md-7, #videos_page  .col-md-8, #videos_page  .col-md-9, #videos_page  .col-sm-1, #videos_page  .col-sm-10, #videos_page  .col-sm-11, #videos_page  .col-sm-12, #videos_page  .col-sm-2, #videos_page  .col-sm-3, #videos_page  .col-sm-4, #videos_page  .col-sm-5, #videos_page  .col-sm-6, #videos_page  .col-sm-7, #videos_page  .col-sm-8, #videos_page  .col-sm-9, #videos_page  .col-xs-1, #videos_page  .col-xs-10,
  #videos_page  .col-xs-11, #videos_page  .col-xs-12, #videos_page  .col-xs-2, #videos_page  .col-xs-3, #videos_page  .col-xs-4, #videos_page  .col-xs-5, #videos_page  .col-xs-6, #videos_page  .col-xs-7, #videos_page  .col-xs-8, #videos_page  .col-xs-9{
    padding:5px;
  }
  #videos_page  .col-sm-4 .video-container{
    min-height: 175px;
  }
  #videos_page  .col-md-3 .video-container{
    min-height: 195px;
  }
</style>
</head>

<body id="videos_page">
  <?php $this->load->view("navigation"); ?>


<div class="wrapper">
  <div id="mainVideo">

    <div class="videobox2 video" video-link="#" style="width:100%; display:block;">
       <div id="videoElement"></div>
       <script type="text/javascript">
         var video_allowed = 1;
         var playerInstance = jwplayer("videoElement");
         var view_confirmed = false;

         playerInstance.setup({
           sources:[
                 <?php foreach($response->vf->video->qualities as $q){ ?>
                   {
                     "file":"<?php echo $q->video_file; ?>",
                     "type" : "mp4",
                     "label":"<?php echo $q->height; ?>",
                     "default":"<?php echo($q->height == 240)? 'true': 'false'; ?>"
                   },
                 <?php } ?>
           ],
           image:"<?php echo $response->vf->video->med_image; ?>",
           mediaid:<?php echo $response->vf->video->id; ?>,

           width: "100%",
           aspectratio: "16:9",
           controls: true,
           autostart: true,
          //  preload : 'none',
           title: "<?php echo $response->vf->video->title; ?>",
           ga: {
             label: "title"
           }
           }).onPlay(function() {
             /*
             if (!view_confirmed){
               jQuery.ajax({
                 dataType: "json",
                 url: "<?php //echo $confirm_view; ?>",
                 data: {},
                 success: function(response) {

                 }
               });
             }
             */
           });
           playerInstance.on('time',function(){
             /*
             if (jwplayer().getPosition()>10) {
               jwplayer().stop(true);
               $("#please_subscribe").removeClass('hide');
               $("html, body").animate({
                 scrollTop: 0
               }, 600);
             }
             */
           });
           </script>


           <!-- Video Info End -->
         </div>
  </div>
  <div class="container">
    <div class="col-lg-12 crk-trending-videos-container">
	      <h1>Related Videos</h1>
	      <div class="cs-seprator">
	        <div class="devider1"></div>
	      </div>
	      <div class="col-md-12" style="padding-left:0; padding-right:0;">

          <?php foreach($response->related_videos as $key=>$video): ?>

            <div class="col-md-3 col-sm-4 col-xs-12 " href="">

              <a href="<?php echo base_url()."highlights/".$video->id."/".seo_url($video->title)."/"; ?>">
          					<div class="video-container">
          						<div class="video-thumbnail">
          							<img src="<?php echo $video->med_image; ?>" class="img-home-video img-responsive" style="width:100%;">
          							<img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">
          							<div class="overlay">
          							</div>
          							<span class="video-view-count"> <?php echo $video->views; ?> views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
          						</div>
          						<div class="video-content">
          							<p class="video-description crop_to_one_line"><?php echo $video->title; ?></p>
          						</div>
          					</div>
              </a>
    				</div>

          <?php endforeach; ?>



      </div>
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    </div>

  </div><!-- container -->
</div>

<!-- jQuery -->
<script src="<?php echo assets_url();  ?>home/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo assets_url();  ?>home/js/bootstrap.min.js"></script>
<script src="<?php echo assets_url();  ?>home/js/jquery.mobile.custom.js"></script>

<script src="<?php echo assets_url();  ?>home/js/owl.carousel.min.js"></script>


</body>

</html>
