<div id="c-calend" class="top-score-title right-score col-md-9">
	<!--TABS START-->
	<div class="tabs animated-slide-2 matches-tbs">
		<div class="row">
			<div class="col-md-12 ">
				<div class="cs-section-title">
					<h2><?php echo $page_heading; ?></h2>
				</div>
			</div>
		</div>
		<div class="tabs animated-slide-2 matches-tbs">
			<div class="tab-content">
				<div id="results" class="tab active">
					<div class="event event-listing event-samll">
						<div id="results-container">
							<?php foreach($results->matches as $result){ ?>
								<div class="col-md-12 results-block" onclick="window.location='<?php echo $result->seo_url; ?>'">
									<div class="row">
										<div class="col-md-2 col-xs-3">
											<div class="row">
												<div class="time"><?php echo $result->local_time->date; ?><br><span><?php echo $result->local_time->month; ?></span></div>
											</div>
										</div>
										<div class="col-md-10 col-xs-9">
											<article class="cs-upcomming-fixtures">
												<div class="calendar-date">
													<figure><a href="<?php echo $result->seo_url; ?>"><img src="<?php echo $result->team_1->team->flag_url ?>" alt="" class="result-img"></a></figure>
												</div>
												<div class="text">
													<h2>
														<a class="hidden-xs" href="<?php echo $result->seo_url; ?>"><?php echo $result->team_1->team->name; ?></a>
														<span>VS</span>
														<a class="hidden-xs" href="<?php echo $result->seo_url; ?>"><?php echo $result->team_2->team->name; ?></a>
													</h2>
													<ul class="post-option mt0 hidden-xs">
														<li><?php echo $result->match_result; ?></li>
													</ul>
												</div>
												<div class="match-result">
													<figure><a href="<?php echo $result->seo_url; ?>"><img src="<?php echo $result->team_2->team->flag_url ?>" alt="" class="result-img"></a></figure>
												</div>
											</article>
											<ul class="post-option mt0 visible-xs"><li><?php echo $result->match_result; ?></li></ul>
										</div>
									</div>
								</div>
							<?php } ?>
								
						</div>
					<div class="bottom-event-panel">
						<a href="javascript:;" id="more-results-btn" data-page-to-load="2"> More Results</a> </div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!--TABS END-->
</div>