<!DOCTYPE html>
<html lang="en-US" >
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
<meta content="text/html;" http-equiv="Content-Type" />
<link rel="shortcut icon" href="<?php echo assets_url(); ?>images/fav/ico-16.png">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-57.png" sizes="57x57">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-72.png" sizes="72x72">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-114.png" sizes="114x114">
<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-144.png" sizes="144x144">
<meta charset="UTF-8">
<meta name="keywords" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta property="og:site_name" 		   content="CricBoom"/>
<meta property="fb:app_id" 			   content="430752913746475" />
<?php if (!empty($video_page)){ ?>
	<title><?php echo $current_video->social->title." - Ufone Cricket"; ?></title>
	<meta property="og:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta property="og:title" content="<?php echo $current_video->social->title." - Ufone Cricket"; ?>" />
	<meta property="og:description" content="<?php echo $current_video->social->description; ?>" />
	<meta property="og:image" content="<?php echo $current_video->social->thumb_file; ?>" />

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta name="twitter:title" content="<?php echo $current_video->social->title." - Ufone Cricket"; ?>">
	<meta name="twitter:description" content="<?php echo $current_video->social->description; ?>">
	<meta name="twitter:image" content="<?php echo $current_video->social->thumb_file; ?>">

<?php } else { ?>
	<title><?php echo $og_tags->title; ?></title>
	<meta property="og:url"  content="<?php echo @$og_tags->link; ?>" />
	<meta property="og:title" content="<?php echo $og_tags->title; ?>" />
	<meta property="og:description" content="<?php echo $og_tags->descr; ?>" />
	<meta property="og:image" content="<?php echo $og_tags->image; ?>" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo @$og_tags->link; ?>" />
	<meta name="twitter:title" content="<?php echo $og_tags->title; ?>">
	<meta name="twitter:description" content="<?php echo $og_tags->descr; ?>">
	<meta name="twitter:image" content="<?php echo $og_tags->image; ?>">

<?php } ?>

<style type="text/css">
	img.wp-smiley, img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
</style>
<link href="<?php echo assets_url();  ?>home/css/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' id='contact-form-7-css'  href="<?php echo assets_url(); ?>css/styles.css?v=<?php echo VERSION; ?>" type='text/css' media='all' />

<!-- <link rel='stylesheet' id='contact-form-7-css'  href="<?php //echo assets_url(); ?>css/style.css?v=<?php //echo VERSION; ?>" type='text/css' media='all' /> -->

<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<link rel='stylesheet' id='font-awesome-css'  href="<?php echo assets_url(); ?>css/font-awesome.min.css?92bc46" type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href="<?php echo assets_url(); ?>css/magnific-popup.css?92bc46" type='text/css' media='all' />

<?php if(isset($login_page)): ?>
	<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>home/css/half-slider.css?v=<?php echo VERSION; ?>" />
<?php endif; ?>

<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>home/css/custom.css?v=<?php echo VERSION; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/scorecard.css?v=<?php echo VERSION; ?>" />
<script type='text/javascript' src="<?php echo assets_url(); ?>js/js/jquery-1.10.2.js" ></script>
<script type='text/javascript' src='<?php echo assets_url(); ?>js/jquery-migrate.min.js'></script>

<!-- <link href="<?php //echo assets_url(); ?>css/cric_css/style_dir.css?v=<?php //echo VERSION; ?>" rel="stylesheet" type="text/css" /> -->

<link href="<?php echo assets_url(); ?>css/cric_css/owl.carousel.css" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/owl.theme.css" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/component.css" rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url(); ?>css/cric_css/widget.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<?php if (empty($remove_responsive)){ ?>
<!-- <link href="<?php //echo assets_url(); ?>css/cric_css/responsive.css?v=<?php //echo VERSION; ?>" rel="stylesheet" type="text/css" /> -->
<?php } ?>


<!-- <link href="<?php //echo assets_url(); ?>css/cric_css/style_dir_asim.css?v=<?php //echo VERSION; ?>" rel="stylesheet" type="text/css" /> -->


<?php if (isset($timeline_page)): ?>
	<link href="<?php echo assets_url(); ?>css/style.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo assets_url(); ?>css/custom.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo assets_url(); ?>css/cric_css/style_dir.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo assets_url(); ?>css/cric_css/style_dir_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo assets_url(); ?>css/cric_css/style.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<?php endif; ?>

<?php if (isset($psl)): ?>
	<link href="<?php echo assets_url(); ?>css/cric_css/style.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<?php endif ?>

<link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/custom-front.css?v=<?php echo VERSION; ?>" />

<link href="<?php echo assets_url(); ?>css/cric_css/style_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo assets_url(); ?>css/layout.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

<?php if (!isset($timeline_page)): ?>
	<link href="<?php echo assets_url(); ?>css/bootstrap-select.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<?php endif; ?>

<link href="<?php echo assets_url(); ?>css/dev.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<script>jwplayer.key="2HZsFtRYlzQZJpY2SmrCGnAe6fBk5mGtGN8MgA==";</script>
<?php
$language = $this->input->get("lang");
if (!$language){
	$language = $this->xession->get("language");
}
if ($language=='urdu' && $this->uri->segment(1)==login){
	?>
	<link rel='stylesheet' href='<?php echo assets_url(); ?>css/style_ur.css?v=<?php echo time(); ?>' type='text/css' media='all' />
	<?php
}
?>


<?php if (@$new_video_page): ?>
	<link href="<?php echo assets_url(); ?>css/rev-settings.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo assets_url(); ?>css/rev-style.css" rel="stylesheet" type="text/css" />
<?php endif ?>


<style id='theme-style-inline-css' type='text/css'>
@media only screen and (max-width: 768px) {
.login.btn-default {
    padding: 5px 10px !important;
}

	.jw-icon-next, .jw-icon-playlist, .jw-icon-prev{
	    display: none !important;
	}
</style>
<?php if (ENVIRONMENT=='production'){ ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-88034149-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-88034149-3');
</script>

<!-- Google Analytics Code -->
<script>
  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  // })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	//
  // ga('create', 'UA-88034149-1', 'auto');
  // ga('send', 'pageview');

</script>

<!-- Google Analytics Code -->
<?php } ?>
</head>
<body class=" <?php echo (isset($psl))?'pls-event' :'' ; ?>" <?php echo !empty($add_video_id_to_body)?'id="videos_page"':''; ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=430752913746475";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

		<?php $this->load->view("jan18/navigation"); ?>


		<?php if(!isset($login_page)): ?>
			<div class="<?php echo(isset($new_video_page))? 'wrapper wrapper-main': 'wrapper-main hide-sm-screen'; ?>">
		<?php endif; ?>

			<?php echo $page; ?>

		<?php if(!isset($login_page)): ?>
			</div>
		<?php endif; ?>
		<?php $this->load->view("footer"); ?>


<script>
	$(document).ready(function(){
		resizeDiv();
		});

		window.onresize = function(event) {
		resizeDiv();
		}

		function resizeDiv() {
		vpw = $(window).width();
		vph = $(window).height();
			vph= vph - 202;
		$('#login-height').css({'min-height': vph + 'px'});
}
	</script>

	<script type='text/javascript' src="<?php echo assets_url(); ?>js/functions.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/jquery.magnific-popup.min.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/bootstrap.min.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/bootstrap-select.js"></script>
	<script type='text/javascript' src="<?php echo assets_url(); ?>js/js/own/owl.carousel.js" ></script>

	<?php if ($new_video_page): ?>
		 <!-- JS Revolution Slider -->
		  <script src="<?php echo assets_url(); ?>js/revolution/jquery.themepunch.tools.min.js"></script>
		  <script src="<?php echo assets_url(); ?>js/revolution/jquery.themepunch.revolution.min.js"></script>

		  <script src="<?php echo assets_url(); ?>js/revolution/extensions/revolution.extension.actions.min.js"></script>
		  <script src="<?php echo assets_url(); ?>js/revolution/extensions/revolution.extension.carousel.min.js"></script>
		  <script src="<?php echo assets_url(); ?>js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
		  <script src="<?php echo assets_url(); ?>js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
		  <script src="<?php echo assets_url(); ?>js/revolution/extensions/revolution.extension.migration.min.js"></script>
		  <script src="<?php echo assets_url(); ?>/home/js/owl.carousel.min.js"></script>
		 <!-- end JS Revolution Slider -->
	<?php endif ?>

	<script>window.twttr = (function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0],
	    t = window.twttr || {};
	  if (d.getElementById(id)) return t;
	  js = d.createElement(s);
	  js.id = id;
	  js.src = "https://platform.twitter.com/widgets.js";
	  fjs.parentNode.insertBefore(js, fjs);

	  t._e = [];
	  t.ready = function(f) {
	    t._e.push(f);
	  };

	  return t;
	}(document, "script", "twitter-wjs"));</script>

<script>

var base_url = "<?php echo base_url(); ?>";
$( document ).ready(function(){

	$("body").on("click","#loadMoreVidosList", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).button("loading");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_videos/'+page_to_load,
			data: [],
			success: function(response) {
				response.timeline_videos.forEach(function(video) {

					html = '<div class="col-xs-6 col-sm-4 col-md-4 pt20"><a href="'+video.seo_url+'" >';
					html = html + '<img src="'+video.med_image+'" alt=""></a>';
					html = html + '<div class="cric-lockup-content"><h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom">'+video.title+'</h3>';
					html = html + '<div class="cric-lockup-meta">';
					html = html + '<ul class="cric-lockup-meta-info pl5 mt0 pb5">';
					html = html + '<li>'+video.views+' views</li>';
					html = html + '<li class="hidden">5 days ago</li>';
					html = html + '</ul></div></div></div>';

					$("#videoListContainter").append(html);
				});

				if (response.current_page==response.total_pages){
					btnRef.hide();
					btnRef.button("reset");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.button("reset");
				}

			}
		});

		return false;
	});

	$("body").on("click",".btn-more-video-filter", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		var containerId = $(this).attr("data-container-id");
		var filter = $(this).attr("data-filter");
		$(this).button("loading");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_filter_videos/'+filter+'/'+page_to_load,
			data: [],
			success: function(response) {
				response.timeline_videos.forEach(function(item) {

					html = '<div class="col-xs-6 col-sm-4 col-md-3 pt20">';
					html = html + '<a href="'+item.seo_url+'"><img src="'+item.med_image+'" alt=""></a>';
					html = html + '<div class="cric-lockup-content">';
					html = html + '<h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom">'+item.title+'</h3>';
					html = html + '<div class="cric-lockup-meta">';
					html = html + '<ul class="cric-lockup-meta-info pl5 mt0 pb5">';
					html = html + '<li>'+item.views+' views</li>';
					html = html + '<li class="hidden">5 days ago</li>';
					html = html + '</ul>';
					html = html + '</div>';
					html = html + '</div>';
					html = html + '</div>';

					$("#"+containerId).append(html);
				});

				if (response.current_page==response.total_pages){
					btnRef.hide();
					btnRef.button("reset");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.button("reset");
				}

			}
		});

		return false;
	});

	$("body").on("click","#loadMoreVidosRelatedTimeline", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		var video_id = $(this).attr("data-video-id");
		$(this).button("loading");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_timeline_related_videos/'+video_id+'/'+page_to_load,
			data: [],
			success: function(response) {
				response.related_videos.forEach(function(video) {

					html = '<div class="col-xs-6 col-sm-4 col-md-4 pt20"><a href="'+video.seo_url+'" >';
					html = html + '<img src="'+video.med_image+'" alt=""></a>';
					html = html + '<div class="cric-lockup-content"><h3 class="cric-lockup-title mt0 mb0 pl5 thumb-title-custom">'+video.title+'</h3>';
					html = html + '<div class="cric-lockup-meta">';
					html = html + '<ul class="cric-lockup-meta-info pl5 mt0 pb5">';
					html = html + '<li>'+video.views+' views</li>';
					html = html + '<li class="hidden">5 days ago</li>';
					html = html + '</ul></div></div></div>';

					$("#videoListContainter").append(html);
				});

				if (response.current_page==response.total_pages){
					btnRef.hide();
					btnRef.button("reset");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.button("reset");
				}

			}
		});

		return false;
	});

	$("body").on("click","#more-results-btn", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'results/more_results/'+page_to_load,
			data: [],
			success: function(response) {
				response.matches.forEach(function(fixture) {
					var flag1 = "<?php echo assets_url(); ?>images/flag-placeholder.jpg";
					var flag2 = "<?php echo assets_url(); ?>images/flag-placeholder.jpg";
						if( fixture.team_1.team.flag_url && fixture.team_1.team.flag_url.indexOf('missing.png') == -1  && fixture.team_1.team.flag_url.indexOf('placeholder.jpg') == -1){
							flag1 = fixture.team_1.team.flag_url;
						}
					var flag2 = "<?php echo assets_url(); ?>images/flag-placeholder.jpg";
						if( fixture.team_2.team.flag_url && fixture.team_2.team.flag_url.indexOf('missing.png') == -1  && fixture.team_2.team.flag_url.indexOf('placeholder.jpg') == -1){
							flag2 = fixture.team_2.team.flag_url;
						}

						var fixture_title = '';

						if(fixture.title && fixture.title.toLowerCase().replace(' ','') !== 'missingtitle' ){
	        		fixture_title += fixture.title;
	        	}

						if( (fixture.title && fixture.title.toLowerCase().replace(' ','') !== 'missingtitle') && (fixture.series.short_name && fixture.series.short_name.toLowerCase().replace(' ','') !== 'missingtitle') ){
	        		fixture_title += ': ';
	        	}
						if(fixture.series.short_name && fixture.series.short_name.toLowerCase().replace(' ','') !== 'missingtitle' ){
	        		fixture_title += fixture.series.short_name;
	        	}



                  	var team_1_a_score = '';
                  	var team_1_b_score = '';

                    var team_2_a_score = '';
                    var team_2_b_score = '';

                    var team_1_a_overs = '';
                    var team_1_b_overs = '';

                    var team_2_a_overs = '';
                    var team_2_b_overs = '';

                    var team_1_class = '';
                    var team_2_class = '';

                    var is_test = '';

                    if( fixture.format  &&  fixture.format.toLowerCase()  ==  'test' ){
                      is_test = 1;
                    }

                    if(fixture.match_won_by_id == fixture.team_1.id){
                      team_1_class = 'match-won';
                    }
                    if(fixture.match_won_by_id == fixture.team_2.id){
                      team_2_class = 'match-won';
                    }

                    var ic =0
                    fixture.innings.forEach(function(i){
                    	if(ic <= 1){
	                        if(i.batting_team_id == fixture.team_1.id){

	                          team_1_a_score = i.runs+"/"+i.wickets;

	                          team_1_a_overs = "("+i.overs+" overs)";

	                        }else{

	                            team_2_a_score = i.runs+"/"+i.wickets;
	                            team_2_a_overs = "("+i.overs+" overs)";
	                        }

	                      }else{

	                        if(i.batting_team_id == fixture.team_1.id){

	                          team_1_b_score = i.runs+"/"+i.wickets;
	                          team_1_b_overs = "("+i.overs+" overs)";

	                        }else{

	                            team_2_b_score = i.runs+"/"+i.wickets;
	                            team_2_b_overs = "("+i.overs+" overs)";
	                        }

	                      }

                    	ic++;
                    });




                  	var html = '';

                  	html += '<div class="col-md-12 col-xs-12 event-fixture-box">';
                  		html += '<div class="">';
                  			html += '<div class="col-xs-12 visible-xs  pb20">';
                  				html += '<h2 class="fw-normal text-left  m0">';
                  					html += '<a class="color-grey-lighter f16" href="'+fixture.seo_url+'">';
              							html += fixture.local_time.date+' '+ fixture.local_time.month;
              						html += '</a>';
              					html += '</h2>';
              					html += '<h4 class="m0 lh28 fw-normal text-left"> ';
              						html += '<a class="color-grey-light mt-5" href="'+fixture.seo_url+'">';
              							html += fixture_title;
              						html += '</a>';
          						html += '</h4>';
          					html += '</div>';
                          	html += '<div class="col-md-2 col-sm-3 col-xs-6 ">';
                          		html += '<div class="row">';
                          			html += '<div class="team-first">';
                          				html += '<ul style="text-align: center;">';
                      	    				html += '<li>';
                      							html += '<a href="'+fixture.seo_url+'"> ';
                          							html += '<img src="'+flag1+'" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">';
                          						html += '</a>';
                      						html += '</li>';
                          						html += '';
                      						html += '<li>';
                      							html += '<h4 class="color-jazz-grey-darker mb0 one-liner2 '+team_1_class+'">'+fixture.team_1.team.short_name+'</h4>';
                      						html += '</li>';

                      						if (is_test){

                      							html += '<li>';
                      								html += '<h4 class="color-jazz-grey-light mt0 mb0">';
                      									if(!team_1_b_score){
                      										html += '<span class="pr5 '+team_1_class+'">';
                  										}else{
                  											html += '<span class="pr5">';
                  										}
                      											html += team_1_a_score;
                  											html += '</span>';

                  											if(team_1_b_score){
              													html += '<span class="color-jazz-grey-darker f12">&amp;</span>';
              													html += '<span class="pl5 '+team_1_class+'">';
              														html += team_1_b_score;
          														html += '</span>';
                  											}

              											html += '</h4>';


                  								html += '</li>';

                  								html += '<li>';
                  									html += '<span class="f12 pr10">'+team_1_a_overs+'</span>';
          											html += '<span class="f12">'+team_1_b_overs+'</span>';
                  								html += '</li>';

                      						}else{
                      							//not test
                      							html += '<li>';
				                                  html += '<h4 class="color-jazz-grey-darker mt0 mb0 '+team_1_class+'">'+ team_1_a_score+'</h4>';
				                                html += '</li>';

				                                html += '<li>';
				                                  html += '<span class="f12">'+team_1_a_overs+'</span>';
				                                html += '</li>';
                      						}







                  						html += '</ul>';
              						html += '</div>';
                  				html += '</div>';
              				html += '</div>';



              				html += '<div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">';
              					html += '<article class="pb0">';
		              				html += '<div class="text-center">';
			              				html += '<h2 class="fw-normal ">';
              								html += '<a class="color-grey-lighter" href="'+fixture.seo_url+'">'+fixture.local_time.date+' '+ fixture.local_time.month+'</a>';
              							html += '</h2>';
              							html += '<h4 class="m0 lh28 fw-normal "> ';
              								html += '<a class="color-grey-light" href="'+fixture.seo_url+'">';
              									html += fixture_title;
          									html += '</a>';
              							html += '</h4>';
              							html += '<h1 class="post-option mt0 pb0">';
              								html += '<span class="color-grey f18">'+fixture.match_result+'</span>';
              							html += '</h1>';
          							html += '</div>';
      							html += '</article>';
  							html += '</div>';


  							html += '<div class="col-md-2 col-sm-3 col-xs-6 ">';
  								html += '<div class="row">';
  									html += '<div class="team-second">';
  										html += '<ul style="text-align: center;">';
  											html += '<li>';
  												html += '<a href="'+fixture.seo_url+'"> ';
  													html += '<img src="'+flag2+'" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">';
					  							html += '</a>';
				  							html += '</li>';
				  							html += '<li>';
					  							html += '<h4 class="color-jazz-grey-darker mb0 one-liner2 '+team_2_class+'">'+fixture.team_2.team.short_name+'</h4>';
			  							html += '</li>';


			  							if (is_test){

                      							html += '<li>';
                      								html += '<h4 class="color-jazz-grey-light mt0 mb0">';
                      									if(!team_2_b_score){
                      										html += '<span class="pr5 '+team_2_class+'">';
                  										}else{
                  											html += '<span class="pr5">';
                  										}
                      											html += team_2_a_score;
                  											html += '</span>';

                  											if(team_2_b_score){
              													html += '<span class="color-jazz-grey-darker f12">&amp;</span>';
              													html += '<span class="pl5 '+team_2_class+'">';
              														html += team_2_b_score;
          														html += '</span>';
                  											}

              											html += '</h4>';


                  								html += '</li>';

                  								html += '<li>';
                  									html += '<span class="f12 pr10">'+team_2_a_overs+'</span>';
          											html += '<span class="f12">'+team_2_b_overs+'</span>';
                  								html += '</li>';

                      						}else{
                      							//not test
                      							html += '<li>';
				                                  html += '<h4 class="color-jazz-grey-darker mt0 mb0 '+team_2_class+'">'+ team_2_a_score+'</h4>';
				                                html += '</li>';

				                                html += '<li>';
				                                  html += '<span class="f12">'+team_2_a_overs+'</span>';
				                                html += '</li>';
                      						}




		  							html += '</ul>';
	  							html += '</div>';
  							html += '</div>';
						html += '</div>';

						html += '<div class="col-md-8 col-xs-12 visible-xs">';
							html += '<article class="mt20">';
								html += '<div class="text-center">';
									html += '<h1 class="post-option mt0">';
										html += '<span class="color-grey f16">'+fixture.match_result+'</span>';
									html += '</h1>';
								html += '</div>';
							html += '</article>';
						html += '</div>';

					html += '</div>';
				html += '</div>';
				$("#fixtures-container").append(html);

				});

				if (response.page_no==response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Results");
				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Results");
				}

			}
		});

		return false;
	});

	$("body").on("click","#more-fixtures-btn", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'fixtures/more_fixtures/'+page_to_load,
			data: [],
			success: function(response) {
				response.matches.forEach(function(fixture) {
					var flag1 = "<?php echo assets_url(); ?>images/flag-placeholder.jpg";
					var flag2 = "<?php echo assets_url(); ?>images/flag-placeholder.jpg";
						if( fixture.team_1.team.flag_url && fixture.team_1.team.flag_url.indexOf('missing.png') == -1 && fixture.team_1.team.flag_url.indexOf('placeholder.jpg') == -1){
							flag1 = fixture.team_1.team.flag_url;
						}
					var flag2 = "<?php echo assets_url(); ?>images/flag-placeholder.jpg";
						if( fixture.team_2.team.flag_url && fixture.team_2.team.flag_url.indexOf('missing.png') == -1 && fixture.team_2.team.flag_url.indexOf('placeholder.jpg') == -1){
							flag2 = fixture.team_2.team.flag_url;
						}


					var fixture_title = '';

					if(fixture.title && fixture.title.toLowerCase().replace(' ','') !== 'missingtitle' ){
						fixture_title += fixture.title;
					}

					if( (fixture.title && fixture.title.toLowerCase().replace(' ','') !== 'missingtitle') && (fixture.series.short_name && fixture.series.short_name.toLowerCase().replace(' ','') !== 'missingtitle') ){
						fixture_title += ': ';
					}
					if(fixture.series.short_name && fixture.series.short_name.toLowerCase().replace(' ','') !== 'missingtitle' ){
						fixture_title += fixture.series.short_name;
					}

										if(fixture.venue && fixture.venue.stadium_name && fixture.venue.title){
											fixture_venue = 'at '+fixture.venue.stadium_name+', '+fixture.venue.title;
										}else{
											fixture_venue = '';
										}


										var html = '';

										html += '<div class="col-md-12 col-xs-12 event-fixture-box">';
											html += '<div class="">';
												html += '<div class="col-xs-12 visible-xs  pb20">';
													html += '<h2 class="fw-normal text-left  m0">';
														html += '<a class="color-grey-lighter f16" href="javascript:;">';
														html += fixture.local_time.date+' '+ fixture.local_time.month+' | '+ fixture.local_time.time;
													html += '</a>';
												html += '</h2>';
												html += '<h4 class="m0 lh28 fw-normal text-left"> ';
													html += '<a class="color-grey-light mt-5" href="javascript:;">';
														html += fixture_title;
													html += '</a>';
											html += '</h4>';
										html += '</div>';
														html += '<div class="col-md-2 col-sm-3 col-xs-6 ">';
															html += '<div class="row">';
																html += '<div class="team-first">';
																	html += '<ul style="text-align: center;">';
																		html += '<li>';
																		html += '<a href="javascript:;"> ';
																				html += '<img src="'+flag1+'" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">';
																			html += '</a>';
																	html += '</li>';
																			html += '';
																	html += '<li>';
																		html += '<h4 class="color-jazz-grey-darker mb0 one-liner2">'+fixture.team_1.team.short_name+'</h4>';
																	html += '</li>';
															html += '</ul>';
													html += '</div>';
													html += '</div>';
											html += '</div>';



											html += '<div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">';
												html += '<article class="pb0">';
													html += '<div class="text-center">';
														html += '<h2 class="fw-normal ">';
															html += '<a class="color-grey-lighter" href="javascript:;">'+fixture.local_time.date+' '+ fixture.local_time.month+' | '+ fixture.local_time.time+'</a>';
														html += '</h2>';
														html += '<h4 class="m0 lh28 fw-normal "> ';
															html += '<a class="color-grey-light" href="javascript:;">';
																html += fixture_title;
														html += '</a>';
														html += '</h4>';
														html += '<h1 class="post-option mt0 pb0">';
															html += '<span class="color-grey f18">'+fixture_venue+'</span>';
														html += '</h1>';
												html += '</div>';
										html += '</article>';
								html += '</div>';


								html += '<div class="col-md-2 col-sm-3 col-xs-6 ">';
									html += '<div class="row">';
										html += '<div class="team-second">';
											html += '<ul style="text-align: center;">';
												html += '<li>';
													html += '<a href="javascript:;"> ';
														html += '<img src="'+flag2+'" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">';
													html += '</a>';
												html += '</li>';
												html += '<li>';
													html += '<h4 class="color-jazz-grey-darker mb0 one-liner2">'+fixture.team_2.team.short_name+'</h4>';
											html += '</li>';
										html += '</ul>';
									html += '</div>';
								html += '</div>';
						html += '</div>';

						html += '<div class="col-md-8 col-xs-12 visible-xs">';
							html += '<article class="mt20">';
								html += '<div class="text-center">';
									html += '<h1 class="post-option mt0">';
										html += '<span class="color-grey f16">'+fixture_venue+'</span>';
									html += '</h1>';
								html += '</div>';
							html += '</article>';
						html += '</div>';

					html += '</div>';
				html += '</div>';


					$("#fixtures-container").append(html);

				});

				if (response.page_number>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Fixtures");

				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Fixtures");
				}
			}
		});

		return false;
	});

	$("body").on('click', '#more-rv-btn', function(){
  		var page_to_load = $(this).attr("data-page-to-load");
  		var video_id = $(this).data('video-id');
  		var series_id = $(this).data('series-id');

  		$(this).find('h5').html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'video/more_related_videos/'+video_id+'/'+series_id+'/'+page_to_load,
			data: [],
			success: function(response) {

				response.videos.forEach(function(video) {
					var html = '';
					if(video.video.id !== video_id){
						html += '<li>';

							html += '<figure>';
								html += '<a href="'+video.seo_url+'">';
									html += '<img src="'+video.video.thumb_image+'" alt="" class="img-responsive">';
								html += '</a>';
							html += '</figure>';

							html += '<div class="infotext"> ';
								html += '<a href="'+video.seo_url+'" title="'+video.video.title+'">';
									if(video.video.match_obj.title){
										html += video.video.match_obj.title+' - ';
									}
									html += video.video.match_obj.team_1+' vs '+video.video.match_obj.team_2+' - ';
									html += video.video.title;
								html += '</a>';
								html += '<p>'+video.video.views+' views</p>';
							html += '</div>';
						html += '</li>';


					}
					$("#rv-ul").append(html);
				});

				if (response.current_page>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.find('h5').html("Load More");

				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.find('h5').html("Load More");
				}

			}
		});

		return false;
  	});

	$("body").on("click","#more-blogs-btn", function(){
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'blog/more_blog/'+page_to_load,
			data: [],
			success: function(response) {
				response.articles.forEach(function(article) {

					var html = '<div class="col-md-12 pt20 pl0 pr0">';
					html = html + '<div class="cs-blog blog-lrg mb20"><div class="cs-media">';
					html = html + '<figure><a href="'+article.seo_url+'"><img src="'+article.med_image+'" alt=""></a></figure>';
					html = html + '</div><section><div class="title">';
					html = html + '<h2 class="mt0"><a href="'+article.seo_url+'">'+article.title+'</a></h2>';
					html = html + '<ul class="post-option pl0"><li class="time-xs"><time>'+article.created_at+'</time></li></ul>';
					html = html + '</div><div class="blog-text">';
					html = html + '<p class="mt0">'+article.details+'</p>';
					html = html + '<div class="cs-seprator"><div class="devider1"></div></div>';
					html = html + '<ul class="post-option pl0">';
					html = html + '<li class="time-xs"><i class="fa fa-user"></i>By<a href="javascript:;"> '+article.author+'</a></li>';
					html = html + '</ul>';
					html = html + '<a href="'+article.seo_url+'" class="btnshare addthis_button_compact at300m"><i class="fa fa-share-square-o"></i></a>';
					html = html + '</div></section></div></div>';

					$("#results-container").append(html);

				});

				if (response.current_page>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Articles");

				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Articles");
				}

			}
		});

		return false;
	});

	$("body").on("click",".social-icons-timeline a", function (e){
		e.preventDefault();
		var url = jQuery(this).prop("href");
		var popup = window.open(url, "_blank", "scrollbars=1,resizable=1,height=300,width=450");
		return false;
	});

	$("body").on("click",".user, .user1", function (e){
		//e.preventDefault();
		var phone = $(this).attr("phone");
		var sub_status = $(this).attr("sub_status");
		var msg = "Welcome <b>"+phone+"</b><BR>";
		msg = msg + "Subscription Status: ";
		if (sub_status==1){
			msg = msg + "Active";
		} else if (sub_status==3){
			msg = msg + "Suspended";
		} else {
			msg = msg + "Trial";
		}
		console.log(sub_status);
		$('.alert-popup .modal-body').html(msg);
		$('.alert-popup').modal('show');
	});

	$("body").on("click",".social_sharing_link", function (e){
		e.preventDefault();
		var url = jQuery(this).prop("href");
		var popup = window.open(url, "_blank", "scrollbars=1,resizable=1,height=300,width=450");
		return false;
	});


	$("body").on("submit","#login-form", function(){
		$("#login-form #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/check_login/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					//alert(response.message);
					window.location = response.url;
				} else {
					alert(response.message);
					if (response.clear_phone){
						$("#phone").val("");
					}
					$("#password").val("");
					$("#login-form #btnSubmit").button("reset");
				}

			}
		});

		return false;
	});

	$("body").on("submit","#phone-form", function(e){
		$('#phone-form .message').hide();
		$("#phone-form #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/validate_phone_number/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					window.location='<?php echo base_url()."login/step2"; ?>';
				} else {
					/*$('.alert-popup .modal-body').html(response.message);
					$('.alert-popup').modal('show');*/

					$('#phone-form .message').html(response.message);
					$('#phone-form .message').show();

					$("#phone-form #btnSubmit").button("reset");
				}
			}
		});
		return false;
	});

	/*
	$("body").on("submit","#register-phone", function(){
		$("#register-phone #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/register_mobile/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					alert(response.message);
					window.location = base_url+"login";
				} else {
					alert(response.message);
					$("#register-phone #btnSubmit").button("reset");
				}

			}
		});
		return false;
	});
	*/
	$("body").on("submit","#regsiter-confirm-pin", function(){
		$("#regsiter-confirm-pin #btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/confirm_pin_register/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					window.location = response.url;
				} else {
					alert(response.message);
					$("#register-phone #btnSubmit").button("reset");
				}

			}
		});
		return false;
	});

	$("body").on("submit","#sub-now", function(){
		$("#btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'home/sub/',
			data: [],
			success: function(response) {
				if(response.user.subscribe_status==1){
					window.location = base_url;
				} else if(response.user.subscribe_status==2){

					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");

				} else if(response.user.subscribe_status==3){

					alert("<?php echo lang("low_balance"); ?>");
					$("#btnSubmit").button("reset");
				} else {
					alert("<?php echo lang("general_error"); ?>");
					$("#btnSubmit").button("reset");
				}
			}
		});
		return false;
	});

	$("body").on("submit","#forget-pass", function(){
		$("#btnSubmit").button("loading");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'login/forget_password/',
			data: $(this).serializeArray(),
			success: function(response) {
				if (response.status==1){
					alert(response.message);
					window.location = base_url+"login";
				} else {
					alert(response.message);
					$("#btnSubmit").button("reset");
				}

			}
		});
		return false;
	});

	$("body").on('click', '#more-news-btn', function(e){
		e.preventDefault();
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		var prefix = "<?php echo BACKEND.'../' ?>";
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'more_news/'+page_to_load,
			data: [],
			success: function(response) {
				if(response.data.length > 0){
					response.news.forEach(function(news) {
						var html = '';
								html += '<div class="col-xs-12 col-md-12 col-sm-12" onclick="window.location=\''+news.seo_url+'\' ">';
									html += '<div class="news-page">';

										html+= '<div class="col-md-3 col-sm-3 col-xs-12"> ';
											html+= '<img src="'+news.full_image+'" alt="" class="img-djoko img-home-item img-responsive"> ';
										html+= '</div>';


										html+= '<div class="col-md-9 col-sm-9 col-xs-12 data-news-pg">';
											html+= '<h4 class="pt0 mb0 mt0 crop_to_one_line">'+news.title+'</h4>';
											html+= '<p class="news-dd">'+news.created_at+'</p>';
											html+= '<div class="clear"> </div>';
												html+= '<p class="mt0"> ';
													html+= '<span class="hidden-xs">';
														html+= news.stripped_body;
													html+= '</span>';
													html+= '<span class="visible-xs">';
														html+= news.stripped_body_XS;
													html+= '</span>';
												html+= '</p>';
										html+= '</div>';

										html+= '<a href="'+news.seo_url+'" class="more-arrow">';
											html+= '<img src="<?php echo assets_url(); ?>images/arrow-right.png">';
										html+= '</a>';


									html += '</div>';
								html += '</div>';

								$('#results-container div.row-xs-10').append(html);
					});
				}


				if (response.page>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Articles");

				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More News");
				}

			}
		});

		return false;
	});

	$("body").on('click', '#more-articles-btn', function(e){
		e.preventDefault();
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'more_articles/'+page_to_load,
			data: [],
			success: function(response) {
				response.data.forEach(function(article) {
					var html = '';
							html += '<div class="col-md-6 col-sm-6 col-xs-12 " >';
								html += '<div class="video-container">';

									html += '<div class="video-thumbnail">';
										html += '<a href="'+article.seo_url+'">';
											html += '<img src="'+article.full_image+'" class="img-home-video img-responsive">';
										html += '</a>';
										html += '<div class="overlay"> </div>';
										//html += '<span class="video-view-count hidden">';
										//	html += '18 views';
										//html += '</span>';
										//html += '<span class="video-duration fa fa-clock-o hidden">';
										//	html += '05:56';
										//html += '</span>';
										html += '<figcaption>';
											html += '<h2 class="mb0 one-liner">'+article.title+'</h2>';
											html += '<p class="mb0 mt0">'+article.created_at;
												html += '<a href="" class="icon-share hidden">';
													html += '<img src="<?php echo assets_url(); ?>images/share-article.svg" alt="">';
												html += '</a>';
											html += '</p>';
										html += '</figcaption>';
									html += '</div>';

									html += '<div class="video-content">';
										html += '<p class="pt10 color-gray mb40">';
											html += article.details;
											html += '<a href="'+article.seo_url+'" class="more-arrow">';
												html += '<img src="<?php echo assets_url(); ?>images/arrow-right.png">';
											html += '</a>';
										html += '</p>';
									html += '</div>';


								html += '</div>';
							html += '</div>';


					$('#results-container  div.row-xs-10').append(html);
				});

				if (response.current_page>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Articles");

				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Articles");
				}

			}
		});

		return false;
	});



	$("body").on('click', '#more-series-btn', function(e){
		e.preventDefault();
		var page_to_load = $(this).attr("data-page-to-load");
		$(this).html("Please Wait...");
		var btnRef = $(this);
		var series_count = parseInt($('#more-series-btn').attr('data-series-count'));
		series_count++;

		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_url+'more_series/'+page_to_load,
			data: [],
			success: function(response) {
				response.series.forEach(function(series) {
					var series_name = series.title;
					if(series.short_name){
						series_name = series.short_name;
					}

					var html = '';




						html += '<h1>';
							html += '<span class="hidden-xs">'+series_name+'</span>';
							html += '<span class="f15 visible-xs">'+series_name+'</span> ';
							html += '<span class="f16 color-jazz-grey-light hidden-xs">';
								html += series.start_date_DM;
								html += '-';
								html += series.end_date_DM;
							html += '</span>';
						html += '</h1>';


						html += '<span class="f13 color-jazz-grey-light visible-xs">';
							html += series.start_date_DM;
							html += '-';
							html += series.end_date_DM;
						html += '</span>';



		html += '<div id="trending'+series_count+'" class="owl-carousel owl-theme" style="opacity: 1; display: block;">';


			var videos_count = 1;
			series.videos.forEach(function(video) {
				if(videos_count > 5 ){return;}
					html += '<div class="item" onclick="window.location=\''+video.seo_url+'\'">';
						html += '<div class="video-container">';
							html += '<div class="video-thumbnail">';
								html += '<img src="'+video.video.med_image+'" class="img-home-video img-responsive" style="opacity: 0;"> ';
								html += '<a href="'+video.seo_url+'">';
									html += '<img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">';
								html += '</a>';
								html += '<div class="overlay"> </div>';
								html += '<figcaption>';
									html += '<h2 class="mb0">';
										html += video.video.match_obj.title+' - '+video.video.match_obj.team_1+' vs '+video.video.match_obj.team_2;
									html += '</h2>';
								html += '</figcaption>';
							html += '</div>';


							html += '<div class="video-content">';
								html += '<p class="video-description one-liner" title="'+video.video.title+'">';
									html += video.video.title;
								html += '</p>';
							html += '</div>';


						html += '</div>';
					html += '</div>';
							html += '';

				videos_count++;
			});







		html += '</div>';
		html += '<div class=" clearfix" style=" margin-bottom:10px;"></div>';


						$("#series-container").append(html);
						series_count++;
						btnRef.attr("data-series-count",parseInt(series_count));
				});
				enableOwls();

				if (response.current_page>=response.total_pages){
					$(".bottom-event-panel").hide();
					btnRef.hide();
					btnRef.html("More Series");

				} else {
					btnRef.attr("data-page-to-load",parseInt(page_to_load)+1);
					btnRef.html("More Series");
				}

			}
		});

		return false;
	});

});

</script>

<script>
$('body').on('click','.playstore_link', function (e){


      sUsrAg = navigator.userAgent;
        if ((sUsrAg.indexOf("Android") > -1)) {
					if(typeof ga != "undefined" ){
						ga('send', 'event', 'APP Banner', 'Click', 'Android', 1);
					}
          window.open("market://details?id=com.Khaleef.cricwickSTC");
      } else {
				if(typeof ga != "undefined" ){
					ga('send', 'event', 'APP Banner', 'Click', 'Non-Android', 1);
				}
        win = window.open('https://play.google.com/store/apps/details?id=com.Khaleef.cricwickSTC', '_blank');
        win.focus();
      }
    });
</script>
<script type="text/javascript">
	function hasTouch() {
    return 'ontouchstart' in document.documentElement
           || navigator.maxTouchPoints > 0
           || navigator.msMaxTouchPoints > 0;
}

if (hasTouch()) { // remove all :hover stylesheets
    try { // prevent exception on browsers not supporting DOM styleSheets properly

        for (var si in document.styleSheets) {
            var styleSheet = document.styleSheets[si];
            if (!styleSheet.rules) continue;

            for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
                if (!styleSheet.rules[ri].selectorText) continue;

                if (styleSheet.rules[ri].selectorText.match(':hover')) {
                    styleSheet.deleteRule(ri);
                }
                if (styleSheet.rules[ri].selectorText.match(':focus')) {
                    styleSheet.deleteRule(ri);
                }
            }
        }
    } catch (ex) {}
}
</script>
</body>
</html>
