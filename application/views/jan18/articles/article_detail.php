<?php 
	$is_this_news = "";
		if(isset($article_type) && $article_type == 'news'){$is_this_news = 1;}	

	$v_image = ""; $v_detail = ""; $v_author=""; $v_date=""; $v_title="";


	if($is_this_news){
		$v_image =  $current_article->full_image;
		$v_detail = $current_article->body;
		// $v_author = $current_article->by; //no need to show author in news
		$v_date = date("j F Y - h:i:s A", strtotime($current_article->created_at));
		$v_title = "News";
	}else{
		$v_image =  $current_article->full_image;
		$v_detail = $current_article->body;
		$v_author = 'By '.$current_article->by;
		$v_date = date("j F Y - h:i:s A", strtotime($current_article->created_at));
		$v_title = "Articles";
	}
?>
<div id="c-calend" class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container article-detail">

  <div class="cs-seprator mt30 hidden-xs">
    <div class="devider1"> </div>
  </div>


  <div class="article-header">

  		<div class="article-title pull-left">
			<h1 class="hidden-xs"><?php echo $current_article->title; ?></h1>
			<h1 class="visible-xs f20"><?php echo $current_article->title; ?></h1>
	  	</div>	

	  	  <div class="article-sharing pull-right">
		  	<div class="socialmedia">
			    <ul class="pull-left">
			      <li>
			      	<a data-original-title="Facebook" class="addthis_button_facebook social_sharing_link new-look" href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u=<?php echo $current_article->seo_url; ?>&display=popup&ref=plugin&src=share_button">
			      		<i class="fa fa-facebook"></i></a></li>
			      <li>
			      	<a data-original-title="twitter" class="addthis_button_twitter social_sharing_link new-look" href="http://www.twitter.com/share?text=<?php echo $current_article->title; ?>&url=<?php echo $current_article->seo_url; ?>">
			      		<i class="fa fa-twitter"></i>
			      	</a>
			      </li>
			      <li>
			      	<a data-original-title="google-plus" class="addthis_button_google social_sharing_link new-look" href="https://plus.google.com/share?url=<?php echo $current_article->seo_url; ?>">
			      		<i class="fa fa-google-plus google"></i>
			      	</a>
			      </li>
			    </ul>
			    <div class="clearfix"></div>
			  </div> 
		  </div>

	  	<div class="clearfix"></div>
  </div>
  



	<div class="col-md-12 padding-md-0">
		<div class=" row-xs-10">
			<div class="col-md-12">
				<div class="post-option-panel">



			<img src="<?php echo $v_image; ?>" alt="" class="">

			<p class="desc_news important_news data">
				<span class="article-author pull-left">
					<?php echo $v_author; ?>
				</span>
				<span class='article-date pull-right'>
					<?php echo $v_date; ?>
				</span> 
				<span class="clearfix"></span>
			</p>

			<div class="article-body">
				<?php echo $v_detail; ?>
			</div>



				</div>
			</div>
		</div>
	</div>

	<div class="cs-seprator mt30">
	    <div class="devider1"> </div>
    </div>






	<h1><?php echo 'More '.$v_title; ?></h1>

	<?php if ($v_title == 'News'): ?>

		<!-- MORE NEWS  -->
		<div id="home-news" class="mt-xs-10 row">
		<?php foreach($more_articles as $key=>$n){
				if($key>3){
					break;
				}
				?>
				<?php if (($key)%2==0){ ?>
					<div class="col-md-12 <?php echo $key>0?'hidden-xs':''; ?>" style="" onclick="window.location='<?php echo $n->seo_url; ?>'">
				<?php } ?>
					<div class="col-xs-12 col-md-6 col-sm-6">
						<div class="news-page">
							<div class="col-md-6 col-sm-6 pl0 pr10 pb10 image-news-pg">
								<img src="<?php echo $n->full_image; ?>" alt="" class="img-djoko img-home-item" style="width:100%">
							</div>
							<div class="col-md-6 data-news-pg pl10 pr0">
								<h4 class="pt0 mb0 mt0 crop_to_one_line"><?php echo $n->title; ?></h4>
								<p class="news-dd hidden">
									<i>by <?php echo $n->by; ?></i>
								</p>
								<p class="news-dd"><?php echo date('F j, Y', strtotime($n->created_at)); ?></p>
								<div class="clear">
								</div>
								<p class="mt0">
									<span class="hidden-xs">
					                  <?php echo substr(strip_tags($n->body),0, 180); ?>...  
					                </span>
					                <span class="visible-xs">
					                  <?php echo substr(strip_tags($n->body),0, 230); ?>...
					                </span>
								</p>
							</div>

							<a href="<?php echo $n->seo_url; ?>" class="more-arrow">
								<img src="<?php echo assets_url(); ?>images/arrow-right.png">
							</a>
						</div>
					</div>
				<?php if (($key)%2!=0){ ?>
					</div>
				<?php } ?>
			<?php } ?>
			</div>

	<?php else: ?>

		<!-- MORE ARTICLES -->
		<div class="col-md-12 col-xs-12  padding-md-0 mt-xs-10 ">
		<div class="row-xs-10">

    	<?php $count = 3; ?>
			<?php foreach($more_articles as $key=>$n){
				if($key > $count){break;}
				if ($n->id!=$current_article->id){
			?>
			<div class="col-md-6 col-sm-6 col-xs-12 ">
	  		<div class="video-container">

					<div class="video-thumbnail">
						<a href="<?php echo $n->seo_url; ?>">
							<img src="<?php echo($v_title == 'News')? $n->full_image : $n->full_image; ?>" class="img-home-video img-responsive">
						</a>
						<div class="overlay"> </div>
						<span class="video-view-count hidden"> 18 views </span>
						<span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
						<figcaption>
                <h2 class="mb0 one-liner">
									<?php echo $n->title; ?>
								</h2>
                <p class="mb0 mt0">
									<?php echo $n->created_at; ?>
									<a href="" class="icon-share hidden">
										<img src="<?php echo assets_url(); ?>images/share-article.svg" alt="">
									</a>
								</p>

            </figcaption>
					</div>

					<div class="video-content">
						<p class="pt10 color-gray mb40">
							<?php echo($v_title == 'News')?substr(strip_tags($n->body),0, 120) : $n->details; ?>
          		<a href="<?php echo $n->seo_url; ?>" class="more-arrow">
								<img src="<?php echo assets_url(); ?>images/arrow-right.png">
							</a>
            </p>
					</div>



				</div>
			</div>
		<?php }else{
        $count ++;
      }
		} ?>


		</div>
	</div>

	<?php endif ?>

	


	<!--Close comment-->
</div>
