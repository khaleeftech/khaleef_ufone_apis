  <div class="image-wrapper pb0">
    <a href="javascript:;" class="playstore_link">
      <img src="<?php echo assets_url(); ?>/ads/add1.jpg?v=<?php echo VERSION ?>" alt="" style="width:100%;" class="img-responsive" width="100%">
    </a>
  </div>
  <div class="clearfix"></div>

  <div id="site-container" class="clearfix rankings" style=" min-height: 500px">
    <div class="scorecard_area custom-content-divs" style="display: block">
      <div class="container"></div>
    </div>
    <div class="match_fact_area custom-content-divs" style="display: none"></div>
    <div class="timeline_area custom-content-divs">
      <div class="clearfix"></div>
      <div class="container">
        <div id="content" role="main" class="mt0">
          <div role="tabpanel" class="tab-pane" id="scorecard">

              <div class="col-md-12 col-xs-12">
               <div class="row">
                  <div class="inplay-filters pt-15">
                    <div class="">
                      <ul class="nav nav-tabs nav-side-tab hidden-xs" role="tablist" style=" ">

                        <li role="presentation" class="<?php echo($rankings_for == 'test')? 'active':'' ; ?>" >
                          <a href="#test" aria-controls="test" role="tab" data-toggle="tab" class=" f-16 pt-15">Test Ranking</a>
                        </li>

                        <li role="presentation" class="<?php echo($rankings_for == 'odi')? 'active':'' ; ?>" >
                          <a href="#odi" aria-controls="odi" role="tab" data-toggle="tab" class=" f-16 pt-15">ODI Ranking </a>
                        </li>

                        <li role="presentation" class="<?php echo($rankings_for == 't20')? 'active':'' ; ?>" >
                          <a href="#t20" aria-controls="t20" role="tab" data-toggle="tab" class=" f-16 pt-15">T20 Ranking </a>
                        </li>

                      </ul>
                      <ul class="nav nav-tabs nav-side-tab visible-xs" role="tablist" style=" ">
                        <li role="presentation" class="active"><a href="#test" aria-controls="test" role="tab" data-toggle="tab" class=" f-16 pt-15">Test </a></li>
                        <li role="presentation"><a href="#odi" aria-controls="odi" role="tab" data-toggle="tab" class=" f-16 pt-15">ODI </a></li>
                        <li role="presentation"><a href="#t20" aria-controls="t20" role="tab" data-toggle="tab" class=" f-16 pt-15">T20</a></li>
                      </ul>
                    </div>
                  </div>



                      <div class="tab-content pl-15 pr-15 pt15">

                        <?php foreach ($ranks as $type=>$rankings): ?>


                        <div role="tabpanel" class="tab-pane <?php echo($rankings_for == $type)? 'active': ''; ?>" id="<?php echo $type ?>">
                          <div class="row">
                            <div class="col-md-12 col-xs-12">
                              <div class="row">

								  <h4>Last updated - 19 Jan 2018</h4>
                                <table class="table table-striped f-xs-12" style="width: 100%">
                                  <thead class="">
                                    <tr class="">
  										                <th colspan="4" class="text-left">
                                        <h2 class="mb0">
                                          <?php echo($type == 'test')?'Test' : strtoupper($type); ?> Team Rankings
                                        </h2>
                                      </th>
  									                  <th width="60">
                                        <a href="" class="pull-right share-rank">
                                          <img src="<?php echo assets_url() ?>images/share-article.png" alt="" style="width: 40px;" class="hidden img-responsive">
                                        </a>
                                      </th>
                                    </tr>
                                  </thead>

                                  <thead class="bg-mobily-green-dark color-white">
                                    <tr class="hidden-xs">
                                      <th class="text-left" width="40">Rank</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-center" width="150">Matches</th>
                                      <th class="text-center" width="150">Points</th>
                                      <th class="text-center" width="150">Rating</th>
                                    </tr>
                                     <tr class="visible-xs">
                                      <th class="text-center" width="5">R</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-center" width="5">M</th>
                                      <th class="text-center" width="5">P</th>
                                      <th class="text-center" width="5">R</th>
                                    </tr>
                                  </thead>

                                  <tbody>

                                    <?php foreach ($rankings->teams as $key => $value): ?>
                                      <tr>
                                      <td><?php echo $key+1; ?></td>
                                      <td class="text-left">
                                        <i class="flag" style="background-size:contain; background-position:left center; background-image: url(<?php echo $value->full_team_flag; ?>)"></i>
                                        <span class="rankings-team"><?php echo $value->team_name; ?> </span>
                                      </td>
                                      <td><?php echo $value->match_count; ?></td>
                                      <td><?php echo $value->points; ?></td>
                                      <td><?php echo $value->rating; ?></td>
                                    </tr>
                                    <?php endforeach ?>

                                  </tbody>
                                </table>
                                <table class="table table-striped f-xs-12" style="width: 100%">
                                  <thead class="">
                                    <tr class="">
                  										<th colspan="3" class="text-left">
                                        <h2 class="mb0">
                                          <?php echo($type == 'test')?'Test' : strtoupper($type); ?> Batsmen
                                        </h2>
                                      </th>
                  										<th width="60">
                                        <a href="" class="pull-right ">
                                          <img src="<?php echo assets_url(); ?>images/share-article.png" alt="" style="width: 40px;" class="hidden img-responsive">
                                        </a>
                                      </th>
                                    </tr>
                                  </thead>

                                  <thead class="bg-mobily-green-dark color-white">
                                    <tr class="hidden-xs">
                                      <th class="text-left" width="5">Rank</th>
                                      <th class="text-left" width="40%">Player</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-left" width="5">Rating</th>
                                    </tr>
                                     <tr class="visible-xs">
                                      <th class="text-center" width="5">R</th>
                                      <th class="text-left" width="35%" >Player</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-center" width="5">R</th>
                                    </tr>
                                  </thead>

                                  <tbody>

                                    <?php foreach ($rankings->batsmen as $key2 => $value2): ?>
                                      <tr>
                                      <td><?php echo $key2+1; ?></td>
                                      <td class="text-left"><?php echo $value2->player_name; ?></td>
                                      <td class="text-left">
                                        <i class="flag" style="background-size:contain; background-position:left center; background-image: url(<?php echo $value2->full_team_flag; ?>)"></i>
                                        <span class="rankings-team"><?php echo $value2->team_name; ?> </span>
                                      </td>
                                      <td><?php echo $value2->rating; ?></td>
                                    </tr>
                                    <?php endforeach ?>

                                  </tbody>
                                </table>

                                <table class="table table-striped f-xs-12" style="width: 100%">
                                  <thead class="">
                                    <tr class="">
                										<th colspan="3" class="text-left"><h2 class="mb0">
                                      <?php echo($type == 'test')?'Test' : strtoupper($type); ?> Bowlers
                                    </h2></th>
                										<th width="60"><a href="" class="pull-right share-rank"><img src="<?php echo assets_url(); ?>images/share-article.png" alt="" style="width: 40px;" class="img-responsive hidden"></a></th>
                                    </tr>
                                  </thead>

                                  <thead class="bg-mobily-green-dark color-white">
                                    <tr class="hidden-xs">
                                      <th class="text-left" width="5">Rank</th>
                                      <th class="text-left" width="40%">Player</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-left" width="5">Rating</th>
                                    </tr>
                                     <tr class="visible-xs">
                                      <th class="text-center" width="5">R</th>
                                      <th class="text-left" width="35%">Player</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-center" width="5">R</th>
                                    </tr>
                                  </thead>

                                  <tbody>

                                    <?php foreach ($rankings->bowler as $key2 => $value2): ?>
                                      <tr>
                                      <td><?php echo $key2+1; ?></td>
                                      <td class="text-left"><?php echo $value2->player_name; ?></td>
                                      <td class="text-left">
                                        <i class="flag" style="background-size:contain; background-position:left center; background-image: url(<?php echo $value2->full_team_flag; ?>)"></i>
                                        <span class="rankings-team"><?php echo $value2->team_name; ?> </span>
                                      </td>
                                      <td><?php echo $value2->rating; ?></td>
                                    </tr>
                                    <?php endforeach ?>

                                  </tbody>
                                </table>
                                <table class="table table-striped f-xs-12" style="width: 100%">
                                  <thead class="">
                                    <tr class="">
										<th colspan="3" class="text-left"><h2 class="mb0"><?php echo($type == 'test')?'Test' : strtoupper($type); ?> All-Rounder</h2></th>
										<th width="60"><a href="" class="pull-right share-rank "><img src="<?php echo assets_url(); ?>images/share-article.png" alt="" style="width: 40px;" class="img-responsive hidden"></a></th>
                                    </tr>
                                  </thead>

                                  <thead class="bg-mobily-green-dark color-white">
                                    <tr class="hidden-xs">
                                      <th class="text-left" width="5">Rank</th>
                                      <th class="text-left" width="40%">Player</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-left" width="5">Rating</th>
                                    </tr>
                                     <tr class="visible-xs">
                                      <th class="text-center" width="5">R</th>
                                      <th class="text-left" width="35%">Player</th>
                                      <th class="text-left">Team</th>
                                      <th class="text-center" width="5">R</th>
                                    </tr>
                                  </thead>

                                  <tbody>

                                    <?php foreach ($rankings->all_rounder as $key2 => $value2): ?>
                                      <tr>
                                      <td><?php echo $key2+1; ?></td>
                                      <td class="text-left"><?php echo $value2->player_name; ?></td>
                                      <td class="text-left">
                                        <i class="flag" style="background-size:contain; background-position:left center; background-image: url(<?php echo $value2->full_team_flag; ?>)"></i>
                                        <span class="rankings-team"><?php echo $value2->team_name; ?> </span>
                                      </td>
                                      <td><?php echo $value2->rating; ?></td>
                                    </tr>
                                    <?php endforeach ?>


                                  </tbody>
                                </table>

                              </div>
                            </div>

                          </div>
                        </div>
                      <?php endforeach ?>

                      </div>


              </div>
            </div>
          </div>
        </div>
        <div class="nextPageLoader text-center" style="display: none;"><img src="http://cricket.jazz.com.pk/assets/images/ajax-loader.gif"> Loading</div>
      </div>
    </div>
  </div>
