<!DOCTYPE html>
<html lang="en">

<head>
	<title><?php echo $og_tags->title; ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo $og_tags->descr; ?>">
	<link rel="shortcut icon" href="<?php echo assets_url(); ?>images/fav/ico-16.png">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-144.png" sizes="144x144">

<meta property="og:site_name" 		   content="Ufone Cricket"/>
<meta property="fb:app_id" 			   content="430752913746475" />
<?php if (!empty($video_page)){ ?>

<meta property="og:url"  content="<?php echo $current_video->social->link; ?>" />
<meta property="og:title" content="<?php echo $current_video->social->title; ?>" />
<meta property="og:description" content="<?php echo $current_video->social->description; ?>" />
<meta property="og:image" content="<?php echo $current_video->social->thumb_file; ?>" />
<?php /*
<meta property="og:type" content="website" />
<meta property="og:video:width" content="640" />
<meta property="og:video:height" content="320" />
<meta property="og:video:type" content="video/mp4" />
<meta property="og:video:url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />
<meta property="og:video:secure_url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />

<meta name="twitter:card" content="player">
<meta name="twitter:app:name:googleplay" content="CricBoom">
<meta name="twitter:app:id:googleplay" content="com.Khaleef.CricBoom" />
<meta name="twitter:player" content="<?php echo base_url(). "embed/video/".$current_video -> id; ?>" />
<meta name="twitter:player:stream" content="<?php echo base_url()."embed/serve/".$current_video -> id; ?>" />
<meta name="twitter:player:stream:content_type" content="video/mp4" />
<meta name="twitter:player:width" content="480">
<meta name="twitter:player:height" content="480">
*/ ?>
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@CricBoomLive">
<meta name="twitter:url"  content="<?php echo $current_video->social->link; ?>" />
<meta name="twitter:title" content="<?php echo $current_video->social->title; ?>">
<meta name="twitter:description" content="<?php echo $current_video->social->description; ?>">
<meta name="twitter:image" content="<?php echo $current_video->social->thumb_file; ?>">

<?php } ?>


<style>
.user {
	/*top: 20px !important;*/
}

@media(max-width: 767px) {
.user {
	/*top: 7px !important;*/
}
}
</style>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() ?>assets/jan18/home/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url() ?>assets/jan18/home/css/half-slider.css?v=<?php echo VERSION; ?>" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/jan18/home/css/custom.css?v=<?php echo VERSION; ?>" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/jan18/css/custom-front.css?v=<?php echo VERSION; ?>>" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/jan18/home/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/jan18/home/css/owl.theme.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/jan18/home/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/jan18/css/cric_css/style_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/jan18/css/layout.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url() ?>assets/jan18/css/dev.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<meta name="google-site-verification" content="vFrwbZ1fAeGzbO4yDWsPPW0q1FH8-3F7KOUTk7S5lOk" />
<meta name="msvalidate.01" content="73818B487022F81E4006E2364DA72CCA" />

<meta property="og:url"  content="<?php echo current_url(); ?>" />
<meta property="og:title" content="<?php echo $og_tags->title; ?>" />
<meta property="og:description" content="<?php echo $og_tags->descr; ?>" />
<meta property="og:image" content="<?php echo base_url()."assets/images/default_banner_icc.jpg"; ?>" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@CricBoomLive">
<meta name="twitter:url"  content="<?php echo current_url(); ?>" />
<meta name="twitter:title" content="<?php echo $og_tags->title; ?>">
<meta name="twitter:description" content="<?php echo $og_tags->descr; ?>">
<meta name="twitter:image" content="<?php echo base_url()."assets/images/default_banner_icc.jpg"; ?>">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.<?php echo assets_url();  ?>home/js/1.4.2/respond.min.js"></script>
	<![endif]-->
<?php if (ENVIRONMENT=='production'){ ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-88034149-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-88034149-3');
</script>

<!-- Google Analytics Code -->
<script>
  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  // })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	//
  // ga('create', 'UA-88034149-1', 'auto');
  // ga('send', 'pageview');

</script>

<!-- Google Analytics Code -->

<?php } ?>
</head>


<body id="videos_page">

<?php $this->load->view("jan18/navigation"); ?>
<div class="hidden-sm hidden-md hidden-lg hidden" style="max-width: 100%;">
	<img src="<?php echo assets_url(); ?>ads/top_app_mob_banner.jpg" class="playstore_link" style="max-width: 100%;"/>
</div>

<div class="wrapper">


	<?php if (!empty($live_matches)): ?>


		  <div id="myCarousel" class="carousel slide">
		    <!-- Indicators -->
		    <div class="banner-side-left">

					<ol class="carousel-indicators">

						<?php $main_count = 0; ?>

						<?php foreach($live_matches as $key=>$live){ ?>
							<?php //if (count($live_matches)>1){ ?>
									<li data-target="#myCarousel" data-slide-to="<?php echo $main_count; ?>" class="<?php echo $main_count==0?"active":''; ?>"></li>
								<?php //} ?>
								<?php $main_count++; ?>
						<?php } ?>


						<?php if(!empty($upcoming_fixtures)): ?>
								<?php foreach($upcoming_fixtures as $key=>$uf){ ?>
									<?php if (count($upcoming_fixtures)>1){ ?>
										<?php
											if ($key>4){
												break;
											}
										?>
											<li data-target="#myCarousel" data-slide-to="<?php echo $main_count; ?>" class=""></li>
										<?php } ?>
							<?php $main_count++; ?>
						<?php } ?>
					<?php endif; ?>

					</ol>

		      <!-- Wrapper for Slides -->
		      <div class="carousel-inner">
						<?php $main_count = 0; ?>


						<?php foreach($live_matches as $key=>$live){
		          $title = $live->team_1->team->name." vs ".$live->team_2->team->name;
		          $match_url = base_url()."live/".$live->id."/".seo_url($title)."/";
		        ?>
		          <div class="item <?php echo $main_count==0?"active":''; ?>">
		            <a href="<?php echo base_url()."live/".$live->id."/".seo_url($title)."/" ?>">
		              <img src="<?php echo( !if_file_exists($live->full_banner_url)  )?$default_banner:$live->full_banner_url; ?>" >
		              <div class="carousel-caption">
		    						<a href="<?php echo $match_url; ?>">
		    						<?php
		      						if ($live->format=='Test' && !empty($live->break_type)){
												echo '<h3><span class="livenow"> Day '.$live->day.' | '.$live->break_type.'</span>';
		                  } else {
		                ?>
		    						<h3>
											<span class="livenow">Watch Live Now!</span>
											<span class="watch hidden-xs">Exclusive Coverage <i class="fa fa-caret-right"></i></span>
										</h3>
		    						<?php } ?>
		    						<h2 class="hidden-xs hidden" style="font-weight: bold;">
											<?php echo $live->title; ?>
										</h2>
		    						<h2 class="hidden-sm hidden-md hidden-lg small_size hidden">
											<?php echo $live->team_1->team->short_name." vs ".$live->team_2->team->short_name; ?>
										</h2>

		  							<?php
		    							if (!empty($live->innings)){
		                    $current_innings = count($live->innings)-1;
		                    if (empty($live->innings[$current_innings]->wickets) || $live->innings[$current_innings]->wickets==''){
		                      $live->innings[$current_innings]->wickets = 0;
		                    }
		                    $live->innings[$current_innings]->runs = empty($live->innings[$current_innings]->runs)?0:$live->innings[$current_innings]->runs;
		                    echo '<h3><span class="score">';
		                    $current_batting_team = $live->innings[$current_innings]->batting_team_id;
		                    echo $teams[$current_batting_team]->short_name." ";
		                    echo $live->innings[$current_innings]->runs."/".$live->innings[$current_innings]->wickets;
		                    echo '<sup>*</sup></span>';
			                  if (!empty($live->innings[$current_innings]->runs_required) && $live->format!='Test'){
			                    echo ' | <span class="score">Target '.($live->innings[$current_innings]->runs_required+1).'</span>';
		                    }
		                  	echo "</h3>";
		                  }
		  							?>

		  							<?php
											if (count($live->innings)==2){
												/* ?>
		        							 | <span class="score">Target <?php echo $live->innings[0]->runs_req+$live->innings[0]->runs; ?></span>
		  									 <?php */
										  }
										?>

		    						</a>
		    					</div>
		            </a>
		          </div>
							<?php $main_count++; ?>
						<?php } ?>


						<?php foreach($upcoming_fixtures as $key=>$uf){ ?>
									<?php
										if ($key>4){
											break;
										}
									?>
										<div class="item " style="cursor: auto; cursor: default;">
												<img src="<?php echo (!if_file_exists( $uf->full_banner_url))?$default_banner: $uf->full_banner_url; ?>" >
												<div class="carousel-caption">
													<a href="javascript:;" class="uf" style="cursor: auto; cursor: default;">
													<h3><span class="upcoming">Upcoming</span></h3>
													<h2 class="hidden-xs hidden" style="font-weight: bold;"><?php echo $uf->team_1->team->name." vs ".$uf->team_2->team->name; ?></h2>
													<h2 class="hidden-sm hidden-md hidden-lg small_size hidden"><?php echo $uf->team_1->team->short_name." vs ".$uf->team_2->team->short_name; ?></h2>
													<?php $local = format_date_homebanner_newserver($uf->match_start); ?>
													<h3><span class="score"><?php echo $local["day"].", ".$local["month"]." ".$local["date"]." | ".$local["hours"].":".$local["minutes"]." ".$local["meridiem"]; ?></span></h3>
													</a>
												</div>
										</div>
							<?php $main_count++; ?>
						<?php } ?>


		      </div>
		      <!-- Controls -->
		      <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="icon-prev"></span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="icon-next"></span> </a> </div>
		  </div>

	<?php else: ?>


		<div id="myCarousel" class="carousel slide">
	    <!-- Indicators -->
	    <div class="banner-side-left">

				<ol class="carousel-indicators">
					<?php if(!empty($upcoming_fixtures)): ?>
			        <?php foreach($upcoming_fixtures as $key=>$uf){ ?>
			        	<?php if (count($upcoming_fixtures)>1){ ?>
			        		<?php
				        		if ($key>4){
				        			break;
				        		}
				        	?>
			            	<li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="<?php echo $key==0?"active":''; ?>"></li>
			            <?php } ?>
					<?php } ?>
					<?php else: ?>
						<!-- <li data-target="#myCarousel" data-slide-to="1" class="active"></li> -->
					<?php endif; ?>
        		</ol>

	      <!-- Wrapper for Slides -->
				<div class="carousel-inner">

					<?php if (!empty($upcoming_fixtures)): ?>
					<?php foreach($upcoming_fixtures as $key=>$uf){ ?>
			        	<?php
			        		if ($key>4){
			        			break;
			        		}
			        	?>
			            <div class="item <?php echo $key==0?"active":''; ?>" style="cursor: auto; cursor: default;">
			                <img src="<?php echo (!if_file_exists($uf->full_banner_url))?$default_banner:$uf->full_banner_url; ?>" >
			                <div class="carousel-caption">
			        					<a href="javascript:;" class="uf" style="cursor: auto; cursor: default;">
			        					<h3><span class="upcoming">Upcoming</span></h3>
			        					<h2 class="hidden-xs hidden" style="font-weight: bold;"><?php echo $uf->team_1->team->name." vs ".$uf->team_2->team->name; ?></h2>
			        					<h2 class="hidden-sm hidden-md hidden-lg small_size hidden"><?php echo $uf->team_1->team->short_name." vs ".$uf->team_2->team->short_name; ?></h2>
			        					<?php $local = format_date_homebanner_newserver($uf->match_start); ?>
			        					<h3><span class="score"><?php echo $local["day"].", ".$local["month"]." ".$local["date"]." | ".$local["hours"].":".$local["minutes"]." ".$local["meridiem"]; ?></span></h3>
			        					</a>
			        				</div>
			            </div>
					<?php } ?>
				<?php else: ?>
					<div class="item active" style="cursor: auto; cursor: default;">
			                <img src="<?php echo $default_banner; ?>" >
			                <div class="carousel-caption">
			        					<a href="javascript:;" class="uf hidden" style="cursor: auto; cursor: default;">
			        					<h3><span class="upcoming">Upcoming</span></h3>
			        					<h2 class="hidden-xs hidden" style="font-weight: bold;"><?php echo $uf->team_1->team->name." vs ".$uf->team_2->team->name; ?></h2>
			        					<h2 class="hidden-sm hidden-md hidden-lg small_size hidden"><?php echo $uf->team_1->team->short_name." vs ".$uf->team_2->team->short_name; ?></h2>
			        					<?php $local = format_date_homebanner_newserver($uf->match_start); ?>
			        					<h3><span class="score"><?php echo $local["day"].", ".$local["month"]." ".$local["date"]." | ".$local["hours"].":".$local["minutes"]." ".$local["meridiem"]; ?></span></h3>
			        					</a>
			        				</div>
			            </div>

				<?php endif ?>

        		</div>


	      <!-- Controls -->
	      <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="icon-prev"></span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="icon-next"></span> </a> </div>
	  </div>

	<?php endif; ?>







  <div class="banner-side-right">

    <!-- Nav tabs category -->
    <ul class="nav nav-tabs nav-side" id="myTabs" role="tablist">
      <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Live</a></li>
      <li role="presentation"><a href="#results" role="tab" id="results-tab" data-toggle="tab" aria-controls="results">Results</a></li>
      <li role="presentation"><a href="#fixture" role="tab" id="fixture-tab" data-toggle="tab" aria-controls="fixture">Fixtures</a></li>
    </ul>
    <div class="tab-content nav-side-content" id="myTabContent">
      <div class="tab-pane fade in active" role="tabpanel" id="home" aria-labelledby="home-tab">
        <ul class="tab-banner live-matches">

					<?php if(empty($live_matches)): ?>
						<li class="">
					  	<h5>
								No live match at the moment.
							</h5>
							<p class="color-light-gray"></p>
						</li>
					<?php endif; ?>

					<?php
						$count_live_matches = count($live_matches);
						foreach($live_matches as $key=>$live):
							if($key >= 4){break;}
							$title = $live->team_1->team->name." vs ".$live->team_2->team->name;
	            $match_url = base_url()."live/".$live->id."/".seo_url($title)."/";
					?>
						<li class="">
				  	<h5>
							<a href="<?php echo $match_url; ?>" title="<?php echo $title; ?>">
								<?php echo $title ?>
							</a>
						</h5>
	            <p class="color-light-gray">
								<?php
								if (!empty($live->innings)){
									$current_innings = count($live->innings)-1;
									if (empty($live->innings[$current_innings]->wickets) || $live->innings[$current_innings]->wickets==''){
										$live->innings[$current_innings]->wickets = 0;
									}
									$live->innings[$current_innings]->runs = empty($live->innings[$current_innings]->runs)?0:$live->innings[$current_innings]->runs;
									echo '<span>';
									$current_batting_team = $live->innings[$current_innings]->batting_team_id;
									echo $teams[$current_batting_team]->short_name." ";
									echo $live->innings[$current_innings]->runs."/".$live->innings[$current_innings]->wickets;
									// echo '<sup>*</sup></span>';
									echo ' (' .$live->innings[$current_innings]->overs. '/' .$live->innings[$current_innings]->total_overs.  ')</span>';
									if (!empty($live->innings[$current_innings]->runs_required) && $live->format!='Test'){
										echo ' | <span class>Target '.($live->innings[$current_innings]->runs_required+1).'</span>';
									}
								}
								?>
							</p>
	          </li>
					<?php endforeach; ?>
        </ul>

				<?php if($count_live_matches > 4 && 0): ?>
					<li class="">
						<a href="javascript:;" class="fw-normal widget-more"><h5 class="text-center more-tb"> More </h5></a>
					</li>
				<?php endif; ?>

      </div>
      <div class="tab-pane fade" role="tabpanel" id="results" aria-labelledby="results-tab">
        <ul class="tab-banner">

					<?php if(empty($recent_matches)): ?>
						<li class="">
					  	<h5>
								No recent results available.
							</h5>
							<p class="color-light-gray"></p>
						</li>
					<?php endif; ?>

					<?php
						$count_recent_matches = count($recent_matches);
						foreach ($recent_matches as $key => $recent):
							if($key >= 3 ){break;}
							$title = '';
							if($recent->title && $recent->title!='Missing Title')
								$title .= $recent->title.': ';
							$title .= $recent->team_1->team->short_name;
							$title .= ' v ';
							$title .= $recent->team_2->team->short_name;
							// $title .= ' - ';
							// $title .= $recent->local_time["month"].'&nbsp;'.$recent->local_time["date"];
							// $title .= ' ';
							// $title .= $recent->local_time["year"];
					?>
						<li class="">
							<h5>
								<a href="<?php echo $recent->seo_url; ?>" title="<?php echo $title; ?>">
									<?php echo $title ?>
								</a>
							</h5>
							<p class="color-light-gray"><?php echo $recent->match_result; ?></p>
						</li>

					<?php endforeach; ?>
        </ul>

				<?php //if($count_recent_matches > 3): ?>
						<a href="<?php echo base_url().'results/' ?>" class="fw-normal widget-more"><h5 class="text-center more-tb"> More </h5></a>
				<?php //endif; ?>


      </div>
      <div class="tab-pane fade" role="tabpanel" id="fixture" aria-labelledby="fixture-tab">
        <ul class="tab-banner">

					<?php if(empty($upcoming_fixtures)): ?>
						<li class="">
					  	<h5>
								No upcoming fixtures available.
							</h5>
							<p class="color-light-gray"></p>
						</li>
					<?php endif; ?>


					<?php
						$count_upcoming_fixtures = count($upcoming_fixtures);
						foreach ($upcoming_fixtures as $key => $upcoming):
							if($key >= 4 ){break;}
							$local = format_date_homebanner_newserver($upcoming->match_start);
							$title = '';
							if($upcoming->title)
								$title .= $upcoming->title.': ';
							$title .= $upcoming->team_1->team->short_name;
							$title .= ' v ';
							$title .= $upcoming->team_2->team->short_name;
							// $title .= ' - ';
							// $title .= $local["month"];
					?>


					<li class="">
            <h5>
							<!-- <a href="javascript:;" title="<?php //echo $title; ?>"> -->
								<?php echo $title; ?>
							<!-- </a> -->
            </h5>

            <p class="color-light-gray">
							<?php echo $local["day"].", ".$local["month"]." ".$local["date"]." | ".$local["hours"].":".$local["minutes"]." ".$local["meridiem"]; ?>
						</p>
          </li>

					<?php endforeach; ?>
        </ul>

				<?php //if($count_upcoming_fixtures > 4 && 0): ?>
						<a href="<?php echo base_url().'fixtures/' ?>" class="fw-normal widget-more"><h5 class="text-center more-tb"> More </h5></a>
				<?php //endif; ?>

      </div>
    </div>
  </div>
</div>
<div class="wrapper-main">
  <div class="adds-hrz hidden" style="padding: 0px;"> <a href="http://vbox.pk/" target="_blank"><img src="<?php echo base_url() ?>assets/jan18/ads/add1.jpg?v=<?php echo VERSION ?>" alt=""/></a> </div>
  <!-- Page Content -->
  <div class="container">

    <div class="col-lg-12 crk-trending-videos-container crk-trending-videos-section">
      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>


      <h1 class="f20">
				Videos
				<a class="next-trending" href="<?php echo base_url().'videos/' ?>"> View All </a>
			</h1>

      <!---->
      <div id="home-video" class="col-md-12 mt-xs-10" style="padding-left:0; padding-right:0;">

        <div class="pleasewait">
          <img src="<?php echo assets_url().'images/loading.gif'; ?>" alt="">
        </div>

			</div>
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    </div>





    <div id="home-news" class="col-lg-12 crk-trending-videos-container">

      <div class="cs-seprator">
        <div class="devider1"> </div>
      </div>

      <h1 class="f20">
				News
				<a class="next-trending" href="<?php echo base_url().'news/' ?>"> View All</a>
			</h1>

      <div id="home-news-container" class="">
        <div class="pleasewait">
          <img src="<?php echo assets_url().'images/loading.gif'; ?>" alt="">
        </div>
      </div>

    </div> <!-- col-lg-12 -->

    <div class="clearfix"></div>





  </div><!-- container -->
  <div class="clear"></div>

		<a href="javascript:;" class="playstore_link"><img src="<?php echo assets_url(); ?>ads/add1.jpg?v=<?php echo VERSION ?>" style="width:100%;" alt=""/></a>
</div>
<!-- /.container -->
<?php $this->load->view("footer"); ?>

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/jan18/home/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() ?>assets/jan18/home/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/jan18/home/js/jquery.mobile.custom.js"></script>
<script src="<?php echo base_url() ?>assets/jan18/home/js/owl.carousel.min.js"></script>

<!-- Script to Activate the Carousel -->

    <script>
    $('body').on('click','.playstore_link', function (e){


      sUsrAg = navigator.userAgent;
        if ((sUsrAg.indexOf("Android") > -1)) {
          ga('send', 'event', 'APP Banner', 'Click', 'Android', 1);
          window.open("market://details?id=com.Khaleef.cricwickSTC");
      } else {
      	ga('send', 'event', 'APP Banner', 'Click', 'Non-Android', 1);
        win = window.open('https://play.google.com/store/apps/details?id=com.Khaleef.cricwickSTC', '_blank');
        win.focus();
      }
    });
    $("body").on("click",".user, .user1", function (e){
		//e.preventDefault();
		var phone = $(this).attr("phone");
		var sub_status = $(this).attr("sub_status");
		var msg = "Welcome <b>"+phone+"</b><BR>";
		msg = msg + "Subscription Status: ";
		if (sub_status==1){
			msg = msg + "Active";
		} else if (sub_status==3){
			msg = msg + "Suspended";
		} else {
			msg = msg + "Trial";
		}
		$('.alert-popup .modal-body').html(msg);
		$('.alert-popup').modal('show');
	});
    </script>


      <script>

          $('.carousel').carousel({
              interval: 5000 //changes the speed
          });


      $(document).ready(function() {


      	$('body').on('click', '.item  .img-home-item', function(event){
  			    var link = $(this).parent().parent().attr("href");
  			    if (link!='javascript:;'){
  			    	window.location = link;
  			    }
  			});

  			$('body').on('click', '.video-container .img-home-video', function(event){
  			    var link = $(this).parent().parent().parent().attr("href");
  			    if (link!='javascript:;'){
  			    	window.location = link;
  			    } else {
  			    	console.log("error");
  			    }
  			});

  			$('body').on('click', '.video-container .video-content .video-description', function(event){
  			    var link = $(this).parent().parent().parent().attr("href");
  			    if (link!='javascript:;'){
  			    	window.location = link;
  			    }
  			});

  			$('body').on('click', '.item .data-news-pg', function(event){
  			    var link = $(this).parent().attr("href");
  			    console.log(link);
  			    if (link!='javascript:;'){
  			    	window.location = link;
  			    }
  			});

        get_home_videos();
        get_home_news();


      });

    </script>

    <script type="text/javascript">
      function get_home_videos(){
        $.ajax({
          method: "POST",
          url: "<?php echo base_url().'get_home_videos'  ?>",
          dataType: "html",
          success: function(response){
            if(response){
              $("#home-video .pleasewait").fadeOut();
              $("#home-video").html(response);
            }
          }
        });
      }

      function get_home_news(){
        $.ajax({
          method: "POST",
          url: "<?php echo base_url().'get_home_news'  ?>",
          dataType: "html",
          success: function(response){
            if(response){
              $("#home-news-container .pleasewait").fadeOut();
              $("#home-news-container").html(response);
            }
          }
        });
      }
    </script>

		<script type="text/javascript">
			var psl18 = "<?php echo $psl18_slug ?>";
			if(psl18){
				gtag('event', 'SMS LINK CLICK', {
				  'event_category' : 'SMS LINK CLICK',
				  'event_label' : 'SMS LINK CLICK'
				});
			}
		</script>

</body>

</html>
