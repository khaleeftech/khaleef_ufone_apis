<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<div class="container">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container"> 
      <!--<div class="cs-seprator mt30">
        <div class="devider1"> </div>
      </div>-->
      
      <div class="col-md-12 news-video videos-full-xs mt-md-10">

        <div id="videoElement"> 
	        <script type="text/javascript">
    			var video_allowed = <?php echo $video_allowed; ?>;
    			var playerInstance = jwplayer("videoElement");
    			var base_url = "<?php echo base_url(); ?>";
    			var view_confirmed = false;
    			playerInstance.setup({
    			    // playlist: <?php //echo $playlist; ?>,
    			    "playlist": [
					    {
					      "sources": [

                    <?php if(!empty($current_video->video_file)): ?>
                      {                   
                        "file": "<?php echo $current_video->video_file; ?>",
                        "label": "HD"
                      },
                    <?php endif; ?>

					      		<?php $cc = 1; ?>
				        		<?php foreach ($current_video->qualities as $key => $value): ?>

  								    {			        			
  						          "default": "<?php echo ($value->height == 240)?'true' : 'false'; ?>",
  						          "file": "<?php echo $value->video_file; ?>",
  						          "label": "<?php echo $value->height ?>"+ "p"
  						      	}<?php echo($cc == count($current_video->qualities))? '': ','; ?>

						      	<?php $cc++; ?>
				        		<?php endforeach ?>
					      ],
					      mediaid: "AD-"+"<?php echo $current_video->id; ?>",
					      image: "<?php echo $current_video->med_image; ?>"
					    }
					  ],
    			    width: "100%",
    			    aspectratio: "16:9",
    			    controls: true,
    			    autostart: true,
    			    title: "<?php echo $current_video->title; ?>",
    			    // sharing: {
    			    //   sites: ['facebook','twitter','googleplus'],
    			    //   link: "<?php //echo $social->link; ?>",
    			   	// },
    			 //   	related: {
    				// 	file: base_url+"ajax/related_video_rss/MEDIAID",
    				// 	onclick: "link",
    				// 	oncomplete: 'show'
    				// },
    				ga: {
    					label: "title"
    				}
    			}).onPlay(function() {
    				if (video_allowed==0){
    					jwplayer("videoElement").stop();
    					alert('Sorry, this content is not available in your country!');
    				} else {
    					if (!view_confirmed){
    						jQuery.ajax({
    							dataType: "json",
    							url: "<?php echo $confirm_view; ?>",
    							data: {},
    							success: function(response) {

    							}
    						});
    						view_confirmed = true;
    					}
    				}

    			}).onComplete(function(){
    				if($("#cmn-toggle-1:checked").length){
    					window.location = $("#rv-ul > li").eq(0).find('a').attr('href');
    				}else{
    				}
    			});
    			</script>
      	</div>
        
      </div>


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-10 col-sm-9 col-xs-12">
         <div class="row">

          	<h1 class="video-title"><?php echo $current_video->title; ?></h1>

		    <h1 class="video-discription color-jazz-grey-light">
		      	
		      	<span class="hidden-xs">
		      		<?php echo(!empty($current_video->match_obj->title))?$current_video->match_obj->title.' - ' :'' ; ?>
		      		<?php echo $current_video->match_obj->team_1_full.' vs '.$current_video->match_obj->team_2_full; ?>
		      	</span>
		      	<span class="visible-xs">
		      		<?php echo(!empty($current_video->match_obj->title))?$current_video->match_obj->title.' - ' :'' ; ?>
		      		<?php echo $current_video->match_obj->team_1.' vs '.$current_video->match_obj->team_2; ?>
		      	</span>
	    	</h1>

          	<h1 class="video-date color-jazz-grey-light">

          		<?php echo date("j F Y ", $current_video->created_at/1000); ?>

          		<div class="socialmedia pull-right visible-xs">
            		<ul class="">
              			<li>
					      	<a data-original-title="Facebook" class="addthis_button_facebook social_sharing_link new-look" href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u=<?php echo $current_video->social->link; ?>&display=popup&ref=plugin&src=share_button">
					      		<i class="fa fa-facebook"></i></a></li>
					      <li>
					      	<a data-original-title="twitter" class="addthis_button_twitter social_sharing_link new-look" href="http://www.twitter.com/share?text=<?php echo $current_video->title; ?>&url=<?php echo $current_video->social->link; ?>">
					      		<i class="fa fa-twitter"></i>
					      	</a>
					      </li>
					      <li>
					      	<a data-original-title="google-plus" class="addthis_button_google social_sharing_link new-look" href="https://plus.google.com/share?url=<?php echo $current_video->social->link; ?>">
					      		<i class="fa fa-google-plus google"></i>
					      	</a>
					      </li>
            		</ul>
          		</div>
          	</h1>

		 </div>
        </div>


        <div class="col-md-2 col-sm-3 col-xs-12 hidden-xs">
          <div class="socialmedia pull-right">
            <ul class="mt10 mr-md-10">
              <li>
					      	<a data-original-title="Facebook" class="addthis_button_facebook social_sharing_link new-look" href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u=<?php echo $current_video->social->link; ?>&display=popup&ref=plugin&src=share_button">
					      		<i class="fa fa-facebook"></i></a></li>
					      <li>
					      	<a data-original-title="twitter" class="addthis_button_twitter social_sharing_link new-look" href="http://www.twitter.com/share?text=<?php echo $current_video->title; ?>&url=<?php echo $current_video->social->link; ?>">
					      		<i class="fa fa-twitter"></i>
					      	</a>
					      </li>
					      <li>
					      	<a data-original-title="google-plus" class="addthis_button_google social_sharing_link new-look" href="https://plus.google.com/share?url=<?php echo $current_video->social->link; ?>">
					      		<i class="fa fa-google-plus google"></i>
					      	</a>
					      </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!--right column-->
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 right-column">
      <div class="top-score-title col-md-12 right-title row-xs-5">
        <div class="">
          <div class="col-md-12 col-xs-12 padding-md-0">

            <div class="widget element-size-100 widget_team">
              <div class="cs-seprator mt0 visible-xs visible-sm mb0">
                <div class="devider1"></div>
              </div>
              <!--start auto play-->
              <div class="col-md-12 col-xs-12">
                <div class="row">
                  <div class="switch pull-right">
                    <input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round" type="checkbox" checked>
                    <label for="cmn-toggle-1" class="mt5"></label>
                  </div>
                  <span class="switch-lable pull-right">Auto Play</span> </div>
              </div>
              <!--end auto play-->
              
              <div class="mt10 col-xs-12 p0 mt0">
                <h1 class="series-name">
                	<?php echo $current_series->short_name; ?>
                </h1>
              </div>
				<div class="mt10 col-xs-12 p0 mt0 series-related-videos" style="">
              <ul id="rv-ul">


              	<?php foreach ($related_videos as $key => $rv): ?>

              		<?php if ($rv->video->id !== $current_video->id): ?>
              			<li>
		                  <figure>
		                  	<a href="<?php echo $rv->seo_url; ?>">
		                  		<img src="<?php echo $rv->video->thumb_image; ?>" alt="" class="img-responsive">
		                  	</a>
		                  </figure>
		                  <div class="infotext"> 
		                  	<a href="<?php echo $rv->seo_url ?>" title="<?php echo $rv->video->title; ?>">
		                  		<?php echo (!empty($rv->video->match_obj->title))?$rv->video->match_obj->title.' - ' :'' ; ?>
		                  		<?php echo $rv->video->match_obj->team_1.' vs '.$rv->video->match_obj->team_2. ' - '; ?>
		                  		<?php echo $rv->video->title; ?>
		                  	</a>
		                    <p><?php echo ($rv->video->views)?$rv->video->views.' views' : '0 views'; ?></p>
		                  </div>
		                </li>		
              		<?php endif ?>
              		

              	<?php endforeach ?>

              
                
              </ul>
          </div>

          <div class="mt10 col-xs-12 <?php echo($current_page >= $total_pages)?'hidden' :'' ; ?>">
          		<a id="more-rv-btn" href="javascript:;" class="fw-normal btn-more-tb" data-page-to-load=2 data-video-id="<?php echo $current_video->id; ?>" data-series-id="<?php echo $current_series->id; ?>">
              		<h5 class="text-center more-tb"> Load More </h5>
              	</a>
		  </div>

            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <!--end right column-->  
    <div class="clearfix"></div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 crk-trending-videos-container hidden-xs">
     <h1 class="color-jazz-grey-light pb0">Recomended For You</h1>
      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>




      <?php $c = 1; ?>
      <?php foreach ($series as $key => $s): ?>
        <?php if($c == 1){$c++;continue;} ?>

        <h1>
      <span class="hidden-xs"><?php echo $s->short_name; ?></span>
      <span class="f15 visible-xs"><?php echo $s->short_name; ?></span> 
      
      <span class="f16 color-jazz-grey-light hidden-xs">
        <?php echo date("M j", strtotime($s->start_date)); ?>
        -
        <?php echo date("M j", strtotime($s->end_date)); ?>
      </span>  
     </h1>


      <span class="f13 color-jazz-grey-light visible-xs">
      <?php echo date("M j", strtotime($s->start_date)); ?>
        -
        <?php echo date("M j", strtotime($s->end_date)); ?>
    </span>
      
      
      <!--carousel 1-->
      
      <div id="trending<?php echo $c; ?>" class="owl-carousel owl-theme" style="opacity: 1; display: block;">

        <?php foreach ($s->videos as $key1 => $v): ?>
          <?php if($key1>5){break;} ?>

          <div class="item" onclick="window.location='<?php echo $v->seo_url; ?>'">
                <div class="video-container">
                  
                      <div class="video-thumbnail"> 
                        <img src="<?php echo $v->video->med_image; ?>" class="img-home-video img-responsive"> 

                        <a href="<?php echo $v->seo_url; ?>">
                          <img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">
                        </a>


                        <div class="overlay"> </div>
              
                      <figcaption>
                        <h2 class="mb0"><?php echo $v->video->match_obj->title.' - '. $v->video->match_obj->team_1.' vs '.$v->video->match_obj->team_2 ?></h2>
                      </figcaption>

                </div>
                  <div class="video-content">
                    <p class="video-description one-liner" title="<?php echo $v->video->title; ?>">
                      <?php echo $v->video->title; ?>
                    </p>
                  </div>
                </div>
              </div>
        

          
        <?php endforeach ?>
         
         
         
         
          
             
         
        <div class="owl-controls clickable" style="display: block;">
          <div class="owl-pagination">
            <div class="owl-page active"><span class=""></span></div>
            <div class="owl-page"><span class=""></span></div>
          </div>
        </div>
      </div>
      <div class=" clearfix" style=" margin-bottom:10px;"></div>
      <!--end carousel 1-->



        <?php $c++; ?>
      <?php endforeach; ?>

      








      </div>
      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    </div>
  
  </div>
  <div class="clear"></div>

  <script type="text/javascript">
  	


  </script>
  
  <script>
    $(document).ready(function(){
      
      $('.owl-carousel.owl-theme').each(function(e){
        var $this = $("#"+$(this).attr('id'));

          $this.owlCarousel({
            "loop": true, "autoplay": true, "autoplayTimeout": 3000,
          items : 4, //10 items above 1000px browser width
          itemsDesktop : [1000,4], //5 items between 1000px and 901px
          itemsDesktopSmall : [900,3.3], // betweem 900px and 601px
          itemsTablet: [600,2.3], //2 items between 600 and 0
          itemsMobile : [400,1.2]  // itemsMobile disabled - inherit from itemsTablet option
        });
      }); 
    
    });
  </script>
