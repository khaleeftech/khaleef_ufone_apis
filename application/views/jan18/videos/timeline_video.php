<style media="screen">
.jw-icon-fullscreen {
	display: none;
}
</style>
<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<script>jwplayer.key="vkFxGOqRxYPKvhFSCw2QvpCcM3C7Aii/jSTZ/w==";</script>
<div class="top-score-title col-md-9 crk-trending-videos-container">

	<div class="cs-seprator mt30">
    <div class="devider1"> </div>
  </div>

	<h1><?php echo $current_video->video->title; ?></h1>


	<div class="col-md-12 news-video list_space_mng_big">
		<div class="row">
			<div id="videoElement"></div>
			<script type="text/javascript">
			var video_allowed = <?php echo $video_allowed; ?>;
			var playerInstance = jwplayer("videoElement");
			var base_url = "<?php echo base_url(); ?>";
			var view_confirmed = false;
			playerInstance.setup({
			    playlist: <?php echo $playlist; ?>,
			    width: "100%",
			    aspectratio: "16:9",
			    controls: true,
			    autostart: true,
			    title: "<?php echo $current_video->video->title; ?>",
			    sharing: {
			      sites: ['facebook','twitter','googleplus'],
			      link: "<?php echo $social->link; ?>",
			   	},
			   	related: {
					file: base_url+"ajax/related_video_rss/MEDIAID",
					onclick: "link",
					oncomplete: 'show'
				},
				ga: {
					label: "title"
				}
			}).onPlay(function() {
				if (video_allowed==0){
					jwplayer("videoElement").stop();
					alert('Sorry, this content is not available in your country!');
				} else {
					if (!view_confirmed){
						jQuery.ajax({
							dataType: "json",
							url: "<?php echo $confirm_view; ?>",
							data: {},
							success: function(response) {

							}
						});
						view_confirmed = true;
					}
				}

			});
			</script>
			<?php
				$video_desc = "";
				if (!empty($current_video->ball)){
					$video_desc = $current_video->ball->commentary;
				} else if (!empty($current_video->desc)){
					$video_desc = $current_video->desc;
				}
			?>
			<div class="video-desc <?php echo empty($video_desc)?"hidden":""; ?>">
				<?php if (!empty($current_video->ball)){ ?>
				<span style="background: #fff !important; display: none;" class="entry-date pl10">
					<span class="entry-meta-date"><time><b><?php echo $current_video->ball->title; ?></b>&nbsp;|&nbsp;<b>Out</b></time></span>
				</span>
				<?php } ?>

				<p class="video-arg"><?php echo $video_desc; ?></p>
			</div>
			<div class="cric-lockup-meta">
				<ul class="cric-lockup-meta-info mt5 pb5 pull-right views pl0 hide-sr">
				<li style="list-style:none"><?php echo $current_video->video->views; ?> views</li>
				</ul>
				<div class="<?php echo !$timeline_btn?"hidden":""; ?>">
					<a class="ca-more timeline" href="<?php echo $timeline_url; ?>"><i class="fa fa-code-fork"></i> Go To Match Timeline</a>
				</div>
				<ul class="cric-lockup-meta-info mt20 pb5 pull-left views-left pl0">
					<li style="list-style:none"><?php echo $current_video->video->views; ?> views</li>
                </ul>
            </div>
		</div>
	</div>
	<div class="fb-comments" data-href="" data-width="100%" data-numposts="5"></div>
	<div class="col-md-12 mt40">
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<div class="cs-section-title mb0">
						<h2 class="mb0">Related Videos</h2>
					</div>
				</div>
			</div>
			<div class="gallary col-md-12 pb20">
				<div class="row">
					<div class="row" id="videoListContainter">
						<?php foreach($more_videos as $video){ ?>
							<div class="col-xs-6 col-sm-4 col-md-4 pt20 list_space_mng">
								<a href="<?php echo base_url()."highlights/".$video->id."/".seo_url($video->title)."/"; ?>" >
								<div class="video-container">
									<div class="video-thumbnail">
										<img src="<?php echo $video->med_image; ?>">
										<img src="<?php echo assets_url(); ?>images/play.png" class="video-play-icon">
										<div class="overlay">
										</div>
									</div>
								</div>
								<div class="cric-lockup-content">
									<h3 class="cric-lockup-title mt0 mb0">
										<span title="" class="cric-uix-sessionlink cric-ui-ellipsis cric-ui-ellipsis-2 spf-link"><?php echo $video->title; ?></span>
									</h3>
									<div class="cric-lockup-meta">
										<ul class="cric-lockup-meta-info pl5 mt0 pb5">
											<li><?php echo $video->views; ?> views</li>
										</ul>
									</div>
								</div>
								</a>
							</div>
						<?php
						}
						?>
					</div>
					<div class="clear"></div>
					<div class="pt20 pb20">
	                  <input type="button" id="loadMoreVidosRelatedTimeline" data-video-id="<?php echo $current_video->video->id; ?>" data-page-to-load="2" data-loading-text="Please Wait..." class="btn-more full-width" value="Load More">
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>
