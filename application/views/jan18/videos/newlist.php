<div class="container" style="overflow: hidden;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 crk-trending-videos-container mt-md-10"> 
     <!--  REVOLUTION SLIDER -->
      <div id="revSliderWrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin: 0 auto; padding: 0; margin-top: 0;margin-bottom: 0;"
           data-alias="carousel-gallery"
           data-source="gallery">
        <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
        <div id="revSlider" class="rev_slider fullwidthabanner" style="display: none;"
             data-version="5.4.1">
          <ul>

            <?php $cm=1; ?>
            <?php foreach ($main_series->videos as $key => $value): ?>
              <?php if($cm>3){break;} ?>

                <!-- SLIDE  -->
                <li data-index="rs-<?php echo $key; ?>"
                    data-transition="fade"
                    data-slotamount="default"
                    data-hideafterloop="0"
                    data-hideslideonmobile="off"
                    data-easein="default"
                    data-easeout="default"
                    data-masterspeed="500"
                    data-rotate="0"
                    data-saveperformance="off" class="sup-li" data-location="<?php echo $value->seo_url; ?>">
                  <!-- MAIN IMAGE -->
                  <img class="rev-slidebg" src="<?php echo assets_url(); ?>images/dummy_2.png" alt=""
                       data-lazyload="<?php echo $value->video->med_image; ?>"
                       data-bgposition="center center"
                       data-kenburns="on"
                       data-duration="30000"
                       data-ease="Linear.easeNone"
                       data-scalestart="100"
                       data-scaleend="120"
                       data-rotatestart="0"
                       data-rotateend="0"
                       data-blurstart="0"
                       data-blurend="0"
                       data-offsetstart="0 0"
                       data-offsetend="0 0">

                       <div id="slide-<?php echo $key; ?>-layer-01"> 
                          <figcaption class="figcaption-4bnr-incremnt">
                            <h1 class="">&nbsp;</h1>
                          </figcaption>
                      </div>


                       <div id="slide-<?php echo $key; ?>-layer-0"> 
                        <img  class="video-play-icon img-home-video hidden-xs" src="<?php echo assets_url(); ?>images/play-big.png">
                        <img  class="video-play-icon img-home-video visible-xs" src="<?php echo assets_url(); ?>images/play-big-xs.png">
                      </div>
                  <!-- LAYERS -->

                  <!-- LAYER NR. 1 -->

                  <div class="hidden-xs">
                    
                      <div id="slide-<?php echo $key; ?>-layer-1" class=" middle-caption tp-caption tp-resizeme" style="z-index: 7; min-width: 580px; max-width: 580px; white-space: normal; font-size: 18px; line-height: 20px; font-weight: 500; color: rgba(255,255,255,0.75);font-family: 'Lato', sans-serif;"
                       data-x="['left','left','left','left']"
                       data-y="['bottom','bottom','bottom','bottom']"
                       data-hoffset="['40','40','30','15']"
                       data-voffset="['80','60','55','50']"
                       
                       data-fontsize="['18','18','14','13']"
                       data-width="['580','480','340','260']"
                       data-height="none"
                       data-whitespace="normal"
                       data-type="text"
                       data-basealign="slide"
                       data-responsive_offset="on"
                       data-frames='[
                         {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},
                         {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                       ]'
                       data-textAlign="['left','left','left','left']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"><?php echo $value->video->match_obj->series_short_name; ?>
                  </div>
                  <!-- LAYER NR. 2 -->
                  <a id="slide-<?php echo $key; ?>-layer-2" class=" middle-caption tp-caption tp-resizeme" href="javascript:;" target="_self" style="z-index: 6; min-width: 580px; max-width: 580px; white-space: normal; font-size: 16px!important; line-height: 40px; font-weight: 500; color: rgba(255, 255, 255, 1);font-family: 'Lato', sans-serif; text-decoration: none;"
                     data-x="['left','left','left','left']"
                     data-y="['bottom','bottom','bottom','bottom']"
                     data-hoffset="['40','40','30','15']"
                     data-voffset="['40','40','30','30']"
                     data-fontsize="['25','25','20','20']"
                     data-lineheight="['40','40','25','20']"
                     data-width="['580','480','340','260']"
                     data-height="none"
                     data-whitespace="normal"
                     data-type="text"
                     data-actions=""
                     data-basealign="slide"
                     data-responsive_offset="on"
                     data-frames='[
                       {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":200,"ease":"Power4.easeOut"},
                       {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                     ]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"><?php echo $value->video->title; ?>
                  </a>

                  <!-- LAYER NR. 3 -->
                  <div id="slide-<?php echo $key; ?>-layer-3" class=" middle-caption tp-caption tp-resizeme" style="z-index: 7; min-width: 580px; max-width: 580px; white-space: normal; font-size: 18px; line-height: 20px; font-weight: 500; color: rgba(255,255,255,0.75);font-family: 'Lato', sans-serif;"
                       data-x="['left','left','left','left']"
                       data-y="['bottom','bottom','bottom','bottom']"
                       data-hoffset="['40','40','30','15']"
                       data-voffset="['20','20','10','5']"
                       data-fontsize="['18','18','14','13']"
                       data-width="['580','480','340','300']"
                       data-height="none"
                       data-whitespace="normal"
                       data-type="text"
                       data-basealign="slide"
                       data-responsive_offset="on"
                       data-frames='[
                         {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},
                         {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                       ]'
                       data-textAlign="['left','left','left','left']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]">
                       <?php echo trimer($value->match_obj->title.' - '.$value->video->match_obj->team_1.' vs '.$value->video->match_obj->team_2, 30) ?>
                  </div>


                  </div>


                  <div class="visible-xs">
                    
                    <div id="slide-<?php echo $key; ?>-layer-1" class=" middle-caption tp-caption tp-resizeme" style="z-index: 7; min-width: 580px; max-width: 580px; white-space: normal; font-size: 18px; line-height: 20px; font-weight: 500; color: rgba(255,255,255,0.75);font-family: 'Lato', sans-serif;"
                       data-x="['left','left','left','left']"
                       data-y="['bottom','bottom','bottom','bottom']"

                       data-hoffset="['40','40','30','15']"
                       data-voffset="['40']"
                       
                       data-fontsize="['13']"
                       data-width="['580','480','340','260']"
                       data-height="none"
                       data-whitespace="normal"
                       data-type="text"
                       data-basealign="slide"
                       data-responsive_offset="on"
                       data-frames='[
                         {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},
                         {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                       ]'
                       data-textAlign="['left','left','left','left']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"><?php echo $value->video->match_obj->series_short_name; ?>
                  </div>

                    <a id="slide-<?php echo $key; ?>-layer-2" class=" middle-caption tp-caption tp-resizeme" href="javascript:;" target="_self" style="z-index: 6; min-width: 580px; max-width: 580px; white-space: normal; font-size: 16px!important; line-height: 40px; font-weight: 500; color: rgba(255, 255, 255, 1);font-family: 'Lato', sans-serif; text-decoration: none;"
                     data-x="['left','left','left','left']"
                     data-y="['bottom','bottom','bottom','bottom']"
                     data-hoffset="['40','40','30','15']"
                     data-voffset="['25']"
                     data-fontsize="['14']"
                     data-lineheight="['40','40','25','20']"
                     data-width="['580','480','340','260']"
                     data-height="none"
                     data-whitespace="normal"
                     data-type="text"
                     data-actions=""
                     data-basealign="slide"
                     data-responsive_offset="on"
                     data-frames='[
                       {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":200,"ease":"Power4.easeOut"},
                       {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                     ]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"><?php echo $value->video->title; ?>
                  </a>

                  <div id="slide-<?php echo $key; ?>-layer-3" class=" middle-caption tp-caption tp-resizeme" style="z-index: 7; min-width: 580px; max-width: 580px; white-space: normal; font-size: 18px; line-height: 20px; font-weight: 500; color: rgba(255,255,255,0.75);font-family: 'Lato', sans-serif;"
                       data-x="['left','left','left','left']"
                       data-y="['bottom','bottom','bottom','bottom']"
                       data-hoffset="['40','40','30','15']"
                       data-voffset="['10']"
                       data-fontsize="['13']"
                       data-width="['580','480','340','300']"
                       data-height="none"
                       data-whitespace="normal"
                       data-type="text"
                       data-basealign="slide"
                       data-responsive_offset="on"
                       data-frames='[
                         {"from":"y:20px;opacity:0;","speed":2000,"to":"o:1;","delay":400,"ease":"Power4.easeOut"},
                         {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}
                       ]'
                       data-textAlign="['left','left','left','left']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]">
                       <?php echo trimer($value->match_obj->title.' - '.$value->video->match_obj->team_1.' vs '.$value->video->match_obj->team_2, 30) ?>
                  </div>


                  </div>
                  
                  
                </li>
              <!-- SLIDE  -->
              <?php $cm++; ?>
            <?php endforeach ?>


            
          </ul>

          <!--<div class="tp-bannertimer" style="height: 8px; background: rgba(255, 255, 255, 0.25);"></div>-->
        </div>
      </div><!-- END REVOLUTION SLIDER --> 
      
    </div>
    
    <div class=" clearfix mb-xs-20"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 crk-trending-videos-container">
      <div class="cs-seprator">
        <div class="devider1"></div>
      </div>

    <div id="series-container">
      <?php $c = 1; ?>
      <?php foreach ($series as $key => $s): ?>
        <?php if($c == 1){$c++;continue;} ?>

        <h1>
      <span class="hidden-xs"><?php echo $s->short_name; ?></span>
      <span class="f15 visible-xs"><?php echo $s->short_name; ?></span> 
      
      <span class="f16 color-jazz-grey-light hidden-xs">
        <?php echo date("M j", strtotime($s->start_date)); ?>
        -
        <?php echo date("M j", strtotime($s->end_date)); ?>
      </span>  
     </h1>


      <span class="f13 color-jazz-grey-light visible-xs">
      <?php echo date("M j", strtotime($s->start_date)); ?>
        -
        <?php echo date("M j", strtotime($s->end_date)); ?>
    </span>
      
      
      <!--carousel 1-->
      
      <div id="trending<?php echo $c; ?>" class="owl-carousel owl-theme" style="opacity: 1; display: block;">

        <?php foreach ($s->videos as $key1 => $v): ?>
          <?php if($key1>5){break;} ?>

          <div class="item" onclick="window.location='<?php echo $v->seo_url; ?>'">
                <div class="video-container">
                  
                      <div class="video-thumbnail"> 
                        <img src="<?php echo $v->video->med_image; ?>" class="img-home-video img-responsive" style="opacity: 0;"> 

                        <a href="<?php echo $v->seo_url; ?>">
                          <img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">
                        </a>


                        <div class="overlay"> </div>
              
                      <figcaption>
                        <h2 class="mb0"><?php echo $v->video->match_obj->title.' - '. $v->video->match_obj->team_1.' vs '.$v->video->match_obj->team_2 ?></h2>
                      </figcaption>

                </div>
                  <div class="video-content">
                    <p class="video-description one-liner" title="<?php echo $v->video->title; ?>">
                      <?php echo $v->video->title; ?>
                    </p>
                  </div>
                </div>
              </div>
        

          
        <?php endforeach ?>
         
         
         
         
          
            
      </div>
      <div class=" clearfix" style=" margin-bottom:10px;"></div>
      <!--end carousel 1-->



        <?php $c++; ?>
      <?php endforeach; ?>
    </div> <!-- series-container --> 
      










      <?php if ($current_page < $total_pages): ?>
        <div class="bottom-event-panel mt10 text-center more-series"> 
          <a href="javascript:;" id="more-series-btn" data-page-to-load="2" data-series-count="<?php echo $c; ?>"> More Series</a> 
        </div>
      <?php endif ?>

      <div class=" clearfix" style=" margin-bottom:20px;"></div>
    </div>
    <div class="clearfix"></div>
     
  </div>
  <div class="clear"></div>

  <script type="text/javascript">
    function enableOwls(){
       $('.owl-carousel.owl-theme').each(function(e){
        var $this = $("#"+$(this).attr('id'));
        


          $this.owlCarousel({
          
          pagination: false,
          loop:true,
          merge:true,
          items : 4, //10 items above 1000px browser width
          itemsDesktop : [1000,4], //5 items between 1000px and 901px
          itemsDesktopSmall : [900,3.3], // betweem 900px and 601px
          itemsTablet: [600,2.3], //2 items between 600 and 0
          itemsMobile : [400,1.2]  // itemsMobile disabled - inherit from itemsTablet option
        });
          
      }); 
       setTimeout(function(){
        $('.video-thumbnail > img.img-home-video ').css('opacity', '1');
      },1000);
    }
  </script>

  <script>
    $(document).ready(function(){
      var movement = 'click';
      var timer;
      var time =0;
      $('.sup-li').mousedown(function(e){
        timer = setInterval(function(){
        time += 10;
        },10);
      });

      $('.sup-li').mouseup(function(e){
        console.log('time:'+time);
        if(time < 160){
          window.location = $(this).data("location");
        }
        clearInterval(timer);
        time =0;
      });

      enableOwls();

      
    
    });
  </script>



  <script>
    $(document).on('ready', function () {
      // initialization of carousel
      // $.HSCore.components.HSCarousel.init('.js-carousel');

      
    });

    var tpj = jQuery,
      revAPI;

    tpj(document).ready(function () {
      if (tpj('#revSlider').revolution == undefined) {
        revslider_showDoubleJqueryError('#revSlider');
      } else {
        revAPI = tpj('#revSlider').show().revolution({
          sliderType: 'carousel',
          jsFileLocation: "<?php echo assets_url() ?>"+'js/revolution/js/',
          sliderLayout: 'fullwidth',
          dottedOverlay: 'none',
          delay: 3000,
      navigation: { 
      bullets:{
       style:"",
       enable:true,
       container:"slider",
       rtl:false,
       hide_onmobile:false,
       hide_onleave:false,
       hide_delay:200,
       hide_delay_mobile:1200,
       hide_under:0,
       hide_over:480,
       tmp:'<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>', 
       direction:"horizontal",
       space:4,       
       h_align:"center",
       v_align:"center",
       h_offset:0,
       v_offset:90
      },
      },
          carousel: {
            horizontal_align: 'center',
            vertical_align: 'center',
            fadeout: 'on',
            maxVisibleItems: 5,
            infinity: 'on',
            space: 4,
            stretch: 'off',
            showLayersAllTime: 'off',
            easing: 'Power3.easeInOut',
            speed: '1000'
          },
          responsiveLevels: [1240, 1024, 778, 480],
          visibilityLevels: [1240, 1024, 778, 480],
          gridwidth: [800, 700, 400, 300],
          gridheight: [403, 353, 201, 151],
          /*lazyType: 'single',*/
          shadow: 0,
          spinner: 'on',
          stopLoop: 'off',
          stopAfterLoops: -1,
          stopAtSlide: -1,
          shuffle: 'off',
          autoHeight: 'off',
          hideThumbsOnMobile: 'on',
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: 'off',
            nextSlideOnWindowFocus: 'off',
            disableFocusListener: false,
          }
        });
      }
    });
  </script>   