<?php $user = $this->xession->get("user"); ?>
<div class="navbar navbar-inverse" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <div class="icon-home hidden" style="width: 90px;">
          <a href="<?php echo base_url() ?>">
            <img src="<?php echo assets_url(); ?>images/zain-logo.png" alt="" class="img-responsive"></div>
          </a>




          <div class="ad-user small hidden">

          <?php if ($user){ ?>
           <a href="javascript:;" class="user1" phone="<?php echo $this->data["phone_no"]; ?>" sub_status="<?php echo $this->data["sub_status"] ?>">
          <?php }else{ ?>
           <a href="<?php echo base_url().'login/' ?>">
          <?php } ?>
            <img src="<?php echo assets_url(); ?>/images/user.svg?v=<?php echo VERSION ?>">
          </a>
        </div>


        <div class="ad-logo">
          <a href="<?php echo base_url() ?>">
            <img src="<?php echo assets_url(); ?>images/zain-logo.png?v=<?php echo VERSION ?>">
          </a>
        </div>
        <div class="ad-logo-sm">
          <a href="<?php echo base_url() ?>">
            <img src="<?php echo assets_url(); ?>images/zain-logo.png?v=<?php echo VERSION ?>" style="width: 90px;"></img>
          </a>
        </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <?php if($user): ?>
              <li class="user-status visible-xs">
                <a type="button" class="edit-profile" href="javascript:;" >
                  <i class="fa fa-pencil-square"></i>
                </a>
                <div class="p5">
                  Welcome
                  <strong class="pl10"><?php echo $this->data["phone_no"]; ?></strong>
                  <br>
                  Subscription Status:
                  <strong>
                    <?php
                      $sub_status = $this->data["sub_status"];

                      if ($sub_status==1){
                        echo "Active";
                      } elseif ($sub_status==3){
                        echo "Suspended";
                      } else {
                        echo "Trial";
                      }
                    ?>

                  </strong>
                </div>
              </li>
            <?php endif ?>

            <li class=" <?php echo ($this->uri->segment(1)=='')?"active":""; ?>" >
              <a href="<?php echo base_url(); ?>">
                Home
                <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i>
              </a>
            </li>
            <li class=" hidden<?php echo ($this->uri->segment(1)=='livestream')?"active":""; ?>" ><a href="<?php echo base_url(); ?>livestream">Live Stream <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>
            <li class="<?php echo ($this->uri->segment(1)=='results')?"active":""; ?>"><a href="<?php echo base_url()."results/"; ?>">Results <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>
            <li class="<?php echo ($this->uri->segment(1)=='blog')?"active":""; ?> hidden"><a href="<?php echo base_url()."blog/"; ?>">Blog<i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>
            <li class="<?php echo ($this->uri->segment(1)=='fixtures')?"active":""; ?>"><a href="<?php echo base_url()."fixtures/"; ?>">Fixtures <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>
            <li class="<?php echo ($this->uri->segment(1)=='articles' || $this->uri->segment(1)=='article' || $this->uri->segment(1)=='cricket-articles')?"active":""; ?>" ><a href="<?php echo base_url()."cricket-articles/"; ?>">Articles <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>
            <li class="<?php echo ($this->uri->segment(1)=='news' || $this->uri->segment(1)=='news-update')?"active":""; ?>" ><a href="<?php echo base_url()."news/"; ?>">News <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>
            <li class="<?php echo ($this->uri->segment(1)=='videos' || $this->uri->segment(1)=='highlights')?"active":""; ?>" ><a href="<?php echo base_url()."videos/"; ?>">Videos <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a></li>

            <li class="<?php echo ($this->uri->segment(1)=='rankings')?"active":""; ?>" >
              <a href="<?php echo base_url()."rankings/"; ?>">Rankings
               <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i>
              </a>
            </li>


            <li class="hidden <?php echo ($this->uri->segment(1)=='jazz-rising-star')?"active":""; ?> " >
              <a href="javascipt:;">
                <strong>Jazz Rising Stars</strong>
                <i class="fa fa-star-o"></i>
                <!-- <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i> -->
              </a>
            </li>

            <li class=" <?php echo ($this->uri->segment(1)=='ipl')?"active":""; ?> " >
              <a href="<?php echo base_url('ipl'); ?>">
                IPL
                <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i>
              </a>
            </li>



            <li class="visible-xs">
              <?php if($user): ?>
                  <a href="<?php echo base_url('logout'); ?>">Logout <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a>
              <?php else: ?>
                  <a href="<?php echo base_url('login'); ?>">Login <i class="fa fa-angle-right pull-right mt5 mr-5 visible-xs color-jazz-grey-light"></i></a>
              <?php endif; ?>
            </li>

            <li class="border-left-hide hidden-xs"></li>


        </ul>

         <?php
          if ($user){
         ?>
           <div class="ad-user">
             <a href="javascript:;" class="user1" phone="<?php echo $this->data["phone_no"]; ?>" sub_status="<?php echo $this->data["sub_status"] ?>">
               <img src="<?php echo base_url() ?>assets/jan18/images/user.svg?v=<?php echo VERSION ?>">
             </a>
           </div>
       <?php }else{ ?>
             <div class="ad-user">
               <a href="<?php echo base_url().'login/' ?>">
                 <img src="<?php echo base_url() ?>assets/jan18/images/user.svg?v=<?php echo VERSION ?>">
               </a>
             </div>
       <?php } ?>
          <!-- /.navbar-collapse -->
      </div>
  </div>
  <!-- /.container -->
</div>
