<div id="content" role="main">

	<?php 
		$innings_label = array();
		$innings_label[1] = array("full"=>"1st Innings","small"=>"1st Inn");
		$innings_label[2] = array("full"=>"1st Innings","small"=>"1st Inn");
		$innings_label[3] = array("full"=>"2nd Innings","small"=>"2nd Inn");
		$innings_label[4] = array("full"=>"2nd Innings","small"=>"2nd Inn");
		
	?>
	<div role="tabpanel" class="tab-pane" id="scorecard">
	    <div class="row">
	        <div class="col-md-12">


                <div class="row">
                  <div class="inplay-filters inplay-filters-margin pt-15">
                    <div class="">
                      <ul class="nav nav-tabs nav-side-tab" role="tablist" style=" ">
                      	<?php foreach($match->innings as $key=>$inning){  ?>
			            	<li role="presentation" class="<?php echo (($key+1)==count($match->innings))?"active":""; ?>">
			            		<a href="#tab<?php echo ($key+1); ?>" aria-controls="#tab<?php echo ($key+1); ?>" role="tab" data-toggle="tab" class=" f-16 pt-15">
			            			<span class="visible-xs">
			            				<?php 
			            					echo $teams[$inning->batting_team_id]->short_name;
			            					echo " (".$innings_label[$inning->innings_order]["small"].")"; 
		            					?>
			            					
		            				</span>
			            			<span class="hidden-xs">
			            				<?php echo $teams[$inning->batting_team_id]->name." (".$innings_label[$inning->innings_order]["small"].")"; ?></span>
			            		</a>
			            	</li>
			            <?php } ?> 
                      </ul>
                    </div>
                  </div>
                </div>
               
                    <div style=" width:100%">

                      <div class="tab-content pl-15 pr-15 pt15">

                      	<?php foreach($match->innings as $key=>$inning){  ?>
                        
                        <div role="tabpanel" class="tab-pane <?php echo (($key+1)==count($match->innings))?"active":""; ?>" id="tab<?php echo ($key+1); ?>">
                          <div class="row">


                            <div class="col-md-12">
                              <div class="row">
                                <table class="table table-striped ">
                                  <thead class="bg-batting-head">
                                    <tr>
                                      <th colspan="2" class="text-left">
                                      	<?php echo $teams[$inning->batting_team_id]->name ?>  Batting
                                      </th>
                                      <th width="20">
                                      	<span class="hidden-xs">Runs</span>
                                      	<span class="visible-xs">R</span>
                                      </th>
                                      <th width="20">
                                      	<span class="hidden-xs">Balls</span>
                                      	<span class="visible-xs">B</span>
                                      </th>
                                      <th width="20" class="hidden-xss">4s</th>
                                      <th width="20" class="hidden-xss">6s</th>
                                      <th width="20">SR</th>
                                    </tr>
                                  </thead>
                                  <tbody>

                                  	<?php foreach($inning->batting_scorecard as $batsman){ ?>
                                  	<?php //echo "<script>console.log(".json_encode($batsman).")</script>"; ?>
									<tr>
										<td class="visible-xs hidden-sm hidden-md text-left" colspan="2">
											<strong><?php echo $batsman->player->name; ?></strong>
											<BR>
											<?php echo $batsman->batsman->out_details?$batsman->batsman->out_details:"(not out)"; ?>
										</td>
										<td class="hidden-xs text-left">
											<strong><?php echo $batsman->player->name; ?></strong>
										</td>
										<td class="hidden-xs text-left">
											<?php echo $batsman->batsman->out_details?$batsman->batsman->out_details:"(not out)"; ?>
										</td>
										<td class="bold">
											<?php echo $batsman->batsman->runs_scored; ?>
										</td>
										<td class="">
											<?php echo $batsman->batsman->balls_played; ?>
										</td>
										<td class="hidden-xss">
											<?php echo $batsman->batsman->boundry_4s_scored; ?>
										</td>
										<td class="hidden-xss">
											<?php echo $batsman->batsman->boundry_6s_scored; ?>
										</td>
										<td class="">
											<?php echo $batsman->batsman->strike_rate; ?>
										</td>
									</tr>
									<?php } ?>
                                     
									<tr class=" ">
										<td class="text-left">
											Extras
										</td>
										<td class="text-left">
										<?php
											$total_extras = 0;
											$string = array();
											if ($inning->extra_leg_bye>0){
												$string[] = "lb ".$inning->extra_leg_bye;
												$total_extras += $inning->extra_leg_bye;
											}
											if ($inning->extra_bye>0){
												$string[] = "b ".$inning->extra_bye;
												$total_extras += $inning->extra_bye;
											}
											if ($inning->wide_ball>0){
												$string[] = "wd ".$inning->wide_ball;
												$total_extras += $inning->wide_ball;
											}
											if ($inning->no_ball>0){
												$string[] = "nb".$inning->no_ball;
												$total_extras += $inning->no_ball;
											}
											if (!empty($string)){
												echo "(".implode(", ",$string).")";
											} else {
												echo "-";
											}
										?>
										</td>
										<td class="">
											<?php echo $total_extras; ?>
										</td>
										<td class="bold"></td>
										<td class="bold"></td>
										<td class="bold hidden-xs"></td>
										<td class="bold hidden-xs"></td>
									</tr>


                                    <tr class="total">
                                      <td class="text-left text-uppercase"><strong>total</strong></td>
                                      <td class="text-left">(<?php echo $inning->wickets; ?> wickets, <?php echo $inning->overs; ?> overs)</td>
                                      <td><strong><?php echo $inning->runs; ?></strong></td>
                                      <td colspan="4" class="text-left">(<?php echo $inning->run_rate; ?> runs/over)</td>
                                    </tr>

                                  </tbody>
                                </table>

                                 <div class="col-md-12 mb-20">
                                  <div class="row">

                                    <div class="col-md-1 col-sm-2 fow-border-right">
                                      <p class="m0 f12-xs"><strong>Fall of Wickets</strong></p>
                                    </div>

                                    <div class="col-md-11 col-sm-10 border-left f12-xs">
                                      <p class="mt0 f12-xs">
                                      	<?php 
											$wickets = array();
											foreach($inning->falls_of_wickets as $key=>$wicket){
												$wickets[] = "<span>$wicket->wicket_order-$wicket->team_score (".$wicket->out_batsman->player->short_name.", $wicket->ball ov)</span>";
											}
											$wickets = implode(", ", $wickets);
											echo $wickets;
										?>
                                      </p>
                                    </div>

                                  </div>
                                </div>

                              </div>
                            </div>


                            <div class="col-md-12 mt20">
                              <div class="row">
                                <table class="table table-striped ">
                                  <thead class=" bg-bowling-head">
                                    <tr>
                                      <th class="text-left">Bowling</th>
                                      <th width="20">O</th>
                                      <th width="20">M</th>
                                      <th width="20">R</th>
                                      <th width="20">W</th>
                                      <th width="20" class="hidden-xss">0s</th>
                                      <th width="20" class="hidden-xss">4s</th>
                                      <th width="20" class="hidden-xss">6s</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php foreach($inning->bowling_scorecard as $bowler){ ?>
										<tr>
											<td class="text-left">
												<strong><?php echo($bowler->player->short_name)?$bowler->player->short_name : $bowler->player->name; ?></strong>
											</td>
											<td><?php echo $bowler->bowler->overs_bowled; ?></td>
											<td><?php echo $bowler->bowler->overs_maiden; ?></td>
											<td><?php echo $bowler->bowler->runs_given; ?></td>
											<td><?php echo $bowler->bowler->wickets_taken; ?></td>
											<td class="hidden-xss"><?php echo $bowler->bowler->dots_bowled; ?></td>
											<td class="hidden-xss"><?php echo $bowler->bowler->boundry_4s_given; ?></td>
											<td class="hidden-xss"><?php echo $bowler->bowler->boundry_6s_given; ?></td>
										</tr>
									<?php } ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>


                          </div>
                        </div>

                        <?php } ?>

                    


                        </div>
                      </div>
            </div>
		</div>
    </div>


</div>