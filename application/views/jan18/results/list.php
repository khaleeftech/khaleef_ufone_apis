<div id="c-calend" class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container">

  <div class="cs-seprator mt30 hidden-xs">
    <div class="devider1"> </div>
  </div>

	<h1 class='hidden-xs'><?php echo $page_heading; ?></h1>
  <h1 class="visible-xs f20"><?php echo $page_heading; ?></h1>

  <div class="col-md-12 results mt-xs-10">
    <div class="row-5-xs">
      <div class="tabs animated-slide-2 matches-tbs">
        <div class="tab-content">
          <div id="results" class="tab active">
            <div class="event event-listing event-samll">


              <div id="fixtures-container" class="">
                <?php foreach($results->matches as $fixture): ?>
                  <?php
                    $flag1 = assets_url().'images/flag-placeholder.jpg';
                    $flag2 = assets_url().'images/flag-placeholder.jpg';
                    if(!empty($fixture->team_1->team->flag_url) && (strpos($fixture->team_1->team->flag_url, 'missing.png') === false && strpos($fixture->team_1->team->flag_url, 'placeholder.jpg') === false)){
                      $flag1 = $fixture->team_1->team->flag_url;
                    }
                    if(!empty($fixture->team_2->team->flag_url) && (strpos($fixture->team_2->team->flag_url, 'missing.png') === false && strpos($fixture->team_2->team->flag_url, 'placeholder.jpg') === false)){
                      $flag2 = $fixture->team_2->team->flag_url;
                    }

                    $team_1_a_score = '';
                    $team_2_a_score = '';

                    $team_1_a_overs = '';
                    $team_2_a_overs = '';

                    $team_1_class = '';
                    $team_2_class = '';

                    $is_test = '';

                    if(strtolower($fixture->format)  ==  'test' ){
                      $is_test = 1;
                    }

                    if($fixture->match_won_by_id == $fixture->team_1->id){
                      $team_1_class = 'match-won';
                    }
                    if($fixture->match_won_by_id == $fixture->team_2->id){
                      $team_2_class = 'match-won';
                    }

                    foreach ($fixture->innings as $keyI => $i) {
                      if($keyI <= 1){
                        if($i->batting_team_id == $fixture->team_1->id){
                          $team_1_a_score = $i->runs."/".$i->wickets;
                          $team_1_a_overs = "(".$i->overs." overs)";
                        }else{
                            $team_2_a_score = $i->runs."/".$i->wickets;
                            $team_2_a_overs = "(".$i->overs." overs)";
                        }
                      }else{

                        if($i->batting_team_id == $fixture->team_1->id){
                          $team_1_b_score = $i->runs."/".$i->wickets;
                          $team_1_b_overs = "(".$i->overs." overs)";
                        }else{
                            $team_2_b_score = $i->runs."/".$i->wickets;
                            $team_2_b_overs = "(".$i->overs." overs)";
                        }

                      }

                    }



                  ?>


                  <div class="col-md-12 col-xs-12 event-fixture-box">
                    <div class="">

                       <div class="col-xs-12 visible-xs  pb20">

                        <h2 class="fw-normal text-left  m0">
                          <a class="color-grey-lighter f16" href="<?php echo $fixture->seo_url; ?>">
                           <?php echo $fixture->local_time->date; ?> <?php echo $fixture->local_time->month; ?>
                          </a>
                        </h2>

                        <h4 class="m0 lh28 fw-normal text-left">
                          <a class="color-grey-light mt-5" href="<?php echo $fixture->seo_url; ?>">
                            <?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle')?$fixture->title:'' ; ?><?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle' && !empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle' )?': ' :'' ; ?><?php echo(!empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle')?$fixture->series->short_name:'' ; ?>
                          </a>
                        </h4>

                      </div>


                      <div class="col-md-2 col-sm-3 col-xs-6 ">
                        <div class="row">

                          <div class="team-first">
                            <ul style="text-align: center;">

                              <li>
                                <a href="<?php echo $fixture->seo_url; ?>">
                                  <img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
                                </a>
                              </li>

                              <li>
                                <h4 class="color-jazz-grey-darker mb0 one-liner2 <?php echo $team_1_class ?>"><?php echo $fixture->team_1->team->short_name; ?></h4>
                              </li>

                              <?php if ($is_test): ?>

                                <li>
                                  <h4 class="color-jazz-grey-light mt0 mb0">
                                     <span class="pr5 <?php  echo(empty($team_1_b_score))? $team_1_class : ''; ?>">
                                      <?php echo $team_1_a_score; ?>
                                      </span>
                                      <?php if (!empty($team_1_b_score)): ?>
                                        <span class="color-jazz-grey-darker f12">&amp;</span>
                                        <span class="pl5 <?php echo $team_1_class; ?>">
                                          <?php echo $team_1_b_score; ?>
                                        </span>
                                      <?php endif ?>

                                   </h4>
                                </li>

                                <li>
                                  <span class="f12 pr10"><?php echo $team_1_a_overs; ?></span>
                                  <span class="f12"><?php echo $team_1_b_overs; ?></span>
                                </li>

                             <?php else: ?>

                                <li>
                                  <h4 class="color-jazz-grey-darker mt0 mb0 <?php echo $team_1_class ?>"><?php echo $team_1_a_score; ?></h4>
                                </li>
                                <li>
                                  <span class="f12"><?php echo $team_1_a_overs; ?></span>
                                </li>

                              <?php endif ?>


                            </ul>
                          </div>

                        </div>
                      </div>


                      <div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">
                        <article class="pb0">
                          <div class="text-center">

                            <h2 class="fw-normal ">
                              <a class="color-grey-lighter" href="<?php echo $fixture->seo_url; ?>"><?php echo $fixture->local_time->date; ?> <?php echo $fixture->local_time->month; ?></a>
                            </h2>

                            <h4 class="m0 lh28 fw-normal ">
                              <a class="color-grey-light" href="<?php echo $fixture->seo_url; ?>">
                                <?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle')?$fixture->title:'' ; ?><?php echo(!empty($fixture->title) && str_replace(' ', '', strtolower($fixture->title))  !== 'missingtitle' && !empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle' )?': ' :'' ; ?><?php echo(!empty($fixture->series->short_name) && str_replace(' ', '', strtolower($fixture->series->short_name))  !== 'missingtitle')?$fixture->series->short_name:'' ; ?>
                              </a>
                             </h4>

                            <h1 class="post-option mt0 pb0">
                              <span class="color-grey f18"><?php echo $fixture->match_result; ?> </span>
                            </h1>
                          </div>

                        </article>

                      </div>

                      <div class="col-md-2 col-sm-3 col-xs-6 ">
                        <div class="row">
                          <div class="team-second">
                            <ul style="text-align: center;">
                              <li>
                                <a href="<?php echo $fixture->seo_url; ?>">
                                  <img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
                                </a>
                              </li>

                              <li>
                                <h4 class="color-jazz-grey-darker mb0 one-liner2 <?php echo $team_2_class ?>"><?php echo $fixture->team_2->team->short_name; ?></h4>
                              </li>

                               <?php if ($is_test): ?>

                                <li>
                                  <h4 class="color-jazz-grey-light mt0 mb0">
                                     <span class="pr5 <?php  echo(empty($team_2_b_score))? $team_2_class : ''; ?>">
                                      <?php echo $team_2_a_score; ?>
                                      </span>
                                      <?php if (!empty($team_2_b_score)): ?>
                                        <span class="color-jazz-grey-darker f12">&amp;</span>
                                        <span class="pl5 <?php echo $team_2_class; ?>">
                                          <?php echo $team_2_b_score; ?>
                                        </span>
                                      <?php endif ?>

                                   </h4>
                                </li>

                                <li>
                                  <span class="f12 pr10"><?php echo $team_2_a_overs; ?></span>
                                  <span class="f12"><?php echo $team_2_b_overs; ?></span>
                                </li>

                             <?php else: ?>

                                <li>
                                  <h4 class="color-jazz-grey-darker mt0 mb0 <?php echo $team_2_class ?>"><?php echo $team_2_a_score; ?></h4>
                                </li>
                                <li>
                                  <span class="f12"><?php echo $team_2_a_overs; ?></span>
                                </li>

                              <?php endif ?>



                              </ul>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-8 col-xs-12 visible-xs">
                        <article class="mt20">
                          <div class="text-center">


                            <h1 class="post-option mt0">
                              <span class="color-grey f16"><?php echo $fixture->match_result; ?>  </span>
                            </h1>
                          </div>

                        </article>

                      </div>

                    </div>
                  </div>



                <?php endforeach; ?>
              </div><!-- #fixtures-container -->


              <div class="bottom-event-panel">
                <a href="javascript:;" id="more-results-btn" data-page-to-load="2"> More Results</a>
              </div><!-- bottom-event-panel -->



            </div><!-- event event-listing event-samll -->
          </div><!-- #results -->
        </div><!-- tab-content -->
      </div><!-- tabs animated-slide-2 matches-tbs -->
    </div><!-- row-5-xs -->
  </div><!-- col-md-12  -->

</div> <!-- c-calend -->
