<style media="screen">
.jw-icon-fullscreen {
	display: none;
}
#timeline:before{
	height:auto!important;
	min-height:auto!important;
}
ul#timeline, body, html{
	overflow: auto!important;
	-webkit-overflow-scrolling: touch!important;
}
#timeline > li{
	height:100%;
}
@media only screen and (max-width:721px;){
	.jwplayer{
		max-height: 300px;
	}
}
</style>
<?php
	$teamA = "";
	$teamB = "";
	$innings = $live->innings;
	$current_inning = count($innings)-1;
	$batting_team_id = $innings[$current_inning]->batting_team_id;
	$fielding_team_id = $innings[$current_inning]->fielding_team_id;

	$teamA = $teams[$batting_team_id];
	$teamB = $teams[$fielding_team_id];

	$batsman_onstrike = "";
	$batsman_offstrike = "";

	$bowler_onspell = "";
	$bowler_offspell = "";

	if (!empty($live->partnership_and_bowlers)){
		if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="facing"){
			$batsman_onstrike = $live->partnership_and_bowlers->batsman_1;
		} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="facing"){
			$batsman_onstrike = $live->partnership_and_bowlers->batsman_2;
		}

		if (!empty($live->partnership_and_bowlers->batsman_1) && $live->partnership_and_bowlers->batsman_1->batsman->state=="batting"){
			$batsman_offstrike = $live->partnership_and_bowlers->batsman_1;
		} else if (!empty($live->partnership_and_bowlers->batsman_2) && $live->partnership_and_bowlers->batsman_2->batsman->state=="batting"){
			$batsman_offstrike = $live->partnership_and_bowlers->batsman_2;
		}

		if (!empty($live->partnership_and_bowlers->bowler)){
			$bowler_onspell = $live->partnership_and_bowlers->bowler;
		}
		if (!empty($live->partnership_and_bowlers->last_bowler)){
			$bowler_offspell = $live->partnership_and_bowlers->last_bowler;
		}
	}

	$banner = empty($live->banner_file_name)?$default_banner: $live->full_banner_url;

	$innings[$current_inning]->runs = empty($innings[$current_inning]->runs)?0:$innings[$current_inning]->runs;
	$innings[$current_inning]->wickets = empty($innings[$current_inning]->wickets)?0:$innings[$current_inning]->wickets;
	$innings[$current_inning]->overs = empty($innings[$current_inning]->overs)?"0.0":$innings[$current_inning]->overs;
	$innings[$current_inning]->run_rate = empty($innings[$current_inning]->run_rate)?"0.0":$innings[$current_inning]->run_rate;

?>

<?php
// echo "<pre>";
// 	print_r($teamB);
// echo "</pre>";
//
// echo "<br>";
// echo "<br>";
//
// echo "<pre>";
// 	print_r($teamA);
// echo "</pre>";
//
// echo "<br>";
// echo "<br>";
// echo "<pre>";
// 	print_r($batting_team_previous_innings);
// echo "</pre>";
//
// exit;
?>


<?php if ($is_live ): ?>
	<?php
		$flag1 = assets_url().'images/flag-placeholder.jpg';
        $flag2 = assets_url().'images/flag-placeholder.jpg';
        if(!empty($teamA->flag_url) && (strpos($teamA->flag_url, 'missing.png') === false && strpos($teamA->flag_url, 'placeholder.jpg') === false)){
          $flag1 = $teamA->flag_url;
        }
        if(!empty($teamB->flag_url) && (strpos($teamB->flag_url, 'missing.png') === false && strpos($teamB->flag_url, 'placeholder.jpg') === false)){
          $flag2 = $teamB->flag_url;
        }

	 ?>

	<div class="image-wrapper bg-gradient-red1" >

		<?php if (!empty($live_stream) && !$stream_allowed){ ?>
			<div class="live-stream hidden-xs">
				<a href="javascript:;" class="buy_stream_anchor" data-loading-text="Please wait...">Watch Live Stream Now!</a>
					<!-- <span>Watch Live Stream</span> -->
				</a>
			</div>
		<?php } ?>

		<div class="top-banner-scorecard" id="liveMatch<?php echo $live->id; ?>">


      <div class="hidden-md hidden-lg hidden-sm header-watch" style="">
        <h2 class="series-xs" style="">
					<?php echo $teamA->short_name.' vs '.$teamB->short_name; ?>
				</h2>

			<?php if (!empty($live_stream) && !$stream_allowed){ ?>
		  	<a class="btn-watch-live buy_stream_anchor"  data-loading-text="Please wait..." href="javascript:;">
					Watch Live!
					<img src="<?php echo assets_url(); ?>images/arrow-right.png" alt="" style="width: 32px;" class="pull-right">
				</a>
			<?php } ?>
      </div>


      <div class="caption-inner-wrap scorecards">

        <div class="vs">
					<span>vs</span>
				</div>

        <div class="col-xs-12 team-card bg-white visible-xs">
          <div class="row">

            <div class="col-xs-6 batting">
	              <div class="team-name">

	                <h1 class="text-uppercase">
										<span class="teamtitle mini">
											<?php echo $teamA->short_name; ?>*
										</span>

										<span class="teamtoss <?php echo($teams[$live->toss_won_by_id]->id == $teamA->id )?'' : 'hidden'; ?>">
										 	<img class="toss" src="<?php echo assets_url(); ?>images/toss.png" alt="Won the toss" title="Won the toss">
									 	</span>


									</h1>

	                <div class="clearfix"></div>
	            </div>
		  	</div>

          <div class="col-xs-6 bowling">
            <div class="team-name pull-right">
              <h1 class="text-uppercase">
								<span class="teamtoss <?php echo($teams[$live->toss_won_by_id]->id == $teamB->id )?'' : 'hidden'; ?>">
								 	<img class="toss" src="<?php echo assets_url(); ?>images/toss.png" alt="Won the toss" title="Won the toss">
							 	</span>
								<span class="teamtitle mini"><?php echo $teamB->short_name; ?></span>
							</h1>
          	</div>
		  </div>

		  </div>
	  </div>

		<!-- ============== MOBILE SCORE ========================== -->
    <div class="col-xs-12 team-card bg-white visible-xs boder-bottom-xs">
		  <div class="row">




			  <div class="col-xs-6 batting">
						<?php if ($live->format!='Test'){ ?>

							<div class="team-score-xs">
								<h4 class="active pull-left teamscore">
									<?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?>
								</h4>
							</div>

						<?php } else { ?>

							<?php
								$batting_team_previous_innings = false;
								foreach($innings as $inn){
									if ($inn->batting_team_id==$batting_team_id && $inn->id!=$innings[$current_inning]->id){
										$batting_team_previous_innings = $inn;
										break;
									}
								}
								if ($batting_team_previous_innings){
									$declared = $batting_team_previous_innings->declared?'d':'';
								?>

									<div class="team-score-xs">
										<h4 class="pull-left hidden">
											<?php echo $batting_team_previous_innings->runs."/".$batting_team_previous_innings->wickets.$declared; ?> <span class="mr10 ml10">&amp;</span></h4>
										<h4 class="active pull-left teamscore">
											<?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?>
										</h4>
									</div>


								<?php }else{ ?>

									<div class="team-score-xs">
										<h4 class="active pull-left teamscore"><?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?> </h4>
									</div>

								<?php } ?>

						<?php } ?>


				  <div class="team-flag">
						<img class="teamflag" src="<?php echo $flag1; ?>" alt="">
					</div>


				  <div class="col-xs-12">
				  	<div class="row">

				  		<div class="team-runrate-xs">
							  <h4 class="pull-left teamovers">(<?php echo $innings[$current_inning]->overs; ?> ov)</h4>
					  	</div>

						</div>
				  </div>

				</div>




			  <div class="col-xs-6 bowling">

					<?php if ($live->format!='Test'){ ?>

						<div class="team-score-xs">
							<h4 class="active pull-right teamscore">
								<?php echo(!empty($innings[$current_inning-1]->runs))? $innings[$current_inning-1]->runs."/".$innings[$current_inning-1]->wickets : '-'; ?>
							</h4>
						</div>

					<?php } else { ?>

						<?php
							$fielding_team_first_inning = false;
							$fielding_team_second_inning = false;
							foreach($innings as $inn){
								if ($inn->batting_team_id==$fielding_team_id){
									if (!$fielding_team_first_inning){
										$fielding_team_first_inning = $inn;
									} else {
										$fielding_team_second_inning = $inn;
									}
								}
							}
							$declared_1 = $fielding_team_first_inning->declared?'d':'';
							$declared_2 = @$fielding_team_second_inning->declared?'d':'';


							if ($fielding_team_second_inning){
						?>
								<div class="team-score-xs">
									<h4 class="active pull-right teamscore"><?php echo $fielding_team_second_inning->runs."/".$fielding_team_second_inning->wickets.$declared_2; ?></h4>
									<h4 class="pull-right hidden"><?php echo $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1; ?> <span class="mr10 ml10">&amp;</span></h4>
								</div>

						<?php
							} else if ($fielding_team_first_inning) {
						?>

						<div class="team-score-xs">
							<h4 class="active pull-right teamscore">
								<?php echo $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1; ?>
							</h4>
						</div>

					<?php }else{ ?>
						<div class="team-score-xs">
							<h4 class="active pull-right">
								-
							</h4>
						</div>

					<?php } ?>


					<?php } ?>



				  <div class="team-flag pull-left">
						<img class="teamflag" src="<?php echo $flag2; ?>" alt="">
					</div>

				  <div class="col-xs-12">
				  	<div class="row">
				  		<div class="team-runrate-xs">
							  <h4 class="pull-right teamovers">
									<?php echo !empty($innings[$current_inning-1])?" (".$innings[$current_inning-1]->overs." ov) ":"-"; ?>
								</h4>
					  	</div>
						</div>
			  	</div>

				</div>

			</div>
		</div>

		<!-- ---------------- MOBILE SCORE -------------------- -->


			<!-- ========= LEFT SIDE DESKTOP ================= -->
          <div class="col-md-6 col-sm-6 col-xs-12 team-card bg-white batting">
             <div class="team-header">
                <div class="team-name">
                   <h1 class="text-uppercase">
										 <span class="teamtitle full">
										 	<?php echo $teamA->name; ?>*
										 </span>

										 	<span class="teamtoss <?php echo($teams[$live->toss_won_by_id]->id == $teamA->id )?'' : 'hidden'; ?>">
											 	<img class="toss" src="<?php echo assets_url(); ?>images/toss.png" alt="Won the toss" title="Won the toss">
										 	</span>

									 </h1>
                   <!--<h2>Pakistan <img class="toss" src="images/toss.png" alt="Won the toss" title="Won the toss"></h2>-->
                   <div class="clearfix"></div>
                </div>

                <div class="team-flag">
									<img class="teamflag" src="<?php echo $flag1 ?>" alt="">
				</div>
             </div>



             <div class="team-content">





					 	 	<?php if ($live->format!='Test'){ ?>

								<div class="team-score">
				                   <h4 class="active pull-left teamscore"> <?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?> </h4>
				                </div>

								<div class="clearfix"></div>

				                <div class="team-runrate">
				                   <h4 class="pull-left teamovers">(<?php echo $innings[$current_inning]->overs; ?> ov) </h4>
				                   <h4 class="pull-right run-rate"> RR: <?php echo $innings[$current_inning]->run_rate; ?> </h4>
				                </div>


						  <?php } else { ?>





								<?php
									$batting_team_previous_innings = false;
									foreach($innings as $inn){
										if ($inn->batting_team_id==$batting_team_id && $inn->id!=$innings[$current_inning]->id){
											$batting_team_previous_innings = $inn;
											break;
										}
									}
									if ($batting_team_previous_innings){
										$declared = $batting_team_previous_innings->declared?'d':'';
									?>
										<div class="team-score">
											<h4 class=" pull-left">
												<?php echo $batting_team_previous_innings->runs."/".$batting_team_previous_innings->wickets.$declared; ?>
												<br>
												(<?php echo $batting_team_previous_innings->overs;?>)
											</h4>
											<h4 class=" pull-left">
												<span class="ml10">&amp;</span>
											</h4>
											 <h4 class="active pull-left ml10">
												 <span class="teamscore">
												 	<?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?>
												 </span>
												 <br>
												 <span class="teamovers">
												 	(<?php echo $innings[$current_inning]->overs; ?>)
												 </span>
											 </h4>
										</div>

										<div class="clearfix"></div>
										<div class="team-runrate">
											 <h4 class="pull-right runrate"> RR: <?php echo $innings[$current_inning]->run_rate; ?> </h4>
										</div>
									<?php }else{ ?>
										<div class="team-score">
											 <h4 class="active pull-left">
											 	<span class="teamscore">
											 		<?php echo $innings[$current_inning]->runs."/".$innings[$current_inning]->wickets; ?>
											 	</span>
											 	<br>
											 	&nbsp;
											 </h4>
						                </div>

										<div class="clearfix"></div>

						                <div class="team-runrate">
						                   <h4 class="pull-left teamovers">(<?php echo $innings[$current_inning]->overs; ?> ov) </h4>
						                   <h4 class="pull-right run-rate"> RR: <?php echo $innings[$current_inning]->run_rate; ?> </h4>
						                </div>

									<?php } ?>


						<?php } ?>







            <div class="clearfix"></div>
              <div class="row ">

					<div class="inplay">
                    	<div class="table-responsive">
                       		<table class="table">
                          		<thead>
		                             <tr class="bating">
		                                <th class="width-75p text-left pl-15">Bating</th>
		                                <th width="50" class="text-right"><span class=" visible-lg visible-md">Runs</span><span class=" visible-sm visible-xs">R</span></th>
		                                <th width="50" class="text-right"><span class=" visible-lg visible-md">Balls</span><span class=" visible-sm visible-xs">B</span></th>
		                                <th width="60" class="text-right pr-20"><span class="">SR</span></th>
		                             </tr>
                          		</thead>
                          		<tbody>

                             		<tr>

                                		<td class="text-left pl-15">
											<span class="active-bold  visible-lg visible-md onstrike">
												<?php echo !empty($batsman_onstrike)?$batsman_onstrike->player->name."*":"-"; ?>
											</span>
											<span class="active-bold  visible-sm visible-xs onstrike-short">
												<?php echo !empty($batsman_onstrike)?$batsman_onstrike->player->short_name."*":"-"; ?>
											</span>
										</td>

                                		<td class="active-bold text-right">
											<span class=" pr-5 active-bold onstrike-score">
												<?php echo ( !empty($batsman_onstrike) )? $batsman_onstrike->batsman->runs_scored :'-' ; ?>
											</span>
										</td>

                                <td class="text-right">
																	<span class=" pr-5 onstrike-balls">
																		<?php echo ( !empty($batsman_onstrike) )? $batsman_onstrike->batsman->balls_played :'-' ; ?>
																	</span>
																</td>
                                <td class="text-right">
																	<span class=" pr-5 onstrike-rate">
																		<?php echo ( !empty($batsman_onstrike) )? $batsman_onstrike->batsman->strike_rate :'-' ; ?>
																	</span>
																</td>
                             </tr>

                             <tr>
                                <td class="text-left pl-15">
																	<span class="visible-lg visible-md offstrike">
																		<?php echo !empty($batsman_offstrike)?$batsman_offstrike->player->name:"-"; ?>
																	</span>
																	<span class="visible-sm visible-xs offstrike-short">
																		<?php echo !empty($batsman_offstrike)?$batsman_offstrike->player->short_name:"-"; ?>
																	</span>
																</td>
                                <td class="text-right">
																	<span class=" pr-5  offstrike-score">
																		<?php echo ( !empty($batsman_offstrike) )? $batsman_offstrike->batsman->runs_scored :'-' ; ?>
																	</span>
																</td>
                                <td class="text-right">
																	<span class=" pr-5  offstrike-balls">
																		<?php echo ( !empty($batsman_offstrike) )? $batsman_offstrike->batsman->balls_played :'-' ; ?>
																	</span>
																</td>
                                <td class="text-right">
																	<span class=" pr-5  offstrike-rate">
																		<?php echo ( !empty($batsman_offstrike) )? $batsman_offstrike->batsman->strike_rate :'-' ; ?>
																	</span>
																</td>
                             </tr>
                          </tbody>
                       </table>
                    </div>
                 </div>


              </div>
              <div class="clearfix"></div>


              <div class="tobat">
                 <div class="needs">
                 	<span class="tagline">
								 	<?php if ($live->format!='Test'){ ?>

	 		            	<?php if (count($innings)==1){ ?>
												<?php echo $teams[$live->toss_won_by_id]->short_name." won the toss and choose to ".($live->chose_to=="Bowl"?"field":"bat")." first"; ?>
	 									<?php } else { ?>
												<?php echo "Need ".($innings[$current_inning]->runs_required+1-$innings[$current_inning]->runs)." runs from ".$innings[$current_inning]->balls_remaining." balls"; ?>
	 									<?php } ?>
	 								<?php } else { ?>
	 									<strong>
											<?php echo $live->title; ?>
										</strong>
										<?php echo !empty($live->day)?" | Day $live->day":''; ?><?php echo !empty($live->break_type)?" | $live->break_type":" | Session $live->session"; ?>
	 								<?php } ?>

					</span>
				 </div>
              </div>



						</div> <!-- team-content -->
          </div> <!-- col-md-6 col-sm-6 col-xs-12 team-card bg-white -->


				<!-- --------------- LEFT SIDE DESKTOP --------------------------------   -->



					<!-- ========= RIGHT SIDE DESKTOP ================= -->
					<div class="col-md-6 col-sm-6 col-xs-12 team-card bg-white bowling">

									 <div class="team-header">
											<div class="team-name pull-right">
												 <h1 class="text-uppercase">
													 	<span class="teamtoss <?php echo($teams[$live->toss_won_by_id]->id == $teamB->id )?'' : 'hidden'; ?>">
														 	<img class="toss" src="<?php echo assets_url(); ?>images/toss.png" alt="Won the toss" title="Won the toss">
														</span>
													 <span class="teamtitle full">
													 	<?php echo $teamB->name; ?>
													 </span>
												 </h1>
											</div>
											<div class="team-flag pull-left">
												<img class="teamflag" src="<?php echo $flag2; ?>" alt="">
											</div>
									 </div>


									 <div class="team-content">


										 <?php if ($live->format!='Test'){ ?>

												<div class="team-score">
													 <h4 class=" pull-right">
													 	<span class="teamscore">
													 		<?php echo !empty($innings[$current_inning-1])?$innings[$current_inning-1]->runs."/".$innings[$current_inning-1]->wickets:"-"; ?>
													 	</span>
													 </h4>
												</div>


												<div class="clearfix"></div>


												<div class="team-runrate">
													 <h4 class="pull-left c-gray-light run-rate">
														 <?php echo !empty($innings[$current_inning-1])? 'RR: '.$innings[$current_inning-1]->run_rate : "-" ; ?>
													 </h4>
													 <h4 class="pull-right c-gray-light teamovers">
													 	<?php echo !empty($innings[$current_inning-1])?" (".$innings[$current_inning-1]->overs." ov) ":"-"; ?>
													 </h4>
												</div>




											<?php } else { ?>
											<?php
                    							$fielding_team_first_inning = false;
												$fielding_team_second_inning = false;
					                    		foreach($innings as $inn){
					                    			if ($inn->batting_team_id==$fielding_team_id){
					                    				if (!$fielding_team_first_inning){
					                    					$fielding_team_first_inning = $inn;
					                    				} else {
					                    					$fielding_team_second_inning = $inn;
					                    				}
					                    			}
					                    		}
													$declared_1 = $fielding_team_first_inning->declared?'d':'';
													$declared_2 = @$fielding_team_second_inning->declared?'d':'';


													if ($fielding_team_second_inning){
												?>
													<div class="team-score">
														 <h4 class=" pull-right" style="text-align:right;">
															 <span class="teamscore">
															 	<?php echo $fielding_team_second_inning->runs."/".$fielding_team_second_inning->wickets.$declared_2; ?>
															 </span>
															 <br>
															 <span class="teamovers">
															 	(<?php echo $fielding_team_second_inning->overs; ?>)
															 </span>
														 </h4>
														 <h4 class="pull-right">
														 	<span class="mr10 ml10">&amp;</span>
														</h4>
														 <h4 class="pull-right"  style="text-align:left;">
															<?php echo $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1; ?>
															<br>
															(<?php echo $fielding_team_first_inning->overs; ?>)
														 </h4>

													</div>

													<div class="clearfix"></div>


													<div class="team-runrate">
														 <h4 class="pull-right c-gray-light" style="color:#ccc; text-transform:none;">
															 &nbsp;
														 </h4>
													</div>

												<?php
											} else if ($fielding_team_first_inning) {
												?>
													<div class="team-score">
	                           <h4 class=" pull-right" style="text-align:right;">
	                           								<span class="teamscore">
															 <?php echo $fielding_team_first_inning->runs."/".$fielding_team_first_inning->wickets.$declared_1; ?>
	                           								</span>
															 <br>
															<span class="teamovers">
																(<?php echo $fielding_team_first_inning->overs; ?>)
															</span>
														 </h4>
	                        </div>

													<div class="clearfix"></div>


													<div class="team-runrate">
														 <h4 class="pull-right c-gray-light" style="color:#ccc; text-transform:none;">
														 	&nbsp;
														 </h4>
													</div>

											<?php }else{ ?>
												<div class="team-score">
													 <h4 class=" pull-right">
														 -
														 <br>
														 &nbsp;
													 </h4>

												</div>

												<div class="clearfix"></div>

												<div class="team-runrate">
													 <h4 class="pull-right c-gray-light">
													 	-
													 </h4>
												</div>

											<?php } ?>








											<?php } ?>

											<div class="clearfix"></div>
											<div class="row">

												 <div class="inplay">
														<div class="table-responsive">
															 <table class="table">

																	<thead>
																		 <tr class="bating">
																				<th class="width-75p text-left pl-15">Bowling</th>
																				<th width="50" class="text-right"><span class=" visible-lg visible-md">Overs</span><span class=" visible-sm visible-xs">O</span></th>
																				<th width="50" class="text-right"><span class=" visible-lg visible-md">Runs</span><span class=" visible-sm visible-xs">R</span></th>
																				<th width="60" class="text-right pr-15"><span class=" visible-lg visible-md">Wkts</span><span class=" visible-sm visible-xs">W</span></th>
																		 </tr>
																	</thead>

																	<tbody>
																		 <tr>
																				<td class="text-left pl-15">
																					<span class=" active-bold visible-lg visible-md onspell">
																						<?php echo !empty($bowler_onspell)?$bowler_onspell->player->name."*":"-"; ?>
																					</span>
																					<span class=" active-bold visible-sm visible-xs onspell-short">
																						<?php echo !empty($bowler_onspell)?$bowler_onspell->player->short_name."*":"-"; ?>
																					</span>
																				</td>
																				<td class="text-right">
																					<span class=" pr-5 active-bold onspell-overs">
																						<?php echo !empty($bowler_onspell)?$bowler_onspell->bowler->overs_bowled:"-"; ?>
																					</span>
																				</td>
																				<td class="text-right">
																					<span class=" pr-5 onspell-runs">
																						<?php echo !empty($bowler_onspell)?$bowler_onspell->bowler->runs_given:"-"; ?>
																					</span>
																				</td>
																				<td class="text-right">
																					<span class=" pr-5 onspell-wickets">
																						<?php echo !empty($bowler_onspell)?$bowler_onspell->bowler->wickets_taken:"-"; ?>
																					</span>
																				</td>
																		 </tr>

																		 <tr>
																				<td class="text-left pl-15">
																					<span class="visible-lg visible-md offspell">
																						<?php echo !empty($bowler_offspell)?$bowler_offspell->player->name:"-"; ?>
																					</span>
																					<span class="visible-sm visible-xs offspell-short">
																						<?php echo !empty($bowler_offspell)?$bowler_offspell->player->short_name:"-"; ?>
																					</span>
																				</td>
																				<td class="text-right active-bold">
																					<span class=" pr-5  offspell-overs">
																						<?php echo !empty($bowler_offspell)?$bowler_offspell->bowler->overs_bowled:"-"; ?>
																					</span>
																				</td>
																				<td class="text-right">
																					<span class=" pr-5  offspell-runs">
																						<?php echo !empty($bowler_offspell)?$bowler_offspell->bowler->runs_given:"-"; ?>
																					</span>
																				</td>
																				<td class="text-right">
																					<span class=" pr-5  offspell-wickets">
																						<?php echo !empty($bowler_offspell)?$bowler_offspell->bowler->wickets_taken:"-"; ?>
																					</span>
																				</td>
																		 </tr>
																	</tbody>

															 </table>
														</div>
												 </div>
											</div>


											<div class="clearfix"></div>


											<div class="tobat">
												<div class="over-balls">
							 					 	<div class="pull-left visible-xs">This Over</div>
							 					 	<span class="latest_balls">
													<?php
											        $latest_balls = "";

															if (!empty($live->partnership_and_bowlers) && !empty($live->partnership_and_bowlers->latest_balls)){
																foreach($live->partnership_and_bowlers->latest_balls as $ball){
																	$latest_balls .= get_ball_score_status_timeline_scorecard($ball);
																}
															}

															if (empty($latest_balls)){
																?>
																&nbsp;&nbsp;
																<?php
															} else {
																echo $latest_balls;
															}

													?>

													</span>
												 </div>
											</div>


									 </div><!-- team content -->
								</div>

								<!-- --------------- RIGHT SIDE DESKTOP --------------------------------   -->







                  <div class="col-xs-12 team-card bg-white visible-xs">
                     <div class="tobat">
                           <div class="needs-xs">
                           	<span class="tagline">
								 <?php if ($live->format!='Test'){ ?>

		 	 		            	<?php if (count($innings)==1){ ?>
										<?php echo $teams[$live->toss_won_by_id]->short_name." won the toss and choose to ".($live->chose_to=="Bowl"?"field":"bat")." first"; ?>
 									<?php } else { ?>
					 												<?php echo $teamA->short_name. " need ".($innings[$current_inning]->runs_required+1-$innings[$current_inning]->runs)." runs from ".$innings[$current_inning]->balls_remaining." balls"; ?>
					 	 									<?php } ?>
					 	 								<?php } else { ?>
					 	 									<strong>
					 											<?php echo $live->title; ?>
					 										</strong>
					 										<?php echo !empty($live->day)?" | Day $live->day":''; ?><?php echo !empty($live->break_type)?" | $live->break_type":" | Session $live->session"; ?>
					 	 								<?php } ?>

							</span>						 </div>
                        </div>
                  </div>


                  <div class="clearfix"></div>



       </div> <!-- caption-inner-wrap scorecards -->
    </div> <!-- top-banner-scorecard -->
	</div> <!-- image-wrapper bg-gradient-red1 -->

		<!-- ======================================================================================= -->


<?php else: ?>

	<div class="image-wrapper end_match p0" style="background-image: url(<?php echo $banner; ?>);">
		<div class="match_result">

			<?php if ($live->format!='Test'){ ?>
				<h2>
					<?php echo $teamA->name." (".@$innings[$current_inning]->runs."/".@$innings[$current_inning]->wickets.")"; ?>
					<span class="visible-xs"><div class="cleafix"></div></span><span>vs</span><span class="visible-xs"><div class="cleafix"></div></span>
					<?php echo $teamB->name." (".@$innings[$current_inning-1]->runs."/".@$innings[$current_inning-1]->wickets.")"; ?>
				</h2>
			<?php } else { ?>
				<?php
					$teamA_first = false;
					$teamA_second = false;
					$teamB_first = false;
					$teamB_second = false;
					foreach($innings as $inn){
	        			if ($inn->batting_team_id==$batting_team_id){
	        				if (!$teamA_first){
	        					$teamA_first = $inn;
	        				} else {
	        					$teamA_second = $inn;
	        				}
	        			} else {
	        				if (!$teamB_first){
	        					$teamB_first = $inn;
	        				} else {
	        					$teamB_second = $inn;
	        				}
	        			}
	        		}
				?>
				<h2>
					<?php
						$declared_1 = $teamA_first->declared?'d':'';
						$declared_2 = $teamA_second->declared?'d':'';
						echo $teamA->name." (".@$teamA_first->runs."/".@$teamA_first->wickets.$declared_1;
						if ($teamA_second){
							echo " & ".@$teamA_second->runs."/".@$teamA_second->wickets.$declared_2;
						}
						echo ")";
					?>
					<span class="visible-xs"><div class="cleafix"></div></span><span>vs</span><span class="visible-xs"><div class="cleafix"></div></span>
					<?php
						$declared_1 = $teamB_first->declared?'d':'';
						$declared_2 = $teamB_second->declared?'d':'';
						echo $teamB->name." (".@$teamB_first->runs."/".@$teamB_first->wickets.$declared_1;
						if ($teamB_second){
							echo " & ".@$teamB_second->runs."/".@$teamB_second->wickets.$declared_2;
						}
						echo ")";
					?>
				</h2>
			<?php } ?>

			<h3><?php echo $live->match_result; ?></h3>
		</div>
	</div>

<?php endif; ?>




  <div class="clearfix"></div>

  <div class="timeline-main-menu" style="background-color: #fff">
	  <div class="timeline-background">
		  <ul class="timeline-base mb15-xs">
			  <li>
					<a href="javascript:;" class="btn-timeline active switch tm" data-src="timeline_area" style="">
						Timeline
					</a>
				</li>
			  <li>
					<a href="javascript:;" class=" btn-scorecard switch sc" data-src="scorecard_area" style="">
						Scorecard
					</a>
				</li>
		  </ul>
	  </div>
  </div>

  <div id="site-container" class="clearfix" style=" min-height: 500px">

    <div class="scorecard_area custom-content-divs" style="display: block">
      <div class="container"></div>
    </div>

    <div class="match_fact_area custom-content-divs" style="display: none"></div>

    <div class="timeline_area custom-content-divs">

      <div class="filter-icons hidden">

				<a href="javascript:;" class="btn-filter-n" data-tag-id="23" data-page="1" data-count="0">
        	<div class="boom">BooM</div>
        </a>

				<a href="javascript:;" class="btn-filter-n" data-tag-id="2" data-page="1" data-count="0">
        	<div class="four">4s</div>
        </a>

				<a href="javascript:;" class="btn-filter-n" data-tag-id="0" data-page="1" data-count="0">
        	<div class="all active">ALL</div>
        </a>

				<a href="javascript:;" class="btn-filter-n" data-tag-id="1" data-page="1" data-count="0">
        	<div class="sIx">6s</div>
        </a>

				<a href="javascript:;" class="btn-filter-n" data-tag-id="3" data-page="1" data-count="0">
        	<div class="wicket">W</div>
        </a>

			</div>

      <div class="timeline-filter-menu">

        <div class="col-md-8 col-xs-10">
		  		<div class="row">
						<span class="hidden">Filters: </span>
						<a class=" btn-filters btn-filter-n btn-all gray active" data-tag-id="0" data-page="1" data-count="0">All</a>
						<a class=" btn-filters btn-filter-n btn-6 gray" data-tag-id="1" data-page="1" data-count="0">6s</a>
						<a class=" btn-filters btn-filter-n btn-4 gray" data-tag-id="2" data-page="1" data-count="0">4s</a>
						<a class=" btn-filters btn-filter-n btn-w gray" data-tag-id="3" data-page="1" data-count="0">W</a>
					</div>
				</div>

		  	<div class="col-md-4 col-xs-2 hidden-xs">
		  		<div class="row">
		  			<div class="switch pull-right switch-timeline-xs">
          		<input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round" type="checkbox" onchange="toggleVideo();">
          		<label for="cmn-toggle-1" class="mt5"></label>
        		</div>

        		<span class="switch-lable pull-right hidden-xs hidden-sm">Videos Only</span>
			  	</div>
				</div>

      </div>

      <div class="clearfix"></div>
      <div class="container pr10-xs">
        <div id="content" role="main">
					<div id="loadingDiv">
						<img src="<?php echo assets_url()."images/loading.gif"; ?>" />
					</div>
        </div>
				<div class="nextPageLoader text-center" style="display: none;"><img src="<?php echo assets_url()."images/ajax-loader.gif"; ?>"> Loading</div>
      </div>
    </div>
  </div>



	<script>
	var base_url = "<?php echo base_url(); ?>";
	var match_id = "<?php echo $match_id; ?>";
	var match_stream_id = "<?php echo $match_stream_id; ?>";
	var current_tag = 0;
	var page_to_load = 0;
	var add_social_icon = true;
	var activate_player = false;
	var sharing_url = "";
	var sharing_title = "";
	var video_allowed = <?php echo $video_allowed; ?>;
	var load_on_scroll = true;
	var loadingImage = '<div id="loadingDiv"><img src="<?php echo assets_url()."images/loading.gif"; ?>" /></div>';
	var top_item_id = 0;
	var bottom_item_id = 0;
	var first_item_of_timeline = 0;
	var current_available_items = <?php echo $this->data["tags_stats"]["t_total"]; ?>;
	var match_title = '<?php echo " | ".$teamA->name." vs ".$teamB->name." | Cricwick"; ?>';

	$( document ).ready(function(){

		var default_title = '<?php echo empty($innings[$current_inning]->runs)?"0/":$innings[$current_inning]->runs."/"; ?>';
		default_title = default_title + '<?php echo empty($innings[$current_inning]->wickets)?0:$innings[$current_inning]->wickets; ?>';
		default_title = default_title + " | "+'<?php echo empty($innings[$current_inning]->overs)?"0.0":$innings[$current_inning]->overs; ?>'+'ov';

		$('title').html(default_title+match_title);

		<?php if (!empty($is_live)){ ?>
			<?php if (empty($live_stream) || !$stream_allowed ){ ?>
				recursive_call_id = setInterval(function() {
					  updateLiveMatches();
				}, 10000);
			<?php } ?>
			recursive_call_id_stats = setInterval(function() {
				  updateLiveMatchFilterStats();
			}, 10000);

			window.onbeforeunload = function(e) {
			  clearInterval(recursive_call_id);
			  clearInterval(recursive_call_id_stats);
			  console.log("Clearing Interval");
			};

		<?php } ?>

		$('.selectpicker').on('loaded.bs.select', function (e) {
			//console.log("adasd");
		  	$(".bootstrap-select .btn-default").addClass("active");
		});



		// $(window).scroll(function() {
		// 	console.log('scrolling');
		//   if($(window).scrollTop() + $(window).height() == $(document).height()) {
		//   	  if ($(".switch.tm").hasClass("active")){

		//   	  	if (load_on_scroll){
		//   	  		load_on_scroll = false;
		//   	  		get_next_page_current_timeline();
		//   	  	}
		//   	  }else{

		// 			}

		//   }
		// });

		$(document.body).on('touchmove, scroll', onScrollMobile); // for mobile
		$(window).on('scroll', onScrollDesktop);

		function onScrollDesktop(){
			if($(window).scrollTop() + $(window).height() == $(document).height()) {
				console.log('end');
		  	  if ($(".switch.tm").hasClass("active")){

		  	  	if (load_on_scroll){
		  	  		load_on_scroll = false;
		  	  		get_next_page_current_timeline();
		  	  	}
		  	  }else{
					}

		  }else{
		  	  	console.log('no end');
		  }
		}
		function onScrollMobile(){
			if($('body').scrollTop() + $(window).height() == $(document).height()) {
				console.log('end');
		  	  if ($(".switch.tm").hasClass("active")){

		  	  	if (load_on_scroll){
		  	  		load_on_scroll = false;
		  	  		get_next_page_current_timeline();
		  	  	}
		  	  }else{
					}

		  }else{
		  	  	console.log('no end');
		  }
		}

		$(".btn-filter-n").on("click",function(){
			load_on_scroll = false;
			var tag_id = $(this).attr("data-tag-id");

			var url = base_url+'ajax/fetch_timeline/'+match_id;
			$(".btn-filter-n").removeClass("active");
			$(this).addClass("active");
			if (tag_id!=0){
				url = url + "/1/"+tag_id;
				//$(this).children(".label").html("");
			} else {
				//$(".btn-filter-n").children(".label").html("");
			}
			current_tag = tag_id;
			// alert(url);
			refresh_timeline(url);
			load_on_scroll = true;
		});

		$("body").on("click",".buy_stream_anchor",function(){
			if (confirm("You will be charged with 4 SAR for live streaming. Are you sure you want to continue")){

				btnRef = $('.btn.btn-success.buy_stream_anchor');
				btnRef.button("loading");
				var url = base_url+'timeline/purchase_live_stream/'+match_stream_id;
				$.ajax({
					dataType: "json",
					url: url,
					data: {},
					success: function(response) {
						if (response.status==1){
							window.location='<?php echo base_url()."livestream"; ?>';
						} else {
							alert(response.message);
							btnRef.button("reset");
						}
					}
				});
			}
			return false;
		});
		$("body").on("click",".scorecard-tabs",function(e){
			e.preventDefault();
			$(".scorecard-tabs").parent().removeClass("active");
			//$("#tab1").removeClass("active");
			$(".tab").removeClass("active");
			$(this).parent().addClass("active");
			$($(this).attr("tabid")).addClass("active");
			return false;
		});

		$("body").on("change","#timeline_filter",function(e){
			$(".switch.sc").removeClass("active");
			var container ="timeline_area";
			$(".custom-content-divs").hide();
			$("."+container).fadeIn();

			load_on_scroll = false;
			var tag_id = $(this).val();
			if (tag_id=="-1"){
				return false;
			}

			var url = base_url+'ajax/fetch_timeline/'+match_id;
			//$(this).siblings().children().removeClass("active");
			//$(this).children().addClass("active");
			if (tag_id!=0){
				url = url + "/1/"+tag_id;
			}
			current_tag = tag_id;
			refresh_timeline(url);
			load_on_scroll = true;
			$(".switch.tm").addClass("active");
			$(this).siblings(0).addClass("active");

		});

		$(".switch").on("click",function(){
			$(this).siblings().removeClass("active");
			$(this).addClass("active");

			if ($(this).hasClass("sc")){ //scorecard clicked
				$('#timeline_filter').siblings(0).removeClass("active");
				$(".switch.tm").removeClass("active");

				var container = $(this).attr("data-src");
				$("."+container+" .container").html(loadingImage);
				$(".custom-content-divs").hide();
				$("."+container).fadeIn();
				var url = base_url+'home/scorecard/'+match_id;
				$.ajax({
					dataType: "html",
					url: url,
					data: {},
					success: function(response) {
						$("."+container+" .container").html(response);
					},
					error: function (e){
						$("."+container+" .container").html('<div class="alert alert-danger">Scorecard not available right now, please try again later!</div>');
					}
				});
			} else if ($(this).hasClass("tm")){
				var container = $(this).attr("data-src");
				$(".switch.sc").removeClass("active");
				$(".custom-content-divs").hide();
				$("."+container).fadeIn();

			} else if ($(this).hasClass("live_stream")){
				var container = $(this).attr("data-src");

				$(".custom-content-divs").hide();
				$("."+container).fadeIn();


			}
		});

		refresh_timeline(base_url+'ajax/fetch_timeline/'+match_id);



	});

	function get_next_page_current_timeline() {

		if (page_to_load!=0){
			$(".nextPageLoader").show();
			var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/previous/'+bottom_item_id;
			load_on_scroll = false;
			$.ajax({
				dataType: "json",
				url: url,
				data: {},
				success: function(response) {
					if(response === null){
						page_to_load = 0;
						$(".nextPageLoader").hide();
						toggleVideo();
						console.log('is_null');
						return false;
					}
					$(".nextPageLoader").hide();

					append_timeline(response,"append");



					/*
					if (response.current_page < response.total_pages){
						page_to_load = response.current_page+1;
					} else {
						page_to_load = 0;
					}
					*/

					if (bottom_item_id == response.first_timeline_id){
						page_to_load = 0
					} else {
						page_to_load = 1;
					}
					load_on_scroll = true;
					toggleVideo();

				},
				error: function (e){

				}
			});
		} else {
			console.log("page to load is zero");
			toggleVideo();
		}

	}

	function refresh_timeline(url_to_call){
		$(".timeline_area .container > #content").html(loadingImage);

		$.ajax({
			dataType: "json",
			url: url_to_call,
			data: {},
			success: function(response) {
				$(".timeline_area .container > #content").html('<ul id="timeline" class="clearfix"></ul>');
				append_timeline(response,"append");
				if (response.current_page < response.total_pages){
					page_to_load = 2;
				} else {
					page_to_load = 0;
				}
				load_on_scroll = true;
				toggleVideo();
			},
			error: function (e){
				$(".container > #content").html('<div class="alert alert-danger">Timeline not available right now, please try again later!</div>');
			}
		});
	}

	function append_timeline(response,toDo){

		var jcount = 1;


		response.timeline.forEach(function(item) {
			add_social_icon = true;
			activate_player = false;
			activate_twitter = false;
			playlist = new Array();

			if (item.tags.length>0){
				var jclass = 'odd';
				if(jcount % 2 == 0){
					jclass = 'even';
				}
				var contentType = item.tags[0].name;

				var boxToPlace = '<li class="    '+jclass+'" data-timeline-id="'+item.id+'">';
				boxToPlace = boxToPlace + '<article  class="post type-post status-publish format-quote has-post-thumbnail hentry category-category-trio tag-post-format tag-quotes post_format-post-format-quote has_thumb clearfix">';

				if (item.tags.length>0){

					boxToPlace = boxToPlace + '<span class="entry-date" style="background: '+item.tags[0].color_code+' !important;"><span class="entry-meta-date">';

					if (item.tags[0].name!='Ball' && item.tags[0].name!='Six' && item.tags[0].name!='Four' && item.tags[0].name!='Out'){
						boxToPlace = boxToPlace + '<time>'+item.tags[0].name+'</time></span></span>';
					} else {
						boxToPlace = boxToPlace + '<time>'+item.tags[0].customized_tag+'</time></span></span>';
					}

				}
				if (!$.isEmptyObject(item.video)){
					boxToPlace = boxToPlace + '<div class="hentry-box is-video" data-viewed="0">';
					boxToPlace = boxToPlace + '<h3 class="image-title">'+item.video.title+'</h3>';
					//boxToPlace = boxToPlace + '<video controls poster="'+item.video.med_image+'">';
					//boxToPlace = boxToPlace + '<source src="'+item.video.video_file+'" />';
					//boxToPlace = boxToPlace + '</video>';
					boxToPlace = boxToPlace + '<div id="video'+item.video.id+'"></div>';

					if (!$.isEmptyObject(item.ball)){
						boxToPlace = boxToPlace + '<p class="image-description">'+item.ball.commentary+'</p>';
					} else {
						boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
					}

					if (!$.isEmptyObject(item.video.pre_ad)){
						var file = {file: item.video.pre_ad.video_file, image: item.video.med_image};
						playlist.push(file);
					}

					qualities = new Array();
					var i = parseInt("1");
					var qualityItem = "";
					item.video.qualities.forEach(function(q) {
						if (i==2){
							qualityItem = {file: q.video_file, label: q.height+"P", "default": true};
						} else {
							qualityItem = {file: q.video_file, label: q.height+"P"};
						}
						i++;
						qualities.push(qualityItem);
					});
					if (!$.isEmptyObject(item.video.video_file)){
						qualityItem = {file: item.video.video_file, label: "720P"};
						qualities.push(qualityItem);
					}
					file = {
						sources: qualities,
						image: item.video.med_image
					};
					playlist.push(file);

					if (!$.isEmptyObject(item.video.post_ad)){
						file = {file: item.video.post_ad.video_file, image: item.video.med_image};
						playlist.push(file);
					}
					//console.log(playlist);
					activate_player = true;
					sharing_url = base_url+'highlights/'+item.video.id+'/'+item.video.seo_url+"/";
					sharing_title = item.video.title;

				} else if (!$.isEmptyObject(item.image)){
					boxToPlace = boxToPlace + '<div class="hentry-box no-video">';
					boxToPlace = boxToPlace + '<h3 class="image-title">'+item.title+'</h3>';
					boxToPlace = boxToPlace + '<img class="icc-image" src="'+item.image+'"/>';
					//boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
					if (!$.isEmptyObject(item.ball)){
						boxToPlace = boxToPlace + '<p class="image-description">'+item.ball.commentary+'</p>';
					} else {
						boxToPlace = boxToPlace + '<p class="image-description">'+item.desc+'</p>';
					}
					add_social_icon = false;

				} else {

					if (contentType=='Ball' || contentType=='Four' || contentType=='Six' || contentType=='Out' ){
						boxToPlace = boxToPlace + '<div class="hentry-box no-video no-padding">';
						boxToPlace = boxToPlace + '<div class="entry-quote-white commentry"><p>';
						boxToPlace = boxToPlace + item.ball.commentary;
						boxToPlace = boxToPlace + '</p></div>';
						add_social_icon = false;

					} else if (contentType=='News'){

						boxToPlace = boxToPlace + '<div class="hentry-box no-video">';
						boxToPlace = boxToPlace + '<h3 class="news-title">'+item.news.title+'</h3>';
						boxToPlace = boxToPlace + '<p class="news-sub-title">By '+item.news.author+'</p>';
						boxToPlace = boxToPlace + '<p class="news-sub-title">'+item.news.created_at+'</p>';
						boxToPlace = boxToPlace + '<p class="news-description">'+item.news.details+'</p>';
						boxToPlace = boxToPlace + '<div class="news-image"><img class="icc-image" src="'+item.news.med_image+'"/></div>';
						add_social_icon = false;


					} else if (contentType=='Article'){

						boxToPlace = boxToPlace + '<div class="hentry-box no-video">';
						boxToPlace = boxToPlace + '<h3 class="article-title">'+item.title+'</h3>';
						boxToPlace = boxToPlace + '<p class="article-sub-title">By '+item.article.author+'</p>';
						boxToPlace = boxToPlace + '<p class="article-sub-title">'+item.article.created_at+'</p>';
						boxToPlace = boxToPlace + '<div class="article-image"><img class="icc-image" src="'+item.article.med_image+'"/></div>';
						boxToPlace = boxToPlace + '<p class="article-details">'+item.article.details+'</p>';
						//boxToPlace = boxToPlace + '<a class="read-more-article" href="'+item.article.seo_url+'"> Read Full Article> </a>';
						add_social_icon = false;

					} else if (contentType=='Tweet'){
						boxToPlace = boxToPlace + '<div id="twitter-feed-'+item.id+'">';
						/*
						boxToPlace = boxToPlace + '<div class="hentry-box" id="twitter-feed-'+item.id+'">';
						boxToPlace = boxToPlace + '<blockquote class="twitter-tweet" lang="es">';
						boxToPlace = boxToPlace + '<p lang="en">'+item.tweet.text+'</p>';
						boxToPlace = boxToPlace + '— '+item.tweet.user.name+' (@'+item.tweet.user.screen_name+')';
						boxToPlace = boxToPlace + '<a href="https://twitter.com/'+item.tweet.user.screen_name+'/status/'+item.desc+'"> '+item.tweet.created_at+'</a>';
						boxToPlace = boxToPlace + '</blockquote>';
						*/
						add_social_icon = false;
						activate_twitter = true;
					} else {
						boxToPlace = boxToPlace + '<div class="hentry-box no-video no-padding">';
						boxToPlace = boxToPlace + '<blockquote class="entry-quote-white commentry"><p>';
						boxToPlace = boxToPlace + item.title;
						boxToPlace = boxToPlace + '</p></blockquote>';
						add_social_icon = false;
					}

				}

				add_social_icon = false; // Hide Social sharing buttons
				if (add_social_icon){
					boxToPlace = boxToPlace + '<div class="social-icons-timeline" class="clearfix">';
					boxToPlace = boxToPlace + '<a href="https://plus.google.com/share?url='+sharing_url+'"><i class="fa fa-google-plus"></i></a>';
					boxToPlace = boxToPlace + '<a href="#" class="hidden"><i class="fa fa-instagram"></i></a>';
					boxToPlace = boxToPlace + '<a href="http://www.twitter.com/share?text='+sharing_title+'&url='+sharing_url+'"><i class="fa fa-twitter"></i></a>';
					boxToPlace = boxToPlace + '<a href="https://www.facebook.com/sharer/sharer.php?app_id=430752913746475&sdk=joey&u='+sharing_url+'&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a>';
					boxToPlace = boxToPlace + '</div></div></article></li>';
				}
				jcount = jcount + 1;

			}
			if(toDo=='append'){
				$(".timeline_area .container > #content #timeline").append(boxToPlace);
				bottom_item_id = item.id;

				if (top_item_id==0){
					top_item_id = item.id;
				}
				//console.log(bottom_item_id);
			} else  if (toDo=='prepend'){
				$(".timeline_area .container > #content #timeline").prepend(boxToPlace);
				top_item_id = item.id;
			}


			if (activate_player){
				// alert("#video"+item.video.id);
				console.log("#video"+item.video.id);
				// alert();
				//console.log("activating"+item.video.id);
				var playerInstance = jwplayer("video"+item.video.id);
				playerInstance.setup({
				    playlist: playlist,
				    width: "100%",
				    controls: true,
				    preload: "metadata",
				    title: item.video.title,
					ga: {
						label: "title"
					}
				}).onPlay(function() {
					if (video_allowed==0){
						// jwplayer("video"+item.video.id).stop();
						// alert('Sorry, this content is not available in your country!');
					} else {
						if (!$("#video"+item.video.id).parent().attr("data-view-confirmed")){
							jQuery.ajax({
								dataType: "json",
								url: base_url+'ajax/confirm_timeline_view/'+item.video.id,
								data: {},
								success: function(response) {

								}
							});
							$("#video"+item.video.id).parent().attr("data-view-confirmed",1);
						}
					}

				});
			}

			if (activate_twitter){
				twttr.widgets.createTweet(
				  item.desc,
				  document.getElementById('twitter-feed-'+item.id),
				  {
				    width: "100%"
				  })
				  .then(function (el) {

				    $('iframe').contents().find("head")
	      				.append($("<style type='text/css'>  .EmbeddedTweet{max-width: 100% !important;}  </style>"));
				  });
			}
			toggleVideo();

		});
	}

	function updateLiveMatchFilterStats(){

		var url = base_url+'ajax/fetch_timeline/'+match_id+'/0/'+current_tag+'/next/'+top_item_id;
		$.ajax({
			dataType: "json",
			url: url,
			data: {},
			success: function(response) {
				append_timeline(response,"prepend");
				console.log("Top Auto Refreshed");
			},
			error: function (e){

			},timeout: 8000
		});
	}

	function updateLiveMatches(){
		var match_updated = false;
		$.ajax({
			dataType: "json",
			url: base_url+'ajax/updateHomeLiveMatchScore',
			data: {},
			success: function(response) {
				response.live_matches.forEach(function(live) {

					if (live.match_id==match_id){
						$("#liveMatch"+live.match_id+" .batting .onstrike-short").html(live.batting.onstrike.short_name);
						$("#liveMatch"+live.match_id+" .batting .onstrike").html(live.batting.onstrike.name);
						$("#liveMatch"+live.match_id+" .batting .onstrike-score").html(live.batting.onstrike.score);
						$("#liveMatch"+live.match_id+" .batting .onstrike-balls").html(live.batting.onstrike.balls);
						$("#liveMatch"+live.match_id+" .batting .onstrike-rate").html(live.batting.onstrike.rate);

						if("<?php echo($live->toss_won_by_id); ?>" == live.batting.id){
							$(".bowling .teamtoss").addClass('hidden');
							$(".batting .teamtoss").removeClass('hidden');
						}else{
							$(".batting .teamtoss").addClass('hidden');
							$(".bowling .teamtoss").removeClass('hidden');
						}



						$("#liveMatch"+live.match_id+" .batting .teamscore").html(live.batting.score);
						$("#liveMatch"+live.match_id+" .batting .run-rate").html("RR: "+live.batting.run_rate);
						$("#liveMatch"+live.match_id+" .batting .team-flag img").attr("src",live.batting.flag);



						$("#liveMatch"+live.match_id+" .batting .offstrike-short").html(live.batting.offstrike.short_name);
						$("#liveMatch"+live.match_id+" .batting .offstrike").html(live.batting.offstrike.name);
						$("#liveMatch"+live.match_id+" .batting .offstrike-score").html(live.batting.offstrike.score);
						$("#liveMatch"+live.match_id+" .batting .offstrike-balls").html(live.batting.offstrike.balls);
						$("#liveMatch"+live.match_id+" .batting .offstrike-rate").html(live.batting.offstrike.rate);
						var overs = '';
						if (live.format!='Test'){
							overs = '('+live.batting.overs+' ov)';
						} else {
							overs = live.batting.overs;
						}

						$("#liveMatch"+live.match_id+" .batting .teamovers").html(overs);
						$("#liveMatch"+live.match_id+" .batting .teamtitle.full").html(live.batting.title+'*');
						$("#liveMatch"+live.match_id+" .batting .teamtitle.mini").html(live.batting.title_small+'*');
						// $("#liveMatch"+live.match_id+" .batting .teamtitle").css("background",live.batting.color);

						if (live.bowling.score!=''){
							$("#liveMatch"+live.match_id+" .bowling .teamscore").html(live.bowling.score);
							$("#liveMatch"+live.match_id+" .bowling .run-rate").html("RR: "+live.bowling.run_rate);
							$("#liveMatch"+live.match_id+" .bowling .teamflag").attr("src",live.bowling.flag);
							$("#liveMatch"+live.match_id+" .bowling .teamtitle.full").html(live.bowling.title);
							$("#liveMatch"+live.match_id+" .bowling .teamtitle.mini").html(live.bowling.title_small);
							// $("#liveMatch"+live.match_id+" .bowling .teamtitle").css("background",live.bowling.color);
							if (live.format!='Test'){
								overs = '('+live.bowling.overs+' ov)';
							} else {
								overs = live.bowling.overs;
							}
							$("#liveMatch"+live.match_id+" .bowling .teamovers").html(overs);
						}

						$("#liveMatch"+live.match_id+" .bowling .onspell").html(live.bowling.onspell.name);
						$("#liveMatch"+live.match_id+" .bowling .onspell-short").html(live.bowling.onspell.short_name);

						$("#liveMatch"+live.match_id+" .bowling .onspell-overs").html(live.bowling.onspell.overs);
						$("#liveMatch"+live.match_id+" .bowling .onspell-runs").html(live.bowling.onspell.runs);
						$("#liveMatch"+live.match_id+" .bowling .onspell-wickets").html(live.bowling.onspell.wickets);

						$("#liveMatch"+live.match_id+" .bowling .onspell-status").html(live.bowling.onspell.status);



						// $("#liveMatch"+live.match_id+" .bowling .offspell").html(live.bowling.offspell.short_name);
						// $("#liveMatch"+live.match_id+" .bowling .offspell-status").html(live.bowling.offspell.status);

						$("#liveMatch"+live.match_id+" .bowling .offspell").html(live.bowling.offspell.name);
						$("#liveMatch"+live.match_id+" .bowling .offspell-short").html(live.bowling.offspell.short_name);

						$("#liveMatch"+live.match_id+" .bowling .offspell-overs").html(live.bowling.offspell.overs);
						$("#liveMatch"+live.match_id+" .bowling .offspell-runs").html(live.bowling.offspell.runs);
						$("#liveMatch"+live.match_id+" .bowling .offspell-wickets").html(live.bowling.offspell.wickets);

						$("#liveMatch"+live.match_id+" .bowling .offspell-status").html(live.bowling.offspell.status);



						if (live.format!='Test'){
							var tagline = '<p>'+live.tagline+'</p>';
							var tagline_small = '<p>'+live.tagline_small+'</p>';
							$("#liveMatch"+live.match_id+" .needs .tagline").html(tagline);
							$("#liveMatch"+live.match_id+" .needs-xs .tagline").html(tagline_small);
							if (live.latest_balls!=""){
								$("#liveMatch"+live.match_id+" .bowling .latest_balls").html(live.latest_balls+" ");
							}
						} else {
							$("#liveMatch"+live.match_id+" .tagline").html(live.tagline);
						}

						$('title').html(live.html_title+match_title);

						match_updated = true;
					}

				});

				if (!match_updated){
					window.location = "<?php echo $post_match_url; ?>";
				}
			},timeout: 8000
		});
	}

	function toggleVideo(){

		if($("#cmn-toggle-1:checked").length){
			//enable video only

			hideNoVideo();
			realignLi();

			if($('.is-video').length > 0){

			}else{
				if(page_to_load > 0){
				// please load more, page to load
					if (load_on_scroll){
			  	  		load_on_scroll = false;
			  	  		get_next_page_current_timeline();
			  	  	}
				}else{
					// no more pages to load and couldnt find video
					hideNoVideo();
					sorryNoVideo4u();
					realignLi();
				}
			}

			return false;
		}else{
			//disable video only
			showNoVideo();
			realignLi();
			return false;
		}

	}

	function hideNoVideo(){
		$('.no-video').closest('li').hide();
	}
	function showNoVideo(){
		$('.no-more-videos').remove();
		$('.no-video').closest('li').show();
	}

	function sorryNoVideo4u(){
		var sorryhtml = '';
		sorryhtml += '<li class="    odd no-more-videos" data-timeline-id="241618">';
			sorryhtml += '<article class="post type-post status-publish format-quote has-post-thumbnail hentry category-category-trio tag-post-format tag-quotes post_format-post-format-quote has_thumb clearfix">';
				sorryhtml += '<span class="entry-date" style="background: #c4161c !important;"><span class="entry-meta-date"><time><b>Sorry!</b></time></span></span>';
		sorryhtml += '<div class="hentry-box no-padding"><div class="entry-quote-white commentry"><p>No more videos</p></div></div></article></li>';

		$("#timeline").append(sorryhtml);
	}

	function realignLi(){

		if($(window).width() > 720 ){
			console.log('realigning');
			var ncount = 1;
			$('.container > #content #timeline li').each(function(){
				$this = $(this);


				if($this.is(':visible')){
					$this.removeClass('odd').removeClass('even');

					nclass = 'odd';
					if(ncount % 2 == 0){
					nclass = 'even';
				}
				$this.addClass(nclass);
				ncount++;

				}
			});

		}else{
			// alert('no re align');
		}
	}

	function mustGetVideo(response){

		if($("#cmn-toggle-1:checked").length){
		console.log('i am trying to get you videos');
		if(page_to_load == 0 && !$('.no-more-videos').length){

			var sorryhtml = '';
			sorryhtml += '<li class="    odd no-more-videos" data-timeline-id="241618">';
				sorryhtml += '<article class="post type-post status-publish format-quote has-post-thumbnail hentry category-category-trio tag-post-format tag-quotes post_format-post-format-quote has_thumb clearfix">';
					sorryhtml += '<span class="entry-date" style="background: #c4161c !important;"><span class="entry-meta-date"><time><b>Sorry!</b></time></span></span>';
			sorryhtml += '<div class="hentry-box no-padding"><div class="entry-quote-white commentry"><p>No more videos</p></div></div></article></li>';

			$("#timeline").append(sorryhtml);
			realignLi();
			return;
		}
			if(response){
								var videofound = 0;
								for(var vf =0; vf < response.timeline.length; vf++){
								 if(response.timeline[vf].video){
										videofound = 1;
									}
								}

								if(videofound == 0){
									// get_next_page_current_timeline();
								}
			}



		}else{
			$(".no-more-videos").remove();
			realignLi();
		}

	}
	 </script>
