<div class="wrapper-main mtb10 bg-login" style="height: 100%;">

  <!-- LOGIN BOX -->
  <div class="col-md-12 col-xs-12 pt20">
    <div class="col-xs-12 hidden">
      <div class="row">
        <div class="btn btn-group pull-right"> <a href="http://cricket.jazz.com.pk/login?lang=english" class="login btn btn-sm btn-primary">English</a> <a href="http://cricket.jazz.com.pk/login?lang=urdu" class="login btn btn-sm btn-default">اردو</a> </div>
      </div>
    </div>


      <div class="col-md-10 col-xs-12 col-xs-offset-1 pl0 pt40">
        <div class="">
          <h4 class="color-white fw-normal text-uppercase login-title">Register | Login Now!</h4>
        </div>
      </div>

    <div class=" col-md-10 col-xs-12 col-xs-offset-1 login-page pr0">
      <div class="col-md-6 col-sm-6 col-xs-12">


        <form method="post" class="login-form" id="phone-form">
          <div class="left-panel-login name" style="">
            <label for="name_login">
              <?php echo lang("enter_phone_number"); ?>
            </label>
            <input id="phone" name="phone" type="text" placeholder="03xxxxxxxxx" value="<?php echo $phone_number; ?>" class="input-number">

            <div id="login-submit">
              <input class="btn-continue" type="submit" id="btnSubmit" value="<?php echo lang("btn_continue"); ?>" data-loading-text="<?php echo lang("label_please_wait"); ?>">
            </div>

            <div class="mt10 color-red message" style="display:none;">
              *Please enter valid number
            </div>

          </div>
        </form>



      </div>
      <div class="col-md-6 col-sm-6  col-xs-12 img-login">
      <a href="javascript:;" class="playstore_link">
        <img src="<?php echo assets_url(); ?>images/img-download.jpg?v=<?php echo VERSION; ?>" alt="" class="img-responsive">
      </a>
      </div>
    </div>
  </div>
  <!--Close Login-->


</div>
<div class="clearfix"></div>
