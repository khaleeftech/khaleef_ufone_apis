<div id="c-calend" class="col-lg-9 col-md-9 col-sm-12 col-xs-12 crk-trending-videos-container">

  <div class="cs-seprator mt30 hidden-xs">
    <div class="devider1"> </div>
  </div>

	<h1 class='hidden-xs'><?php echo $page_heading; ?></h1>
  <h1 class="visible-xs f20"><?php echo $page_heading; ?></h1>




		<div id="results-container" class="col-md-12 col-xs-12  padding-md-0 mt-xs-10 news-container">
      <div class="row-xs-10">
			<?php foreach($news->news as $key => $n){ ?>

        <div class="col-xs-12 col-md-12 col-sm-12">
          <div class="news-page">

            <div class="col-md-3 col-sm-3 col-xs-12">
              <img src="<?php echo $n->full_image; ?>" alt="" class="img-djoko img-home-item img-responsive">
            </div>


            <div class="col-md-9 col-sm-9 col-xs-12 data-news-pg">
              <h4 class="pt0 mb0 mt0 crop_to_one_line"><?php echo $n->title; ?></h4>
              <p class="news-dd"><?php echo $n->created_at ?></p>
              <div class="clear"> </div>
              <p class="mt0">
                <span class="hidden-xs">
                  <?php echo substr(strip_tags($n->body),0, 180); ?>...
                </span>
                <span class="visible-xs">
                  <?php echo substr(strip_tags($n->body),0, 230); ?>...
                </span>

              </p>
            </div>

            <a href="<?php echo $n->seo_url; ?>" class="more-arrow">
              <img src="<?php echo assets_url(); ?>images/arrow-right.png">
            </a>

          </div>
        </div>



			<?php } ?>

		</div>
  </div>

    <?php if ($news->page<$news->total_pages): ?>
		<div class="bottom-event-panel">
			<a href="javascript:;" id="more-news-btn" data-page-to-load="2"> More News</a>
		</div>
  <?php endif; ?>



</div>
