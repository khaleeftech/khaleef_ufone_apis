<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<div class="cs-seprator p0 hidden-xs" style=" margin-bottom:10px; padding: 0">
					<div class="devider1"> </div>
				</div>

				<?php if (!empty($live_matches)): ?>
					<h1 class="hidden-xs">Live Matches</h1>
				<?php endif; ?>

				<div class="event event-listing event-samll">
					<div id="fixtures-container">

				  		<?php if (!empty($live_matches) ): ?>


						<h2 class="visible-xs mt20">Live Matches</h2>


						<?php foreach ($live_matches as $key => $value): ?>
						<?php
							$title = $value->team_1->team->name." vs ".$value->team_2->team->name;
	            			$match_url = base_url()."live/".$value->id."/".seo_url($title)."/";



	            			$flag1 = assets_url().'images/flag-placeholder.jpg';
					        $flag2 = assets_url().'images/flag-placeholder.jpg';

					        if(!empty($value->team_1->team->flag_url) && (strpos($value->team_1->team->flag_url, 'missing.png') === false && strpos($value->team_1->team->flag_url, 'placeholder.jpg') === false)){
					          $flag1 = $value->team_1->team->full_flag_url;
					        }
					        if(!empty($value->team_2->team->flag_url) && (strpos($value->team_2->team->flag_url, 'missing.png') === false && strpos($value->team_2->team->flag_url, 'placeholder.jpg') === false)){
					          $flag2 = $value->team_2->team->full_flag_url;
					        }
						?>

						<!-- livematch -->
						<div class="col-md-12 col-xs-12 event-fixture-box">
							<div class="live hidden-xs" style=" ">live</div>
							<div class="">

								<div class="col-xs-12 visible-xs  pb20">
									<h2 class="fw-normal text-left  m0">
									 	<a class="color-grey-lighter f16" href="<?php echo $match_url; ?>">
									 		<?php
									 			$start_date =  format_date_newserver2($value->match_start);
									 			echo $start_date["date"]." ";
									 			echo $start_date["month"];

								 			?>
									 	</a>
									</h2>

									<h4 class="m0 lh28 fw-normal text-left">
										<a class="color-grey-light mt-5" href="<?php echo $match_url; ?>">
											<?php echo $value->title; ?>
										</a>
									</h4>
								</div>

								<div class="col-md-2 col-sm-3 col-xs-6 ">
									<div class="row">
										<div class="team-first">
								    		<ul style="text-align: center;">
									   			<li>
									   				<a href="<?php echo $match_url; ?>">
									   					<img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
									   				</a>
									   			</li>
											    <li>
											    	<h4 class="color-jazz-grey-darker mb0"><?php echo $value->team_1->team->name; ?></h4>
											    </li>
											    <li>
											    	<h4 class="color-jazz-grey-darker mt0 mb0">

											    		<?php
											    			if(!empty($value->innings[0]->runs)){
											    				echo $value->innings[0]->runs."/".$value->innings->wickets;
											    			}else{
											    				echo "-";
											    			}
											    		?>

											    	</h4>
											    </li>
									     	    <li>
									     	    	<span class="f12">
									     	    		<?php
											    			if(!empty($value->innings[0]->overs)){
											    				echo "(".$value->innings[0]->overs.")";
											    			}else{
											    				echo "-";
											    			}
											    		?>
									     	    	</span>
									     	    </li>

								   			</ul>
								   		</div>
									</div>
								  </div>

								  <div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">
									<article class="mt10">
									  <div class="text-center">

										  <h2 class="fw-normal "><a class="color-grey-lighter" href="<?php echo $match_url; ?>">
										  	<?php
									 			$start_date =  format_date_newserver2($value->match_start);
									 			echo $start_date["date"]." ";
									 			echo $start_date["month"];

								 			?>
										  </a>
										  </h2>

										<h4 class="m0 lh28 fw-normal "> <a class="color-grey-light" href="<?php echo $match_url; ?>">
										<?php echo $value->title; ?></a>
										 </h4>
										<h1 class="post-option mt0">
										  <span class="color-grey f18"> <?php echo(isset($value->match_status))? $value->match_status : '&nbsp;'; ?> </span>
										</h1>
									  </div>

									</article>

								  </div>

								  <div class="col-md-2 col-sm-3 col-xs-6 ">
									<div class="row">
										<div class="team-second">
								   <ul style="text-align: center;">
									   <li><a href="<?php echo $match_url; ?>">
									   	<img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
									   <li><h4 class="color-jazz-grey-darker mb0"><?php echo $value->team_2->team->name; ?></h4></li>
									   <li><h4 class="color-jazz-grey-darker mt0 mb0">
									   	<?php
							    			if(!empty($value->innings[1]->runs)){
							    				echo $value->innings[1]->runs."/".$value->innings->wickets;
							    			}else{
							    				echo "-";
							    			}
							    		?>
									   </h4></li>
									   <li><span class="f12">
									   	<?php
							    			if(!empty($value->innings[1]->overs)){
							    				echo "(".$value->innings[1]->overs.")";
							    			}else{
							    				echo "-";
							    			}
							    		?>
									   </span></li>

								   </ul> </div>
									</div>
								  </div>

								  <div class="col-md-8 col-xs-12 visible-xs">
									<article class="mt20">
									  <div class="text-center">


										<h1 class="post-option mt0">
										  <span class="color-grey f16"> <?php echo(isset($value->match_status))? $value->match_status : '&nbsp;'; ?>  </span>
										</h1>
									  </div>

									</article>

								  </div>

								</div>
							  </div>
							  <!-- end livematch -->
							  <?php endforeach; ?>

							  <?php endif; ?>






							  	<!-- results -->
							  	<?php if(!empty($recent_matches)): ?>
						  		<h1 class="hidden-xs">Results</h1>
						  		<h2 class="visible-xs mt20">Result</h2>

						  		<div id="psl-recent-matches">
						  			<?php $this->load->view('jan18/psl/psl_recent_matches', $upcoming_matches); ?>
					  			</div>

						  		<div class="bottom-event-panel">
									<a href="javascript:;" id="more-psl-recent-btn" data-page-to-load="2" class='psl-more' data-role="more_recent_matches" data-container="psl-recent-matches"> More Results</a>
								</div>

						  		<?php endif; ?>





							  	<!-- upcoming	 -->
							  	<?php if(!empty($upcoming_matches)): ?>
						  		<h1 class="hidden-xs">Upcoming Matches</h1>
						  		<h2 class="visible-xs mt20">Upcoming Matches</h2>

						  		<div id="psl-upcoming-matches">
					  				<?php $this->load->view('jan18/psl/psl_upcoming_matches', $upcoming_matches); ?>
					  			</div>
					  			<div class="bottom-event-panel">
									<a href="javascript:;" id="more-psl-fixtures-btn" data-page-to-load="2" class='psl-more' data-role="more_upcoming_matches" data-container="psl-upcoming-matches"> More Fixtures</a>
								</div>


								<?php endif; ?>


						</div> <!-- fixtures-container -->




				  </div>
				  </div>




			<?php
          // $this->memcached_library->delete("cricwick_session_news");
					$news = mb_convert_encoding($this->memcached_library->get('cricwick_zain_home_news'), "UTF-8");
					$news = json_decode($news);
					$videos = mb_convert_encoding($this->memcached_library->get('cricwick_zain_home_videos'), "UTF-8");
					$videos = json_decode($videos)->timeline_videos;
					if(isset($news) && !empty($news)){
						$news = $news->data[0];
				}

          ?>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 right-column hidden-xs">
					 <div class="top-score-title right-title row-xs-10">
					 	<?php if (!empty($videos)): ?>



						<div class="">
						  	<div class="col-md-12 col-xs-12">
								<div class="widget element-size-100 widget_team">
								    <div class="cs-seprator padding-md-0" style=" margin-bottom:10px; padding: 0">
										<div class="devider1"></div>
								    </div>
						    	    <div class="">
										<h1>Featured Videos</h1>
									</div>
									<ul class="">
										<?php foreach ($videos as $keyv => $v): ?>
											<?php if($keyv > 3){break;} ?>
											<li>
										  	<figure>
										  		<a href="<?php echo base_url().'highlights/'.$v->id.'/'.$v->match_obj->series_id.'/'.seo_url($v->title).'/'; ?>">
										  			<img src="<?php echo $v->med_image; ?>" alt="" class="img-responsive">
										  		</a>
										  	</figure>
											<div class="infotext">
												<a href="<?php echo base_url().'highlights/'.$v->id.'/'.$v->match_obj->series_id.'/'.seo_url($v->title).'/'; ?>">
													<?php echo $v->title; ?>
												</a>
												<p><?php echo $v->views; ?> views</p>
											</div>
										</li>
										<?php if ($keyv+1 < count($videos)): ?>
											<li class="divider"></li>
										<?php endif ?>

										<?php endforeach ?>




									</ul>
								</div>
							</div>
						</div>




						<?php if (!empty($news)): ?>


						<div class="">
							<div class="col-md-12 col-xs-12">
								<div class="right-content bg-gradient-red1 p15">
									<h2><?php echo $news->title; ?></h2>
					          			<!-- <h5><?php //echo $news->title; ?></h5> -->
					                <p class="">
										<?php echo substr(strip_tags($news->body),0, 180); ?>...
					                </p>
					          			<h5>
						                  <a href="<?php echo $news->seo_url; ?>">Read More</a>
						                </h5>

								</div>
							</div>
						</div>

						<?php endif ?>




					<?php endif; ?>
					</div>
					<div class="clear"></div>
				</div>
            </div>
        </div>
    </div>
