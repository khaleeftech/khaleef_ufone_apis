

	<?php foreach ($upcoming_matches as $key => $value): ?>

	<?php
		$flag1 = assets_url().'images/flag-placeholder.jpg';
        $flag2 = assets_url().'images/flag-placeholder.jpg';

		// if(!empty($value->team_1->team->flag_url) && (strpos($value->team_1->team->flag_url, 'missing.png') === false && strpos($value->team_1->team->flag_url, 'placeholder.jpg') === false)){
          $flag1 =  $value->team_1->team->flag_url;
        // }

        // if(!empty($value->team_2->team->flag_url) && (strpos($value->team_2->team->flag_url, 'missing.png') === false && strpos($value->team_2->team->flag_url, 'placeholder.jpg') === false)){
          $flag2 =  $value->team_2->team->flag_url;
        // }

    ?>


	<div class="col-md-12 col-xs-12 event-fixture-box">
	<div class="remove-cursor">

	 <div class="col-xs-12 visible-xs  pb20">
		<h2 class="fw-normal text-left  m0">
	 		<a class="color-grey-lighter f16" href="javascript:;">
	 			<?php
		 			$start_date =  format_date_newserver2($value->match_start);
		 			echo $start_date["date"]." ";
		 			echo $start_date["month"];
		 			echo " | ";
		 			echo $start_date["time"];

	 			?>
	 		</a>
		</h2>

		<h4 class="m0 lh28 fw-normal text-left">
			<a class="color-grey-light mt-5" href="javascript:;">
				<?php echo($value->title)? $value->title : '&nbsp;'; ?>
			</a>
		</h4>
	 </div>


	  <div class="col-md-2 col-sm-3 col-xs-6 ">
		<div class="row">
			<div class="team-first">
	   			<ul style="text-align: center;">
				   <li><a href="javascript:;"> <img src="<?php echo $flag1; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;"></a></li>
				   <li>
				   	<h4 class="color-jazz-grey-darker mb0">
				   		<?php echo $value->team_1->team->name; ?>
				   	</h4>
				   </li>
				   <li class="hidden"><h4 class="color-jazz-grey-darker mt0 mb0">&nbsp;</h4></li>
				   <li class="hidden"><span class="f12">&nbsp;</span></li>

	   			</ul>
	   		</div>
		</div>
	  </div>
	  <div class="col-md-8 col-sm-6 col-xs-9 hidden-xs">
		<article class="mt10">
		  <div class="text-center">

			  <h2 class="fw-normal ">
			  	<a class="color-grey-lighter" href="javascript:;">
			  		<?php
		 			$start_date =  format_date_newserver2($value->match_start);
		 			echo $start_date["date"]." ";
		 			echo $start_date["month"];
		 			echo " | ";
		 			echo $start_date["time"];

	 			?>
			  	</a>
			  </h2>

			<h4 class="m0 lh28 fw-normal ">
				<a class="color-grey-light" href="javascript:;">
					<?php echo($value->title)? $value->title : '&nbsp;'; ?>
				</a>
			</h4>
			<h1 class="post-option mt0">
			  <span class="color-grey f18">
			  	<?php if(isset($value->venue) && !empty($value->venue) ){
			  			echo "".$value->venue->stadium_name.", ".$value->venue->title;
		  			}else{
		  				echo "&nbsp;";
		  			}
		  		?>
			  </span>
			</h1>
		  </div>

		</article>

	  </div>

	  <div class="col-md-2 col-sm-3 col-xs-6 ">
		<div class="row">
			<div class="team-second">
	   <ul style="text-align: center;">
		   <li>
		   	<a href="javascript:;">
		   		<img src="<?php echo $flag2; ?>" alt="" class="result-img img-responsive" style="width: 60px; margin: 0 auto;">
		   	</a>
		   </li>
		   <li><h4 class="color-jazz-grey-darker mb0 "><?php echo $value->team_2->team->name; ?>	</h4></li>
		   <li class="hidden"><h4 class="color-jazz-grey-darker mt0 mb0 ">&nbsp;</h4></li>
		   <li class="hidden"><span class="f12">&nbsp;</span></li>

	   </ul> </div>
		</div>
	  </div>

	  <div class="col-md-8 col-xs-12 visible-xs">
		<article class="mt20">
		  <div class="text-center">


			<h1 class="post-option mt0">
			  <span class="color-grey f16">
			  	<?php if(isset($value->venue) && !empty($value->venue) ){
			  			echo "".$value->venue->stadium_name.", ".$value->venue->title;
		  			}else{
		  				echo "&nbsp;";
		  			}
		  		?>
			  </span>
			</h1>
		  </div>

		</article>

	  </div>

	</div>
	</div>

<?php endforeach ?>
