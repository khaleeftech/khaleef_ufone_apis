<?php if (!empty($data)): ?>
  <table width="100%" class="table table-striped table-bordered table-hover" id="example">
    <thead>
        <tr>
            <th>Date</th>
            <th>New App Registrations</th>
            <th>New WAP Registrations</th>
            <th>Total Charged</th>
            <th>Total Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $key => $value): ?>
          <tr>
            <td><?php echo $value->the_date; ?></td>
            <td><?php echo number_format($value->app_registrations); ?></td>
            <td><?php echo number_format($value->wap_registrations); ?></td>
            <td><?php echo number_format($value->total_charged); ?></td>
            <td><?php echo number_format($value->total_amount); ?></td>
          </tr>
        <?php endforeach; ?>

    </tbody>
</table>
<?php endif; ?>
