<?php $this->load->view('figures/header'); ?>

        <div id="page-wrapper" style="margin-lefts:0;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overview <small><?php echo date('Y-m-d',strtotime("-1 days")); ?></small></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-mobile fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $app_registers ?></div>
                                    <div style="line-height:1em; height: 2em;">App Resgistrations, Yesterday</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class='hidden'>
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-laptop fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $wap_registers ?></div>
                                    <div style="line-height:1em; height: 2em;">WAP Resgistrations, Yesterday</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-group fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo number_format($billed_today) ?></div>
                                    <!-- <div style="line-height:1em; height: 2em;">Low Balance Unsubs</div> -->
                                    <div style="line-height:1em; height: 2em;">Billed Yesterday</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo "PKR ".number_format(($total_amount),0) ?></div>
                                    <!-- <div style="line-height:1em; height: 2em;">Low Balance Unsubs</div> -->
                                    <div style="line-height:1em; height: 2em;">Yesterday's Revenue</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-key fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $access ?></div>
                                    <div style="line-height:1em; height: 2em;">APP Accessed Yesterday</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class='hidden'>
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-thumbs-o-up fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $active_subs ?></div>
                                    <div style="line-height:1em; height: 2em;">Active Subs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-thumbs-o-down fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $low_unsub ?></div>
                                    <!-- <div style="line-height:1em; height: 2em;">Low Balance Unsubs</div> -->
                                    <div style="line-height:1em; height: 2em;">In-Active Subs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-glass fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $free_subs ?></div>
                                    <div style="line-height:1em; height: 2em;">Free Trial Subscriptions</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


<!-- ----------------------------------------------------------------------------------------------- -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-warning fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $nopin_today ?></div>
                                    <div style="line-height:1em; height: 2em;">Incomplete Pinflow Yesterday</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-headphones fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $cc_unsub ?></div>
                                    <div style="line-height:1em; height: 2em;">Customer Care Unsubs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $user_unsub; ?></div>
                                    <div style="line-height:1em; height: 2em;">User Unsubs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user-times  fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $purged_today ?></div>
                                    <div style="line-height:1em; height: 2em;">Purged Users</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="hidden">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>




            </div>
            <!-- /.row -->
            <div class="row">
              <?php $this_month = date('F, Y'); ?>
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Monthly Revenue
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                      <span class="graphtitle">
                                        <?php echo $this_month; ?>
                                      </span>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">

                                      <?php for($m=1; $m<=12; ++$m): ?>
                                          <li><a href="javascript:;" class="graphmonth" data-month="<?php echo date('F', mktime(0, 0, 0, $m, 1)); ?>" data-year="<?php echo date('Y', mktime(0, 0, 0, $m, 1)); ?>" ><?php echo date('F, Y', mktime(0, 0, 0, $m, 1));  ?></a>
                                          </li>
                                      <?php endfor; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>


                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-money fa-fw"></i> Calculate Revenue
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">

                              <div class="input-group date" >
                                <div class="input-group-addon" style="width:125px;">
                                    <span class="">From</span>
                                </div>
                                <input id="from" type="text" class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                              </div>

                              <div class="input-group date" >
                                <div class="input-group-addon" style="width:125px;">
                                    <span class="">To</span>
                                </div>
                                <input id="to" type="text" class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                              </div>

                              <div class="input-group">
                                <div class="input-group-addon" style="width:125px;">
                                    <span class="">Gross Revenue</span>
                                </div>
                                <input id="g_revenue" type="text" class="form-control" disabled >
                                <div class="input-group-addon">
                                    <span class="fa fa-dollar" style="width: 14px;"></span>
                                </div>
                              </div>

                              <div class="input-group">
                                <div class="input-group-addon" style="width:125px;">
                                    <span class="">Tax</span>
                                </div>
                                <input id="tax" type="text" class="form-control" disabled >
                                <div class="input-group-addon">
                                    <span class="fa fa-dollar" style="width: 14px;"></span>
                                </div>
                              </div>

                              <div class="input-group">
                                <div class="input-group-addon" style="width:125px;">
                                    <span class="">Total Revenue</span>
                                </div>
                                <input id="revenue" type="text" class="form-control" disabled >
                                <div class="input-group-addon">
                                    <span class="fa fa-dollar" style="width: 14px;"></span>
                                </div>
                              </div>

                              <br>
                              <button id="get_revenue" type="button" class="btn btn-outline btn-primary btn-lg btn-block">Get Revenue</button>

                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
                    </div>


                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

  <?php $this->load->view('figures/footer'); ?>


    <script type="text/javascript">
      revenue = '<?php echo $graph ?>';
    </script>

    <script type="text/javascript">
      function graph(json){

        // json = $.parseJSON(json);
        // Morris.Area({
        //     parseTime: false,
        //     element: 'morris-area-chart',
        //     data: json,
        //     xkey: 'date(created_at)',
        //     ykeys: ['total_charged', 'total_amount'],
        //     labels: ['Total Charged', 'Total Amount'],
        //     pointSize: 2,
        //     hideHover: 'auto',
        //     resize: true
        // });
        revenue_graph.setData(json);

      }
    </script>

    <script type="text/javascript">
      var revenue_graph = Morris.Line({
          parseTime: false,
          element: 'morris-area-chart',
          data: $.parseJSON(revenue),
          xkey: 'date(created_at)',
          ykeys: ['total_charged', 'total_amount'],
          labels: ['Total Charged', 'Total Amount'],
          pointSize: 2,
          hideHover: 'auto',
          resize: true
      });
    </script>



    <script type="text/javascript">
      $(document).ready(function(){
        // setTimeout(function(){
          // graph(revenue);
        //  }, 3000);


        $('.date').datepicker({
            // format: 'mm/dd/yyyy',
            format: 'yyyy-mm-dd',
            // startDate: '+1d',
            autoclose: true
        });


        $('.graphmonth').click(function(){
          year = $(this).attr('data-year');
          month = $(this).attr('data-month');
          title = month+", "+year;
          $('.graphtitle').html("Please wait...");

          $.ajax({
            method: "get",
            url: "<?php echo base_url().'figures/get_revenue_by_month/'; ?>"+month+"/"+year,
            data: '',
            dataType: 'json',
            success: function(response){
              if(!response || response.length == '0'){
                alert('Sorry, no data available for this time');
              }
              graph(response);
              $('.graphtitle').html(title);
            }
          });


        });

        $("#get_revenue").click(function(){
          var from = $("#from").val();
          var to = $("#to").val();
          $this = $(this);

          if(from && to){
            $this.html("Please wait...");

              $.ajax({
                method: "get",
                url: "<?php echo base_url().'figures/get_revenue/'; ?>"+from+"/"+to,
                data: '',
                dataType: 'json',
                success: function(response){
                  // alert(response.status);
                  if(response.status == '6'){
                    alert('Please input valid dates');
                  }else{
                    $("#revenue").val(response);
                    var t_rev = parseFloat(response);
                    var g_rev = (t_rev / 1.195).toFixed(2);
                    $('#g_revenue').val(g_rev);
                    var tax = (t_rev - g_rev).toFixed(2);
                    $('#tax').val(tax);
                  }

                  $this.html('Get Revenue');
                  // $("#from").val('');
                  // $("#to").val('');

                }
              });
          }


        });

      });
    </script>



</body>

</html>
