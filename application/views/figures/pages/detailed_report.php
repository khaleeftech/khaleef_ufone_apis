<?php $this->load->view('figures/header'); ?>

<div id="page-wrapper" style="margin-lefts:0;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Detailed Report</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row form-group">
      <div class="col-xs-12 col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon" style="width:84px;">
            <span class="">From</span>
          </div>
          <input id="from" type="text" class="form-control">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-6 ">
        <div class="input-group date">
          <div class="input-group-addon" style="width:84px;">
            <span class="">To</span>
          </div>
          <input id="to" type="text" class="form-control">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="row form-group">
      <div class="col-xs-12">
        <button id="get_report" type="button" class="btn btn-danger btn-lg btn-block">Get Report</button>
      </div>

    </div>


    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">


            <div class="panel panel-default" id="table-container" style="display:none;">
                <div class="panel-heading">
                    Detailed Report ( <span id="h-from"></span> TO <span id="h-to"></span> )
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body table-responsive">

                  <?php $this->load->view('figures/pages/detailed_report_table', array("data" => "")); ?>



                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->





</div><!-- $page-wrapper -->

<?php $this->load->view('figures/footer'); ?>
<script type="text/javascript">
  function makeDT(){
    $('#example').DataTable({
        bAutoWidth: false,
        responsive: true,
        dom: 'Bfrtip',
         buttons: [
             'copyHtml5',
             'excelHtml5',
             'csvHtml5',
             'pdfHtml5'
         ]
    });
    $("#table-container").show();
  }
</script>

<script type="text/javascript">
  // $(document).ready(makeDT);
</script>

<script type="text/javascript">
  $(document).ready(function(){

    $('.date').datepicker({
      // format: 'mm/dd/yyyy',
      format: 'yyyy-mm-dd',
      // startDate: '+1d',
      autoclose: true
    });


    $("#get_report").click(function(){
      var from = $("#from").val();
      var to = $("#to").val();
      $this = $(this);

      if(from && to){
        $this.html("Please wait...");
        $("#table-container").fadeOut();

          $.ajax({
            method: "get",
            url: "<?php echo base_url().'figures/get_report/'; ?>"+from+"/"+to,
            data: '',
            dataType: 'html',
            success: function(response){
              if(response.status == '6'){
                alert('Please input valid dates');
              }else{
                $("#h-from").html(from);
                $("#h-to").html(to);
                $("#table-container .panel-body").html(response);
                makeDT();
                $("#table-container").show();
              }

              $this.html('Get Report');
              // $("#from").val('');
              // $("#to").val('');

            }
          });
      }


    });

  });
</script>
