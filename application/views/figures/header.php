<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ufone Cricket - Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/figures/vendor/' ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url().'assets/figures/vendor/' ?>bootstrap/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url().'assets/figures/vendor/' ?>metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/figures/dist/' ?>css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url().'assets/figures/vendor/' ?>morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/figures/vendor/' ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"/>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<style media="screen">
  a{
    color: #333!important;
  }
</style>

<body>

    <div id="wrapper">

      <!-- Navigation -->
       <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                   <span class="sr-only">Toggle navigation</span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="<?php echo base_url().'figures' ?>" style="padding: 0 0 0 15px; margin-right: 15px;">
                 <div class="" style="display: inline-block; width: 30px; margin-right: 15px; line-height: 0; padding-top: 10px;">
                   <img src="<?php echo 'http://cricket.ufone.com/assets/april19/images/zain-logo.jpg?v=15.91'; ?>" alt="" style="max-width:100%;  ">
                 </div>
                 <div style="display: inline-block; color: #a32e16;">
                   Business Reporting
                 </div>
               </a>
           </div>
           <!-- /.navbar-header -->


           <!-- /.navbar-top-links -->

           <div class="navbar-default sidebar" role="navigation">
               <div class="sidebar-nav navbar-collapse">
                   <ul class="nav" id="side-menu">
                     <li class="sidebar-search" style="opacity:1;">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search..." disabled>
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                       <li>
                           <a href="<?php echo base_url().'figures' ?>"><i class="fa fa-dashboard fa-fw"></i> Overview</a>
                       </li>
                       <li>
                           <a href="<?php echo base_url().'figures/detailed_report' ?>"><i class="fa fa-table fa-fw"></i> Detailed Report</a>
                       </li>

                   </ul>
               </div>
               <!-- /.sidebar-collapse -->
           </div>
           <!-- /.navbar-static-side -->
       </nav>
