<div id="c-calend" class="top-score-title right-score col-md-9">
	<!--TABS START-->
	<div class="tabs animated-slide-2 matches-tbs">
		<div class="row">
			<div class="col-md-12 ">
				<div class="cs-section-title">
					<h2><?php echo $page_heading; ?></h2>
				</div>
			</div>
		</div>
		<div class="tabs animated-slide-2 matches-tbs">
			<div class="tab-content">
				<div id="results" class="tab active">
					<div class="event event-listing event-samll">
						<div id="fixtures-container">
							<?php foreach($fixtures->matches as $fixture){ ?>
								<div class="col-md-12" style="border-bottom: 6px solid #efefef;">
									<div class="row">
										<div class="col-md-2 col-xs-3">
											<div class="row">
												<div class="time"><?php echo $fixture->local_time->date; ?>
													<br><span><?php echo $fixture->local_time->month; ?></span><BR>
													<span><?php echo $fixture->local_time->time; ?></span>
												</div>
											</div>
										</div>
										<div class="col-md-10 col-xs-9">
											<article class="cs-upcomming-fixtures">
												<div class="calendar-date">
													<figure><a href="javascript:;">
														<img src="<?php echo $fixture->team_1->team->flag_url; ?>" alt="" class="result-img"></a></figure>
												</div>
											<div class="text">
												<h2 >
													<a class="hidden-xs" href="javascript:;"><?php echo $fixture->team_1->team->name; ?></a>
													<span>VS</span>
													<a class="hidden-xs" href="javascript:;"><?php echo $fixture->team_2->team->name; ?></a>
												</h2>
												<ul class="post-option mt0 hidden-xs">
													<li><?php echo $fixture->title; ?><BR><?php echo $fixture->series->title; ?></li>
												</ul>
											</div>
												<div class="match-result">
													<figure><a href="javascript:;">
														<img src="<?php echo $fixture->team_2->team->flag_url; ?>" alt="" class="result-img"></a></figure>
												</div>
											</article>
											<ul class="post-option mt0 visible-xs"><li><?php echo $fixture->title; ?><BR><?php echo $fixture->series->title; ?></li></ul>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<?php if ($fixtures->page_number<$fixtures->total_pages){ ?>
							<div class="bottom-event-panel">
								<a href="javascript:;" id="more-fixtures-btn" data-page-to-load="2"> More Fixtures</a> 
							</div>
						<?php } ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!--TABS END-->
</div>