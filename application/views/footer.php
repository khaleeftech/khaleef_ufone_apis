<footer id="site-footer" class="clearfix">
	<div id="footer-bottom" class="clearfix">
		<div class="container">
			<div class="copyright text-center">
				<small id="copyright" role="contentinfo"> &copy; <?php echo date('Y'); ?> Ufone Cricket. All Rights Reserved.</small>
			</div>
			<div class="socialmedia pull-right hidden" tabindex="1000">
				<ul class="mt5 pl0">
					<li><a data-original-title="Facebook" class="addthis_button_facebook at300b" title="Facebook" href="javascript:;" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a data-original-title="twitter" class="addthis_button_twitter at300b" title="Tweet" href="javascript:;" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a data-original-title="google-plus" class="addthis_button_google at300b" target="_blank" title="Google Bookmark" href="javascript:;"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<div id='user-modal' class="modal fade bs-example-modal-sm alert-popup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
	  <div class="modal-header">
	  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="opacity:1;color:#333;">×</button>
	  	<h4 class="mt0 mb0">Ufone Cricket</h4>
	  </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-confirm" data-dismiss="modal" onclick="window.location = '<?php echo base_url('logout'); ?>'">Logout</button>
      </div>
    </div>
  </div>
</div>
