<BR>
<div class="top-score-title right-score col-md-12" >
	<div class="top-score-title player-vs">

		<div class="main">
			
			
			<div class="tab-content">
				<div class=" pt10 pr10 pl10">
					<div class="innings-information">
						<?php if ($match->format!='Test'){ ?>
							<div class="team-1-name">
								<?php
								$match->innings[$current_inning]->wickets = empty($match->innings[$current_inning]->wickets)?0:$match->innings[$current_inning]->wickets;
	
								$match->innings[$current_inning]->run_rate = empty($match->innings[$current_inning]->run_rate)?"0.0":$match->innings[$current_inning]->run_rate;
	
								$match->innings[$current_inning]->runs = empty($match->innings[$current_inning]->runs)?0:$match->innings[$current_inning]->runs;
	
								$match->innings[$current_inning]->overs = empty($match->innings[$current_inning]->overs)?0:$match->innings[$current_inning]->overs;
	
								?>
	                        	<?php echo $teams[$match->innings[$current_inning]->batting_team_id]->name; ?> 
	                        	<span class="innings-1-score innings-current"> 
	                        	<?php echo $match->innings[$current_inning]->runs."/".$match->innings[$current_inning]->wickets." (".$match->innings[$current_inning]->overs."/".$match->innings[$current_inning]->total_overs.")"; ?>	
	                        	</span> 
	                        </div>
	                        
	                        <?php if (count($match->innings)>1){ ?>
	                        	<div class="team-2-name"> 
		                        	<?php echo $teams[$match->innings[$current_inning-1]->batting_team_id]->name; ?>
		                        	<span class="innings-1-score "> 
		                        		<?php echo $match->innings[$current_inning-1]->runs."/".$match->innings[$current_inning-1]->wickets." (".$match->innings[$current_inning-1]->overs."/".$match->innings[$current_inning-1]->total_overs.")"; ?>
		                        	</span> 
	                        	</div>
	                        <?php } ?>
                        <?php } ?>
						<div class="innings-requirement">
							<?php
								echo $teams[$match->toss_won_by_id]->name;
							?>
							won the toss and chose to <?php echo $match->chose_to=="Bowl"?'Field':'Bat'; ?> first.
							<BR>
							<?php
							if ($match->match_state=="Over"){
								echo $match->match_result;
							} else {
								?>
								<?php if (count($match->innings)==1){ ?>
									<div class="innings-requirement"> Run Rate: <?php echo $match->innings[$current_inning]->run_rate; ?>  </div>
								<?php } else { ?>
		                        	<div class="innings-requirement">
		                        		Run Rate: <?php echo $match->innings[$current_inning]->run_rate; ?><BR>
		                        		Req Rate: <?php echo $match->innings[$current_inning]->required_rate; ?><BR>
		                        		Need <?php echo ($match->innings[$current_inning]->runs_required+1-$match->innings[$current_inning]->runs) ?> Runs for <?php echo $match->innings[$current_inning]->balls_remaining; ?> Balls
		                        	</div>
		                        <?php } ?>
		                    <?php
							}
							?>
						</div>
					</div>
					<hr class="secondary-hr">
				</div>
				<?php 
					$innings_label = array();
					$innings_label[1] = array("full"=>"1st Innings","small"=>"1st Inn");
					$innings_label[2] = array("full"=>"1st Innings","small"=>"1st Inn");
					$innings_label[3] = array("full"=>"2nd Innings","small"=>"2nd Inn");
					$innings_label[4] = array("full"=>"2nd Innings","small"=>"2nd Inn");
					
				?>
				<div class=" pr10 pl10">
					<ul class="tab-links-match">
						<?php foreach($match->innings as $key=>$inning){  ?>
			            	<li class="<?php echo (($key+1)==count($match->innings))?"active":""; ?>">
			            		<a class="scorecard-tabs" tabid="#tab<?php echo ($key+1); ?>">
			            			<span class="visible-xs"><?php echo $teams[$inning->batting_team_id]->short_name." (".$innings_label[$inning->innings_order]["small"].")"; ?></span>
			            			<span class="hidden-xs"><?php echo $teams[$inning->batting_team_id]->name." (".$innings_label[$inning->innings_order]["full"].")"; ?></span>
			            		</a>
			            	</li>
			            <?php } ?>
			        </ul>
		        </div>
				<div class="clearfix"></div>
				<?php
				

				foreach($match->innings as $key=>$inning){ 
				?>
							
				<div id="tab<?php echo ($key+1); ?>" class="tab <?php echo (($key+1)==count($match->innings))?"active":""; ?>">
					<div class="col-md-12">
						<div class="row">
							<div class="large 20 columns">
								<table width="100%" class="batting-table innings">
								<tbody>
								<tr class="tr-heading">
									<th colspan="2" scope="col" class="th-innings-heading">
										<?php echo $teams[$inning->batting_team_id]->name ?> 
									</th>
									<th scope="col" class="th-r">R</th>
									<th scope="col" class="th-b">B</th>
									<th scope="col" class="th-4s">4s</th>
									<th scope="col" class="th-6s">6s</th>
									<th scope="col" class="th-sr hide-sr">SR</th>
								</tr>
								<?php foreach($inning->batting_scorecard as $batsman){ ?>
								<tr>
									<td class="visible-xs hidden-sm hidden-md scorecard_palyer_name" colspan="2">
										<a href="javascript:;" class="playerName"><?php echo $batsman->player->name; ?></a>
										<BR>
										<?php echo $batsman->batsman->out_details?$batsman->batsman->out_details:"(not out)"; ?>
									</td>
									<td class="batsman-name hidden-xs">
										<a href="javascript:;" class="playerName"><?php echo $batsman->player->name; ?></a>
									</td>
									<td class="dismissal-info hidden-xs"><?php echo $batsman->batsman->out_details?$batsman->batsman->out_details:"(not out)"; ?></td>
									<td class="bold"><?php echo $batsman->batsman->runs_scored; ?></td>
									<td class=""><?php echo $batsman->batsman->balls_played; ?></td>
									<td class=""><?php echo $batsman->batsman->boundry_4s_scored; ?></td>
									<td class=""><?php echo $batsman->batsman->boundry_6s_scored; ?></td>
									<td class="hide-sr"><?php echo $batsman->batsman->strike_rate; ?></td>
								</tr>
								<?php } ?>
								<tr class="extra-wrap">
									<td class="extra">
										Extras
									</td>
									<td class="extra-details">
										<?php
											$total_extras = 0;
											$string = array();
											if ($inning->extra_leg_bye>0){
												$string[] = "lb ".$inning->extra_leg_bye;
												$total_extras += $inning->extra_leg_bye;
											}
											if ($inning->extra_bye>0){
												$string[] = "b ".$inning->extra_bye;
												$total_extras += $inning->extra_bye;
											}
											if ($inning->wide_ball>0){
												$string[] = "wd ".$inning->wide_ball;
												$total_extras += $inning->wide_ball;
											}
											if ($inning->no_ball>0){
												$string[] = "nb".$inning->no_ball;
												$total_extras += $inning->no_ball;
											}
											if (!empty($string)){
												echo "(".implode(", ",$string).")";
											} else {
												echo "-";
											}
										?>
									</td>
									<td class="bold">
										<?php echo $total_extras; ?>
									</td>
									<td class="bold"></td>
									<td class="bold"></td>
									<td class="bold"></td>
									<td class="bold"></td>
								</tr>
								<tr class="total-wrap">
									<td class="total">
										<b>Total</b>
									</td>
									<td class="total-details">
										(<?php echo $inning->wickets; ?> wickets, <?php echo $inning->overs; ?> overs)
									</td>
									<td class="bold">
										<b><?php echo $inning->runs; ?></b>
									</td>
									<td class="rpo" colspan="4">(<?php echo $inning->run_rate; ?> runs/over)</td>
									
								</tr>
								</tbody>
								</table>
								<div class="more-match-stats">
									<hr class="secondary-hr">
									<div class="to-bat hidden">
										<p>
											<span class="head">Did not bat</span><span class="bold"><a href=" " target="" class="playerName color-theme" title="view the player profile for Kane Richardson">KW Richardson</a></span>, <span class="bold"><a href=" " target="" class="playerName color-theme" title="view the player profile for Nathan Lyon">NM Lyon</a></span>
										</p>
									</div>
									<hr class="secondary-hr hidden">
									<div class="fow">
										<p>
											<span class="head">Fall of wickets</span>
											<a href="javascript:;" class="fowLink" title="">
											<?php 
												$wickets = array();
												foreach($inning->falls_of_wickets as $key=>$wicket){
													$wickets[] = "<span>$wicket->wicket_order-$wicket->team_score (".$wicket->out_batsman->player->short_name.", $wicket->ball ov)</span>";
												}
												$wickets = implode(", ", $wickets);
												echo $wickets;
											?>
											</a>
										</p>
									</div>
								</div>
								<table width="100%" class="bowling-table">
								<tbody>
								<tr class="tr-heading">
									<th scope="col" class="th-innings-heading">Bowling</th>
									<th scope="col" class="th-o" title="overs bowled"><b>O</b></th>
									<th scope="col" class="th-m" title="maidens bowled"><b>M</b></th>
									<th scope="col" class="th-r" title="runs conceded"><b>R</b></th>
									<th scope="col" class="th-w" title="wickets taken"><b>W</b></th>
									<th scope="col" class="th-w" title="dot balls bowled"><b>0s</b></th>
									<th scope="col" class="th-w" title="boundary fours conceded"><b>4s</b></th>
									<th scope="col" class="th-w" title="boundary sixes conceded"><b>6s</b></th>
								</tr>
								<?php foreach($inning->bowling_scorecard as $bowler){ ?>
									<tr>
										<td class="bowler-name">
											<a href="javascript:;" target="" class="playerName" title=""><?php echo $bowler->player->name; ?></a>
										</td>
										<td><?php echo $bowler->bowler->overs_bowled; ?></td>
										<td><?php echo $bowler->bowler->overs_maiden; ?></td>
										<td><?php echo $bowler->bowler->runs_given; ?></td>
										<td><?php echo $bowler->bowler->wickets_taken; ?></td>
										<td><?php echo $bowler->bowler->dots_bowled; ?></td>
										<td><?php echo $bowler->bowler->boundry_4s_given; ?></td>
										<td><?php echo $bowler->bowler->boundry_6s_given; ?></td>
									</tr>
								<?php } ?>
								</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>
				<?php } ?>

				
			</div>
				
		</div>
	</div>
</div>
<BR><BR>