<html lang="en-US" xmlns:fb="http://ogp.me/ns/fb#">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# video: http://ogp.me/ns/video#">
<title>CricBoom: The Latest Cricket Updated</title>
<script src="<?php echo base_url(); ?>jwplayer/swfobject.js"></script>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-88034149-3"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-119615143-1');
</script>

<!-- Google Analytics Code -->



<style>
.jw-icon-next, .jw-icon-playlist, .jw-icon-prev{
    display: none !important;
}
</style>
<script src="<?php echo base_url(); ?>jwplayer/jwplayer.js"></script>
<script>jwplayer.key="vkFxGOqRxYPKvhFSCw2QvpCcM3C7Aii/jSTZ/w==";</script>
</head>
<body style="margin: 0px;">
<div id="myElement"></div>
<script type="text/javascript">
var playerInstance = jwplayer("myElement");
playerInstance.setup({
    playlist: [{
		file: "<?php echo $video_file; ?>",
	    image: "<?php echo $video->thumb_file; ?>",
	    title: '<?php echo $video->title; ?>'
	}],
    width: "100%",
    height: "480",
    controls: true,
    ga: {}
});
</script>
</body>
</html>
