<!DOCTYPE html>
<html lang="en">

<head>
	<title><?php echo $og_tags->title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $og_tags->descr; ?>">
    <link rel="shortcut icon" href="<?php echo assets_url(); ?>images/fav/ico-16.png">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="<?php echo assets_url(); ?>images/fav/ico-144.png" sizes="144x144">

    <meta property="og:site_name" 		   content="Jazz Cricket"/>
	<meta property="fb:app_id" 			   content="430752913746475" />
	<?php if (!empty($video_page)){ ?>

	<meta property="og:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta property="og:title" content="<?php echo $current_video->social->title; ?>" />
	<meta property="og:description" content="<?php echo $current_video->social->description; ?>" />
	<meta property="og:image" content="<?php echo $current_video->social->thumb_file; ?>" />
	<?php /*
	<meta property="og:type" content="website" />
	<meta property="og:video:width" content="640" />
	<meta property="og:video:height" content="320" />
	<meta property="og:video:type" content="video/mp4" />
	<meta property="og:video:url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />
	<meta property="og:video:secure_url" content="<?php echo base_url() . "embed/serve/" . $current_video -> id; ?>" />
	
	<meta name="twitter:card" content="player">
	<meta name="twitter:app:name:googleplay" content="CricBoom">
	<meta name="twitter:app:id:googleplay" content="com.Khaleef.CricBoom" />
	<meta name="twitter:player" content="<?php echo base_url(). "embed/video/".$current_video -> id; ?>" />
	<meta name="twitter:player:stream" content="<?php echo base_url()."embed/serve/".$current_video -> id; ?>" />
	<meta name="twitter:player:stream:content_type" content="video/mp4" />
	<meta name="twitter:player:width" content="480">
	<meta name="twitter:player:height" content="480">
	*/ ?>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo $current_video->social->link; ?>" />
	<meta name="twitter:title" content="<?php echo $current_video->social->title; ?>">
	<meta name="twitter:description" content="<?php echo $current_video->social->description; ?>">
	<meta name="twitter:image" content="<?php echo $current_video->social->thumb_file; ?>">
	
	<?php } ?>
	<style>
		.user {
		   top: 20px !important;
		}
		@media(max-width: 767px) { 
			.user {
			   top: 7px !important;
			}
		}
	</style>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo assets_url();  ?>home/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo assets_url();  ?>home/css/half-slider.css?v=<?php echo VERSION; ?>" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/custom.css?v=<?php echo VERSION; ?>" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>css/custom-front.css?v=<?php echo VERSION; ?>>" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/owl.theme.css" rel="stylesheet">
    <link href="<?php echo assets_url();  ?>home/css/font-awesome.min.css" rel="stylesheet">  
	<link href="<?php echo assets_url();  ?>css/cric_css/style_asim.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />  
	<link href="<?php echo assets_url(); ?>css/layout.css?v=<?php echo VERSION; ?>" rel="stylesheet" type="text/css" />

	<meta name="google-site-verification" content="vFrwbZ1fAeGzbO4yDWsPPW0q1FH8-3F7KOUTk7S5lOk" />
	<meta name="msvalidate.01" content="73818B487022F81E4006E2364DA72CCA" />
	
	<meta property="og:url"  content="<?php echo current_url(); ?>" />
	<meta property="og:title" content="<?php echo $og_tags->title; ?>" />
	<meta property="og:description" content="<?php echo $og_tags->descr; ?>" />
	<meta property="og:image" content="<?php echo base_url()."assets/images/default_banner_icc.jpg"; ?>" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@CricBoomLive">
	<meta name="twitter:url"  content="<?php echo current_url(); ?>" />
	<meta name="twitter:title" content="<?php echo $og_tags->title; ?>">
	<meta name="twitter:description" content="<?php echo $og_tags->descr; ?>">
	<meta name="twitter:image" content="<?php echo base_url()."assets/images/default_banner_icc.jpg"; ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.<?php echo assets_url();  ?>home/js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php if (ENVIRONMENT=='production'){ ?>
<!-- Google Analytics Code -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88965851-3', 'auto');
  ga('send', 'pageview');

</script>
<!-- Google Analytics Code -->

<script src="https://www.gstatic.com/firebasejs/3.7.5/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyArlqEz6Toa5ueId7_X45Sft1jm5gQDOXA",
    authDomain: "jazz-cricwick.firebaseapp.com",
    databaseURL: "https://jazz-cricwick.firebaseio.com",
    projectId: "jazz-cricwick",
    storageBucket: "jazz-cricwick.appspot.com",
    messagingSenderId: "721784746258"
  };
  firebase.initializeApp(config);
</script>
<?php } ?>
</head>

<body id="videos_page">

	<?php $this->load->view("navigation"); ?>
  <div class="hidden-sm hidden-md hidden-lg" style="max-width: 100%;">
    <img src="<?php echo assets_url(); ?>ads/top_app_mob_banner.jpg" class="playstore_link" style="max-width: 100%;"/>
  </div>
<div class="wrapper">
  
    <!-- Half Page Image Background Carousel Header -->
<?php if (!empty($live_matches)){ ?>
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        <?php foreach($live_matches as $key=>$live){ ?>
        	<?php if (count($live_matches)>1){ ?>
            	<li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="<?php echo $key==0?"active":''; ?>"></li>
            <?php } ?>
		<?php } ?>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
        <?php foreach($live_matches as $key=>$live){ 
            $title = $live->team_1->team->name." vs ".$live->team_2->team->name;

            $match_url = base_url()."live/".$live->id."/".seo_url($title)."/";
            ?>
            <div class="item <?php echo $key==0?"active":''; ?>">
                <a href="<?php echo base_url()."live/".$live->id."/".seo_url($title)."/" ?>">
	                <img src="<?php echo empty($live->banner_file_name)?$default_banner: $live->full_banner_url; ?>" >
	                <div class="carousel-caption">
        						<a href="<?php echo $match_url; ?>">
        						<?php 
        						if ($live->format=='Test' && !empty($live->break_type)){
									echo '<h3><span class="livenow"> Day '.$live->day.' | '.$live->break_type.'</span>';
		                        } else {
		                        ?> 
        						<h3><span class="livenow">Watch Live Now!</span><span class="watch hidden-xs">Exclusive Coverage <i class="fa fa-caret-right"></i></span></h3>
        						<?php } ?>
        						<h2 class="hidden-xs hidden" style="font-weight: bold;"><?php echo $live->title; ?></h2>
        						<h2 class="hidden-sm hidden-md hidden-lg small_size hidden"><?php echo $live->team_1->team->short_name." vs ".$live->team_2->team->short_name; ?></h2>
        						
        							<?php
                      
        							if (!empty($live->innings)){
				                        $current_innings = count($live->innings)-1;
				                        if (empty($live->innings[$current_innings]->wickets) || $live->innings[$current_innings]->wickets==''){
				                          $live->innings[$current_innings]->wickets = 0;
				                        }
				                        $live->innings[$current_innings]->runs = empty($live->innings[$current_innings]->runs)?0:$live->innings[$current_innings]->runs;
				                        echo '<h3><span class="score">';
				                        $current_batting_team = $live->innings[$current_innings]->batting_team_id;
				                        echo $teams[$current_batting_team]->short_name." ";
				                        echo $live->innings[$current_innings]->runs."/".$live->innings[$current_innings]->wickets;
				                        echo '<sup>*</sup></span>';
				                        if (!empty($live->innings[$current_innings]->runs_required) && $live->format!='Test'){
				                          echo ' | <span class="score">Target '.($live->innings[$current_innings]->runs_required+1).'</span>';
				                        } 
				                        echo "</h3>";
				                      }
        							?>
        							
        							<?php if (count($live->innings)==2){/* ?>
        							 | <span class="score">Target <?php echo $live->innings[0]->runs_req+$live->innings[0]->runs; ?></span>
        							<?php */} ?>
        						
        						</a>
        					</div>
                </a>
            </div>
		<?php } ?>
        </div>
		<?php if (count($live_matches)>1){ ?>
	        <!-- Controls -->
	        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
	            <span class="icon-prev"></span>
	        </a>
	        <a class="right carousel-control" href="#myCarousel" data-slide="next">
	            <span class="icon-next"></span>
	        </a>
		<?php } ?>
    </header>
    
<?php } else { ?>

    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        <?php foreach($upcoming_fixtures as $key=>$uf){ ?>
        	<?php if (count($upcoming_fixtures)>1){ ?>
        		<?php 
	        		if ($key>4){
	        			break;
	        		}
	        	?>
            	<li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="<?php echo $key==0?"active":''; ?>"></li>
            <?php } ?>
		<?php } ?>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
        <?php foreach($upcoming_fixtures as $key=>$uf){ ?>
        	<?php 
        		if ($key>4){
        			break;
        		}
        	?>
            <div class="item <?php echo $key==0?"active":''; ?>">
                <img src="<?php echo empty($uf->banner_file_name)?$default_banner: $uf->full_banner_url; ?>" >
                <div class="carousel-caption">
        					<a href="javascript:;">
        					<h3><span class="upcoming">Upcoming</span></h3>
        					<h2 class="hidden-xs hidden" style="font-weight: bold;"><?php echo $uf->team_1->team->name." vs ".$uf->team_2->team->name; ?></h2>
        					<h2 class="hidden-sm hidden-md hidden-lg small_size hidden"><?php echo $uf->team_1->team->short_name." vs ".$uf->team_2->team->short_name; ?></h2>
        					<?php $local = format_date_homebanner_newserver($uf->match_start); ?>
        					<h3><span class="score"><?php echo $local["day"].", ".$local["month"]." ".$local["date"]." | ".$local["hours"].":".$local["minutes"]." ".$local["meridiem"]; ?></span></h3>
        					</a>
        				</div>
            </div>
		<?php } ?>
        </div>
    </header>
    
<?php } ?>
</div>
<div class="wrapper-main">
	<div class="adds-hrz hidden" style="padding: 0px;">
		<a href="http://vbox.pk/" target="_blank"><img src="<?php echo assets_url(); ?>ads/vbox.jpg" alt=""/></a>
	</div>
    <!-- Page Content -->
    <div class="container">
    	
    	<div class="col-lg-12 crk-trending-videos-container">
	      <h1>Trending</h1>
	      <div class="cs-seprator">
	        <div class="devider1"></div>
	      </div>
	      <div class="customNavigation"><a class="next-trending" href="<?php echo base_url()."videos/"; ?>"> More <i class="fa fa-chevron-right"> </i> </a> </div>
	      <div class="col-md-12" style="padding-left:0; padding-right:0;">
	      	<?php foreach($timeline_videos as $key=>$video){ 
	      		if($key>3){
	      			break;
	      		}
	      		?>
				<div class="col-md-3 col-sm-3 col-xs-6 <?php echo $key>1?'hidden-xs':''; ?>" href="<?php echo base_url()."highlights/".$video->id."/".seo_url($video->title)."/"; ?>">
					<div class="video-container">
						<div class="video-thumbnail">
							<img src="<?php echo $video->med_image; ?>" class="img-home-video img-responsive">
							<img class="video-play-icon img-home-video" src="<?php echo assets_url(); ?>images/play.png">
							<div class="overlay">
							</div>
							<span class="video-view-count"> <?php echo $video->views; ?> views </span><span class="video-duration fa fa-clock-o hidden"> 05:56 </span>
						</div>
						<div class="video-content">
							<p class="video-description crop_to_one_line"><?php echo $video->title; ?></p>
						</div>
					</div>
				</div>
			<?php } ?>
	      </div>
	      <div class=" clearfix" style=" margin-bottom:20px;"></div>
	    </div>
		
		<div class="col-lg-12 crk-trending-videos-container">
			<h1>News &amp; Features</h1>
			<div class="cs-seprator">
				<div class="devider1">
				</div>
			</div>
			<div class="customNavigation">
				<a class="next-trending" href="<?php echo base_url()."news/"; ?>"> More <i class="fa fa-chevron-right"></i></a>
			</div>
			<?php foreach($news as $key=>$n){ 
				if($key>3){
					break;
				}
				?>
				<?php if (($key)%2==0){ ?>
					<div class="col-md-12 <?php echo $key>0?'hidden-xs':''; ?>" style="">
				<?php } ?>
					<div class="col-xs-12 col-md-6 col-sm-6" onclick="window.location='<?php echo base_url()."news-update/$n->id/".seo_url($n->title)."/"; ?>'">
						<div class="news-page">
							<div class="col-md-6 col-sm-6 pl0 pr10 pb10">
								<img src="<?php echo $n->image_or_thumb_file; ?>" alt="" class="img-djoko img-home-item" style="width:100%">
							</div>
							<div class="col-md-6 data-news-pg pl10 pr0">
								<h4 class="pt0 mb0 mt0 crop_to_one_line"><?php echo $n->title; ?></h4>
								<p class="news-dd">
									<i>by <?php echo $n->author; ?></i>
								</p>
								<p class="news-dd"><?php echo date('F j, Y', $n->created_at/1000); ?></p>
								<div class="clear">
								</div>
								<p class="mt0"><?php echo substr(strip_tags($n->details),0, 180); ?>...
									<a class="read_more_anchor" href="<?php echo base_url()."news-update/$n->id/".seo_url($n->title)."/"; ?>"> Read More <i class="fa fa-chevron-right"></i></a>
								</p>
							</div>
						</div>		
					</div>
				<?php if (($key)%2!=0){ ?>
					</div>
				<?php } ?>
			<?php } ?>
		</div>

        <div class="clearfix"></div>
		
		<div class="col-lg-12">
	      <div class="adds-hrz mt20 ">
			<a href="javascript:;" class="playstore_link"><img src="<?php echo assets_url(); ?>ads/bottom_banner_1.jpg" alt=""/></a>
		  </div>
	    </div>
		
    </div>
    
    <div class="clear"></div>
    <!-- /.container -->
    <?php $this->load->view("footer"); ?>
</div>
    <!-- jQuery -->
    <script src="<?php echo assets_url();  ?>home/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo assets_url();  ?>home/js/bootstrap.min.js"></script>
    <script src="<?php echo assets_url();  ?>home/js/jquery.mobile.custom.js"></script>  

    <script src="<?php echo assets_url();  ?>home/js/owl.carousel.min.js"></script>

    <!-- Script to Activate the Carousel -->

    <script>
    $('body').on('click','.playstore_link', function (e){
    
    
      sUsrAg = navigator.userAgent;
        if ((sUsrAg.indexOf("Android") > -1)) {
          ga('send', 'event', 'APP Banner', 'Click', 'Android', 1);
          window.open("market://details?id=com.Khaleef.CricWickMobilink");
      } else {
      	ga('send', 'event', 'APP Banner', 'Click', 'Non-Android', 1);
        win = window.open('https://play.google.com/store/apps/details?id=com.Khaleef.CricWickMobilink', '_blank');
        win.focus();
      }
    });
    $("body").on("click",".user", function (e){
		//e.preventDefault();	
		var phone = $(this).attr("phone");
		var sub_status = $(this).attr("sub_status");
		var msg = "Welcome <b>"+phone+"</b><BR>";
		msg = msg + "Subscription Status: ";
		if (sub_status==1){
			msg = msg + "Active";
		} else if (sub_status==3){
			msg = msg + "Suspended";
		} else {
			msg = msg + "Trial";
		}
		$('.alert-popup .modal-body').html(msg);
		$('.alert-popup').modal('show');
	});
    </script>

    <script>
		
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        });
		

        $(document).ready(function() {
        	
        	
        	
        	$("#myCarousel").swiperight(function() {  
		      $("#myCarousel").carousel('prev');  
		    });  
		   $("#myCarousel").swipeleft(function() {  
		      $("#myCarousel").carousel('next');  
		   }); 
        	
        	$('.item  .img-home-item').on('click', function(event){
			    var link = $(this).parent().parent().attr("href");
			    if (link!='javascript:;'){
			    	window.location = link;
			    }
			});
			
			$('.video-container .img-home-video').on('click', function(event){
			    var link = $(this).parent().parent().parent().attr("href");
			    if (link!='javascript:;'){
			    	window.location = link;
			    } else {
			    	console.log("error");
			    }
			});
			
			$('.video-container .video-content .video-description').on('click', function(event){
			    var link = $(this).parent().parent().parent().attr("href");
			    if (link!='javascript:;'){
			    	window.location = link;
			    }
			});
			
			$('.item .data-news-pg').on('click', function(event){
			    var link = $(this).parent().attr("href");
			    console.log(link);
			    if (link!='javascript:;'){
			    	window.location = link;
			    }
			});
         
            var trending = $("#trending");

            trending.owlCarousel({
              items : 4, //10 items above 1000px browser width
              itemsDesktop : [1000,3], //5 items between 1000px and 901px
              itemsDesktopSmall : [900,2], // betweem 900px and 601px
              itemsTablet: [600,2], //2 items between 600 and 0
              itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
            });

            // Custom Navigation Events
            $(".next-trending").click(function(){
            trending.trigger('owl.next');
            });

            $(".prev-trending").click(function(){
            trending.trigger('owl.prev');
            });

            var news = $("#news");

            news.owlCarousel({
              items : 2, //10 items above 1000px browser width
              itemsDesktop : [1000,2], //5 items between 1000px and 901px
              itemsDesktopSmall : [900,2], // betweem 900px and 601px
              itemsTablet: [600,2], //2 items between 600 and 0
              itemsMobile : [400,1] // itemsMobile disabled - inherit from itemsTablet option
            });

            // Custom Navigation Events
            $(".next-news").click(function(){
            news.trigger('owl.next');
            });

            $(".prev-news").click(function(){
            news.trigger('owl.prev');
            });
			/*
            var articles = $("#articles");

            articles.owlCarousel({
              items : 4, //10 items above 1000px browser width
              itemsDesktop : [1000,3], //5 items between 1000px and 901px
              itemsDesktopSmall : [900,2], // betweem 900px and 601px
              itemsTablet: [600,2], //2 items between 600 and 0
              itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
            });

            // Custom Navigation Events
            $(".next-articles").click(function(){
            articles.trigger('owl.next');
            });

            $(".prev-articles").click(function(){
            articles.trigger('owl.prev');
            });

            var memes = $("#memes");

            memes.owlCarousel({
              items : 4, //10 items above 1000px browser width
              itemsDesktop : [1000,3], //5 items between 1000px and 901px
              itemsDesktopSmall : [900,2], // betweem 900px and 601px
              itemsTablet: [600,2], //2 items between 600 and 0
              itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
            });

            // Custom Navigation Events
            $(".next-memes").click(function(){
                var memes = $("#memes");
                console.log("next");
            memes.trigger('owl.next');
            });

            $(".prev-memes").click(function(){
            memes.trigger('owl.prev');
            });
			*/
        });

    </script>

</body>

</html>
