<?php
  class Figure extends CI_Model {

    function __construct(){
      parent::__construct(); // construct the Model class
      $this->mdb = $this->load->database('figure', TRUE);
    }

    function get_revenue($from = '', $to = ''){
      if(empty($from) && empty($to)){

        $year = date('Y');
        $month = date('m');

        // $month = '01';

        // $day = '01';
        $day1 = date('d', strtotime('-1 day', strtotime('01'.'-'.$month.'-'.$year)));
        $month1 = date('m', strtotime('-1 day', strtotime('01'.'-'.$month.'-'.$year)));
        $year1 = date('Y', strtotime('-1 day', strtotime('01'.'-'.$month.'-'.$year)));

        $time = '19:00:00';

        $from = $year1."-".$month1."-".$day1." ".$time;

        $year2 = $year;


        $month2 = $month;

        $day2 = date('m', strtotime('+1 month', strtotime('01'.'-'.$month.'-'.$year)));
          $day2 = date('d', strtotime('-1 day', strtotime('01'.'-'.$day2.'-'.$year)));

        $time2 = '19:00:00';

        $to = $year2."-".$month2."-".$day2." ".$time2;

      }else{
        // $time = '19:00:00';

        // $to = date('Y-m-d', strtotime('+1 day', strtotime($to)));
        // $to = trim($to)." $time";


        // $from = trim($from)." $time";
        // $from = date('Y-m-d', strtotime('-1 day', strtotime($from)));
        // $from = date('Y-m-d', strtotime($from));
        // $from = trim($from)." $time";

        $sql = "select SUM(amount) as total_amount from ufone_charges where date >= '$from' AND date <= '$to'";
        $query = $this->mdb->query($sql);
        $result = $query->row();
        $total_amount = round($result->total_amount,2);
        #foreach ($result as $key => $value) {
        #  $total_amount = $total_amount + $value['total_amount'];
        #}
        // return json_encode( $sql );
        return json_encode( $total_amount );


      }


      $sql = "select *, date(created_at) from ufone_charge_stats where created_at >= '$from' AND created_at <= '$to'";
      $query = $this->mdb->query($sql);
      // echo $sql;
      // exit;
      return json_encode($query->result(true));

    }


    function get_overview(){
      $date = date('Y-m-d',strtotime("-1 days"));
      $sql = "SELECT ";
        $sql .= " 	SUM( CASE WHEN udid IS NOT NULL AND DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) = '{$date}' and (telco_id=8) and status in (1,2) THEN 1 ELSE 0 END ) app_registrations, ";
        $sql .= " 	SUM( CASE WHEN udid IS NULL AND DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) = '{$date}' and (telco_id=8) and status in (1,2)  THEN 1 ELSE 0 END ) wap_registrations, ";
        $sql .= " 	SUM( CASE WHEN DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) = '{$date}' and (telco_id=8) and status =0 and unsub_reason is null  THEN 1 ELSE 0 END ) nopin_today, ";
        $sql .= " 	SUM( CASE WHEN status = '1' and (telco_id=8) THEN 1 ELSE 0 END ) active_subs, ";
        $sql .= " 	SUM( CASE WHEN status = '2' AND current_tick = '1' and (telco_id=8) THEN 1 ELSE 0 END ) free_subs, ";
        $sql .= " 	SUM( CASE WHEN DATE(CONVERT_TZ(last_accessed_at, '+00:00', '+05:00')) = '{$date}' AND udid IS NOT NULL and (telco_id=8) THEN 1 ELSE 0 END ) app_accessed_today, ";
        $sql .= " 	SUM( CASE WHEN DATE(CONVERT_TZ(updated_at, '+00:00', '+05:00')) = '{$date}' AND status=0 AND unsub_reason=6 and (telco_id=8) THEN 1 ELSE 0 END ) purged_today, ";
        $sql .= " 	SUM( CASE WHEN unsub_reason = '2' and (telco_id=8) THEN 1 ELSE 0 END ) cc_unsub, ";
        $sql .= " 	SUM( CASE WHEN unsub_reason = '1' and (telco_id=8) THEN 1 ELSE 0 END ) user_unsub, ";
        $sql .= " 	SUM( CASE WHEN unsub_reason = '3' and status='2' and (telco_id=8) THEN 1 ELSE 0 END ) low_unsub ";
        // $sql .= "   SUM( CASE WHEN unsub_reason = '3' and (telco_id=5 OR telco_id=6) THEN 1 ELSE 0 END ) low_unsub ";
        
      $sql .= " FROM subscriptions ";

      $query = $this->mdb->query($sql);
      $result = $query->result(true);

      $sql2 = "select SUM(amount) as total_amount, count(id) as billed_today from ufone_charges where date = '{$date}'";
      $query = $this->mdb->query($sql2);
      $billed = $query->result(true);
      // echo "<pre>";
      // print_r($billed);
      // echo "</pre>";exit;
      $result[0]['billed_today'] = $billed[0]['billed_today'];
      $result[0]['total_amount'] = $billed[0]['total_amount'];

      return json_encode($result);
    }


    function get_report($from = '', $to = ''){

      $array = [];
      $sql = "SELECT DATE_FORMAT(CONVERT_TZ(created_at, '+00:00','+05:00'), '%Y-%m-%d') AS the_date, COUNT(*) AS count  ";
      // $sql = "SELECT DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) AS the_date, COUNT(*) AS count ";
        $sql .= " FROM subscriptions ";
          $sql .= " WHERE ";
            if($from == $to){
              $sql .= "   DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) = ?  ";
              $array[] = $from;
            }else{
              $sql .= " DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d') ";
              $array[] = $from;
              $array[] = $to;
            }
      $sql .= " AND (udid <> '')  ";
      $sql .= " GROUP BY the_date ";



      $query = $this->mdb->query($sql, $array);
      $result = $query->result(true);

      $array2 = [];
      $sql2 = "SELECT DATE_FORMAT(CONVERT_TZ(created_at, '+00:00','+05:00'), '%Y-%m-%d') AS the_date, COUNT(*) AS count  ";
      // $sql = "SELECT DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) AS the_date, COUNT(*) AS count ";
        $sql2 .= " FROM subscriptions ";
          $sql2 .= " WHERE ";
            if($from == $to){
              $sql2 .= "   DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) = ?  ";
              $array2[] = $from;
            }else{
              $sql2 .= " DATE(CONVERT_TZ(created_at, '+00:00', '+05:00')) BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d') ";
              $array2[] = $from;
              $array2[] = $to;
            }
      $sql2 .= " AND (udid = '' OR udid IS NULL) ";
      $sql2 .= " GROUP BY the_date ";



      // $query2 = $this->mdb->query($sql2, array('2018-03-03','2018-03-07'));
      $query2 = $this->mdb->query($sql2, $array2);
      $result2 = $query2->result(true);

// -----------------------------------------------------------------------------------------------------------
      $array3 = [];
      $sql3 = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') AS the_date, total_charged, total_amount ";
      $sql3 .= " FROM charge_stats WHERE ";
      if($from == $to){
        $sql3 .= " DATE(created_at) = ?  ";
        $array3[] = $from;
      }else{
        $sql3 .= " DATE(created_at) BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d') ";
        $array3[] = $from;
        $array3[] = $to;
      }

      $query3 = $this->mdb->query($sql3, $array3);
      // $query3 = $this->mdb->query($sql3, array('2018-03-03','2018-03-07'));

      $result3 = $query3->result(true);



    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
      $data = [];

      foreach($result as $value){
        $data[] = array("the_date" => $value["the_date"], "app_registrations" => $value["count"], "wap_registrations" => "0", "total_charged" => "0", "total_amount" => "0");
      }

      foreach($result2 as $value2){
        $date_exists = 0;
        foreach($data as $dkey => $d){
          if($d["the_date"] == $value2["the_date"]){
            $data[$dkey]["wap_registrations"] = $value2["count"];
            $date_exists++;
          }
        }
        if($date_exists == 0){
          $data[] = array("the_date" => $value2["the_date"], "app_registrations" => "0", "wap_registrations" => $value2["count"], "total_charged" => "0", "total_amount" => "0");
        }
      }

      foreach($result3 as $value3){
        $date_exists = 0;
        foreach($data as $dkey => $d){
          if($d["the_date"] == $value3["the_date"]){
            $data[$dkey]["total_charged"] = $value3["total_charged"];
            $data[$dkey]["total_amount"] = $value3["total_amount"];
            $date_exists++;
          }
        }
        if($date_exists == 0){
          $data[] = array("the_date" => $value3["the_date"], "app_registrations" => "0", "wap_registrations" => "0", "total_charged" => $value3["total_charged"], "total_amount" => $value3["total_amount"]);
        }
      }


      function cmp($a, $b)
      {
          if ($a["the_date"] == $b["the_date"]) {
              return 0;
          }
          return ($a["the_date"] < $b["the_date"]) ? -1 : 1;
      }

      usort($data,"cmp");


      return json_encode($data);

      // return json_encode($sql);
      // return json_encode($this->mdb->last_query());

      // return json_encode($result);


      // return json_encode("yo");
    }





    function get_revenue_by_month($month, $year){
      $month = date('m', strtotime('01'.'-'.$month.'-'.$year) );
      $year = date('Y');
      // // $month = date('m');
      // // $month = '01';
      //
      //
      //
      // $day = '01';
      // $time = '19:00:00';
      //
      // $from = $year."-".$month."-".$day." ".$time;
      //
      // $year2 = $year;
      // if($month == '12'){
      //   $year2 = date('Y', strtotime('+1 year', strtotime($year)));
      // }
      //
      // $month2 = date('m', strtotime('+1 month', strtotime('01'.'-'.$month.'-'.$year)));
      // $day2 = '01';
      // $time2 = '19:00:00';
      //
      // $to = $year2."-".$month2."-".$day2." ".$time2;


      $day1 = date('d', strtotime('-1 day', strtotime('01'.'-'.$month.'-'.$year)));
      $month1 = date('m', strtotime('-1 day', strtotime('01'.'-'.$month.'-'.$year)));
      $year1 = date('Y', strtotime('-1 day', strtotime('01'.'-'.$month.'-'.$year)));

      $time = '19:00:00';

      $from = $year1."-".$month1."-".$day1." ".$time;

      $year2 = $year;


      $month2 = $month;

      $day2 = date('m', strtotime('+1 month', strtotime('01'.'-'.$month.'-'.$year)));
        $day2 = date('d', strtotime('-1 day', strtotime('01'.'-'.$day2.'-'.$year)));

      $time2 = '19:00:00';

      $to = $year2."-".$month2."-".$day2." ".$time2;

      $sql = "select *, date(created_at) from ufone_charge_stats where created_at >= '$from' AND created_at <= '$to'";
      // return json_encode($sql);
      $query = $this->mdb->query($sql);
      return json_encode($query->result(true));

    }




    function get_app_registers(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');


      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      // $to = $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));

      $from = $yesterday;


      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND ( udid <> '' OR udid IS NOT NULL ) ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);

      return json_encode(count($result));



    }

    function get_wap_registers(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND (udid = '' OR udid IS NULL) ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);
      // echo $sql;
      // exit;
      return json_encode(count($result));

    }

    function get_active_subs(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      // $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND status = '1' ";
      $sql = "SELECT id FROM subscriptions WHERE status = '1' ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);
      return json_encode(count($result));
    }

    function get_free_subs(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      // $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND status = '2' ";
      $sql = "SELECT id FROM subscriptions WHERE status = '2' AND current_tick = '1'  ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);
      return json_encode(count($result));
    }



    function get_access(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      $sql = "SELECT id FROM subscriptions WHERE last_accessed_at >= '$from' AND last_accessed_at <= '$to' AND udid IS NOT NULL ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);

      return json_encode(count($result));
    }

    function get_cc_unsub(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      // $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND status = '2' ";
      $sql = "SELECT id FROM subscriptions WHERE unsub_reason = '2' ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);
      return json_encode(count($result));
    }

    function get_user_unsub(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      // $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND status = '2' ";
      $sql = "SELECT id FROM subscriptions WHERE unsub_reason = '1' ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);
      return json_encode(count($result));
    }

    function get_low_unsub(){
      $from = date('Y-m-d');
      $to = date('Y-m-d');

      $tomorrow = date('Y-m-d',strtotime($from . "+1 days"));
      $yesterday = date('Y-m-d',strtotime($from . "-1 days"));

      $from = trim($yesterday)." 19:00:00";
      $to = trim($to)." 19:00:00";

      // $sql = "SELECT id FROM subscriptions WHERE created_at >= '$from' AND created_at <= '$to' AND status = '2' ";
      $sql = "SELECT id FROM subscriptions WHERE unsub_reason = '3' ";
      $query = $this->mdb->query($sql);
      $result = $query->result(true);
      return json_encode(count($result));
    }


    function test(){
      $query = $this->mdb->get('charge_stats', 10);
      return $query->result();
    }

  }
?>
